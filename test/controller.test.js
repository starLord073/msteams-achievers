const expect = require('chai').expect;
const request = require('request');
const fs = require('fs');
const jwt = require('jsonwebtoken');

const dotenv = require('dotenv');
const chai = require('chai');
const chaiHttp = require('chai-http');
let { server } = require('../dist/out-tsc/src/app');
chai.use(chaiHttp);

dotenv.config();

const baseUrl = process.env.APP_BASE_URL;
const TOKEN_METADATA = {
  issuer: "achievers.com",
  expiresIn: (60 * 60 * 24) - 1,
  subject: "msteams-connect"
}
console.log(baseUrl + "checking App Url");

afterAll(async () => {
    await new Promise(resolve => setTimeout(() => resolve(), 500)); // avoid jest open handle error
  });


describe('Get User', () => {
    console.log("*********************** process ********************",process.env.CONNECTOR_SECRET_TOKEN);
    it('Userfetch api test', async () => {
        const token = jwt.sign({
            createdDate: new Date(),
            appUserId:"test123",
            appTenantId:"test",
            connectorType: "msteams"
        }, process.env.CONNECTOR_SECRET_TOKEN, TOKEN_METADATA);


            const postData = {
                userName: "testUser",
                userToken: token
            };
            let resp = await chai.request(server)
                .post('/api/achievers/userfetch')
                .send(postData);

            console.log('Status Code: ' + JSON.stringify(resp));

            expect(resp.status).to.satisfy(function (num) {
              var checkCondition = num == 200 || num == 401 || num == 400;
              console.log("checkCondition", checkCondition);
               return !!checkCondition;

            });
    });
})
describe('Get recogTypes', () => {
    it('recogTypes api test', async () =>  {

        const token = jwt.sign({
            createdDate: new Date(),
            appUserId:"test123",
            appTenantId:"test",
            connectorType: "msteams"
        }, process.env.CONNECTOR_SECRET_TOKEN, TOKEN_METADATA);

        const postData = {
                userIds: '10726821,10726846',
                userToken: token
            }
            let res = await chai.request(server)
                .post('/api/achievers/getrecogtypes')
                .send(postData)

            console.log("getRecogTypes response code: " + res.status);
            expect(res.status).to.satisfy(function (num) {
              var checkCondition = num == 200 || num == 401 || num == 400;
              console.log("checkCondition1", checkCondition);
               return !!checkCondition;
            })
    })
})

describe('Post recognition', () => {
    it('default image case success or expired token response test', async () => {

        const token = jwt.sign({
            createdDate: new Date(),
            appUserId:"test123",
            appTenantId:"test",
            connectorType: "msteams"
        }, process.env.CONNECTOR_SECRET_TOKEN, TOKEN_METADATA);


            const formData = {
                nominees: 'vamsi.guggulla',
                recognitionText: 'unit Test default image',
                criterionId: 28601,
                criterionVal: 'a Thank You',
                userFullName: 'test User',
                nomineeEmails: 'vamsi.guggulla@valuelabs.com',
                points: 0,
                userToken: token,
                imageSelected: 'default',
            };

            let response = await chai.request(server)
            .post('/api/achievers/postrecog')
            .type('form')
            .send(formData);

            console.log("getRecogTypes response code: " + response.status);
                expect(response.status).to.satisfy(function (num) {
                  var checkCondition = num == 200 || num == 401 || num == 400;
                  console.log("checkCondition2", checkCondition);
                  return !!checkCondition;
                })

    })
});


describe('User Logout', () => {
  it('User Logout api test', async () => {
    const token = jwt.sign({
      createdDate: new Date(),
      appUserId: 'test123',
      appTenantId: 'test',
      connectorType: 'msteams'
    }, process.env.CONNECTOR_SECRET_TOKEN, TOKEN_METADATA)

    const postData = {
      userToken: token
    }
    console.log('post data' + JSON.stringify(postData))
    const resp = await chai.request(server)
      .post('/api/achievers/logout')
      .send(postData)

    expect(resp.status).to.satisfy(function (num) {
      var checkCondition = num == 200 || num == 401 || num == 400;
      console.log("checkCondition3", checkCondition);
      return !!checkCondition;
    })
  })
})

describe('image search', () => {
  it('image Search api test', async () => {
    const token = jwt.sign({
      createdDate: new Date(),
      appUserId: 'test123',
      appTenantId: 'test',
      connectorType: 'msteams'
    }, process.env.CONNECTOR_SECRET_TOKEN, TOKEN_METADATA)

    const postData = {
      userToken: token
    }
    console.log('post data' + JSON.stringify(postData))
    const resp = await chai.request(server)
      .post('/api/achievers/imageSearch')
      .send(postData)

    expect(resp.status).to.satisfy(function (num) {
      var checkCondition = num == 200 || num == 401 || num == 400;
      console.log("checkCondition4", checkCondition);
               return !!checkCondition;
    })
  })
})

// describe('Get Current User member', () => {
//   it('Get Current User member api test', async (done) => {
//     const token = jwt.sign({
//       createdDate: new Date(),
//       appUserId: 'test123',
//       appTenantId: 'test',
//       connectorType: 'msteams'
//     }, process.env.CONNECTOR_SECRET_TOKEN, TOKEN_METADATA)

//     const query = {
//       // userName: "testUser",
//       userToken: token
//     }
//     const resp = await chai.request(server)
//       .get('/api/achievers/curMember')
//       .query(query)

//     console.log('Status Code: ' + JSON.stringify(resp))

//     expect(resp.status).to.satisfy(function (num) {
//       if (num === 200 || num === 401) {
//         return true
//       } else {
//         return false
//       }
//     })
//     done()
//   })
// })