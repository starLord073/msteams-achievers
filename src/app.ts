import * as dotenv from "dotenv";
import * as express from "express";
require('newrelic');
const bodyParser = require("body-parser");
import AchieversAuth from "./middlewareAuth";
import { botDialog } from "../controller/botController";
import { apiController } from "../controller/apiController";
import { viewSettings } from "../models/viewSettings";
import setRoutes from "./routes";
import setBotRoutes from "./botRoutes";
const logger = require('../util/winlogger.js').logger;
import * as Sentry from "@sentry/node";
import * as swaggerJSDoc from "swagger-jsdoc";
import * as swaggerUI from "swagger-ui-express";
const config = dotenv.config();
const sentryDsn = process.env.SENTRY_DSN;
Sentry.init({
  dsn: sentryDsn,
  debug: true,
  beforeSend(event) {
    console.log("sending exception to sentry....");
    return event;
  }
});

const cors = require('cors');

const builder = require("botbuilder");
const teams = require("botbuilder-teams");
const router = express.Router();
const app = express();
app.use(Sentry.Handlers.requestHandler());
app.use(Sentry.Handlers.errorHandler());
const path = require('path');

const PORT = process.env.PORT || 4001;
const MicrosoftAppId = process.env.APP_CLIENT_ID;
const MicrosoftAppPassword = process.env.APP_CLIENT_SECRET;
const baseUrl = process.env.APP_BASE_URL;
const authURL = process.env.MIDDLEWARE_INTERNAL_API_URI;
const externalAuthUrl = process.env.MIDDLWARE_API_URI;
const connectorSecretToken = process.env.CONNECTOR_SECRET_TOKEN;
const achieversBaseUrl = process.env.ACHIEVER_API_BASE_URI;
const connector = new teams.TeamsChatConnector({
  appId: MicrosoftAppId,
  appPassword: MicrosoftAppPassword
});

const botSigninSettings: any = {
  appBaseUrl: baseUrl,
  appAuthURL: authURL,
  appExternalAuthUrl : externalAuthUrl,
  CONNECTOR_SECRET_TOKEN: connectorSecretToken
};

const achieverAPI = {
  baseUrl: achieversBaseUrl,
  getUsers: '/search',
  getRecogTypes: '/recognitions',
  postRecog: '/recognitions',
  currentMember: '/current-member',
  imageSearch: '/image-search',
  iamgePostUrl: '/uploads'
};

const viewSettingsObj = new viewSettings(baseUrl, MicrosoftAppId);
viewSettingsObj.sentryDsnFe = process.env.SENTRY_DSN_FE;

const botAuth = new AchieversAuth(app, connector, botSigninSettings, config);
const inMemoryStorage = new builder.MemoryBotStorage();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.engine( "html", require( "ejs" ).renderFile );
app.set('view engine', 'html');
app.set('port', PORT);
app.use('/docs/', express.static(path.join(__dirname, '../../../pages')));
app.use('/languages/', express.static(path.join(__dirname, '../../../languages')));
app.set('views', path.join(__dirname, '../../../src/views'));



const swaggerDefinition = {
  info: {
    title: "REST API for Achievers MS Teams connect", // Title of the documentation
    version: "1.0.0", // Version of the app
    description: "This is the REST API for Achievers MS Teams connect" // short description of the app
  },
  host: process.env.APP_BASE_URL // the host or url of the app
  // basePath: "/api", // the basepath of your endpoint
};

// options for the swagger docs
const options = {
  swaggerDefinition,
  explorer: true,
  // path to the API docs
  apis: [path.join( __dirname,"./*.js" )]
};

// initialize swagger-jsdoc
const swaggerSpec = swaggerJSDoc( options );

if (process.env.APP_BASE_URL.indexOf("staging") > -1) {
  app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(swaggerSpec));
}



app.get('/', function (req, res) {
  res.send("App running on 3rd may 2021 " + app.get('port'));
});

const server = app.listen(app.get('port'), () => {
  logger.debug('App platform listen on 24th Nov 2020 ' + app.get('port'));
  console.log('App platform listen on 24th Nov 2020 ' + app.get('port'));
});

server.keepAliveTimeout = 1000 * (60 * 6); // 6 minutes

const bot = new builder.UniversalBot(connector).set('storage', inMemoryStorage);

setRoutes(app, viewSettingsObj, connector, botAuth, achieverAPI, baseUrl, bot, MicrosoftAppId);
setBotRoutes(bot, connector, baseUrl, MicrosoftAppId, botAuth);

export { server };
