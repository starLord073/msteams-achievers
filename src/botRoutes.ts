import { botDialog } from '../controller/botController';
import { botEventHandler } from '../controller/eventHandlerController';

const builder = require("botbuilder");
const teams = require("botbuilder-teams");

export default function setBotRoutes(bot, connector, baseUrl, MicrosoftAppId, botAuth) {
  const botObj = new botDialog(baseUrl, bot, MicrosoftAppId, botAuth);

  bot.dialog('/', botObj.choiceDialog);
  bot.dialog('Help', botObj.Help);
  bot.dialog('Group Help', botObj.GroupHelp);
  bot.dialog('Bot Help', botObj.BotHelp);
  bot.on('conversationUpdate', botObj.conversationUpdate);
  bot.dialog('Sign Out', botObj.signOut);
  bot.dialog('Sign In', botObj.signIn);

  const eventHandlerObj = new botEventHandler(bot, botAuth, baseUrl, connector);

  connector.onComposeExtensionSubmitAction(eventHandlerObj.composeExtensionSubmitActionHandler);
  connector.onComposeExtensionFetchTask(eventHandlerObj.composeExtensionFetchHandler);
  connector.onInvoke(eventHandlerObj.invokeHandler);
  connector.onSigninStateVerification(eventHandlerObj.onSigninStateVerificationHandler);
}