const request = require("request-promise");
const logger = require('../util/winlogger.js').logger;
const newrelic = require('newrelic');
import { consoleLogData, loggerLogData, newRelicErrLog } from '../util/logsHandler';

export const trackEventInPendo = async(event : string ,accountId : string ,visitorId : string ,properties : any )=>{
  try{
    const pendoDataTrackOptions = {
      uri: "https://app.pendo.io/data/track",
      method: "POST",
      json: true,
      body: {
        type: "track",
        event: event,
        visitorId: accountId+"-"+visitorId,
        accountId: accountId,
        properties,
        timestamp: Date.now(),
      },
      headers: {
        "Content-Type": "application/json",
        "x-pendo-integration-key": process.env.PENDO_INTEGRATION_KEY,
      },
    };
    // console.log("******************Pendo track events",pendoDataTrackOptions)
    try{
      await request(pendoDataTrackOptions);
      // console.log("Tracked the event :'"+event+"' successfully")
    }
    catch(error){
      // let errArr = ['try-catch-err', 'msteams-connect', 'pendoTrackEvent.ts', 'trackEventInPendo', error.message];
      // newrelic.noticeError(error);
      // logger.debug(errArr.join(":"));
      // console.log(error.message)
      newRelicErrLog(error,"pendoTrackEvent.ts/trackEventInPendo","");
      loggerLogData(true,"*******************error in trackEventInPendo function*******************","",true);
      consoleLogData(true,"error in trackEventInPendo function", "",false);

    }
  }catch(error){
    // let errArr = ['try-catch-err', 'msteams-connect', 'pendoTrackEvent.ts', 'trackEventInPendo', error.message];
    // newrelic.noticeError(error);
    // logger.debug(errArr.join(":"));
    // console.log(error.message)
    newRelicErrLog(error,"pendoTrackEvent.ts/trackEventInPendo","");
    loggerLogData(true,"*******************error in trackEventInPendo 2222 function*******************","",true);
    consoleLogData(true,"error in trackEventInPendo 2222 function", "",false);

  }
};
