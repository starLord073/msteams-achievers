const crypto = require("crypto");
const jwt = require("jsonwebtoken");
import * as rp from "request-promise";
const bodyparser = require("body-parser");
const express = require("express");
const builder = require("botbuilder");
const logger = require('../util/winlogger.js').logger;
const newrelic = require('newrelic');
import { consoleLogData, loggerLogData, newRelicErrLog } from '../util/logsHandler';
const moment = require("moment-timezone");

let achieversTokenCache: any = {};
const AuthStartPath = "/auth/start";
const AchieversTokenPath = "";
const GetUserInfoPath = "/api/shared/userInfo";
const UpdateUserInfoPath = "/api/shared/userInfo";
const SaveGraphTokensPath = "/api/shared/saveGraphTokens";
const GetConversationInfoPath = "/api/shared/getConversationData";
const GetConversationInfoByAppId = "/api/shared/getConversationDataByAppId";
const saveConversationInfoPath = "/api/shared/saveConversationData";
const saveClientDetails = "/api/shared/addClientInfo";
const updateIsRecognitionSuggestionHide = "/api/shared/updateIsRecognitionSuggestionHide";

const LogoutPath = "/api/shared/logout";
const TeamsSDK = "https://statics.teams.microsoft.com/sdk/v1.4.2/js/MicrosoftTeams.js";
const AuthStartOAuthPath = "/auth/oauth";
const AuthCallbackPath = "/auth/callback";
const TOKEN_METADATA = {
  issuer: "achievers.com",
  expiresIn: (60 * 60 * 24) - 1,
  subject: "msteams-connect"
};
import { LanguageHelper } from "../languages/languageHelper";
import { trackEventInPendo } from './pendoTrackEvent';
import { jwtSign, jwtVerify } from './../util/jwtTokenHandler';
// let AchieversAuth: any;
let connector: any;
let config: any;
let settings: any;

function parseCookie(rawCookie: any) {
  const cookies = {};
  if (rawCookie) {
    const c = rawCookie.split('; ');
    for (let i = c.length - 1; i >= 0; i--) {
      const v = c[i].split('=');
      cookies[v[0]] = v[1];
    }
  }
  return cookies;
}

function authStart(req: any, res: any, next: any) {
  try {
    const userId = req.params.userId;
    const tenantId = req.params.tenantId;
    const domain = req.query.domain || "";
    const authUrl = settings.appBaseUrl + AuthStartOAuthPath;
    const body = "\n      <html>\n        <head>\n          <script src='" + TeamsSDK + "'>\n</script>\n        </head>\n        <body>\n          <script>\n            microsoftTeams.initialize();\n            microsoftTeams.getContext(function(context){\n              //- Save user and channel id to cookie\n              document.cookie = 'userId=' + '" + userId + "' + '; Path=/';\n               document.cookie = 'tenantId=' + '" + tenantId + "' + ';Path=/';\n               document.cookie = 'domain=' + '" + domain + "' + ';Path=/';\n               window.location = '" + authUrl + "';\nwindow.resizeTo(0.6*(window.screen.width),0.6*(window.screen.height));\n window.moveTo(0.2*(window.screen.width),0.2*(window.screen.height));\n           });        \n          </script>\n        </body>\n      </html>\n    ";
    res.writeHead(200, {
      'Content-Length': Buffer.byteLength(body),
      'Content-Type': 'text/html'
    });
    res.write(body);
    res.end();
  }
  catch (e) {
    // let errArr = ['try-catch-err', 'msteams-connect', 'middlewareAuth.ts', 'authStart', e.message];
    // newrelic.noticeError(e);
    // logger.debug(errArr.join(":"));
    newRelicErrLog(e, "middlewareAuth.ts/authStart", "");
    const description = {
      "RequestDetails": { userId: req.params.userId, tenantId: req.params.tenantId, reqUrl: req.url, reqMethod: req.method },
      "ErrMessage": e.message
    };
    this.saveLogData(JSON.stringify(description), "middlewareAuth.ts/authStart", req.params.tenantId, "error");
    loggerLogData(true, "*******************error in authStart function*******************", "", true);
  }
}

function authCallback(req: any, res: any, next: any) {
  try {
    const uId = req.query.appUserId;
    const aId = req.query.appId;
    const onAuthResultBody = function (succeeded, state) {
      return succeeded ? "\n      <html>\n        <head>\n          <script src='" + TeamsSDK + "'></script>        \n        </head>\n        <body>\n          <script>\n            microsoftTeams.initialize();\n            microsoftTeams.authentication.notifySuccess('" + state + "');\n          </script>\n        </body>\n      </html>\n      " : "\n      <html>\n        <head>\n          <script src='" + TeamsSDK + "'></script>\n        </head>\n        <body>\n          <script>\n            microsoftTeams.initialize();\n            microsoftTeams.authentication.notifyFailure();\n          </script>\n        </body>\n      </html>\n    ";
    };
    let body = '';
    const state = {
      userId: uId,
      tenantId: aId
    };

    body = onAuthResultBody(true, state);

    res.writeHead(200, {
      'Content-Length': Buffer.byteLength(body),
      'Content-Type': 'text/html'
    });
    res.write(body);
    res.end();
  }
  catch (e) {
    // let errArr = ['try-catch-err', 'msteams-connect', 'middlewareAuth.ts', 'authCallback', e.message];
    // newrelic.noticeError(e);
    // logger.debug(errArr.join(":"));
    newRelicErrLog(e, "middlewareAuth.ts/authCallback", "");
    const description = {
      "RequestDetails": { appUserId: req.query.appUserId, appId: req.query.appId, reqUrl: req.url, reqMethod: req.method },
      "ErrMessage": e.message
    };
    this.saveLogData(JSON.stringify(description), "middlewareAuth.ts/authCallback", req.query.appId, "error");
    loggerLogData(true, "*******************error in authCallback function*******************", "", true);
  }
}


function authStartOAuth(req: any, res: any, next: any) {
  const cookie: any = parseCookie(req.headers.cookie);
  try {

    const privateKey = process.env.RSA_SECRET_KEY.replace(/\\n/g, '\n')
    const token = jwtSign({
      responseUrl: '',
      appUserId: cookie.userId,
      appName: cookie.tenantId,
      appId: cookie.tenantId,
      connectorType: "msteams",
      // populate callback later
      callbackUrl: settings.appBaseUrl + AuthCallbackPath,
      domain: cookie.domain || ""
    })
    // const token = jwt.sign({
    //   responseUrl: '',
    //   appUserId: cookie.userId,
    //   appName: cookie.tenantId,
    //   appId: cookie.tenantId,
    //   connectorType: "msteams",
    //   // populate callback later
    //   callbackUrl: settings.appBaseUrl + AuthCallbackPath
    // }, settings.CONNECTOR_SECRET_TOKEN, TOKEN_METADATA);

    const achieversOAuthUrl = settings.appExternalAuthUrl + AchieversTokenPath + "?state=" + token;
    res.redirect(achieversOAuthUrl);
  }
  catch (e) {
    // let errArr = ['try-catch-err', 'msteams-connect', 'middlewareAuth.ts', 'authStartOAuth', e.message];
    // newrelic.noticeError(e);
    // logger.debug(errArr.join(":"));
    newRelicErrLog(e, "middlewareAuth.ts/authStartOAuth", "");
    const description = {
      "RequestDetails": { userId: cookie.userId, tenantId: cookie.tenantId, reqUrl: req.url, reqMethod: req.method },
      "ErrMessage": e.message
    };
    this.saveLogData(JSON.stringify(description), "middlewareAuth.ts/authStartOAuth", cookie.tenantId, "error");
    loggerLogData(true, "*******************error in authStartOAuth function*******************", "", true);
  }
}

class AchieversAuth {

  constructor(server: any, _connector: any, _settings: any, _config: any) {
    achieversTokenCache = {};
    server.get(AuthStartPath + '/:userId/:tenantId', function (req: any, res: any, next: any) { return authStart(req, res, next); });
    server.get(AuthCallbackPath, function (req: any, res: any, next: any) { return authCallback(req, res, next); });
    server.get(AuthStartOAuthPath, function (req: any, res: any, next: any) { return authStartOAuth(req, res, next); });

    connector = _connector;
    settings = _settings;
    config = _config;
  }

  jwtDecodePayload = async function (token) {
    try {
      const data = await jwtVerify(token)
      return data;
    }
    catch (e) {
      // let errArr = ['try-catch-err', 'msteams-connect', 'middlewareAuth.ts', 'jwtDecodePayload', e.message];
      // newrelic.noticeError(e);
      // logger.debug(errArr.join(":"));
      newRelicErrLog(e, "middlewareAuth.ts/jwtDecodePayload", "");
      loggerLogData(true, "*******************error in jwtDecodePayload function*******************", "", true);
      return Promise.reject(e)
    }
  }

  jwtEncodePayload = function (data) {
    try {
      const token = jwtSign(data);
      return token;
    }
    catch (e) {
      // let errArr = ['try-catch-err', 'msteams-connect', 'middlewareAuth.ts', 'jwtEncodePayload', e.message];
      // newrelic.noticeError(e);
      // logger.debug(errArr.join(":"));
      newRelicErrLog(e, "middlewareAuth.ts/jwtEncodePayload", "");
      loggerLogData(true, "*******************error in jwtEncodePayload function*******************", "", true);
    }
  }

  updateUserInfo = function (userId: string, tenantId: string, email: string, name: string, msTeamBotId: string, msTeamBotName: string, msTeamBotServiceUrl: string, languageUpdatedNotificationSent: boolean, azureUserId: string = null) {
    let options: any;
    return new Promise(function (resolve, reject) {
      try {
        const token = jwtSign({
          createdDate: new Date(),
          connectorType: "msteams"
        });
        options = {
          uri: settings.appAuthURL + UpdateUserInfoPath,
          method: "POST",
          body: {
            email: email,
            name: name,
            msTeamBotId: msTeamBotId,
            msTeamBotName: msTeamBotName,
            msTeamBotServiceUrl: msTeamBotServiceUrl,
            connectorType: 'msteams',
            languageUpdatedNotificationSent: languageUpdatedNotificationSent || null,
            appUserId: userId,
            appId: tenantId,
            azureUserId: azureUserId
          },
          headers: {
            Authorization: token
          },
          json: true // Automatically parses the JSON string in the response
        };

        // logger.debug("************* updateUserInfo *******************" + options.body);
        // console.log("************* updateUserInfo *******************" + options.body);

        rp(options)
          .then(function (response) {
            resolve(response);
          })
          .catch(function (err) {
            reject(err);
          });
      }
      catch (e) {
        // let errArr = ['try-catch-err', 'msteams-connect', 'middlewareAuth.ts', 'updateUserInfo', e.message];
        // newrelic.noticeError(e);
        // logger.debug(errArr.join(":"));
        newRelicErrLog(e, "middlewareAuth.ts/updateUserInfo", "");
        const description = {
          "RequestDetails": { userId, tenantId, options },
          "ErrMessage": e.message
        };
        this.saveLogData(JSON.stringify(description), "middlewareAuth.ts/updateUserInfo", tenantId, "error");
        loggerLogData(true, "*******************error in updateUserInfo function*******************", "", true);

        reject(e);
      }
    });
  }



  getUserInfo = function (userId, tenantId, userAzureId = false) {
    let options: any;
    return new Promise(function (resolve, reject) {
      try {
        const token = jwtSign({
          createdDate: new Date(),
          connectorType: "msteams"
        });
        options = {
          uri: settings.appAuthURL + GetUserInfoPath,
          qs: {
            appUserId: userId,
            appId: tenantId,
            userAzureId: userAzureId
          },
          headers: {
            Authorization: token
          },
          json: true // Automatically parses the JSON string in the response
        };

        rp(options)
          .then(function (response) {
            // console.log("===response",response)
            resolve(response);
          })
          .catch(function (err) {
            reject(err);
          });
      }
      catch (e) {
        // let errArr = ['try-catch-err', 'msteams-connect', 'middlewareAuth.ts', 'getUserInfo', e.message];
        // newrelic.noticeError(e);
        // logger.debug(errArr.join(":"));
        newRelicErrLog(e, "middlewareAuth.ts/getUserInfo", "");
        const description = {
          "RequestDetails": { userId, tenantId, options },
          "ErrMessage": e.message
        };
        this.saveLogData(JSON.stringify(description), "middlewareAuth.ts/getUserInfo", tenantId, "error");
        loggerLogData(true, "*******************error in getUserInfo function*******************", "", true);
        reject(e);
      }
    });
  }

  updateRecognitionSuggestionStatus = function (isRecognitionSuggestionHide: boolean, appId: string, appUserId: string, appType: string) {
    let options: any;
    return new Promise(function (resolve, reject) {
      try {
        const token = jwtSign({
          createdDate: new Date(),
          connectorType: "msteams"
        });

        options = {
          uri: settings.appAuthURL + updateIsRecognitionSuggestionHide,
          method: 'POST',
          body: {
            appUserId: appUserId,
            appId: appId,
            appType: appType,
            isRecognitionSuggestionHide: isRecognitionSuggestionHide
          },
          headers: {
            Authorization: token
          },
          json: true // Automatically parses the JSON string in the response
        };

        rp(options)
          .then(function (response) {
            resolve(response);
          })
          .catch(function (err) {
            reject(err);
          });
      }
      catch (e) {
        newRelicErrLog(e, "middlewareAuth.ts/updateRecognitionSuggestionStatus", "");
        const description = {
          "RequestDetails": { appUserId, appId, appType, isRecognitionSuggestionHide, options },
          "ErrMessage": e.message
        };
        this.saveLogData(JSON.stringify(description), "middlewareAuth.ts/updateRecognitionSuggestionStatus", appId, "error");
        loggerLogData(true, "*******************error in updateRecognitionSuggestionStatus function*******************", "", true);
        reject(e);
      }
    });
  }

  BotSignIn(session: any, domain: string) {
    let showDomainPage = false;
    if (((process.env.MIDDLEWARE_INTERNAL_API_URI.indexOf('uat') > -1 || process.env.MIDDLEWARE_INTERNAL_API_URI.indexOf('stg') > -1) && (session.message.sourceEvent.tenant.id == "555f757f-ad8d-4678-a7f2-ef5f9b02f5d6" || session.message.sourceEvent.tenant.id == "8bd0082d-d958-423a-821f-dd0f22a1b519")) || (session.message.sourceEvent.tenant.id == "5571c7d4-286b-47f6-9dd5-0aa688773c8e")) {
      showDomainPage = true;
    }
    const _this = this;
    const userId = session.message.address.user.id;
    const tenantId = session.message.sourceEvent.tenant.id;
    const curLang = session._locale;
    const langHelper = new LanguageHelper(curLang);

    const issueNewCard = function () {
      const authUrl = settings.appBaseUrl + AuthStartPath + '/' + userId + '/' + tenantId + (showDomainPage ? "" : ("?domain=" + domain));
      const card = new builder.SigninCard(session).text(langHelper.getTranslation('Sign_in_Achievers_app')).button(langHelper.getTranslation('Sign_in'), authUrl);
      const msg = new builder.Message(session).attachments([card]);
      // session.send(msg);
      session.endDialog(msg);
    };

    issueNewCard();
    const description = {
      event: "Sign in card sent to user",
      userId,
      tenantId
    };
    this.saveLogData(description, "middlewareAuth.ts/BotSignIn", "AUDIT_LOGS");
  }

  BotSignOut(session: any, cb: any) {
    const _this = this;
    const userId = session.message.address.user.id;
    const tenantId = session.message.sourceEvent.tenant.id;
    const token = jwtSign({
      createdDate: new Date(),
      connectorType: "msteams"
    });

    const options = {
      // uri: AchieversAuth.AchieversTokenUrl,
      method: 'POST',
      uri: settings.appAuthURL + LogoutPath,
      body: {
        appUserId: userId,
        appId: tenantId
      },
      headers: {
        Authorization: token
      },
      json: true // Automatically parses the JSON string in the response
    };
    const description = {
      event: "BotSignOut flow initiated",
      sessionData: session.message
    };
    this.saveLogData(description, "middlewareAuth.ts/BotSignOut", "AUDIT_LOGS");

    rp(options)
      .then(function (response: any) {
        cb(response, null);
      })
      .catch(function (err: any) {
        cb(null, err);
      });
  }



  ExtensionSignIn(event: any, subDomain: string) {
    let showDomainPage = false;
    if (((process.env.MIDDLEWARE_INTERNAL_API_URI.indexOf('uat') > -1 || process.env.MIDDLEWARE_INTERNAL_API_URI.indexOf('stg') > -1) && (event.sourceEvent.tenant.id == "555f757f-ad8d-4678-a7f2-ef5f9b02f5d6" || event.sourceEvent.tenant.id == "8bd0082d-d958-423a-821f-dd0f22a1b519"))|| (event.sourceEvent.tenant.id == "5571c7d4-286b-47f6-9dd5-0aa688773c8e")) {
      showDomainPage = true;
    }
    try {
      const userId = event.address.user.id;
      const tenantId = event.sourceEvent.tenant.id;
      const authUrl = settings.appBaseUrl + AuthStartPath + '/' + userId + '/' + tenantId + (showDomainPage ? "" : ("?domain=" + subDomain));
      const curLang = event.entities[0].locale;
      let langHelper = new LanguageHelper(curLang);
      const description = {
        event: "User is redirected to sign in page in app",
        userId,
        tenantId,
        authUrl,
        curLang
      };
      this.saveLogData(description, "middlewareAuth.ts/ExtensionSignIn", "AUDIT_LOGS");
      consoleLogData(true, "In extension sign in : ", langHelper.getTranslation("Sign_in_to_this_app"), false);
      const response =
      {
        "composeExtension": {
          "type": "auth",
          "suggestedActions": {
            "actions": [
              {
                "type": "openUrl",
                "value": authUrl,
                "title": langHelper.getTranslation("Sign_in_to_this_app"),
              }
            ]
          }
        }
      };
      return response;
    }
    catch (e) {
      // let errArr = ['try-catch-err', 'msteams-connect', 'middlewareAuth.ts', 'ExtensionSignIn', e.message];
      // newrelic.noticeError(e);
      // logger.debug(errArr.join(":"));
      newRelicErrLog(e, "middlewareAuth.ts/ExtensionSignIn", "");
      const description = {
        "RequestDetails": { userId: event.address.user.id, tenantId: event.sourceEvent.tenant.id },
        "ErrMessage": e.message
      };
      this.saveLogData(JSON.stringify(description), "middlewareAuth.ts/ExtensionSignIn", event.sourceEvent.tenant.id, "error");
      loggerLogData(true, "*******************error in ExtensionSignIn function*******************", "", true);
    }

  }

  extensionSignOut(userId: any, tenantId: any, cb: any) {
    let options: any;
    const token = jwtSign({
      createdDate: new Date(),
      connectorType: "msteams"
    });

    options = {
      // uri: AchieversAuth.AchieversTokenUrl,
      method: 'POST',
      uri: settings.appAuthURL + LogoutPath,
      body: {
        appUserId: userId,
        appId: tenantId
      },
      headers: {
        Authorization: token
      },
      json: true // Automatically parses the JSON string in the response
    };
    const description = {
      event: "extensionSignOut flow initiated",
      userId,
      tenantId
    };
    this.saveLogData(description, "middlewareAuth.ts/extensionSignOut", "AUDIT_LOGS");

    rp(options)
      .then(function (response) {
        cb(response, null);
      })
      .catch(function (err) {
        cb(null, err);
      });
  }


  encryptState(state: any) {
    const cipher = crypto.createCipher('aes192', settings.achieversAppClientSecret);
    let encryptedState = cipher.update(JSON.stringify(state), 'utf8', 'base64');
    encryptedState += cipher.final('base64');
    const parsedState = JSON.parse(encryptedState);
    return parsedState;

  }

  decryptState(rawState: any) {
    const decipher = crypto.createDecipher('aes192', settings.achieversAppClientSecret);
    let state = decipher.update(rawState, 'base64', 'utf8');
    state += decipher.final('utf8');
    const parsedState = JSON.parse(state);
    return parsedState;

  }

  async saveTokens(userId, tenantId, name, msTeamBotId, msTeamBotName, msTeamBotServiceUrl, graphAccessToken, graphRefreshToken) {

    consoleLogData(true, "Saving access tokens at time : ", new Date(), false);
    let options: any;

    return new Promise(function (resolve, reject) {
      try {
        const token = jwtSign({
          createdDate: new Date(),
          connectorType: "msteams"
        });
        options = {
          uri: settings.appAuthURL + SaveGraphTokensPath,
          method: "POST",
          body: {
            msTeamBotId: msTeamBotId,
            name: name,
            msTeamBotName: msTeamBotName,
            msTeamBotServiceUrl: msTeamBotServiceUrl,
            connectorType: 'msteams',
            appUserId: userId,
            appId: tenantId,
            msGraphAccessToken: graphAccessToken,
            msGraphRefreshToken: graphRefreshToken
          },
          headers: {
            Authorization: token
          },
          json: true // Automatically parses the JSON string in the response
        };

        // logger.debug("************* updateGraphTokens *******************" + options.body);
        // console.log("************* updateGraphTokens *******************" + options.body);
        // console.log(options.body);

        rp(options)
          .then(function (response) {
            resolve(response);
          })
          .catch(function (err) {
            reject(err);
          });
      }
      catch (e) {
        // let errArr = ['try-catch-err', 'msteams-connect', 'middlewareAuth.ts', 'saveTokens', e.message];
        // newrelic.noticeError(e);
        // logger.debug(errArr.join(":"));
        newRelicErrLog(e, "middlewareAuth.ts/saveTokens", "");
        const description = {
          "RequestDetails": { userId, tenantId, options },
          "ErrMessage": e.message
        };
        this.saveLogData(JSON.stringify(description), "middlewareAuth.ts/saveTokens", tenantId, "error");
        loggerLogData(true, "*******************error in saveTokens function*******************", "", true);

        reject(e);
      }
    });
  }

  async refreshTokens(refreshToken) {
    // TODO : Define logic for when the function receives an expired refresh token
    return new Promise(function (resolve, reject) {
      try {
        const options = {
          uri: "https://login.microsoftonline.com/common/oauth2/v2.0/token",
          method: "GET",
          formData: {
            'client_id': process.env.APP_CLIENT_ID,
            'scope': 'Calendars.Read offline_access',
            'refresh_token': refreshToken,
            'redirect_uri': process.env.APP_BASE_URL + '/auth/redirect',
            'grant_type': 'refresh_token',
            'client_secret': process.env.APP_CLIENT_SECRET
          }
        };

        // logger.debug("************* InRefreshTokens *******************" + options);
        // console.log("************* InRefreshTokens *******************" + options);

        rp(options)
          .then(function (response) {
            consoleLogData(true, "Successfully called for new tokens", "", false);
            response = JSON.parse(response);
            resolve(response);
          })
          .catch(function (err) {
            // console.log("Error in getting new tokens : ", err);
            // logger.debug("Error in getting new tokens : ", err);
            resolve({ "refreshToken": "Invalid", "error": err });
          });
      }
      catch (e) {
        // let errArr = ['try-catch-err', 'msteams-connect', 'middlewareAuth.ts', 'refreshTokens', e.message];
        // newrelic.noticeError(e);
        // logger.debug(errArr.join(":"));
        newRelicErrLog(e, "middlewareAuth.ts/refreshTokens", "");
        loggerLogData(true, "*******************error in refreshTokens function*******************", "", true);
        consoleLogData(true, "Error in sending request for new tokens", "", false);
        reject(e);
      }
    });
  }

  getConversationInfo = function (tenantId, conversationId) {

    return new Promise(function (resolve, reject) {
      try {
        const token = jwtSign({
          createdDate: new Date(),
          connectorType: "msteams"
        });
        const options = {
          uri: settings.appAuthURL + GetConversationInfoPath,
          qs: {
            appId: tenantId,
            conversationId: conversationId
          },
          headers: {
            Authorization: token
          },
          json: true // Automatically parses the JSON string in the response
        };

        rp(options)
          .then(function (response) {
            resolve(response);
          })
          .catch(function (err) {
            reject(err);
          });
      }
      catch (e) {
        // let errArr = ['try-catch-err', 'msteams-connect', 'middlewareAuth.ts', 'getConversationInfo', e.message];
        // newrelic.noticeError(e);
        // logger.debug(errArr.join(":"));
        newRelicErrLog(e, "middlewareAuth.ts/getConversationInfo", "");
        const description = {
          "RequestDetails": { tenantId, conversationId },
          "ErrMessage": e.message
        };
        this.saveLogData(JSON.stringify(description), "middlewareAuth.ts/getConversationInfo", tenantId, "error");
        loggerLogData(true, "*******************error in getConversationInfo function*******************", "", true);
        reject(e);
      }
    });
  }
  getConversationInfoByAppId = function (tenantId) {
    let options: any;
    return new Promise(function (resolve, reject) {
      try {
        const token = jwtSign({
          createdDate: new Date(),
          connectorType: "msteams"
        });
        options = {
          uri: settings.appAuthURL + GetConversationInfoByAppId,
          qs: {
            appId: tenantId,
            appType: "msteams"
          },
          headers: {
            Authorization: token
          },
          json: true // Automatically parses the JSON string in the response
        };

        rp(options)
          .then(function (response) {
            resolve(response);
          })
          .catch(function (err) {
            reject(err);
          });
      }
      catch (e) {
        // let errArr = ['try-catch-err', 'msteams-connect', 'middlewareAuth.ts', 'getConversationInfoByAppId', e.message];
        // newrelic.noticeError(e);
        // logger.debug(errArr.join(":"));
        newRelicErrLog(e, "middlewareAuth.ts/getConversationInfoByAppId", "");
        const description = {
          "RequestDetails": { tenantId, options },
          "ErrMessage": e.message
        };
        this.saveLogData(JSON.stringify(description), "middlewareAuth.ts/getConversationInfoByAppId", tenantId, "error");
        loggerLogData(true, "*******************error in getConversationInfoByAppId function*******************", "", true);
        reject(e);
      }
    });
  }

  updateConversationInfo = function (tenantId: string, conversationId: string) {
    let options: any;
    return new Promise(function (resolve, reject) {
      try {
        const token = jwtSign({
          createdDate: new Date(),
          connectorType: "msteams"
        });
        options = {
          uri: settings.appAuthURL + saveConversationInfoPath,
          method: "POST",
          body: {
            appId: tenantId,
            conversationId: conversationId,
            appType: "msteams"
          },
          headers: {
            Authorization: token
          },
          json: true // Automatically parses the JSON string in the response
        };

        // logger.debug("************* updateConversationInfo *******************" + options.body);
        // console.log("************* updateConversationInfo *******************" + options.body);

        rp(options)
          .then(function (response) {
            resolve(response);
          })
          .catch(function (err) {
            reject(err);
          });
      }
      catch (e) {
        // let errArr = ['try-catch-err', 'msteams-connect', 'middlewareAuth.ts', 'updateConversationInfo', e.message];
        // newrelic.noticeError(e);
        // logger.debug(errArr.join(":"));
        newRelicErrLog(e, "middlewareAuth.ts/updateConversationInfo", "");
        const description = {
          "RequestDetails": { tenantId, conversationId, options },
          "ErrMessage": e.message
        };
        this.saveLogData(JSON.stringify(description), "middlewareAuth.ts/updateConversationInfo", tenantId, "error");
        loggerLogData(true, "*******************error in updateConversationInfo function*******************", "", true);
        reject(e);
      }
    });
  }

  async getUserInfoFromAPI(info: any) {
    let requestData: any;
    try {
      // console.log("INFO : ", info);
      if (info.achieverMemberId && info.achieverAccessToken) {
        requestData = {
          uri: "https://" + info.subDomain + process.env.ACHIEVER_API_BASE_URI + "/users/" + info.achieverMemberId,
          headers: {
            Authorization: "Bearer " + info.achieverAccessToken
          },
          json: true // Automatically parses the JSON string in the response
        };

        let result = await rp(requestData);
        // console.log("getUserInfo result is : ", result);
        // console.log(JSON.stringify(result));
        return JSON.stringify(result);
      } else {
        consoleLogData(true, "**********achieverMemberId or achieverAccessToken is missing in the backend**********", "", false);
        return null;
      }

    } catch (err) {
      // logger.error("******************* getUserInfo err *******************", err);
      // let errArr = ['try-catch-err', 'msteams-connect', 'middlewareAuth.ts', 'getUserInfoFromAPI', err.message];
      // newrelic.noticeError(err);
      // logger.debug(errArr.join(":"));
      newRelicErrLog(err, "middlewareAuth.ts/getUserInfoFromAPI", "");
      const description = {
        "RequestDetails": { achieverMemberId: info.achieverMemberId, requestData },
        "ErrMessage": err.message
      };
      this.saveLogData(JSON.stringify(description), "middlewareAuth.ts/getUserInfoFromAPI", info.subDomain, "error");
      loggerLogData(true, "*******************error in getUserInfoFromAPI function*******************", "", true);
      return null;
    }

  }

  saveLogData(description: any, sourceFunction: string, subDomain: string, logLevel="info") {
    const token = jwtSign({
      createdDate: new Date(),
      connectorType: "msteams"
    });

    loggerLogData(true, "*************************** saveLogData **************************", "", false);
    const options = {
      uri: `${process.env.MIDDLEWARE_INTERNAL_API_URI}/api/shared/saveLogs`,
      method: "POST",
      body: {
        appType: "msteams",
        logLevel,
        description,
        sourceFunction,
        subDomain
      },
      headers: {
        Authorization: token
      },
      json: true // Automatically parses the JSON string in the response
    };
    rp(options);
  }

  saveClientDetails(tenantId, botId, botName, serviceUrl, programFk = null) {
    const token = jwtSign({
      createdDate: new Date(),
      connectorType: "msteams"
    });

    loggerLogData(true, "*************************** saveClientDetails **************************", "", false);
    const options = {
      uri: process.env.MIDDLEWARE_INTERNAL_API_URI + saveClientDetails,
      method: "POST",
      body: {
        appType: "msteams",
        appId: tenantId,
        appName: "msteams",
        msTeamBotId: botId,
        msTeamBotName: botName,
        msTeamBotServiceUrl: serviceUrl,
        programFK: programFk
      },
      headers: {
        Authorization: token
      },
      json: true // Automatically parses the JSON string in the response
    };
    rp(options);
  }
  async getNewsFeedActionDetails(actionType: string, newsFeedEventId: string, userdetails: any, optionsCall: boolean, postCall: boolean, postLikesCall: boolean, deleteLikesCall: boolean, getBoostsCall: boolean, getLikesCall: boolean) {
    // console.log("postLikesCall*****", postLikesCall+ "  deleteLikesCall    " + deleteLikesCall);
    try {
      if (userdetails.achieverAccessToken) {
        // checking for tracking in pendo
        const notValidForTracking = ["null", "undefined"];
        userdetails.achieverApplicationID = userdetails.achieverApplicationID && !notValidForTracking.includes(userdetails.achieverApplicationID) ? userdetails.achieverApplicationID : "";
        userdetails.achieverMemberId = userdetails.achieverMemberId && !notValidForTracking.includes(userdetails.achieverMemberId) ? userdetails.achieverMemberId : "";
        const properties = {
          medium: "msTeams",
          action: actionTypeForPendo(actionType, postCall, postLikesCall, deleteLikesCall)
        };
        if (optionsCall) {
          let requestData = {
            uri: "https://" + userdetails.subDomain + process.env.ACHIEVER_API_BASE_URI + "/newsfeed-events/" + newsFeedEventId + "/" + actionType,
            method: "OPTIONS",
            headers: {
              Authorization: "Bearer " + userdetails.achieverAccessToken
            },
            json: true // Automatically parses the JSON string in the response
          };
          let result = await rp(requestData);
          return result;
        } else if (postCall) {
          let requestData = {
            uri: "https://" + userdetails.subDomain + process.env.ACHIEVER_API_BASE_URI + "/newsfeed-events/" + newsFeedEventId + "/" + actionType,
            method: "POST",
            headers: {
              Authorization: "Bearer " + userdetails.achieverAccessToken
            },
            json: true // Automatically parses the JSON string in the response
          };
          let result = await rp(requestData);
          // tracking an event in pendo if successfully boosted/liked
          // External | Integration: Custom tab – Actions performed
          if (result && !result.message && properties.action && userdetails.achieverApplicationID && userdetails.achieverMemberId) {
            trackEventInPendo("External | Integration: Custom tab – Actions performed", userdetails.achieverApplicationID, userdetails.achieverMemberId, properties);
          }
          return result;
        } else if (postLikesCall) {
          consoleLogData(true, "inside POST LIKE callll", "", false);
          let requestData = {
            uri: "https://" + userdetails.subDomain + process.env.ACHIEVER_API_BASE_URI + "/newsfeed-events/" + newsFeedEventId + "/" + actionType,
            method: "POST",
            headers: {
              Authorization: "Bearer " + userdetails.achieverAccessToken
            },
            json: true // Automatically parses the JSON string in the response
          };
          let result = await rp(requestData);
          // console.log("getNewsFeedActionDetails result is : ", result);
          // tracking an event in pendo if successfully boosted/liked
          // External | Integration: Custom tab – Actions performed
          if (result && !result.message && properties.action && userdetails.achieverApplicationID && userdetails.achieverMemberId) {
            trackEventInPendo("External | Integration: Custom tab – Actions performed", userdetails.achieverApplicationID, userdetails.achieverMemberId, properties);
          }
          return result;
        } else if (deleteLikesCall) {
          consoleLogData(true, "inside DELE callll", "", false);
          let requestData = {
            uri: "https://" + userdetails.subDomain + process.env.ACHIEVER_API_BASE_URI + "/newsfeed-events/" + newsFeedEventId + "/" + actionType,
            method: "DELETE",
            headers: {
              Authorization: "Bearer " + userdetails.achieverAccessToken
            },
            json: true // Automatically parses the JSON string in the response
          };
          let result = await rp(requestData);
          consoleLogData(true, "getNewsFeedActionDetails result is : ", result, false);
          // tracking an event in pendo if successfully boosted/liked
          // External | Integration: Custom tab – Actions performed
          if (result && !result.message && properties.action && userdetails.achieverApplicationID && userdetails.achieverMemberId) {
            trackEventInPendo("External | Integration: Custom tab – Actions performed", userdetails.achieverApplicationID, userdetails.achieverMemberId, properties);
          }
          return result;
        } else if (getBoostsCall) {
          consoleLogData(true, "inside get boosts callll", "", false);
          let requestData = {
            uri: "https://" + userdetails.subDomain + process.env.ACHIEVER_API_BASE_URI + "/newsfeed-events/" + newsFeedEventId + "/" + actionType,
            method: "GET",
            headers: {
              Authorization: "Bearer " + userdetails.achieverAccessToken
            },
            json: true // Automatically parses the JSON string in the response
          };
          let result = await rp(requestData);
          consoleLogData(true, "getNewsFeedActionDetails result is : ", result, false);
          // tracking an event in pendo if successfully boosted/liked
          // External | Integration: Custom tab – Actions performed
          if (result && !result.message && properties.action && userdetails.achieverApplicationID && userdetails.achieverMemberId) {
            trackEventInPendo("External | Integration: Custom tab – Actions performed", userdetails.achieverApplicationID, userdetails.achieverMemberId, properties);
          }
          return result;
        } else if (getLikesCall) {
          consoleLogData(true, "inside getLikes Call callll", "", false);
          let requestData = {
            uri: "https://" + userdetails.subDomain + process.env.ACHIEVER_API_BASE_URI + "/newsfeed-events/" + newsFeedEventId + "/" + actionType,
            method: "GET",
            headers: {
              Authorization: "Bearer " + userdetails.achieverAccessToken
            },
            json: true // Automatically parses the JSON string in the response
          };
          let result = await rp(requestData);
          consoleLogData(true, "getNewsFeedActionDetails result is : ", result, false);
          // tracking an event in pendo if successfully boosted/liked
          // External | Integration: Custom tab – Actions performed
          if (result && !result.message && properties.action && userdetails.achieverApplicationID && userdetails.achieverMemberId) {
            trackEventInPendo("External | Integration: Custom tab – Actions performed", userdetails.achieverApplicationID, userdetails.achieverMemberId, properties);
          }
          return result;
        } else {
          let requestData = {
            uri: "https://" + userdetails.subDomain + process.env.ACHIEVER_API_BASE_URI + "/newsfeed-events/" + newsFeedEventId + "/" + actionType,
            headers: {
              Authorization: "Bearer " + userdetails.achieverAccessToken
            },
            json: true // Automatically parses the JSON string in the response
          };
          let result = await rp(requestData);
          return result;
        }
      } else {
        consoleLogData(true, "**********achieverAccessToken is missing in the backend**********", "", false);
        return null;
      }

    } catch (err) {
      // logger.error("******************* getNewsFeedActionDetails err *******************", err);
      // let errArr = ['try-catch-err', 'msteams-connect', 'middlewareAuth.ts', 'getNewsFeedActionDetails', err.message];
      // newrelic.noticeError(err);
      // logger.debug(errArr.join(":"));
      newRelicErrLog(err, "middlewareAuth.ts/getNewsFeedActionDetails", "");
      const description = {
        "RequestDetails": { achieverMemberId: userdetails.achieverMemberId },
        "ErrMessage": err.message
      };
      this.saveLogData(JSON.stringify(description), "middlewareAuth.ts/getNewsFeedActionDetails", userdetails.subDomain, "error");
      loggerLogData(true, "*******************error in getNewsFeedActionDetails function*******************", "", true);
      return null;
    }

  }

  async getOutlookCalendarView(accessToken) {
    const startDate = moment().subtract(9, "days").format("YYYY-MM-DD HH:mm:ss"); // 9 days which includes weekends
    const endDate = moment().format("YYYY-MM-DD HH:mm:ss");
    return new Promise(function (resolve, reject) {
      try {
        const requestData: any = {
          uri: `https://graph.microsoft.com/v1.0/me/calendarView?startDateTime=${startDate}&endDateTime=${endDate}&top=100`,
          // timeout: shouldTimeout ? 3000 : 30000,
          headers: {
            Authorization: `Bearer ${accessToken}`
          },
          json: true // Automatically parses the JSON string in the response
        };
        rp(requestData)
          .then(function (response) {
            resolve(response);
          })
          .catch(function (err) {
            newRelicErrLog(err, "middlewareAuth.ts/getOutlookCalendarView", "");
            loggerLogData(true, "*******************error in fetching calendar view Invalid*******************", "", true);
            resolve({ "Calendar view Error": "Invalid", "error": err });
          });
      }
      catch (e) {
        // let errArr = ['try-catch-err', 'msteams-connect', 'middlewareAuth.ts', 'getOutlookCalendarView', e.message];
        // newrelic.noticeError(e);
        // logger.debug(errArr.join(":"));
        // console.log("Error in fetching calendar view list");
        newRelicErrLog(e, "middlewareAuth.ts/getOutlookCalendarView", "");
        loggerLogData(true, "*******************error in fetching calendar view list*******************", "", true);
        reject(e);
      }
    });
  }

  //Celebratiobs
  async fetchCelebrationsOrRecognitionsapi(info: any, endPoint: any, startDate: string, endDate: string, celebrationType, visibility, page, pageSize) {
    let requestData: any;
    try {
      requestData = {
        uri: `https://${info.user.subDomain}${process.env.ACHIEVER_API_BASE_URI}/${endPoint}?startDate=${startDate}&endDate=${endDate}&celebrationType=${celebrationType}&visibility=${visibility}&page=${page}&pageSize=${pageSize}`,
        // timeout: shouldTimeout ? 3000 : 30000,
        headers: {
          Authorization: `Bearer ${info.user.achieverAccessToken}`
        },
        json: true // Automatically parses the JSON string in the response
      };

      const result = await rp(requestData);

      //  console.log("FINAL Result", result);

      return result && result.items ? result.items : null;

    } catch (err) {
      newRelicErrLog(err, "middlewareAuth.ts/fetchCelebrationsOrRecognitionsapi", "");
      const description = {
        "RequestDetails": { subDomain: info.user.subDomain, requestData },
        "ErrMessage": err.message
      };
      this.saveLogData(JSON.stringify(description), "middlewareAuth.ts/fetchCelebrationsOrRecognitionsapi", info.user.subDomain, "error");
      loggerLogData(true, "*******************error in fetchCelebrationsOrRecognitionsapi function*******************", "", true);
      // logger.error("******************* Celebration err *******************", err);
      if (err.error === "Error: ETIMEDOUT" || err.error === "Error: ESOCKETTIMEDOUT") {
        return null;
      } else {
        // newrelic.noticeError(err);
        return {
          status: 400,
          statusCode: err.statusCode,
          error: err.error
        };
      }
    }
  }

  async getUserNewsfeedConfiguration(userPk, programPk, subDomain, langCode = "en-US") {
    try {
        const token = jwtSign({
            createdDate: new Date(),
            connectorType: "msteams"
        });
        programPk = parseInt(programPk);
        userPk = parseInt(userPk);
        if (!(programPk && userPk)) {
            return null;
        }
        else {
            const requestData = {
                uri: `${process.env.MIDDLEWARE_INTERNAL_API_URI}/api/shared/grpcNewsfeedConfig`,
                headers: {
                    Authorization: token
                },
                qs: {
                    programId: programPk,
                    userPk: userPk,
                    language_code: langCode
                },
                json: true
            };
            const response = await rp(requestData);
            return response;
        }
    } catch (err) {
        newRelicErrLog(err, "middlewareAuth.ts/getUserNewsfeedConfiguration", "");
        loggerLogData(true, "*******************error in getUserNewsfeedConfiguration function*******************", "", true);
        return null;
    }
}

  async updateUserNewsfeedConfiguration(userPk, programPk, subDomain, newsfeedConfigPks, langCode="en-US") {
    try {
      const token = jwtSign({
        createdDate: new Date(),
        connectorType: "msteams"
      });
      const requestData = {
        uri: `${process.env.MIDDLEWARE_INTERNAL_API_URI}/api/shared/grpcNewsfeedConfig`,
        method: "POST",
        headers: {
          Authorization: token
        },
        body: {
          userPk: userPk,
          programId: programPk,
          language_code: langCode,
          newsfeedConfigPks: newsfeedConfigPks
        },
        json: true
      };
      const response = await rp(requestData);
      return response;
    } catch (err) {
      newRelicErrLog(err, "middlewareAuth.ts/getUserNewsfeedConfiguration", "");
      loggerLogData(true, "*******************error in updateUserNewsfeedConfiguration function*******************", "", true);
      return null;
    }
  }
}

const actionTypeForPendo = (actionType, postCall, postLikesCall, deleteLikesCall) => {
  actionType = actionType === "boosts" && postCall ? actionType : (actionType === "likes" && postLikesCall && !deleteLikesCall ? "likes+" : "likes-");
  switch (actionType) {
    case "boosts":
      return "Boosted";
    case "likes+":
      return "Liked";
    case "likes-":
      return "Unliked";
    default:
      return "";
  }
};

export default AchieversAuth;
