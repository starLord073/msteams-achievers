import * as express from 'express';
import { createValidator } from 'express-joi-validation';
import * as Joi from '@hapi/joi';
import { apiController } from '../controller/apiController';
import { MyBot } from '../controller/activityController';
import { viewSettings } from '../models/viewSettings';
import { checkIfAuthorized } from "../util/authentication";
const configFile = require("../config.json");
import { PendoController } from '../controller/pendoController';
import { HealthController } from '../controller/healthController';

const multer = require('multer');
const upload = multer().single('file');
const storage = multer.memoryStorage();
const validator = createValidator();

export default function setRoutes(app: any, viewSettings: viewSettings, connector: any, botAuth: any, achieverAPI: any, baseUrl: any, bot: any, MicrosoftAppId: any) {

  const router = express.Router();
  const appController = new apiController(botAuth, achieverAPI, bot, viewSettings);
  const activityController = new MyBot(baseUrl, bot, MicrosoftAppId, botAuth);
  const PendoEventTrack = new PendoController();
  const healthCtrl = new HealthController();

  const userInfoSchema = Joi.object({
    userName: Joi.string().optional(),
    userToken: Joi.string().required()
  });

  const tokenSchema = Joi.object({
    userToken: Joi.string().required()
  });

  const recogTypesSchema = Joi.object({
    userToken: Joi.string().required(),
    userIds: Joi.string().required()
  });

  const postRecognitionSchema = Joi.object({
    userToken: Joi.string().required(),
    imageSelected: Joi.string().required(),
    criterionVal: Joi.string().required(),
    userFullName: Joi.string().required(),
    nomineeEmails: Joi.string().required(),
    nominees: Joi.string().required(),
    recognitionText: Joi.string().required(),
    criterionId: Joi.number().required(),
    points: Joi.number().optional(),
    imageUrl: Joi.string().optional()
  }).pattern(/./, Joi.any());

  const imageSearchSchema = Joi.object({
    squery: Joi.string().optional().allow('').allow(null),
    userToken: Joi.string().required()
  });

  const pageInfoScheme = Joi.object({
    state: Joi.string().optional(),
    userName: Joi.string().optional(),
    lang: Joi.string().optional(),
    serviceUrl: Joi.string().optional(),
    conversationId: Joi.string().optional(),
    sourceName: Joi.string().optional(),
    conversationType: Joi.string().optional(),
    subDomain: Joi.string().optional(),
    channelSize: Joi.string().optional(),
    userPk: Joi.number().optional(),
    programPk: Joi.number().optional(),
    platform: Joi.string().optional(),
    userEmailForAdding: Joi.string().optional()
  });
  const newsfeedActionsSchema = Joi.object({
    actionType: Joi.string().required(),
    newsFeedEventId: Joi.string().required(),
    userToken: Joi.string().required(),
    optionsCall: Joi.string().optional(),
    postCall: Joi.string().optional(),
    postLikesCall: Joi.string().optional(),
    deleteLikesCall: Joi.string().optional(),
    getBoostsCall: Joi.string().optional(),
    getLikesCall: Joi.string().optional()
  });
  const recognitionSuggestionHideSchema = Joi.object({
    isRecognitionSuggestionHide: Joi.boolean().required(),
    azureUserId: Joi.string().required(),
    tenantId: Joi.string().required()
  });
    
    const getRecogSuggestionsSchema = Joi.object({
        userToken: Joi.string().required(),
        azureUserId: Joi.string().required(),
        tenantId: Joi.string().required(),
        userName: Joi.string().optional(),
    })
  // console.log("****configFile***", configFile);

  app.post('/api/messages', connector.listen());
  // Add here different language routes based on the languages from the eventHandlerController (query is lang)
  app.get('/views/*', validator.query(pageInfoScheme), async function (req: any, res: any) {
    let pgDetails;
    pgDetails = viewSettings;
    pgDetails['programPk'] = req.query.programPk;
    pgDetails['userPk'] = req.query.userPk;
    pgDetails['pendoInstallationKey'] = process.env.PENDO_INSTALLATION_KEY;
    pgDetails['configFile'] = configFile;
    pgDetails = JSON.stringify(pgDetails);
    res.render("view", { pgDetails });
  });

  app.use('/api/achievers', router);





  /**
   * @swagger
   *
   * /api/achievers/userfetch:
   *   post:
   *     description: API for users list
   *     parameters:
   *       - name: authorization
   *         in: header
   *         description: Token to be passed as a header.
   *         required: true,
   *       - name: userName
   *         description: Username to search user.
   *         in: body
   *         required: true
   *         type: string
   *       - name: userToken
   *         description: Signed connect userToken.
   *         in: body
   *         required: true
   *         type: string
   *     produces:
   *      - application/json
   *     responses:
   *       200:
   *         description: Success
   *         examples:
   *          application/json: {"members":[{"id":1,"firstName":"Mathew","preferredFirstName":"Mathew","lastName":"Mathew","fullName":"Mathew Mathew","username":"Mathew.Mathew","emailAddress":"Mathew.Mathew@example.com","regionCode":"US","dateCreated":"2021-03-16T10:41:20-04:00","status":"Enabled","links":[],"profileImageUrl":"https://s.example.com/statics/icons/user/Mathew.jpg","isPersonalizedProfileImage":false,"largeImageUrl":"https://s.example.com/statics/icons/user/Mathew.jpg","displayValues":[{"id":0,"type":"email","value":"Mathew.Mathew@example.com"},{"id":1526,"type":"hierarchy","value":"Binary Experience"}],"reportsTo":[]},{"id":2,"firstName":"Selina","preferredFirstName":"Selina","lastName":"Selina","fullName":"Selina Selina","username":"Selina.Selina","emailAddress":"Selina.Selina@example.com","regionCode":"CA","dateCreated":"2021-06-11T14:11:13-04:00","status":"Enabled","links":[],"profileImageUrl":"https://s.example.com/statics/icons/user/Selina.jpg","isPersonalizedProfileImage":false,"largeImageUrl":"https://s.example.com/statics/icons/user/Selina.jpg","displayValues":[{"id":0,"type":"email","value":"Selina.Selina@example.com"},{"id":1526,"type":"hierarchy","value":"Binary Experience"}],"reportsTo":{"rt_766":"Cristiano"}}],"groups":[],"isOverLimit":false}
   *       401:
   *         description: Unauthorized Error
   *       400:
   *         description: Error in getting response from achievers API
   *
   *
   */



  router.route("/api/achievers/userfetch").post(checkIfAuthorized, validator.body(userInfoSchema), appController.getNames);
  router.route("/auth/redirect").get(appController.getAccessTokens);


   /**
   * @swagger
   *
   * /api/achievers/postrecog:
   *   post:
   *     description: Post a recognition onto the Achievers platform
   *     parameters:
   *       - name: authorization
   *         in: header
   *         description: Token to be passed as a header
   *         required: true
   *       - name: userToken
   *         description: Signed connect userToken
   *         in: body
   *         required: true
   *         type: string
   *       - name: imageSelected
   *         description: ImageSelected by the user
   *         in: body
   *         required: true
   *         type: string
   *       - name: criterionVal
   *         description: Criteria value of the recognition
   *         in: body
   *         required: true
   *         type: string
   *       - name: userFullName
   *         description: Name of the nominator
   *         in: body
   *         required: true
   *         type: string
   *       - name: nomineeEmails
   *         description: Comma separated emails of the nominees
   *         in: body
   *         required: true
   *         type: string
   *       - name: nominees
   *         description: Name Nominee for the recognition
   *         in: body
   *         required: true
   *         type: string
   *       - name: recognitionText
   *         description: Text related to the recognition that is to be created
   *         in: body
   *         required: true
   *         type: string
   *       - name: criterionId
   *         description: Criteria id of the recognition
   *         in: body
   *         required: true
   *         type: string
   *       - name: points
   *         description: Point to award to the nominees of the recognition
   *         in: body
   *         required: true
   *         type: string
   *       - name: imageUrl
   *         description: ImageUrl format gif | jpg | png | jpeg
   *         in: formData
   *         required: true
   *         type: file
   *
   *     produces:
   *      - application/json
   *     responses:
   *       200:
   *         description: Success
   *         examples:
   *          application/json: {"message":"Success","newsFeedUrl":"https://sample.example.com/recent_activity?utm_medium=msteams&utm_term=recognition"}
   *       401:
   *         description: Unauthorized Error
   *       400:
   *         description: Error in getting response from achievers API
   *
   */
  router.route("/api/achievers/postrecog").post(
    checkIfAuthorized, function (req, res, next) {
      upload(req, res, function (err: any) {
        if (err instanceof multer.MulterError) {
          // A Multer error occurred when uploading.
          next(err);

        } else if (err) {
          next(err);
          // An unknown error occurred when uploading.
        } else {
          next();
        }
      });
    }, validator.body(postRecognitionSchema), appController.postRecognition);
  // appController.postRecognition);
  router.route("/api/achievers/getnewsFeedActionsInfo").post(checkIfAuthorized, validator.body(newsfeedActionsSchema), appController.newsFeedActions);
  router.route("/api/achievers/getnewsFeedActionsInfo").delete(checkIfAuthorized, validator.body(newsfeedActionsSchema), appController.newsFeedActions);

/**
   * @swagger
   *
   * /api/achievers/getrecogtypes:
   *   post:
   *     description: Endpoint to get possible recognition types that the nominee can receive by nominator
   *     parameters:
   *       - name: authorization
   *         in: header
   *         description: Token to be passed as a header.
   *         required: true,
   *       - name: userIds
   *         description: UserId of the nominee for recognition.
   *         in: body
   *         required: true
   *         type: string
   *       - name: userToken
   *         description: Signed connect userToken.
   *         in: body
   *         required: true
   *         type: string
   *     produces:
   *      - application/json
   *     responses:
   *       200:
   *         description: Success
   *         examples:
   *          application/json: {"paging":{"totalItems":2,"totalPages":1,"page":1,"pageSize":100,"self":"http://sample.example.com/api/v5/recognitions?page=1&pageSize=100&useUserIds=true&nominees=10726871%2C10726821%2C10726846","first":"http://sample.example.com/api/v5/recognitions?page=1&pageSize=100&useUserIds=true&nominees=10726871%2C10726821%2C10726846","last":"http://sample.example.com/api/v5/recognitions?page=1&pageSize=100&useUserIds=true&nominees=10726871%2C10726821%2C10726846"},"items":[{"id":"31301","name":"Our Core Values","description":"Our values are who we are and how we accomplish our goals. They are the compass that guides us to greatness.When we recognize based on our values and great work, we focus... and we Achieve.","bannerURL":"https://sample.example.com/platform_content/shard_10271/apiexa/private/module/peer_to_peer/31301/banner/en-US/banner.jpg?156430","moduleType":"POINT-BASED","notifyReportsTo":false,"enableGiphy":true,"enablePhotoUpload":true,"moduleReceivePermissions":"W3sicHJvZ3JhbSI6dHJ1ZX1d","criteria":[{"id":"28361","name":"Integrity","description":"We employ the highest ethical standards, demonstrating honesty and fairness in every action that we take. ","criterionBannerUrl":"https://sample.example.com/platform_content/shard_10271/apiexa/private/module/peer_to_peer/31301/postcard_images/en-US/screen/28361.png?1571142211","pointValue":"500","pointValueInfo":{"min":"500","max":"500","values":["500"],"defaultValue":"500"}}],"customFields":[]},{"id":"31536","name":"Everyday Recognition","description":"Greatness deserves recognition – even when greatness is found in the small, daily things that make your day easier. Build a culture of engagement one free recognition at a time, without limits and without borders. Never underestimate the ability for your kindness to make someone’s day.","bannerURL":"https://sample.example.com/platform_content/shard_10271/apiexa/private/module/peer_to_peer/31536/banner/en-US/banner.jpg?1564826832","moduleType":"FREE","notifyReportsTo":false,"enableGiphy":true,"enablePhotoUpload":true,"moduleReceivePermissions":"W3sicHJvZ3JhbSI6dHJ1ZX1d","criteria":[{"id":"28571","name":"Going the Extra Mile","description":"You didn't just get the job done. You put in that critical extra effort, which directly impacted your success and the success of those around you.","criterionBannerUrl":"https://sample.example.com/platform_content/shard_10271/apiexa/private/module/peer_to_peer/31536/postcard_images/en-US/screen/28571.png?1571049512","pointValue":"0","pointValueInfo":{"min":"0","max":"0","values":["0"],"defaultValue":"0"}},{"id":"28601","name":"a Thank You","description":"Perhaps you helped a coworker in need or just refilled the office coffee machine. Whatever you did, your thoughtful actions should be celebrated.","criterionBannerUrl":"https://sample.example.com/platform_content/shard_10271/apiexa/private/module/peer_to_peer/31536/postcard_images/en-US/screen/28601.png?1571134178","pointValue":"0","pointValueInfo":{"min":"0","max":"0","values":["0"],"defaultValue":"0"}},{"id":"28611","name":"Passion","description":"You don't just clock your hours and go home; you truly care about the work you do, and it shows. Your passion is inspiring. ","criterionBannerUrl":"https://sample.example.com/platform_content/shard_10271/apiexa/private/module/peer_to_peer/31536/postcard_images/en-US/screen/28611.png?1571312819","pointValue":"0","pointValueInfo":{"min":"0","max":"0","values":["0"],"defaultValue":"0"}}],"customFields":[]}]}
   *       401:
   *         description: Unauthorized Error
   *       400:
   *         description: Error in getting response from achievers API
   *
   *
   */

  router.route("/api/achievers/getrecogtypes").post(checkIfAuthorized, validator.body(recogTypesSchema), appController.getRecogTypes);


  /**
   * @swagger
   *
   * /api/achievers/logout:
   *   post:
   *     description: Endpoint to logout the user
   *     parameters:
   *       - name: authorization
   *         in: header
   *         description: Token to be passed as a header.
   *         required: true
   *       - name: userToken
   *         description: Signed connect userToken.
   *         in: body
   *         required: true
   *         type: string
   *     produces:
   *      - application/json
   *     responses:
   *       200:
   *         description: userObj
   *         examples:
   *          application/json: {"message":"success"}
   *       401:
   *         description: Unauthorized Error
   *       400:
   *         description: Error stack
   *
   *
   */



  router.route("/api/achievers/logout").post(checkIfAuthorized, validator.body(tokenSchema), appController.logout);


/**
   * @swagger
   *
   * /api/achievers/curMember:
   *   post:
   *     description: Endpoint to get information about the  current signed in user
   *     parameters:
   *       - name: authorization
   *         in: header
   *         description: Token to be passed as a header
   *         required: true
   *       - name: userToken
   *         description: Signed connect userToken.
   *         in: query
   *         required: true
   *         type: string
   *     produces:
   *      - application/json
   *     responses:
   *      200:
   *        description: Success
   *        examples:
   *          application/json: {"id":1,"firstName":"Kevin","preferredFirstName":"Kevin","lastName":"Kevin","fullName":"Kevin Kevin","username":"Kevin.Kevin","emailAddress":"Kevin.Kevin@example.com","regionCode":"IN","dateCreated":"2021-03-10T09:45:15-05:00","status":"Enabled","links":[],"profileImageUrl":"https://s.example.com/platform_content/shard_10271/apiexa/public/user/1/KCRiFRLFBA/icon_med11.jpg?1","isPersonalizedProfileImage":true,"largeImageUrl":"https://s1.sandbox.example.com/platform_content/shard_10271/apiexa/public/user/1/KC1QQRLFBA/icon_large11.jpg?3","displayValues":[{"id":0,"type":"email","value":"Kevin.Kevin@example.com"},{"id":1526,"type":"hierarchy","value":"Binary Experience"},{"id":1531,"type":"hierarchy","value":"Boston"},{"id":6691,"type":"customField","value":""}],"reportsTo":"Mathew Mathew","featureFlags":["deeplinking-enabled"],"localeId":"en-US","timezone":"America/New_York","memberBudget":{"totalPointsAvailable":37808,"recognitionBudgets":{"31301":{"moduleId":31301,"budgetRemaining":{"points":"37808","regionCode":"IN"},"moduleType":"POINT-BASED","budgetType":"USER","budgetId":"4714991","moduleName":"Our Core Values"}}},"pointsBalance":0,"pointsAccumulated":0,"namePronunciationUrl":"","namePronunciationFilePath":"","pointsExemptStatus":0,"isRedeemOnly":false,"sessionLocaleId":false}
   *      401:
   *        description: Unauthorized Error
   *      400:
   *        description: Error in getting response from achievers API
   *
   *
   */


  router.route("/api/achievers/curMember").post(checkIfAuthorized, validator.body(tokenSchema), appController.getCurrentMember);


  /**
   * @swagger
   *
   * /api/achievers/imageSearch:
   *   post:
   *     description: Endpoint to search for custom images during recognition process
   *     parameters:
   *       - name: authorization
   *         in: header
   *         description: Token to be passed as a header
   *         required: true
   *       - name: squery
   *         description: Query string to search images or gifs on.
   *         in: body
   *         required: false
   *         type: string
   *       - name: userToken
   *         description: Signed connect userToken.
   *         in: body
   *         required: true
   *         type: string
   *     produces:
   *      - application/json
   *     responses:
   *       200:
   *         description: imagesArr
   *         examples:
   *          application/json: {"paging":{"totalItems":7049,"totalPages":294,"page":1,"pageSize":24,"self":"http://sample.example.com/api/v5/image-search?pageSize=24&q=","first":"http://sample.example.com/api/v5/image-search?pageSize=24&q=&page=1","last":"http://sample.example.com/api/v5/image-search?pageSize=24&q=&page=294","next":"http://sample.example.com/api/v5/image-search?pageSize=24&q=&page=2"},"items":[{"previewUrl":"https://media4.giphy.com/media/3oEjHV0z8S7WM4MwnK/200w_d.gif?cid=18e4c9ddi03eas6el368zjkwpevm&rid=200w_d.gif&ct=g","url":"https://media4.giphy.com/media/3oEjnK/giphy.gif?cid=18e4c9ddi0pwc6j272l28eas6elvm&rid=giphy.gif&ct=g"},{"previewUrl":"https://media1.giphy.com/media/26BRHRPyg/200w_d.gif?cid=18e4c9ddi0335ocl28eas6el368zjkwpevm&rid=200w_d.gif&ct=g","url":"https://media1.giphy.com/media/26BRrHRPyg/giphy.gif?cid=18e4c9ddi0335ocxzeb8pwc6j272l28eas6el368zjkwpevm&rid=giphy.gif&ct=g"},{"previewUrl":"https://media4.giphy.com/media/6mBnSxjhtP/200w_d.gif?cid=18e4cs6el368zjkwpevm&rid=200w_d.gif&ct=g","url":"https://media4.giphy.com/media/6mBnSusjhtP/giphy-downsized.gif?cid=18e4c9ddas6el368zjkwpevm&rid=giphy-downsized.gif&ct=g"},{"previewUrl":"https://media3.giphy.com/media/3oEk9cmCTC/200w_d.gif?cid=18e4c9ddi0335o368zjkwpevm&rid=200w_d.gif&ct=g","url":"https://media3.giphy.com/media/3oEjHPG425Ck9cmCTC/giphy.gif?cid=18e4c9dd72l28eas6el368zjkwpevm&rid=giphy.gif&ct=g"}]}
   *       401:
   *         description: Unauthorized Error
   *       400:
   *         description: Error in getting response from achievers API
   *
   *
   */



  router.route("/api/achievers/imageSearch").post(checkIfAuthorized, validator.body(imageSearchSchema), appController.getgifgrid);

  router.route("/api/achievers/sendNotification").post(checkIfAuthorized, activityController.sendNotification);
  router.route("/api/achievers/sendLogoutSuccess").get(checkIfAuthorized, activityController.sendProactiveLogout);






  router.route("/api/achievers/getAllUserDetails").post(checkIfAuthorized, appController.getAllUserDetails);

  // staticTab endpoint
  router.route("/api/achievers/newsFeedActivities").get(appController.newsFeedActivities);

    /**
   * @swagger
   *
   * /api/achievers/pendo:
   *   post:
   *     description: Endpoint to track events on pendo for statistics
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: authorization
   *         in: header
   *         description: Token to be passed as a header
   *         required: true
   *       - name: event
   *         description: Event
   *         in: body
   *         required: true
   *         type: string
   *       - name: accountId
   *         description: accountId
   *         in: body
   *         required: true
   *         type: string
   *       - name: properties
   *         description: properties
   *         in: body
   *         required: true
   *         type: string
   *       - name: visitorId
   *         description: visitorId
   *         in: body
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         description: successful
   *       401:
   *         description: Unauthorized Error
   *
   *
   */

  router.route("/api/achievers/pendo").post(checkIfAuthorized, PendoEventTrack.pendoTracking);


  /**
   * @swagger
   *
   * /api/achievers/fetchCelebrationsOrRecognitions:
   *   post:
   *     description: API for fetchCelebrationsOrRecognitions
   *     parameters:
   *       - name: authorization
   *         in: header
   *         description: Token to be passed as a header
   *         required: true
   *       - name: found
   *         description: found
   *         in: body
   *         required: true
   *         type: string
   *       - name: loggedInAchieversUser
   *         description: loggedInAchieversUser
   *         in: body
   *         required: true
   *         type: string
   *       - name: userLocale
   *         description: userLocale
   *         in: body
   *         required: true
   *         type: string
   *       - name: userToken
   *         description: User's userToken.
   *         in: body
   *         required: true
   *         type: string
   *     produces:
   *      - application/json
   *     responses:
   *      200:
   *        description: Success
   *        examples:
   *          application/json: [{"user": {"id": 1,"name": "Traci Bank","profileImageUrl": "https://s1.sandbox.achievers.com/platform_content/shard_10271/apiexa/public/user/1/KC02NEosRkBFLiYo/icon_med.jpg?1511467945","href": "https://example.sandbox.achievers.com/api/v5/users/1","method": "GET","rel": "user"},"celebrationDate": "2021-08-26","numberOfYears": 8,"celebrationType": "Service Awards","cardLandingPageURL": "https://example.sandbox.achievers.com/card/1131/1dfc9f45bd856756fe3195d0cc2da2d","milestoneConfigId": 821,"isSignedByCurrentUser": false,"cardId": 1131,"upcomingDateDiff": 12,"userProfileImageFromUser": "https://s1.sandbox.achievers.com/platform_content/shard_10271/apiexa/public/user/1/KC02NEosRkBFLiYo/icon_med.jpg?1511467945","userFullName": "Traci Bank","cardTitle": "Congratulate Traci on 8 years!","cardText": "Sign their celebration card","cardType": "celebration","finalDate": "August 26, 2021","typeOfCelebration": "anniversary","redirectURL": "https://example.sandbox.achievers.com/card/1131/1dfc9f45bd856756fe3195d0cc2da2d"}]
   *      401:
   *        description: Unauthorized Error
   *      400:
   *        description: Error in getting response from achievers API
   *
   *
   */

  // upcomig celebrations
  router.route("/api/achievers/fetchCelebrationsOrRecognitions").post(checkIfAuthorized, appController.fetchCelebrationsOrRecognitions);

  /**
   * @swagger
   *
   * /api/achievers/calenderPermission:
   *   post:
   *     description: API for calender permission
   *     parameters:
   *       - name: appUserId
   *         in: body
   *         description: appUserID of user
   *         required: true
   *         type: string
   *       - name: appId
   *         in: body
   *         description: appId of user
   *         required: true
   *         type: string
   *     produces:
   *      - application/json
   *     responses:
   *      200:
   *        description: Success
   *      401:
   *        description: Unauthorized Error
   *      400:
   *        description: Error in sending permission card to user
   *
   *
   */

  router.route('/api/achievers/calenderPermission').post(checkIfAuthorized,activityController.sendCalenderPermissionCard)

   /**
   * @swagger
   *
   * /api/achievers/getRecognitionSuggestions:
   *   post:
   *     description: API for fetching getRecognitionSuggestions
   *     parameters:
   *       - name: azureUserId
   *         in: body
   *         description: azureUserId of user
   *         required: true
   *         type: string
   *       - name: tenantId
   *         in: body
   *         description: tenantId of user
   *         required: true
   *         type: string
   *       - name: userName
   *         in: body
   *         description: userName of user
   *         required: true
   *         type: string
   *     produces:
   *      - application/json
   *     responses:
   *      200:
   *        description: Success
   *        examples:
   *          application/json: {"finalArray":[{"email":["example.example@nodemailer.com","mary.mary@nodemailer.com"],"startTime":"2021-08-04T17:00:00.0000000","endTime":"2021-08-04T18:00:00.0000000","organizerEmailAdd":"monica.monica@nodemailer.com","type":"group","daysDiff":13,"timeDiff":18768,"profileImages":["https://example.s1.com/platform_content/shard_10271/apiexa/public/user/1/KDk2KE/icon_med.jpg?1601","https://s0.example.com/statics/icons/user/v_4490a_icon_med.jpg"],"profileNames":["Example","Mary Mary"]},{"email":["mary.mary@nodemailer.com"],"startTime":"2021-08-04T14:00:00.0000000","endTime":"2021-08-04T15:00:00.0000000","organizerEmailAdd":"mary.mary@example.com","type":"group","daysDiff":13,"timeDiff":18948,"profileImages":["https://s0.example.com/icons/user/v_490a_icon_med.jpg"],"profileNames":["Mary Mary"]},{"email":"mary.mary@nodemailer.com","startTime":"2021-08-04T14:00:00.0000000","endTime":"2021-08-04T15:00:00.0000000","organizerEmailAdd":"ronaldo.cristiano@example.com","type":"collaboration","count":2,"daysDiff":13,"timeDiff":18948,"profileImages":"https://s0.example.com/icons/user/v_490a_icon_med.jpg","profileNames":"Sample Sample","memberId":1}]}
   *      401:
   *        description: Unauthorized Error
   *      400:
   *        description: Error in fetching recognitions
   *        examples:
   *          application/json: {"finalArray":[]}
   *
   *
   */
  // fetch getRecognitionSuggestions
  router.route("/api/achievers/getRecognitionSuggestions").post( checkIfAuthorized, validator.body(getRecogSuggestionsSchema) ,appController.getRecognitionSuggestions);
  router.route("/api/achievers/fetchUpcomingCelebrationConfig").get( checkIfAuthorized, appController.fetchUpcomingCelebrationConfig);

    /**
   * @swagger
   *
   * /api/achievers/createComment:
   *   post:
   *     description: API for posting comment
   *     parameters:
   *       - name: azureUserId
   *         in: body
   *         description: azureUserId of user
   *         required: true
   *         type: string
   *       - name: tenantId
   *         in: body
   *         description: tenantId of user
   *         required: true
   *         type: string
   *       - name: newsfeedEventId
   *         in: body
   *         description: event id of post
   *         required: true
   *         type: integer
   *       - name: comment
   *         in: body
   *         description: comment to post
   *         required: true
   *         type: string
   *     produces:
   *      - application/json
   *     responses:
   *      200:
   *        description: Success
   *        examples:
   *          application/json: {"paging":{"totalItems":2,"totalPages":1,"page":1,"pageSize":25,"self":"http://example.sandbox.achievers.com/api/v5/newsfeed-events/71440911/comments","first":"http://example.sandbox.achievers.com/api/v5/newsfeed-events/71440911/comments?page=1","last":"http://example.sandbox.achievers.com/api/v5/newsfeed-events/71440911/comments?page=1"},"items":[{"commentId":"5457881","comment":"testing","dateCommented":"2021-09-28 05:20:40","canDelete":true,"user":{"id":10726846,"name":"Pankaj Jindal","profileImageUrl":"https://s1.sandbox.achievers.com/statics/icons/user/v_83579ef5_icon_med.jpg","href":"https://example.sandbox.achievers.com/api/v5/users/10726846","method":"GET","rel":"commenter"},"mentionData":null},{"commentId":"5457876","comment":"tetsing","dateCommented":"2021-09-28 05:17:34","canDelete":true,"user":{"id":10726846,"name":"Pankaj Jindal","profileImageUrl":"https://s1.sandbox.achievers.com/statics/icons/user/v_83579ef5_icon_med.jpg","href":"https://example.sandbox.achievers.com/api/v5/users/10726846","method":"GET","rel":"commenter"},"mentionData":null}]}
   *      401:
   *        description: Unauthorized Error
   *      400:
   *        description: 	Bad Request
   *        examples:
   *          application/json: "{\n  \"code\": -1,\n  \"message\": \"Invalid Params\",\n  \"errors\" : [\n    {\n      \"username\": \"Invalid username\"\n    },\n    {\n      \"password\": \"Password can not be blank\"\n    }\n  ]\n}\n"
   *
   *
   */
  router.route("/api/achievers/createComment").post( checkIfAuthorized, appController.createComment);
   /**
   * @swagger
   *
   * /api/achievers/updateIsRecognitionSuggestionHide:
   *   post:
   *     description: API for modifing isRecognitionSuggestionHide
   *     parameters:
   *       - name: azureUserId
   *         in: body
   *         description: azureUserId of user
   *         required: true
   *         type: string
   *       - name: isRecognitionSuggestionHide
   *         in: body
   *         description: isRecognitionSuggestionHide is boolean value
   *         required: true
   *         type: boolean
   *       - name: tenantId
   *         in: body
   *         description: tenantId of user
   *         required: true
   *         type: string
   *
   *     produces:
   *      - application/json
   *     responses:
   *      200:
   *        description: Success
   *        examples:
   *          application/json: [1]
   *      401:
   *        description: Unauthorized Error
   *      400:
   *        description: 	Bad Request
   *        examples:
   *          application/json:
   *
   *
   */
  router.route("/api/achievers/updateIsRecognitionSuggestionHide").post(checkIfAuthorized, validator.body(recognitionSuggestionHideSchema), appController.updateUserRecognitionSuggestionHide);
  router.route("/api/achievers/health").get(healthCtrl.getHealthInfo);

  router.route("/api/achievers/feedDetailInfo").get(checkIfAuthorized, appController.feedDetailView);

  router.route("/api/achievers/feedDetailComments").get(checkIfAuthorized, appController.feedDetailComments);
  router.route("/api/achievers/getNewsfeedConfig").get(checkIfAuthorized, appController.getUserNewsfeedConfig);
  router.route("/api/achievers/getNewsfeedConfig").post(checkIfAuthorized, appController.updateUserNewsfeedConfig);

  app.use('/', router);
}
