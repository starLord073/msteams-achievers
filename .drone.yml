---
# ##
# Language upload and download
# ##

kind: pipeline
type: docker
name: language-download

platform:
  os: linux
  arch: amd64

trigger:
  event:
    include:
    - push
    exclude:
    - promote

steps:
  - name: language-translation-download
    image: gcr.io/achievers-shd/utilities/gke-achievers-translation-integrator:v1.0.0
    pull: always
    environment:
      LANGUAGE_GITHUB_SSH:
        from_secret: SERVICE_DEPLOY_GITHUB_SSH
      UPSTREAM: BE/connect-language
      APP_NAME: connect_msteams
      COMMIT_ID: ${DRONE_COMMIT_SHA:0:8}
      GITHUB_HOST: github.corp.achievers.com
      GITHUB_USER:
        from_secret: DOCKER_USERNAME
      GITHUB_PASSWORD:
        from_secret: DOCKER_PASSWORD
    volumes:
      - name: tmp
        path: "/tmp"
    commands:
      - echo ${DRONE_COMMIT_AUTHOR}
      - echo ${DRONE_COMMIT_AUTHOR_EMAIL}
      - bash /entrypoint.sh download_translations
      - cp -r /drone/src/workspace/[a-z][a-z]-[A-Z][A-Z]/ /tmp
      - echo "****Show shared volume files****"
      - find /tmp/**-**/

image_pull_secrets:
 - DOCKER_GCR_AUTHENTICATION

volumes:
  - name: tmp
    temp: {}

---
# ##
# test Pipeline
#  - run unit tests
#  -
# ##

kind: pipeline
type: docker
name: test-feature-branch

platform:
  os: linux
  arch: amd64

trigger:
  event:
    include:
    - push
    - pull_request
    exclude:
    - promote
  branch:
    exclude:
      - master
            
steps:
  - name: unit-test
    image: node:10
    environment:
      APP_CLIENT_ID:
        from_secret: APP_CLIENT_ID
      APP_CLIENT_SECRET:
        from_secret: APP_CLIENT_SECRET
      CONNECTOR_SECRET_TOKEN:
        from_secret: CONNECTOR_SECRET_TOKEN
      PORT: 4001
      APP_BASE_URL: https://connect.devapi.achievers.com/msTeams
      MIDDLWARE_API_URI: https://connect.devapi.achievers.com/middleware
      ACHIEVER_API_BASE_URI: .achievers.com/api/v5
      APP_LOG_MAX_SIZE: 10M
      APP_LOG_MAX_ARCHIVE_LIMIT: 14d
      APP_LOG_LEVEL: debug
    commands:
      - npm install
      - npm run test
      - npm run lint
      
  - name: create-sonarqube-prop-file
    image: alpine:latest
    commands:
      - echo "sonar.qualitygate.wait=false" > sonar-project.properties
      - echo "sonar.buildbreaker.skip=true" >> sonar-project.properties
      - echo "sonar.xunit.skipDetails=false" >> sonar-project.properties
      - echo "sonar.projectBaseDir=." >> sonar-project.properties
      - echo "sonar.scm.provider=git" >> sonar-project.properties
      - echo "sonar.scm.forceReloadAll=true" >> sonar-project.properties
      - echo "sonar.exclusions=docker-compose.yml, output/**/* , test/*, output.xml, lint-output.json" >> sonar-project.properties
      - echo "sonar.log.level=INFO" >> sonar-project.properties

      - echo "sonar.projectKey=${DRONE_REPO//\//:}" >> sonar-project.properties
      - echo "sonar.projectName=${DRONE_REPO}" >> sonar-project.properties
      - echo "sonar.projectVersion=${DRONE_COMMIT_SHA:0:8}" >> sonar-project.properties
      - echo "sonar.links.ci=${DRONE_BUILD_LINK}" >> sonar-project.properties
      - echo "sonar.links.scm=${DRONE_REPO_LINK}" >> sonar-project.properties
      - if [ $DRONE_COMMIT_BRANCH != "master" ] ; then echo "sonar.branch.name=${DRONE_COMMIT_BRANCH}" >> sonar-project.properties ; fi
      - if [ $DRONE_BUILD_EVENT == "pull_request" ] ; then echo "sonar.pullrequest.key=${DRONE_PULL_REQUEST}" >> sonar-project.properties ; fi
      - if [ $DRONE_BUILD_EVENT == "pull_request" ] ; then echo "sonar.pullrequest.branch=${DRONE_SOURCE_BRANCH}" >> sonar-project.properties ; fi
      - if [ $DRONE_BUILD_EVENT == "pull_request" ] ; then echo "sonar.pullrequest.base=master" >> sonar-project.properties ; fi

      - echo "sonar.sources=." >> sonar-project.properties
      - echo "sonar.javascript.lcov.reportPaths=output/coverage/jest/lcov.info" >> sonar-project.properties
      - echo "sonar.testExecutionReportPaths=output/coverage/jest/sonar-report.xml" >> sonar-project.properties
      - echo "sonar.tests=test" >> sonar-project.properties
      - echo "sonar.test.inclusions=test/*" >> sonar-project.properties
      - echo "sonar.eslint.reportPaths=lint-output.json" >> sonar-project.properties

  - name: perform-code-analysis
    image: aosapps/drone-sonar-plugin
    settings:
      showProfiling: true
      enableGateBreaker: true
      timeout: 60
      usingProperties: true
      sonar_host:
        from_secret: SONARQUBE_HOST
      sonar_token:
        from_secret: SONARQUBE_TOKEN

---
# ##
# Build Pipeline
#  - build containers
#  - store in a registry
# ##

kind: pipeline
type: docker
name: test-master-branch

platform:
  os: linux
  arch: amd64

trigger:
  event:
    include:
    - push
    - pull_request
    exclude:
    - promote
  branch:
    - master
    
    
steps:
  - name: unit-test
    image: node:10
    environment:
      APP_CLIENT_ID:
        from_secret: APP_CLIENT_ID
      APP_CLIENT_SECRET:
        from_secret: APP_CLIENT_SECRET
      CONNECTOR_SECRET_TOKEN:
        from_secret: CONNECTOR_SECRET_TOKEN
      PORT: 4001
      APP_BASE_URL: https://connect.devapi.achievers.com/msTeams
      MIDDLWARE_API_URI: https://connect.devapi.achievers.com/middleware
      ACHIEVER_API_BASE_URI: .achievers.com/api/v5
      APP_LOG_MAX_SIZE: 10M
      APP_LOG_MAX_ARCHIVE_LIMIT: 14d
      APP_LOG_LEVEL: debug
    commands:
      - npm install
      - npm run test
      - npm run lint

  - name: create-sonarqube-prop-file
    image: alpine:latest
    commands:
      - echo "sonar.qualitygate.wait=false" > sonar-project.properties
      - echo "sonar.buildbreaker.skip=true" >> sonar-project.properties
      - echo "sonar.xunit.skipDetails=false" >> sonar-project.properties
      - echo "sonar.projectBaseDir=." >> sonar-project.properties
      - echo "sonar.scm.provider=git" >> sonar-project.properties
      - echo "sonar.scm.forceReloadAll=true" >> sonar-project.properties
      - echo "sonar.exclusions=docker-compose.yml, output/**/* , test/*, output.xml, lint-output.json" >> sonar-project.properties
      - echo "sonar.log.level=INFO" >> sonar-project.properties

      - echo "sonar.projectKey=${DRONE_REPO//\//:}" >> sonar-project.properties
      - echo "sonar.projectName=${DRONE_REPO}" >> sonar-project.properties
      - echo "sonar.projectVersion=${DRONE_COMMIT_SHA:0:8}" >> sonar-project.properties
      - echo "sonar.links.ci=${DRONE_BUILD_LINK}" >> sonar-project.properties
      - echo "sonar.links.scm=${DRONE_REPO_LINK}" >> sonar-project.properties
      - if [ $DRONE_COMMIT_BRANCH != "master" ] ; then echo "sonar.branch.name=${DRONE_COMMIT_BRANCH}" >> sonar-project.properties ; fi
      - if [ $DRONE_BUILD_EVENT == "pull_request" ] ; then echo "sonar.pullrequest.key=${DRONE_PULL_REQUEST}" >> sonar-project.properties ; fi
      - if [ $DRONE_BUILD_EVENT == "pull_request" ] ; then echo "sonar.pullrequest.branch=${DRONE_SOURCE_BRANCH}" >> sonar-project.properties ; fi
      - if [ $DRONE_BUILD_EVENT == "pull_request" ] ; then echo "sonar.pullrequest.base=master" >> sonar-project.properties ; fi

      - echo "sonar.sources=." >> sonar-project.properties
      - echo "sonar.javascript.lcov.reportPaths=output/coverage/jest/lcov.info" >> sonar-project.properties
      - echo "sonar.testExecutionReportPaths=output/coverage/jest/sonar-report.xml" >> sonar-project.properties
      - echo "sonar.tests=test" >> sonar-project.properties
      - echo "sonar.test.inclusions=test/*" >> sonar-project.properties
      - echo "sonar.eslint.reportPaths=lint-output.json" >> sonar-project.properties

  - name: perform-code-analysis
    image: aosapps/drone-sonar-plugin
    settings:
      showProfiling: true
      enableGateBreaker: true
      timeout: 60
      usingProperties: true
      sonar_host:
        from_secret: SONARQUBE_HOST
      sonar_token:
        from_secret: SONARQUBE_TOKEN

---
# ##
# Language upload and download
# ##

kind: pipeline
type: docker
name: language-source-push

platform:
  os: linux
  arch: amd64

trigger:
  event:
    include:
    - push
    exclude:
    - promote
  branch:
    - master

steps:
  - name: language-source-push
    image: gcr.io/achievers-shd/utilities/gke-achievers-translation-integrator:v1.0.0
    pull: always
    environment:
      LANGUAGE_GITHUB_SSH:
        from_secret: SERVICE_DEPLOY_GITHUB_SSH
      UPSTREAM: BE/connect-language
      SOURCE_DIR: /drone/src/public/language/en-US/
      UPLOAD_PATH: ./en-US/connect_msteams/
      APP_NAME: connect_msteams
      BRANCH_NAME: ${DRONE_BRANCH}
      COMMIT_ID: ${DRONE_COMMIT_SHA:0:8}
      GITHUB_HOST: github.corp.achievers.com
      GITHUB_USER:
        from_secret: DOCKER_USERNAME
      GITHUB_PASSWORD:
        from_secret: DOCKER_PASSWORD
    commands:
      - echo ${DRONE_COMMIT_AUTHOR}
      - echo ${DRONE_COMMIT_AUTHOR_EMAIL}
      - bash /entrypoint.sh create_pull_request

image_pull_secrets:
 - DOCKER_GCR_AUTHENTICATION
---
# ##
# Build Pipeline
#  - build master-branch containers
#  - store in a registry
# ##

kind: pipeline
type: docker
name: build_master

platform:
  os: linux
  arch: amd64

depends_on:
  - test-master-branch

trigger:
  event:
    include:
    - push
    exclude:
    - promote
  branch:
    - master
    
   
steps:
  - name: image-build-gcr
    image: plugins/gcr
    settings:
      registry: gcr.io
      repo: gcr.io/achievers-shd/connect-apps/integrations/msteams
      json_key:
        from_secret: DRONE_GCR_WRITER_GSA
      context: .
      dockerfile: ./Dockerfile
      tags:
        - "${DRONE_COMMIT_SHA:0:8}"
        - "latest"
      force_tag: true
      use_cache: true
      purge: false
      custom_dns: 169.254.169.254
     
---
# ##
# Deploy Pipeline
#  - build master-deploy GCP
#  - build
# ##

kind: pipeline
type: docker
name: deploy-retag-stg

clone:
  disable: true

trigger:
  status:
    # Only runs if the first pipeline was fully successful
    - success
  event:
    include:
    - push
    exclude:
    - promote
  branch:
    - master
    
       
depends_on:
  - build_master

steps:
  - name: promote-tag-dev-na-ne1-dev-cow-01
    image: gcr.io/achievers-shd/utilities/gke-achievers-tag-updater:v0.0.6
    pull: always
    environment:
      CLUSTER_NAME: na-ne1-dev-cow-01
      APP_NAME: connect-msteams
      APP_ENV: stg
      TAG: ${DRONE_COMMIT_SHA:0:8}
      USERNAME:
        from_secret: DOCKER_USERNAME
      PASSWORD:
        from_secret: DOCKER_PASSWORD
    commands:
       - sh /entrypoint.sh update_git_tag
image_pull_secrets:
 - DOCKER_GCR_AUTHENTICATION 
 
---
kind: pipeline
type: docker
name: deploy-retag-uat


trigger:
  event:
  - promote
  target:
  - UAT

clone:
  disable: true

steps:
  - name: update-tag-uat
    image: gcr.io/achievers-shd/utilities/gke-achievers-pr-creator:v1.0.0
    pull: always
    environment:
      SERVICE_DEPLOY_GITHUB_SSH:
        from_secret: SERVICE_DEPLOY_GITHUB_SSH
      UPSTREAM: DevOps/abattoir-deployments:UAT
      APP_NAME: connect-msteams
      BRANCH_NAME: UAT
      COMMIT_ID: ${DRONE_COMMIT_SHA:0:8}
      GITHUB_HOST: github.corp.achievers.com
      GITHUB_USER:
        from_secret: DOCKER_USERNAME
      GITHUB_PASSWORD:
        from_secret: DOCKER_PASSWORD
    commands:
      - echo ${DRONE_COMMIT_AUTHOR}
      - echo ${DRONE_COMMIT_AUTHOR_EMAIL}
      - bash /entrypoint.sh create_pull_request
  
  - name: deploy-notify-uat
    image: plugins/webhook
    settings:
      urls: https://achieversllc.webhook.office.com/webhookb2/47020690-a6fc-44b2-adc2-49522ee19ded@8bd0082d-d958-423a-821f-dd0f22a1b519/IncomingWebhook/3788d1b54d3244e7abb7f8cdf7f70267/e4366948-831e-4476-8348-3fe8cc979e7a
      content_type: application/json
      template: |
        {
           "summary":"Deployment",
           "sections":[
              {
                "activityTitle":"<b>Deployment - [ uat ][ {{ repo.name }} - # {{ build.status }} ]</b>",
                "activitySubtitle": "Deployment for {{ repo.name }} - # {{ build.number }}",
                "activityImage": "https://github.com/drone/brand/raw/master/logos/png/dark/drone-logo-png-dark-1024.png"
              },
              {
                 "title":"Details:",
                 "facts":[
                    {
                      "name":"Repo",
                      "value":"{{ repo.name }}"
                    },
                    {
                      "name":"Status",
                      "value":"{{ build.status }}"
                    },
                    {
                      "name":"By",
                      "value":"{{ lowercase build.author }}"
                    },
                    {
                      "name":"Commit ID",
                      "value":"{{ build.commit }}"
                    }
                 ]
              }
           ],
           "potentialAction":[
              {
                 "@context":"http://schema.org",
                 "@type":"ViewAction",
                 "name":"View in Drone",
                 "target":[
                    "{% raw %} {{ build.link }} {% endraw %}"
                 ]
              }
           ]
        }
image_pull_secrets:
  - DOCKER_GCR_AUTHENTICATION

---
kind: pipeline
type: docker
name: deploy-retag-prd


trigger:
  event:
  - promote
  target:
  - PRD

clone:
  disable: true

steps:
  - name: update-tag-prd
    image: gcr.io/achievers-shd/utilities/gke-achievers-pr-creator:v1.0.0
    pull: always
    environment:
      SERVICE_DEPLOY_GITHUB_SSH:
        from_secret: SERVICE_DEPLOY_GITHUB_SSH
      UPSTREAM: DevOps/abattoir-deployments:PRD
      APP_NAME: connect-msteams
      BRANCH_NAME: PRD
      COMMIT_ID: ${DRONE_COMMIT_SHA:0:8}
      GITHUB_HOST: github.corp.achievers.com
      GITHUB_USER:
        from_secret: DOCKER_USERNAME
      GITHUB_PASSWORD:
        from_secret: DOCKER_PASSWORD
    commands:
      - bash /entrypoint.sh create_pull_request
  - name: deploy-notify-prd
    image: plugins/webhook
    settings:
      urls: https://achieversllc.webhook.office.com/webhookb2/47020690-a6fc-44b2-adc2-49522ee19ded@8bd0082d-d958-423a-821f-dd0f22a1b519/IncomingWebhook/766ff5de6f85470a8cac3fba8192c2a9/e4366948-831e-4476-8348-3fe8cc979e7a
      content_type: application/json
      template: |
        {
           "summary":"Deployment",
           "sections":[
              {
                "activityTitle":"<b>Deployment - [ PRD ][ {{ repo.name }} - # {{ build.status }} ]</b>",
                "activitySubtitle": "Deployment for {{ repo.name }} - # {{ build.number }}",
                "activityImage": "https://github.com/drone/brand/raw/master/logos/png/dark/drone-logo-png-dark-1024.png"
              },
              {
                 "title":"Details:",
                 "facts":[
                    {
                      "name":"Repo",
                      "value":"{{ repo.name }}"
                    },
                    {
                      "name":"Status",
                      "value":"{{ build.status }}"
                    },
                    {
                      "name":"By",
                      "value":"{{ lowercase build.author }}"
                    },
                    {
                      "name":"Commit ID",
                      "value":"{{ build.commit }}"
                    }
                 ]
              }
           ],
           "potentialAction":[
              {
                 "@context":"http://schema.org",
                 "@type":"ViewAction",
                 "name":"View in Drone",
                 "target":[
                    "{% raw %} {{ build.link }} {% endraw %}"
                 ]
              }
           ]
        }
image_pull_secrets:
 - DOCKER_GCR_AUTHENTICATION
