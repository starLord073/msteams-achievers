var global = {};
var recogObj = {};

var POINTSDIV = "#pointsDiv";
var POINTSDIVDROPDOWN = "#selectPointsDivDropdown";
var POINTSDIVRADIO =  "#selectPointsDivRadio";
var POINTSDIVTEXTBOX = "#selectPointsDivTextbox";
var POINTBASED = "POINT-BASED";
var POINTSPENT = "#pointSpent";
var POINTBALANCE = "#pointsbalance";


microsoftTeams.initialize();
var newSize = {
    height: 'large', width: 'large'
}
microsoftTeams.tasks.updateTask(newSize);
$(document).ready(function () {
    let queryParams = getUrlVars();
    if (queryParams['sourceName']) {
        pgDetails.source = queryParams['sourceName'];
    }

    var supportedLangs = ["en-US", "en-AU", "en-GB", "es-MX", "fr-CA", "fr-FR", "zh-CN"];
    // var supportedLangs = ["en-US"]
    if (supportedLangs.includes(queryParams.lang)) {
        queryParams.lang = queryParams.lang
        pgDetails.lang = queryParams["lang"];
    }
    else {
        queryParams.lang = "en-US"
    }
    global.userData = {
        nomineesSelected: [],
        recType: '',
        recVal: '',
        criterion: '',
        recogText: '',
        pointval: '',
        sharedNomineesSelected: [],
        notifyManagers: '',
        localizedText: getTranslatedStrings(queryParams),
        fallbackStrings: getTranslatedStrings(queryParams, true)

    }

    // If you want ot execute a script in onReady of recognition flow
    // Add in this if statement
    if(!pgDetails.source || pgDetails.source != "homeTab"){
        // $(".col-sm-73").hide();
        var Points_sliding = pgDetails.configFile.Points_sliding_scale
        var programsEnabled = Points_sliding["programPkEnabled"];
        var programsNotEnabled = Points_sliding["programPkNotEnabled"];
        var usersEnabled = Points_sliding["userPkEnabled"]
        if(programsNotEnabled.indexOf(queryParams.programPk) > -1){
            showPointsScaleTab = false;
        }else if(programsEnabled.indexOf(queryParams.programPk) > -1 || programsEnabled.indexOf("all") > -1){
            showPointsScaleTab = true;
        }
        if (usersEnabled.indexOf(queryParams.userPk) > -1) {
            showPointsScaleTab = true;
        }
        // console.log(queryParams["subDomain"]);
        if (pgDetails.source != "recogSuggestion") {
            if (parseInt(queryParams["channelSize"]) > 50) {
                if (queryParams["conversationType"] == "channel") {
                    document.getElementsByClassName("tooltiptext")[0].innerHTML = "This feature is not available for channels larger than 50 members."
                } else if (queryParams["conversationType"] == "groupChat") {
                    document.getElementsByClassName("tooltiptext")[0].innerHTML = "This feature is not available for group chats larger than 50 members."
                }
                document.getElementsByClassName("tooltiptext")[0].style.transform = "translateY(-8%)";
                document.getElementById("btn_addAll").disabled = true;
            }
            if (queryParams["sourceName"] == "powerbar" || queryParams["conversationType"] == "personal") {
                $(".col-sm-73").hide();
            } else if (queryParams["conversationType"] == "groupChat") {
                var addAllBtn = document.getElementById("btn_addAll")
                addAllBtn.innerHTML = addAllBtn.innerHTML.replace(global.userData.localizedText["Add_channel_members"], global.userData.localizedText["Add_chat_members"])
                $(".col-sm-73").show();
            } else if (queryParams["conversationType"] == "channel") {
                var addAllBtn = document.getElementById("btn_addAll")
                addAllBtn.innerHTML = addAllBtn.innerHTML.replace(global.userData.localizedText["Add_channel_members"], global.userData.localizedText["Add_channel_members"])
                $(".col-sm-73").show();
            }
            pgDetails.platform = queryParams["platform"].toLowerCase();
            if (pgDetails.platform == "android" || pgDetails.platform == "ios") {
                document.getElementsByClassName("tooltiptext")[0].style.display = "none";
            }
            pgDetails.conversationId = queryParams["conversationId"];

            // adding scope to pgDetails for pendo
            pgDetails.scope = queryParams.conversationType === "groupChat" ? "chat" : (queryParams.conversationType != "undefined" ? queryParams.conversationType : (queryParams.sourceName === "powerbar" ? "searchBar" : queryParams.sourceName))
        }
        // load(false);


        pgDetails.userToken = queryParams["state"];
        pgDetails.userFullName = decodeURI(queryParams["userName"]);
        pgDetails.serviceUrl = decodeURIComponent(decodeURI(queryParams["serviceUrl"]));




        // adding scope programpk, userpk  to pgDetails for pendo
        pgDetails.programPk = queryParams.programPk
        pgDetails.userPk = queryParams.userPk

        viewPage("userDetails");

        $("#selRtype").selectric({
            disableOnMobile: false,
            nativeOnMobile: false,
            onChange: function () {
                $("#inpRmessage").val("");
                clearAllWarnings();
                populateRecogValues();
                getPoints();
                var selectedRtypeObject = JSON.parse($("#selRtype").val());
                // points page
                pgDetails.showPointsUIOnly = false;

                for (let module of global.currentRecogData.items) {
                    if (module.id == selectedRtypeObject.id) {
                        global.userData.notifyManagers = module.notifyReportsTo;
                        break;
                    }
                }
                if(global.userData.notifyManagers){
                $('#shareCheck').prop('checked', true);
                $('#shareCheck').prop("disabled",true);
                }
                global.userData.recognitionTypeId = selectedRtypeObject.id;
                var budgetObj = pgDetails.memberBudget && pgDetails.memberBudget.recognitionBudgets[global.userData.recognitionTypeId];
                if (budgetObj && budgetObj.budgetRemaining && budgetObj.moduleType == POINTBASED) {
                $(POINTBALANCE).html(global.userData.localizedText["Points_available_to_award"] + "<b>" + getLocalizedNumber(parseInt(budgetObj.budgetRemaining.points)) + "</b>");
                $(POINTBALANCE).show();
                global.userData.budgetPoints = budgetObj.budgetRemaining.points;
                } else {
                $(POINTBALANCE).text('');
                $(POINTBALANCE).hide();
                $(POINTSPENT).html('');
                $(POINTSPENT).hide();
                }
            }
        });
        $("#selRval").selectric({
            disableOnMobile: false,
            nativeOnMobile: false,
            onChange: function () {
                $("#inpRmessage").val("");
                clearAllWarnings();
                global.userData.recVal = $("#selRval").val();
                if(!showPointsScaleTab){
                    global.userData.pointval = getRecogPointVal(global.userData.recVal);
                    global.userData.pointForRecogPage = getRecogPointVal(global.userData.recVal);
                } else {
                    global.userData.pointval = getRecogPointVal(global.userData.recVal);
                    global.userData.pointForRecogPage = getRecogPointVal(global.userData.recVal);

                }
                // global.userData.pointval = getRecogPointVal(global.userData.recVal);
                global.userData.criterion = $("#selRval  option:selected").text();
                setDefaultImage();
                // if(showPointsScaleTab){
                    calPointsBasedOnSlctdOptn();
                // }
                var nomineesSelectedLength = global.userData.nomineesSelected.length
                if(showPointsScaleTab &&
                    ["Textbox","Radio","Dropdown"].indexOf(pgDetails.pointsOptionsToDisplay.displayOptionsSty)>-1
                    && !pgDetails.pointsOptionsToDisplay.valuesStatic){
                    var pointsRange = getLocalizedNumber(pgDetails.pointsOptionsToDisplay.minRange) + " - " +
                    getLocalizedNumber(pgDetails.pointsOptionsToDisplay.maxRange)
                    var html = getTranslation("Points_per_recipient") + "<b>" + pointsRange + "</b>";
                    $(POINTSPENT).html(html);
                    $(POINTSPENT).show();
                }else{
                    if(pgDetails.pointsOptionsToDisplay && pgDetails.pointsOptionsToDisplay.selectedModuleType === POINTBASED){
                        var pointSpent = nomineesSelectedLength * global.userData.pointForRecogPage;
                        var pointForRecog =  global.userData.pointForRecogPage;
                        if(pgDetails.pointsOptionsToDisplay && pgDetails.pointsOptionsToDisplay.showOnlySingleValue){
                        pointSpent = nomineesSelectedLength * pgDetails.pointsOptionsToDisplay.minRange
                        pointForRecog = pgDetails.pointsOptionsToDisplay.minRange
                        }
                        var html = global.userData.localizedText["Total_points_you_are_awarding"] + "<b>" + getLocalizedNumber(parseInt(pointSpent)) + "</b>";
                        if (nomineesSelectedLength > 1) {
                        html += '<br><span style="float: right;"> (' + nomineesSelectedLength+" "+global.userData.localizedText["people"]+" x "+ pointForRecog + ")</span>";
                        }
                    } else {
                        var pointSpent = 0;
                    }
                }
                // let pointSpent = global.userData.nomineesSelected.length * global.userData.pointval;

                // let html = global.userData.localizedText["Total_points_you_are_awarding"] + '<b>' + pointSpent + '</b>';

                // if (global.userData.nomineesSelected.length > 1) {
                //     html += '<br><span style="float: right;"> (' + global.userData.nomineesSelected.length + ' ' + global.userData.localizedText["people"] + ' x ' + global.userData.pointval + ')</span>'
                // }
                if (pointSpent > 0 && pgDetails.pointsOptionsToDisplay.selectedModuleType === POINTBASED) {
                    $("#pointSpent").html(html);
                    $("#pointSpent").show();
                }
            }
        });

        $("#searchGifs").keyup(function (e) {
            setTimeout(imageSearch(e.target.value), 200);
        });

        $("#btn_Target2").click(function () {

            if (pageOneValid()) {
                viewPage("recogDetails");
            }
        });

        $("#btn_Target3").click(function () {
            if (pageOneValid()) {
                showHideShareMgr();
                return false;
            }
        });

        $("#inpRmessage").focus(clearAllWarnings());
        $("input[type=text]").focus(clearAllWarnings());
        $("select").focus(clearAllWarnings());
        $("#btn_rtNext").click(function () {
            var selectedRecognitionType = $('#selRtype :selected').text();
            var enableImages = global.currentRecogData.items;
            var enableImagesBoolean;
            for (let i = 0; i < enableImages.length; i++) {
                if (enableImages[i].name === selectedRecognitionType) {
                    enableImagesBoolean = enableImages[i];
                }
            }
            if (enableImagesBoolean.enableGiphy) {
                $("#gif-tab").show();
            }
            else {
                $("#gif-tab").hide();
            }
            if (enableImagesBoolean.enablePhotoUpload) {
                $("#Upload-tab").show();
            }
            else {
                $("#Upload-tab").hide();
                $("#gif-tab").hide();
            }

            global.userData.recogText = $("#inpRmessage").val();
            if (global.userData.recogText.length > 0) {
                //form preview
                $("#recogTypePlaceholder").text('"' + global.userData.criterion + '"');
                $("#previewRecogNominees").text(getCommaSeparatedString(getFullNames(global.userData.nomineesSelected)));
                $("#previewRecogMessage").text(global.userData.recogText);
                $("#previewRecogFrom").text(" " + pgDetails.userFullName);
                viewPage("imageSelection");
                imageSearch('');
            }
            else {
                alertWarningRecog(global.userData.localizedText["You_must_enter_a_recognition_message"]);
            }
        });



        $("#btn_rt_Back").click(function () {
            viewPage("userDetails");
        })

        $("#btn_Submit_Back").click(function () {
            // viewPage("shareRecognition");
            // return false;
            if(showPointsScaleTab && pgDetails.pointsOptionsToDisplay.selectedModuleType === POINTBASED){
                // points page
                if(pgDetails.showPointsUIOnly){
                    viewPage('pointsDiv');
                }else{
                    showPoints();
                }
            }else{
                viewPage('shareRecognition');
            }
        })

        $("#btn_Submit").click(function () {
            postRecognition(microsoftTeams);
        })

        $("#btn_Preview").click(function () {
            global.userData.recogText = $("#inpRmessage").val();
            if (global.userData.recogText.length > 0) {
                $("#userDetails").hide();
                $("#recogDetails").hide();
                $("#logoutnotification").hide();
                //form preview
                $("#recogTypePlaceholder").text('"' + global.userData.criterion + '"');
                $("#previewRecogNominees").text(getCommaSeparatedString(getFullNames(global.userData.nomineesSelected)));
                $("#previewRecogMessage").text(global.userData.recogText);
                $("#previewRecogFrom").text(" " + pgDetails.userFullName);
                $("#previewRecog").show();
                $("#shareRecognition").hide();
            }
            else {
                alertWarningRecog(global.userData.localizedText["You_must_enter_a_recognition_message"]);
            }
        })

        $("#to").keyup(function (e) {
            setTimeout(getSearchUserData(e.target.value), 500);
        });

        $("#to").focus(clearAllWarnings());

        $("#shareRecognition").keyup(function (e) {
            setTimeout(getshareRecognition(e.target.value), 500);
        });

        $("#shareRecognition").focus(clearAllWarnings());

        if (typeof getSearchUserData === 'function' === false) {
            alertWarning(global.userData.localizedText['The_Achievers_app_is_not_responding_Close_the_app_and_start_again']);
        }

        if (typeof getshareRecognition === 'function' === false) {
            alertWarning(global.userData.localizedText['The_Achievers_app_is_not_responding_Close_the_app_and_start_again']);
        }

        $("#inpRmessage").keydown(function (e) {
            if ($("#inpRmessage").val().length == 65000 && event.keyCode == 8) {
                alertWarningRecog("");
            }
            else if ($("#inpRmessage").val().length >= 65000) {
                var inp = String.fromCharCode(event.keyCode);
                if (/[a-zA-Z0-9-_ ,.'"]/.test(inp)) {
                    e.preventDefault();
                    alertWarningRecog(global.userData.localizedText["Recognition_message_cannot_exceed_65000_characters"]);
                }
            }
            else {
                alertWarningRecog("");
            }
        })
        changeHTML("popuptab");

        // points page

        let time = 0;

        $("#pointsTextbox").keypress(function(e) {
            if (e.which < 48 || e.which > 57) {
                return(false);
            }
        });

        $('#pointsTextbox').keyup(function (ele) {

            var ma = pgDetails.pointsOptionsToDisplay.maxRange;
            var mi = pgDetails.pointsOptionsToDisplay.minRange;
            var nomineesSltdLength = pgDetails.pointsOptionsToDisplay.nomineesSelectedLength;
            var enteredVal =  ele.target.value;
            // Reset the timer
            clearTimeout(time);

            time = setTimeout(function() {
            $('#pointsTextbox').attr('max',ma);
            // enter code here or a execute function.
            if (enteredVal.indexOf('.') === -1) {
                if (enteredVal < mi ||  enteredVal == 0){
                enteredVal="";
                global.userData.pointval = enteredVal;
                $('#pointsTextbox').val("");
                }
                if (enteredVal > ma){
                enteredVal=ma
                $('#pointsTextbox').val(ma);
                }
                if (enteredVal < mi){
                    enteredVal=mi
                    $('#pointsTextbox').val(mi);
                }
                enteredVal = parseInt(enteredVal);
                document.getElementById('awardedPoints').innerHTML="";
                document.getElementById('calPoints').innerHTML="";
                clearAllWarnings();
                if(enteredVal >= mi && enteredVal <= ma){
                $('#awardedPoints').append( global.userData.localizedText["Total_points_you_are_awarding"]  + "<b>" + getLocalizedNumber(nomineesSltdLength * enteredVal)+ "</b>");
                $('#calPoints').append("("+nomineesSltdLength+" recipients x " + "<b>" + getLocalizedNumber(enteredVal) +" points" + "</b>"+")");
                global.userData.pointval = calPointsValue(enteredVal);

                }else if (mi == 0 && ma == 0){
                clearAllWarnings();
                $('#awardedPoints').append( global.userData.localizedText["Total_points_you_are_awarding"]  + "<b>" + getLocalizedNumber(nomineesSltdLength * enteredVal)+ "</b>");
                $('#calPoints').append("("+nomineesSltdLength+" "+getTranslation("recipients") +" x " + "<b>" + getLocalizedNumber(enteredVal) +" points" + "</b>"+")");
                global.userData.pointval = calPointsValue(enteredVal);
                }else{
                    $('#awardedPoints').append( global.userData.localizedText["Total_points_you_are_awarding"]  + "<b>" +" -" + "</b>");
                    $('#calPoints').append("("+nomineesSltdLength+" "+getTranslation("recipients") +" x " + "<b>" +"0 points" + "</b>"+")");
                }
            }
            }, 800);
        })


        // points page
        $(POINTSDIVRADIO).change(function (eve){
            clearAllWarnings();
            global.userData.pointval = calPointsValue(eve.target.value);
        })

        // points page
        $("#selectPointsType").change(function (eve){
            document.getElementById('calPointsDropdown').innerHTML="";
            var selectedPointsBudget = eve.target.value;
            clearAllWarnings();
            var nomineesSltdLength = pgDetails.pointsOptionsToDisplay.nomineesSelectedLength;
            // $("#calPointsDropdown").append(nomineesSltdLength+" "+pgDetails.langData["recipients"]+" = <b>"+
            // selectedPointsBudget * nomineesSltdLength+" points </b>");
            var calPointsDropdownData = getTranslation("NUMBER_recipients",[nomineesSltdLength]) +
            " = <b>"+ getTranslation("POINTS_points",[getLocalizedNumber(selectedPointsBudget * nomineesSltdLength)])+"</b>";
            $("#calPointsDropdown").append(calPointsDropdownData)
            global.userData.pointval = calPointsValue(selectedPointsBudget);
        })
        if (queryParams['userEmailForAdding']) {
            searchAndAddAll(queryParams['userEmailForAdding']);
        }
    } else if (pgDetails.source && pgDetails.source == "homeTab") {
        // Add scripts that you need for on Ready of static tab page.
        getgrpcNewsfeedInitial();

        if (pgDetails.accessTokenValid) {
            const recogSuggestionsAllowed = pgDetails.configFile.Show_recognition_suggestions.programPkEnabled;
            const recogSuggestionsNotAllowed = pgDetails.configFile.Show_recognition_suggestions.programPkNotEnabled;
            const celebrationSuggestionAllowed = pgDetails.configFile.Show_celebration_suggestions.programPkEnabled;
            const celebrationSuggestionNotAllowed = pgDetails.configFile.Show_celebration_suggestions.programPkNotEnabled;
            const hideCelebrations = getUpcomingCelebrationConfig();

            if (((recogSuggestionsAllowed.indexOf(pgDetails.achieverApplicationID) > -1 || recogSuggestionsAllowed.indexOf("all") > -1) && recogSuggestionsNotAllowed.indexOf(pgDetails.achieverApplicationID) == -1)) {
                fetchRecognitionSuggestions();
            } else {
                document.getElementsByClassName("recognitionSuggestions")[0].style.display = "none";
                document.getElementsByClassName("Get_recognition_suggestions")[0].style.display = "none";
            }
            if ((celebrationSuggestionAllowed.indexOf(pgDetails.achieverApplicationID) > -1 || celebrationSuggestionAllowed.indexOf("all") > -1) && celebrationSuggestionNotAllowed.indexOf(pgDetails.achieverApplicationID) == -1 && !hideCelebrations) {
                fetchCelebrations();
            } else {
                document.getElementsByClassName("celebrationsnudgesList")[0].style.display = "none";
            }
        }
        changeHTML("hometab");
    }
})

function addEvent(evnt, elem, func) {
    if (elem.addEventListener)  // W3C DOM
        elem.addEventListener(evnt, func, false);
    else if (elem.attachEvent) { // IE DOM
        elem.attachEvent("on" + evnt, func);
    }
    else { // No much to do
        elem["on" + evnt] = func;
    }
}

function closeAllLists() {
    $("#autocomplete-list").remove();
}

function deletePanel(el) {
    load(true);
    var divel = $(el).parent();
    var uId = divel.attr('id');

    $(divel).remove();
    var nom = global.userData.nomineesSelected;
    for (var i = 0; i < nom.length; i++) {
        if (nom[i].userId == uId) {
            nom.splice(i, 1);
        }
    }
    if (global.userData.nomineesSelected.length > 0) {
        recognitionTypes();
        $("#pointsbalance").hide();
        $("#pointSpent").hide();
    }
    else {
        clearRecogTypes();
        $("#pointsbalance").hide();
        $("#pointSpent").hide();
        $("#btn_removeAll").hide();
    }
    load(false);
}

function deleteSharedPanel(el) {
    load(true);
    var divel = $(el).parent();
    var uId = divel.attr('id');

    $(divel).remove();
    var delSharedNomineesSelected = global.userData.sharedNomineesSelected;
    for (var i = 0; i < delSharedNomineesSelected.length; i++) {
        if (delSharedNomineesSelected[i].userId == uId) {
            delSharedNomineesSelected.splice(i, 1);
        }
    }
    if (delSharedNomineesSelected.length === 0) {
        $('#removeSharedNominees').hide();
    }
    load(false);
}


function populateRecogValues() {
    load(true);
    var slctdRtypeObject = JSON.parse($("#selRtype").val());
    // For points screen
    global.userData.selectedModuleType = slctdRtypeObject.moduleType;
    global.userData.recType = slctdRtypeObject.id;
    clearRecogValues();
    var curVal = slctdRtypeObject.id;
    global.mainCriteria = [];
    for (var i = 0; i < global.currentRecogData.items.length; i++) {
        if (global.currentRecogData.items[i].id == curVal) {
            global.mainCriteria = global.currentRecogData.items[i].criteria;
            let orderByCriteriaSequence = global.mainCriteria.slice(0);
            orderByCriteriaSequence.sort(function(a,b) {
                return parseInt(a.criteriaSequence) - parseInt(b.criteriaSequence);
            });
            global.mainCriteria = orderByCriteriaSequence;
        }
    }
    $("#selRval").append("<option value= '0' selected=true> " + global.userData.localizedText["Select"] + "</option>")
    if (global.mainCriteria.length > 0) {
        for (i = 0; i < global.mainCriteria.length; i++) {
            var option = "<option value='" + global.mainCriteria[i].id + "'>" + global.mainCriteria[i].name + "</option>";
            $("#selRval").append(option);
        }
        $("#selRval").selectric('refresh');
        $('#selRval').selectric('init');
    }
    load(false);
}

function clearRecogTypes() {
    $("#selRtype").empty();
    global.userData.recType = '';
    $("#selRtype").selectric('refresh');
    $('#selRtype').selectric('init');
    $('#pointsbalance').hide();
    $('#pointSpent').hide();
    $('#errorMessage').empty();
    clearRecogValues();

}
function clearRecogValues() {
    $("#selRval").empty();
    global.userData.recVal = '';
    global.userData.criterion = '';
    $("#selRval").selectric('refresh');
    $('#selRval').selectric('init');
}

function alertWarning(msg) {
    $("#errorMessage").html(msg);
}

function alertWarningSubmit(msg) {
    $("#errorMessageSubmit").html(msg);
}

function alertWarningRecog(msg) {
    $("#errorMessageRecog").html(msg);
}
function alertWarningImg(msg) {
    $("#errorMessageImage").html(msg);
}

function pageOneValid() {
    if (global.userData.nomineesSelected.length == 0) {
        alertWarning(global.userData.localizedText["You_must_select_at_least_one_recipient"]);
        return false;
    }
    if (global.currentRecogData.items.length == 0) {
        alertWarning(global.userData.localizedText["This_recognition_cannot_be_created_Someone_you_selected_is_not_eligible_to_receive_a_recognition"]);
        return false;
    }
    if (!global.userData.recType) {
        alertWarning(global.userData.localizedText["A_Recognition_type_is_required"]);
        return false;
    }
    else if (global.userData.recType == '0' | global.userData.recType == 0) {
        alertWarning(global.userData.localizedText["A_Recognition_type_is_required"]);
        return false;
    }
    if (!global.userData.recVal) {
        alertWarning(global.userData.localizedText["A_Recognition_value_is_required"]);
        return false;
    }
    else if (global.userData.recVal == '0' | global.userData.recVal == 0) {
        alertWarning(global.userData.localizedText["A_Recognition_value_is_required"]);
        return false;
    }
    // if ((global.userData.nomineesSelected.length * global.userData.pointval) > global.userData.budgetPoints) {
    //     alertWarning(global.userData.localizedText["You_do_not_have_enough_points_available_to_award"]);
    //     return false;
    // }

    if(showPointsScaleTab){
        var nomineesSelectedLength = global.userData.nomineesSelected.length;
        if (nomineesSelectedLength * global.userData.pointForRecogPage > global.userData.budgetPoints) {
            alertWarning(global.userData.localizedText["You_do_not_have_enough_points_available_to_award"]);
          return false;
        }

        if(global.userData.budgetPoints < nomineesSelectedLength * pgDetails.pointsOptionsToDisplay.minRange){
            alertWarning(global.userData.localizedText["You_do_not_have_enough_points_available_to_award"]);
          return false;
        }
    }else{
        if ((global.userData.nomineesSelected.length * global.userData.pointval) > global.userData.budgetPoints) {
            alertWarning(global.userData.localizedText["You_do_not_have_enough_points_available_to_award"]);
            return false;
        }
    }
    return true;
}

function checkUserPresence(arr, val) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i].userId == val) {
            return true;
        }
    }
    return false;
}

function getUserIds(arr) {
    var resArr = [];
    for (i = 0; i < arr.length; i++) {
        resArr.push(arr[i].userId);
    }
    return resArr;
}

function getUserNames(arr) {
    var resArr = [];
    for (i = 0; i < arr.length; i++) {
        resArr.push(arr[i].userName);
    }
    return resArr;
}

function getFullNames(arr) {
    var resArr = [];
    for (i = 0; i < arr.length; i++) {
        resArr.push(arr[i].userFullName);
    }
    return resArr;
}

function getNomineeEmails(arr) {
    var resArr = [];
    for (i = 0; i < arr.length; i++) {
        resArr.push(arr[i].userEmail);
    }
    return resArr;
}

function alertSuccess(msg) {
    $("#successAlertMsg").text(msg);
    $("#successAlert").slideDown().delay(6000).slideUp();
}

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function cleanImageUri(uri) {
    var clean_uri = ''
    if (uri.indexOf("?") > 0) {
        clean_uri = uri.substring(0, uri.indexOf("?"));
    }
    return clean_uri;
}

function getCommaSeparatedString(arr) {
    var retString = '';
    if (arr.length > 0) {
        for (i = 0; i < arr.length; i++) {
            if (i == 0) {
                retString = arr[i];
            }
            else if (i == arr.length - 1) {
                retString = retString + ' ' + global.userData.localizedText['and'] + ' ' + arr[i];
            }
            else if (i > 0) {
                retString = retString + ', ' + arr[i];
            }

        }
    }
    return retString;
}

function load(changeSwitch) {
    if (changeSwitch) {
        $("#loader").show();
    }
    else {
        $("#loader").hide();
    }
}

function removeElement(id) {
    if ($('#' + id).length) {
        $('#' + id).remove();
    }
}

function alertWarningSubmit(msg) {
    $("#errorMessageSubmit").html(msg);
}

function alertWarningRecog(msg) {
    $("#errorMessageRecog").html(msg);
}

function clearAllWarnings() {
    $("#errorMessageSubmit").text("");
    $("#errorMessageRecog").text("");
    $("#errorMessageImage").text("");
    $("#errorMessage").text("");
    $(".errorMessagePost").text("");
}


function getRecogPointVal(criterionId) {
    var points = 0;

    if (global.mainCriteria && global.mainCriteria.length > 0) {
        for (i = 0; i < global.mainCriteria.length; i++) {
            if (global.mainCriteria[i].id == criterionId) {
                points = global.mainCriteria[i].pointValue;
                pgDetails.recognitionImage = global.mainCriteria[i].criterionBannerUrl;
                // For points screen
                pgDetails.selectedMainCriteria =  global.mainCriteria[i];
            }
        }
    }
    return points;
}

function signout() {
    signoutHandler(false);
}

function expirysignout() {
    signoutHandler(true);
}
function viewPage(pageDivId) {
    load(true);
    $("#userDetails").hide();
    $("#recogDetails").hide();
    $("#previewRecog").hide();
    $("#imageSelection").hide();
    $("#logoutnotification").hide();
    $("#sessionexpirynotification").hide();
    $("#shareRecognition").hide();
    $("#pointsDiv").hide();
    $("#" + pageDivId).show();
    load(false);
}

function setDefaultImage() {
    let element = '<img src="' + pgDetails.recognitionImage + '" class="defaultImage">';

    document.getElementById("defaultImage").innerHTML = element;
}

function gifImgClick(e) {
    pgDetails.imageUrl = e.id;

    $("#gifList").find('div.borderBlue').removeClass('borderBlue');
    $(e).parent().addClass('borderBlue');
    clearAllWarnings();
}

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');

    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }

    return vars;
}

function cleanImageUri(uri) {
    var clean_uri = '';

    if (uri.indexOf("?") > 0) {
        clean_uri = uri.substring(0, uri.indexOf("?"));
    }
    return clean_uri;
}

function getUserObj() {
    var userObj = {
        Title: pgDetails.userFullName,
        userToken: pgDetails.userToken,
        siteUrl: pgDetails.siteUrl
    }
    return JSON.stringify(userObj);
}

//points page

function mapCriteriaValues(criteriaValues,criteriaDefautlValue,nomineesSelectedLength,criteriaMin){
    var options =[];
    for(var i=0; i < criteriaValues.length;i++){
      var pValue = !criteriaMin ? parseInt(criteriaValues[i]) * nomineesSelectedLength : criteriaMin * nomineesSelectedLength
      var criteriaObj={
        id : i ,
        spentBudget : !criteriaMin ? parseInt(criteriaValues[i]): criteriaMin,
        // aboveText : !criteriaMin ? criteriaValues[i] + " points each" : criteriaMin + " points each",
        aboveText : !criteriaMin ?getTranslation("POINTS_points_each",[getLocalizedNumber(criteriaValues[i])]) :
        getTranslation("POINTS_points_each",[getLocalizedNumber(criteriaMin)]) ,
        aboveTextDropdown : !criteriaMin ? getLocalizedNumber(criteriaValues[i]) : getLocalizedNumber(criteriaMin) ,
        // beneathText : nomineesSelectedLength +" recipients" + " = "+ pValue  +" points",
        beneathText : getTranslation("NUMBER_recipients",[nomineesSelectedLength]) +
        " = "+ getTranslation("POINTS_points",[getLocalizedNumber(pValue)]),
        defaultValue : (criteriaDefautlValue === false) ? false : i == criteriaDefautlValue,
      }
      options.push(criteriaObj);
    }
    return options;
  }

// points page
function calPointsBasedOnSlctdOptn(){
    var pointsObj = {};
    var criteriaValues = pgDetails.selectedMainCriteria.pointValueInfo.values;
    var criteriaMin = parseInt(pgDetails.selectedMainCriteria.pointValueInfo.min);
    var criteriaMax = parseInt(pgDetails.selectedMainCriteria.pointValueInfo.max);
    var criteriaPointValue = parseInt(pgDetails.selectedMainCriteria.pointValue);
    var criteriaDefautlValue = pgDetails.selectedMainCriteria.pointValueInfo.defaultValue;
    var nomineesSelectedLength = global.userData.nomineesSelected.length;
    var totalPointsAvailable = pgDetails.memberBudget.totalPointsAvailable;
    pointsObj.selectedModuleType = global.userData.selectedModuleType;
    pointsObj.nomineesSelectedLength = nomineesSelectedLength;
    pointsObj.valuesStatic = false;

    if(pointsObj.selectedModuleType !== POINTBASED){
      pgDetails.pointsOptionsToDisplay = pointsObj;
    }else{
      if(criteriaValues.length > 1){
        var displayOptionsSty = criteriaValues.length > 5 ?"Dropdown":"Radio";
        pointsObj.displayOptionsSty = displayOptionsSty ;
        pointsObj.minRange = criteriaMin;
        pointsObj.maxRange = criteriaMax;
        pointsObj.options = mapCriteriaValues(criteriaValues, criteriaDefautlValue,nomineesSelectedLength,false);

      } else {
        pointsObj.displayOptionsSty = "Textbox";
        var totalPointDivNomiselectd = Math.floor(totalPointsAvailable/nomineesSelectedLength);
        if(criteriaMin === 0 && criteriaMax === 0){
          pointsObj.minRange = 0;
          pointsObj.maxRange = totalPointDivNomiselectd;
        }else if(criteriaMin <  criteriaMax){
          const criteriaMaxMulBudget = (criteriaMax*nomineesSelectedLength);
          if(criteriaMaxMulBudget < totalPointsAvailable){
            pointsObj.minRange = criteriaMin;
            pointsObj.maxRange = criteriaMax;
          }else if(criteriaMaxMulBudget > totalPointsAvailable){

            if(totalPointDivNomiselectd == criteriaMin){
              pointsObj.displayOptionsSty = "Radio";
              pointsObj.minRange = criteriaMin;
              pointsObj.maxRange = criteriaMax;
              pointsObj.valuesStatic = true;
              pointsObj.showOnlySingleValue = true
              pointsObj.options = mapCriteriaValues([criteriaMin], false, nomineesSelectedLength,false);
            }else{
              if(totalPointDivNomiselectd < criteriaMin){
                pointsObj.minRange = criteriaMin;
                pointsObj.maxRange = criteriaMax;
              }else{
                pointsObj.minRange = criteriaMin;
                pointsObj.maxRange = totalPointDivNomiselectd;
              }
            }
          }
        }else if(criteriaMin == criteriaMax ){
          if(criteriaMin == criteriaPointValue && criteriaMax == criteriaPointValue){
            pointsObj.displayOptionsSty = "Radio";
            pointsObj.minRange = criteriaMin;
            pointsObj.maxRange = criteriaMax;
            pointsObj.valuesStatic = true;
            pointsObj.options = mapCriteriaValues(criteriaValues, criteriaDefautlValue, nomineesSelectedLength,false);
          }
          if(criteriaMin !== criteriaPointValue && criteriaMax !== criteriaPointValue){
            pointsObj.displayOptionsSty = "Radio";
            pointsObj.minRange = criteriaMin;
            pointsObj.maxRange = criteriaMax;
            pointsObj.valuesStatic = true;
            pointsObj.options = mapCriteriaValues(criteriaValues, criteriaDefautlValue, nomineesSelectedLength,criteriaMin);

          }
        }
      }
      pgDetails.pointsOptionsToDisplay = pointsObj;
    }

  }


  // points page

function pagePointsValid(){
    if (!global.userData.pointval || global.userData.pointval == "0") {
      alertWarningSubmit(global.userData.localizedText["Your_recognition_point_value_is_invalid"]);
      return false;
    }
    if (global.userData.pointval * global.userData.nomineesSelected.length > parseInt(global.userData.budgetPoints)) {
      alertWarningSubmit(global.userData.localizedText["You_do_not_have_enough_points_available_to_award"]);
      return false;
    }

    return true;
  }


// Create Recognition
function createRecognitionFromNewsFeed() {
    const URL = `${pgDetails.baseAppUrl}/views/viewRecognizePage/?state=${pgDetails.token}&lang=en-US&serviceUrl=${encodeURIComponent(pgDetails.msTeamBotServiceUrl)}&subDomain=${pgDetails.subDomain}&userName=${pgDetails.userName}&userPk=${pgDetails.achieverMemberId}&programPk=${pgDetails.achieverApplicationID}&sourceName=recogSuggestion`
    pgDetails.source = "recogSuggestion";
    let taskInfo = {
        title: "Achievers Recognition",
        height: "Large",
        width: "Large",
        url: URL
    };
    let submitHandler = (err, result) => {
        if (pgDetails.source == "recogSuggestion" && err == null) {
            pageNumber = 0;
            showMoreActivities()
        }

    };
    // tracking an event in pendo when recognition has been started in custom tab
    // Track event : External | Integration: Recognition – Started
    const properties = {
        medium: "msTeams",
        scope: "customTab"
    }
    pendoTracking("External | Integration: Recognition – Started", pgDetails.achieverApplicationID, pgDetails.achieverMemberId, JSON.stringify(properties));
    microsoftTeams.tasks.startTask(taskInfo, submitHandler);
}

function feedSuggestionRecognition(emails, type) {
    var userEmail = emails;
    let userName = pgDetails.userName || "";
    let userPk = pgDetails.userPk || "";
    const URL = `${pgDetails.baseAppUrl}/views/viewRecognizePage/?state=${pgDetails.token}&lang=en-US&serviceUrl=${encodeURIComponent(pgDetails.msTeamBotServiceUrl)}&subDomain=${pgDetails.subDomain}&userName=${userName}&programPk=${pgDetails.achieverApplicationID}&sourceName=recogSuggestion&userPk=${userPk}&userEmailForAdding=${userEmail}`
    pgDetails.source = "recogSuggestion";
    let taskInfo = {
        title: "Achievers Recognition",
        height: "Large",
        width: "Large",
        url: URL
    };
    let submitHandler = (err, result) => {
        if (pgDetails.source == "recogSuggestion" && err == null) {
            pageNumber = 0;
            showMoreActivities()
        }

    };
    // tracking an event in pendo when recognition has been started in custom tab (group/collaboration)
    // Track event : External | Integration: Recognition – Started
    const properties = {
        medium: "msTeams",
        scope: "customTab - "+ type
    }
    pendoTracking("External | Integration: Recognition – Started", pgDetails.achieverApplicationID, pgDetails.achieverMemberId, JSON.stringify(properties));
    microsoftTeams.tasks.startTask(taskInfo, submitHandler);
}
