var showPointsScaleTab = false
var boostHovered = false
var currentRequest = null;

var detailBoostpopup = false;
// newsfeed logout

function newdfeedSignoutHandler() {
  load(true);
  $.ajax({
    url: pgDetails.baseAppUrl + pgDetails.logout,
    timeout: 10000,
    headers: {
      authorization: pgDetails.token
    },
    data: {
      userToken: pgDetails.token
    },
    type: 'POST',
    crossDomain: true,
    success: function (data) {
      load(false);
      $(".secondDiv").show();
      $(".firstDiv").hide();
      $.ajax({
        url: pgDetails.baseAppUrl + pgDetails.proactiveLogout,
        headers: {
          authorization: pgDetails.token
        },
        data: {
          appUserId: pgDetails.appUserId,
          appId: pgDetails.tenantId,
          msTeamBotId: pgDetails.msTeamBotId,
          msTeamBotName: pgDetails.msTeamBotName,
          msTeamBotServiceUrl: pgDetails.msTeamBotServiceUrl,
          achieverApplicationID : pgDetails.achieverApplicationID,
          achieverMemberId: pgDetails.achieverMemberId
        },
        success: function (data) {

        }, error: function (resp, textStatus, errorMessage) {

        }
      })
    },
    error: function (resp, textStatus, errorMessage) {
      load(false);
    }
  });
}

// Shared Managers API

function getshareRecognition(str) {
  if (str.length == 0) {
    removeElement("autocomplete-list");
  }
  else {
    load(true);
    try {
      if (str.length == 0) {
        load(false);
        return;
      }
      $.ajax({
        url: pgDetails.baseAppUrl + pgDetails.getNames,
        timeout: 10000,
        headers: {
          authorization: pgDetails.userToken
        },
        data: {
          userName: str,
          userToken: pgDetails.userToken
        },
        type: 'POST',
        crossDomain: true,
        success: function (data) {
          load(false);
          var a, b, i, len
          removeElement("autocomplete-list");
          a = document.createElement("div");
          a.setAttribute("id", "autocomplete-list");
          a.setAttribute("class", "autocomplete-items container-fluid");
          $("#shareRecogbind").append(a);
          var checkEmpNominee = global.userData.nomineesSelected;;
          var checkMgrNominee = data.members;

          function removeDuplicatesSharedMgr() {
            for (i = 0, len = checkEmpNominee.length; i < len; i++) {
              for (var j = 0, len2 = checkMgrNominee.length; j < len2; j++) {
                if (checkEmpNominee[i].userId === checkMgrNominee[j].id) {
                  checkMgrNominee.splice(j, 1);
                  len2 = checkMgrNominee.length;
                }
              }
            }

            // console.log(checkEmpNominee);
            // console.log(checkMgrNominee);

          }

          removeDuplicatesSharedMgr();

          for (i = 0, len = checkMgrNominee.length; i < len; i++) {
            if (data.members.length > 0) {
              $("#shareRecogContainer").show();
            }
          }
          // if (data.members.length > 0) {
          //     $("#shareRecogContainer").show();
          // }
          $.each(data.members, function (i, item) {
            b = document.createElement("div");
            b.setAttribute("class", "row");
            b.innerHTML = '<image class="rounded-circle searchImg" src="' + item.profileImageUrl + '" onerror="this.src=\'' + pgDetails.baseAppUrl + '/docs/icon_med.jpg' + '\';"><p class="searchName">' + item.fullName + '</p>';
            if (!checkUserPresence(global.userData.sharedNomineesSelected, item.id)) {
              addEvent("click", b, function (e) {
                try {
                  $("#inpRemoveText").val('');
                  //var notifyManagers = ($('#shareCheck').is(":checked"));
                  var notifyManagers = global.userData.notifyReportsTo ? global.userData.notifyReportsTo : $('#shareCheck').is(":checked");
                  var container = document.getElementById("sharedmanagersArea");
                  var el = document.createElement('div');
                  el.setAttribute("id", item.id);
                  el.setAttribute("class", "row userDisplayPanel");
                  el.innerHTML = '<image src="' + item.profileImageUrl + '" onerror="this.src=\'' + pgDetails.baseAppUrl + '/docs/icon_med.jpg' + '\';" class="rounded-circle recipientImage"><span class=receipientName> ' + item.fullName + ' </span><a class="icon close" style="cursor: pointer" onclick="deleteSharedPanel(this)"></a>'
                  container.appendChild(el);
                  var userItem = { userId: item.id, userFullName: item.fullName, userName: item.username, userEmail: item.emailAddress }
                  global.userData.sharedNomineesSelected.push(userItem);
                  global.userData.notifyManagers = notifyManagers;
                  closeAllLists();
                  // recognitionTypes();
                  if (userItem.userId > 0) {
                    $('#removeSharedNominees').show();
                  }

                }
                catch (e) {
                }
              }, { passive: false })
              a.appendChild(b);
            }
          });
        },
        error: function (jqXHR, textStatus, errorThrown) {
          load(false);
          if (parseInt(jqXHR.status) == 401) {
            expirysignout();
          }
        }
      });
    }
    catch (e) {
      load(false);
    }
  }
}



//Search User Data
function getSearchUserData(str) {
  if (str.length == 0) {
    removeElement("autocomplete-list");
  }
  else {
    load(true);
    try {
      if (str.length == 0) {
        load(false);
        return;
      }
      currentRequest = $.ajax({
        url: pgDetails.baseAppUrl + pgDetails.getNames,
        timeout: 10000,
        headers: {
          authorization: pgDetails.userToken
        },
        data: {
          userName: str,
          userToken: pgDetails.userToken
        },
        type: 'POST',
        crossDomain: true,
        beforeSend: function () {
          if (currentRequest != null) {
            currentRequest.abort();
            load(true);
          }
        },
        success: function (data, textStatus, xhr) {
          currentRequest = null;
          if (xhr.responseText == "invalid_token") {
            load(false);
            expirysignout();
          } else {
            load(false);
            var a, b, i
            removeElement("autocomplete-list");
            a = document.createElement("div");
            a.setAttribute("id", "autocomplete-list");
            a.setAttribute("class", "autocomplete-items container-fluid");
            $("#grpUserNamSel").append(a);
            if (data.members.length > 0) {
              $("#recipientContainer").show();
            }
            $.each(data.members, function (i, item) {
              b = document.createElement("div");
              b.setAttribute("class", "row");
              b.innerHTML = '<image class="rounded-circle searchImg" src="' + item.profileImageUrl + '" onerror="this.src=\'' + pgDetails.baseAppUrl + '/docs/icon_med.jpg' + '\';"><p class="searchName">' + item.fullName + '</p>';
              if (!checkUserPresence(global.userData.nomineesSelected, item.id)) {
                addEvent("click", b, function (e) {
                  try {
                    $("#to").val('');
                    var container = document.getElementById("recipientsArea");
                    var el = document.createElement('div');
                    el.setAttribute("id", item.id);
                    el.setAttribute("class", "row userDisplayPanel");
                    el.innerHTML = '<image src="' + item.profileImageUrl + '" onerror="this.src=\'' + pgDetails.baseAppUrl + '/docs/icon_med.jpg' + '\';" class="rounded-circle recipientImage"><span class=receipientName> ' + item.fullName + ' </span><a class="icon close" style="cursor: pointer" onclick="deletePanel(this)"></a>'
                    container.appendChild(el);
                    $("#btn_removeAll").show();
                    var userItem = { userId: item.id, userFullName: item.fullName, userName: item.username, userEmail: item.emailAddress }
                    global.userData.nomineesSelected.push(userItem);

                    // points page
                    pgDetails.showPointsUIOnly = false;

                    closeAllLists();
                    recognitionTypes();

                  }
                  catch (e) {
                  }
                }, { passive: false })
                if (item.id != pgDetails.userPk) {
                  a.appendChild(b);
                }
              }
            });
          }

        },
        error: function (jqXHR, textStatus, errorThrown) {
          load(false);
        }
      });
    }
    catch (e) {
      load(false);
    }
  }
}

// recognition types

function recognitionTypes() {
  load(true);
  try {
    clearRecogTypes();
    var _userIds = getUserIds(global.userData.nomineesSelected).join();
    $.ajax({
      url: pgDetails.baseAppUrl + pgDetails.getRecogTypes,
      timeout: 10000,
      headers: {
        authorization: pgDetails.userToken
      },
      data: {
        userIds: _userIds,
        userToken: pgDetails.userToken
      },
      type: 'POST',
      crossDomain: true,
      success: function (data, textStatus, xhr) {
        if (xhr.responseText == "invalid_token") {
          load(false);
          expirysignout();
        } else {
          // Change data - Sort based on moduleSequence
          let recTypeItems = data.items;
          let orderByModuleSequence = recTypeItems.slice(0);
          orderByModuleSequence.sort(function(a,b) {
              return parseInt(a.moduleSequence) - parseInt(b.moduleSequence);
          });
          data.items = orderByModuleSequence;
          load(false);
          global.currentRecogData = data;
          // console.log("Localized text in get recog types : ", global.userData.localizedText);
          // console.log("Localized value of select : ", global.userData.localizedText["Select"]);
          $("#selRtype").append("<option value= '0' selected=true> " + global.userData.localizedText["Select"] + "</option>");
          var option = '';
          for (i = 0; i < data.items.length; i++) {
            let recognitionTypeItem = { id: data.items[i].id }
            // For points screen
            recognitionTypeItem.moduleType = data.items[i].moduleType;
            option = "<option value= '" + JSON.stringify(recognitionTypeItem) + "'>" + data.items[i].name + "</option>"
            $("#selRtype").append(option);
          }
          $("#selRtype").selectric('refresh');
          $('#selRtype').selectric('init');
        }
      },
      error: function (jqXHR, textStatus, errorThrown) {
        load(false);
      }
    });
  }
  catch (e) {
    load(false);
  }
}

// post recognition


function postRecognition(microsoftTeamsObj) {
  load(true);
  // if (global.userData.pointval || typeof global.userData.pointval !== "nmumber") {
  //   global.userData.pointval = 0;
  // }
  try {
    var reqData = {
      nominees: getUserIds(global.userData.nomineesSelected).join(),
      recognitionText: global.userData.recogText,
      criterionId: parseInt(global.userData.recVal),
      criterionVal: global.userData.criterion,
      userFullName: pgDetails.userFullName,
      nomineeEmails: getNomineeEmails(global.userData.nomineesSelected),
      userToken: pgDetails.userToken,
      imageSelected: pgDetails.imageSelected,
      shareWith: getUserIds(global.userData.sharedNomineesSelected).join(),
      notifyManagers: global.userData.notifyManagers,
      useUserIds: true
    }
    if(pgDetails.pointsOptionsToDisplay && pgDetails.pointsOptionsToDisplay.selectedModuleType === POINTBASED){
      reqData.points = global.userData.pointval;
    } else {
        let isPointBased = false;
        global.currentRecogData.items.forEach(item => {
            if (item.id == global.userData.recType) {
                isPointBased = (item.moduleType === "POINT-BASED");
            }
        });
        if (isPointBased) {
            reqData.points = global.userData.pointval;
        } else {
            reqData.points = 0;
        }
    }
    var formData = new FormData();

    formData.append('nominees', reqData.nominees);
    // formData.append('nomineesFullName', reqData.nomineesFullName);
    formData.append('recognitionText', reqData.recognitionText);
    formData.append('criterionId', reqData.criterionId);
    formData.append('criterionVal', reqData.criterionVal);
    formData.append('userFullName', reqData.userFullName);
    formData.append('nomineeEmails', reqData.nomineeEmails);
    formData.append('points', reqData.points);
    formData.append('userToken', reqData.userToken);
    formData.append('imageSelected', reqData.imageSelected);
    formData.append('shareWith', reqData.shareWith);
    formData.append('notifyManagers', reqData.notifyManagers);
    formData.append('useUserIds', reqData.useUserIds);
    formData.append('scope', pgDetails.scope)
    if (reqData.imageSelected == "upload") {
      var fileInput = document.getElementById('inputFile');
      var file = fileInput.files[0];
      formData.append("file", file);
      if (!formData.get("file")) {
        $(".errorMessagePost").html("The image was not uploaded successfully. Please try again.");
        alertWarningSubmit("The image was not uploaded successfully. Please try again.");
        load(false);
        return;
      }
    }
    if (reqData.imageSelected == "giphy") {
      formData.append("imageUrl", pgDetails.imageUrl);
      //reqData.imageUrl = pgDetails.imageUrl;
    }
    if (reqData.imageSelected == "default") {
      formData.append("imageUrl", pgDetails.recognitionImage);
    }
    $.ajax({
      url: pgDetails.baseAppUrl + pgDetails.postrecog,
      headers: {
        authorization: pgDetails.userToken
      },
      data: formData,
      type: 'POST',
      contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
      processData: false, // NEEDED, DON'T OMIT THIS
      crossDomain: true,
      success: function (data, status, xhr) {
        if (data.message.toLowerCase() == "success") {
          document.getElementById("btn_Submit").disabled = true;
          appIds = [pgDetails.appId];
          reqData.nominees = getCommaSeparatedString(getFullNames(global.userData.nomineesSelected));
          reqData.shareWith = getCommaSeparatedString(getFullNames(global.userData.sharedNomineesSelected));
          reqData.newsFeedUrl = data.newsFeedUrl;
          reqData.imageUrl = data.imageUrl;
          reqData.userLocale = pgDetails.lang;
          microsoftTeamsObj.tasks.submitTask(reqData, appIds);
          load(false);
        }
      },
      error: function (xhr, status, error) {
        if (xhr.status == 401) {
          logoutRefresh();
          unloadApp();
        }
        else {
          load(false);
          var err = xhr.responseText;
          if(err){
            $(".errorMessagePost").html(err);
            alertWarningSubmit(err);
          }
        }
      }
    });
  }
  catch (e) {
    load(false);
  }
}

// sign out

function signoutHandler(isExpired) {
  load(true);
  $.ajax({
    url: pgDetails.baseAppUrl + pgDetails.logout,
    timeout: 10000,
    headers: {
      authorization: pgDetails.userToken
    },
    data: {
      userToken: pgDetails.userToken
    },
    type: 'POST',
    crossDomain: true,
    success: function (data) {
      load(false);
      $("#userDetails").hide();
      $("#recogDetails").hide();
      $("#previewRecog").hide();
      $("#imageSelection").hide();
      $("#shareRecognition").hide();
      if (isExpired) {
        $("#sessionexpirynotification").show();
      }
      else {
        $("#logoutnotification").show();
      }

    },
    error: function (resp, textStatus, errorMessage) {
      load(false);
    }
  });
}

// get recogntion points

function getPoints() {
  load(true);
  $.ajax({
    url: pgDetails.baseAppUrl + pgDetails.curMember,
    timeout: 10000,
    headers: {
      authorization: pgDetails.userToken
    },
    data: {
      userToken: pgDetails.userToken
    },
    type: 'POST',
    crossDomain: true,
    success: function (data, textStatus, xhr) {
      if (xhr.responseText == "invalid_token") {
        alertWarning(global.userData.localizedText["Error_processing_request"]);
        load(false);
      } else {
        let budgetObj = data.memberBudget.recognitionBudgets[String(global.userData.recType)];
        pgDetails.memberBudget = data.memberBudget;
        pgDetails.displayName = data.fullName;
        if (budgetObj && budgetObj.budgetRemaining && budgetObj.moduleType != "FREE") {
          $("#pointsbalance").html(global.userData.localizedText['Points_available_to_award'] + '<b>' + getLocalizedNumber(parseInt(budgetObj.budgetRemaining.points)) + '<b/>');
          $("#pointsbalance").show();
          global.userData.budgetPoints = budgetObj.budgetRemaining.points;
        }
        else {
          $("#pointsbalance").text('');
          $("#pointsbalance").hide();
          $("#pointSpent").hide();
        }
        load(false);
      }
    },
    error: function (xhr, textStatus) {
      alertWarning(global.userData.localizedText["Error_processing_request"]);
      load(false);
    }
  })
}

// show or hide shred manager screen

function showHideShareMgr() {
  load(true);
  $.ajax({
    url: pgDetails.baseAppUrl + pgDetails.curMember,
    timeout: 10000,
    headers: {
      authorization: pgDetails.userToken
    },
    data: {
      userToken: pgDetails.userToken
    },
    type: 'POST',
    crossDomain: true,
    success: function (data, textStatus, xhr) {
      if (xhr.responseText == "invalid_token") {
        alertWarning(global.userData.localizedText["Error_processing_request"]);
        load(false);
      } else {

        $("#shareRecognition").show();
        $("#imageSelection").hide();
        $("#btn_Submit_Back").click(function () {
          if (showPointsScaleTab && pgDetails.pointsOptionsToDisplay.selectedModuleType === POINTBASED) {
            // points page
            if (pgDetails.showPointsUIOnly) {
              viewPage('pointsDiv');
            } else {
              showPoints();
            }
          } else {
            viewPage('shareRecognition');
          }
        })

        load(false);
      }
    },
    error: function (xhr, textStatus) {
      alertWarning(global.userData.localizedText["Error_processing_request"]);
      load(false);
    }
  })
}

// get image search

function imageSearch(search) {
  load(true);
  $.ajax({
    url: pgDetails.baseAppUrl + pgDetails.imageSearch,
    timeout: 10000,
    headers: {
      authorization: pgDetails.userToken
    },
    data: {
      userToken: pgDetails.userToken,
      squery: search
    },
    type: 'POST',
    crossDomain: true,
    success: function (data, textStatus, xhr) {
      load(false);
      pgDetails.searchImages = data.items;
      pgDetails.url = pgDetails.searchImages[0].previewUrl
      pgDetails.imageUrl = null;

      var list = '';
      document.getElementById("gifList").innerHTML = '';
      for (i = 0; i < data.items.length; i++) {
        var list = document.createElement("div");
        list.innerHTML = "<img src='" + data.items[i].previewUrl + "'  onclick='gifImgClick(this)' id='" + data.items[i].url + "' class='gifImage'/>";
        document.getElementById("gifList").appendChild(list);
      }
    },
    error: function (xhr, textStatus) {
      alertWarning(global.userData.localizedText["Error_processing_request"]);
      load(false);
    }
  });
}
function getTranslatedStrings(queryParams, fallback = false) {
  load(true)
  if (fallback) {
    queryParams.lang = "en-US";
  }
  var jsonData = null;
  var fileLocation = "/languages/translated/msteams_connect_";
  if (queryParams.lang == "en-US") {
    fileLocation = "/languages/msteams_connect_";
  }
  $.ajax({
    'async': false,
    'global': false,
    'url': pgDetails.baseAppUrl + fileLocation + queryParams.lang + ".json",
    'dataType': "json",
    'type': 'GET',
    'success': function (data) {
      jsonData = data;
    }
  });
  load(false)
  return jsonData;
}
function searchAndAddAll(str) {
  // console.log("get search user data************* : ", str);
  if (str.length == 0) {
    removeElement("autocomplete-list");
  }
  else {
    load(true);
    try {
      if (str.length == 0) {
        load(false);
        return;
      }
      $.ajax({
        url: pgDetails.baseAppUrl + pgDetails.getNames,
        timeout: 10000,
        headers: {
          authorization: pgDetails.userToken
        },
        data: {
          userName: str,
          userToken: pgDetails.userToken
        },
        type: 'POST',
        async: false,
        crossDomain: true,
        success: function (data, textStatus, xhr) {

          if (xhr.responseText == "invalid_token") {
            load(false);
            expirysignout();
          } else {
            load(false);
            removeElement("autocomplete-list");
            if (data.members.length > 0) {
              $("#recipientContainer").show();
            }
            $.each(data.members, function (i, item) {
              if (!checkUserPresence(global.userData.nomineesSelected, item.id)) {
                $("#to").val('');
                if (item.id != pgDetails.userPk) {
                  var container = document.getElementById("recipientsArea");
                    var el = document.createElement('div');
                    el.setAttribute("id", item.id);
                    el.setAttribute("class", "row userDisplayPanel");
                    el.innerHTML = '<image src="' + item.profileImageUrl + '" onerror="this.src=\'' + pgDetails.baseAppUrl + '/docs/icon_med.jpg' + '\';" class="rounded-circle recipientImage"><span class=receipientName> ' + item.fullName + ' </span><a class="icon close" style="cursor: pointer" onclick="deletePanel(this)"></a>'
                    container.appendChild(el);
                    $("#btn_removeAll").show();
                    var userItem = { userId: item.id, userFullName: item.fullName, userName: item.username, userEmail: item.emailAddress }
                    global.userData.nomineesSelected.push(userItem);


                } else {
                  console.log("Not added user as he is the moninator !!!");
                }
                closeAllLists();
              }
            });
            recognitionTypes();
          }
        },
        error: function (jqXHR, textStatus, errorThrown) {
          load(false);
        }
      });
    }
    catch (e) {
      load(false);
    }
  }
}
function addAll() {
  // console.log("Trying to add all");
  load(true);
  $.ajax({
    url: pgDetails.baseAppUrl + "/api/achievers/getAllUserDetails",
    timeout: 10000,
    headers: {
      authorization: pgDetails.userToken
    },
    data: {
      serviceUrl: pgDetails.serviceUrl,
      conversationId: pgDetails.conversationId,
      programPk: pgDetails.programPk,
      userPk: pgDetails.userPk,
      scope: pgDetails.scope

    },
    type: 'POST',
    crossDomain: false,
    success: function (data) {
      // console.log("Success : ", data);
      // if (data.includes("pavankumar@nadupuru.onmicrosoft.com")) {
      //     data = data.replace("pavankumar@nadupuru.onmicrosoft.com", "tatum.coppage@example.com")
      // }
      // if (data.includes("vltest@nadupuru.onmicrosoft.com")) {
      //     data = data.replace("vltest@nadupuru.onmicrosoft.com", "adele.olson@example.com")
      // }
      // if (data.includes("vltest1@nadupuru.onmicrosoft.com")) {
      //     data = data.replace("vltest1@nadupuru.onmicrosoft.com", "vikki.borland@example.com")
      // }
      // if (data.includes("vltest2@nadupuru.onmicrosoft.com")) {
      //     data = data.replace("vltest2@nadupuru.onmicrosoft.com", "thi.lager@example.com")
      // }
      let allPeople = data.split("**&&**")
      // console.log("All people found : ", allPeople);
      const allPeopleString = allPeople.join(",");
      // console.log(allPeopleString);
      if (!allPeopleString) {
        alertWarning("Error communicating with our servers. Please try again after sometime.");
      } else {
        searchAndAddAll(allPeopleString);
      }
      // console.log("The end")
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log("ERROR");
      load(false);
      if (parseInt(jqXHR.status) == 401) {
        expirysignout();
      }
    }
  });
}

// show boosts info
function showBoostsInfo(newsfeedEventId){
    $(`#${newsfeedEventId}`).show()
    if(!boostHovered){
        $.ajax({
            url: pgDetails.baseAppUrl + "/api/achievers/getnewsFeedActionsInfo",
            // timeout: 10000,
            headers: {
                authorization: pgDetails.token
            },
            data: {
                newsFeedEventId:newsfeedEventId,
                actionType:"boosts",
                userToken: pgDetails.token
            },
            type: 'POST',
            crossDomain: false,
            success: function (data) {

                $(`.${newsfeedEventId}boost`).empty();
                if(data.items && data.items.length){
                    let el2=`<div class="row boostClassHover">
                                <div class="boostHoverImage">
                                    <img src="../../docs/boostPost.png" class="boostedTextImage">
                                </div>
                                <span class="hasBoostedText">Has Boosted</span>
                            </div>
                            <div class="cardHoverBorder"></div>`

                    data.items.map(item=>{
                        el2 += `<div class="row boostClassHover">
                                    <div class="userHoverImage">
                                        <img src=${item.user.profileImageUrl} class="userTextImage">
                                    </div>
                                    <div class="boostedUserText">${item.user.name}</div>
                                </div>`
                    })

                    $(`.${newsfeedEventId}boost`).append(el2)
                }else{
                    $(`.${newsfeedEventId}boost`).attr('style', 'border: none !important');
                }
                boostHovered = true

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("ERROR");
                load(false);
            }
        });
    }
}

function removeAll() {
  load(true);
  global.userData.nomineesSelected = []
  document.getElementById("recipientsArea").innerHTML = '';
  closeAllLists();
  clearRecogTypes();
  clearAllWarnings();
  $("#pointsbalance").hide();
  $("#pointSpent").hide();
  $("#btn_removeAll").hide();
  load(false);
}

// Activities page
function showMoreActivities(){
    pageNumber===0 ? staticTabloader(true,false):staticTabloader(false,true);
    if(pageNumber === 0){
      pageNumber = ''
    }
    if(pgDetails.source === "homeTab" && pageNumber === ''){
      $(".activityArea").html("")
      $("#showMoreButtonId").hide();
    }
    $.ajax({
        url: pgDetails.baseAppUrl + pgDetails.newsFeedActivities,
        // timeout: 10000,
        // headers: {
        //     authorization: pgDetails.userToken
        // },
        data: {
            offset: pageNumber,
            viewPage: false,
            azureUserId : pgDetails.azureUserId,
            tenantId : pgDetails.tenantId
        },
        type: 'GET',
        crossDomain: true,
        success: function (data, textStatus, xhr) {
          if(data.finalObject){
            let newsFeedData = JSON.parse(data.finalObject)
            pageNumber = data.nextCursor
            if(newsFeedData.length){
              mapNewsFeedActivity(newsFeedData,data.nextCursor)
            }
          }
          staticTabloader(false,false)
        },
        error: function (xhr, textStatus) {
            staticTabloader(false,false)
        }
    });
}

// show boost modal and post an event
function showBoostModal(newsfeedEventId,newsFeedURL,totalBudget,boostsCount,boostedByUser, detailBoostpopup){
    $("#errorMessageBoost").text("");
    if(!boostedByUser){
        staticTabloader(true,false)
        $.ajax({
            url: pgDetails.baseAppUrl + "/api/achievers/getnewsFeedActionsInfo",
            // timeout: 10000,
            headers: {
                authorization: pgDetails.token
            },
            data: {
                newsFeedEventId:newsfeedEventId,
                actionType:"boosts",
                userToken: pgDetails.token,
                optionsCall: true
            },
            type: 'POST',
            crossDomain: false,
            success: function (data, textStatus, xhr) {
                staticTabloader(false,false)
                document.getElementById("openBoostModal").innerHTML=""

                // Don't have enough points to boost -- Demo purpose
                // totalBudget = 200
                if(totalBudget && data && data.pointsForBoost && data.pointsForBoost.totalPoints &&
                totalBudget >= data.pointsForBoost.totalPoints && data.boostableMembers &&
                data.boostableMembers.length){
                    let boostableMembersNames = data.boostableMembers.map((item)=>{
                        return item.member.name
                    })

                    if(boostableMembersNames.length === 1){
                        boostableMembersNames = boostableMembersNames[0]
                    }else if(boostableMembersNames.length ===2){
                        boostableMembersNames = `${boostableMembersNames[0]} and ${boostableMembersNames[1]}`
                    }else{
                        boostableMembersNames = `${boostableMembersNames[0]} and ${boostableMembersNames.length-1} others`
                    }
                    let el =`<div>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        <h5 class="boostClickTitle" id="exampleModalLongTitle">Give this a boost</h5>
                        <hr class="hrClass">
                        <img src="../../docs/horse.png">
                        <div class="boostClickPointsTxt">
                            <h5 class="sendPointsText">Send points to ${boostableMembersNames}</h5>
                            <p class="totalPointsText">Total: ${data.pointsForBoost.totalPoints} points</p>
                        </div>
                        <div class="boostClickPointsTxt">
                            <button type="button" class="btn boostPostClick">
                                <a class="boostPostClickP" onclick="postBoostEvent(${newsfeedEventId},${boostsCount})">Boost
                                    <img class="boostWhiteImage" src="../../docs/boostWhite.png"/>
                                </a>
                            </button>
                        </div>
                        <div class="col-sm-12">
                            <p class="wrng-msg wrng-msg-boost" id="errorMessageBoost"></p>
                        </div>
                    </div>`
                    $("#openBoostModal").append(el);
                    $(".thirdDiv").show();
                }else{
                    let el=`<div>
                        <button type="button" class="btn-close boostClickClose" data-bs-dismiss="modal" aria-label="Close"></button>
                        <h5 class="boostClickTitle" id="exampleModalLongTitle">Leave a comment instead</h5>
                        <hr class="hrClass">
                        <img src="../../docs/keyboard.png" class="boostkeyBoard">
                        <div class="boostClickPointsTxt">
                            <h5 class="sendPointsText">You don't have enough points to boost</h5>
                            <p class="totalPointsText">Leave a comment instead to show your appreciation</p>
                        </div>`
                        if(detailBoostpopup === true) {
                          el +=  `<div class="boostClickPointsTxt">
                                    <button type="button" class="btn boostPostClick" data-bs-toggle="modal" aria-label="Close" onclick="closeBoostModalPopup(${newsfeedEventId})">
                                      Comment
                                    </button>
                                  </div>
                                  <div class="col-sm-12">
                                    <p class="wrng-msg wrng-msg-boost" id="errorMessageBoost"></p>
                                  </div>`

                        } else {
                          el +=  `<div class="boostClickPointsTxt">
                                    <button type="button" class="btn boostPostClick" data-bs-toggle="modal" aria-label="Close" onclick="CommentmapNewsFeedActivity(${newsfeedEventId})">
                                      Comment
                                    </button>
                                  </div>
                                  <div class="col-sm-12">
                                    <p class="wrng-msg wrng-msg-boost" id="errorMessageBoost"></p>
                                  </div>`
                          }

                  `</div>`
                  $("#openBoostModal").append(el);
                  $(".thirdDiv").show();
                }
            },
            error: function (xhr, textStatus) {
                staticTabloader(false,false)
            }
        });
    }

}

function reloadPage() {
  // window.location.reload();
  detailBoostpopup = false;
  staticTabloader(true, false);
  $(".feedDetailedView").append("")
  $(".activityArea").show();
  $(".feedDetailedView").hide();
  $(".backtoRecentActivity").hide();
  $(".Recent_Activity").show();
  $(".showMoreClass").show();
  staticTabloader(false, false);
}
function closeBoostModalPopup(newsfeedEventId) {
  $("#exampleModalCenter").modal("hide");
}
function BoostModelCount(eventId, numberOfBoosts) {
  if(numberOfBoosts == 0) {
    document.getElementById("BoostModalOpen").innerHTML = "";
    $('#boost-' + eventId).removeAttr("data-toggle");
    $('#BoostModalCenter').modal('hide');
    $("#BoostModalOpen").hide();
  }
  else {
    staticTabloader(true,false);
    document.getElementById("BoostModalOpen").innerHTML = "";
    $('#boost-' + eventId).attr("data-toggle");
    $('#BoostModalCenter').modal('show');
    $("#BoostModalOpen").show();
  $.ajax({
    url: pgDetails.baseAppUrl + "/api/achievers/getnewsFeedActionsInfo",
    // timeout: 10000,
    headers: {
        authorization: pgDetails.token
    },
    data: {
        newsFeedEventId:eventId,
        actionType:"boosts",
        userToken: pgDetails.token,
        getBoostsCall: true
    },
    type: 'POST',
    crossDomain: false,
    success: function (data, textStatus, xhr) {
      staticTabloader(false,false)
      // document.getElementById("BoostModalOpen").innerHTML = "";
      let usersProfileAndName = "";
      if (data && data.items && data.items.length > 0) {
        data.items.forEach(boost => {
          usersProfileAndName += `<div class="users-items">
          <div class="event-user-img">
            <img src="${boost.user.profileImageUrl}" class="rounded-circle">
          </div>
          <div class="event-user-name">
            ${boost.user.name}
          </div>
        </div>`
        });
      }
      let el = `<div id="boostCountModal"><div class="modal-content">
        <div class="modal-header">
          <div class="popup-icon">
            <img class="boostImage" src="../../docs/boostPost.png" />
          </div>
          <h5 class="modal-title">Has Boosted</h5>
          <button type="button" data-bs-toggle="modal" aria-hidden="true" class="boostClickCross popupCloseBtn">&times;</span>
        </div>
        <div class="modal-body">
          <div class="event-users-list">
          ${usersProfileAndName}
          </div>
        </div>
      </div></div>`
      $("#BoostModalOpen").append(el);
      $(".thirdDiv").show();
    },
    error: function (xhr, textStatus) {
      let el = `<div id="boostCountModal"><div class="modal-content">
        <div class="modal-header">
          <div class="popup-icon">
            <img class="boostImage" src="../../docs/boostPost.png" />
          </div>
          <h5 class="modal-title">Has Boosted</h5>
          <button type="button" data-bs-toggle="modal" aria-hidden="true" class="boostClickCross popupCloseBtn">&times;</span>
        </div>
        <div class="modal-body">
          <div class="event-users-list">
          </div>
        </div>
      </div></div>`
      $("#BoostModalOpen").append(el);
      $(".thirdDiv").show();
    }
});
}
}

function LikestModelCount(eventId, numberOfLikes) {
  if(numberOfLikes == 0) {
    document.getElementById("LikesModalOpen").innerHTML=""
    $('#likes-' + eventId).removeAttr("data-toggle");
    $('#LikesModalCenter').modal('hide');
    $("#LikesModalOpen").hide();
  }
  else {
  staticTabloader(true,false)
  document.getElementById("LikesModalOpen").innerHTML=""
  $('#likes-' + eventId).attr("data-toggle");
  $('#LikesModalCenter').modal('show');
  $("#LikesModalOpen").show();
  $.ajax({
    url: pgDetails.baseAppUrl + "/api/achievers/getnewsFeedActionsInfo",
    // timeout: 10000,
    headers: {
        authorization: pgDetails.token
    },
    data: {
        newsFeedEventId:eventId,
        actionType:"likes",
        userToken: pgDetails.token,
        getLikesCall: true
    },
    type: 'POST',
    crossDomain: false,
    success: function (data, textStatus, xhr) {
      staticTabloader(false,false);

      let usersProfileAndName = "";
      if (data && data.items && data.items.length > 0) {
        data.items.forEach(like => {
          usersProfileAndName += `<div class="users-items">
          <div class="event-user-img">
            <img src="${like.user.profileImageUrl}" class="rounded-circle">
          </div>
          <div class="event-user-name">
            ${like.user.name}
          </div>
        </div>`
        });
      }
      let el =`<div id="LikesCountModal"><div class="modal-content">
          <div class="modal-header">
            <div class="popup-icon">
              <i class="fa fa-heart modalLikesIcon"  aria-hidden="true"/>
            </div>
            <h5 class="modal-title">Has Liked</h5>
            <button type="button" data-bs-toggle="modal"  aria-hidden="true" class="boostClickCross popupCloseBtn">&times;</span>
          </div>
          <div class="modal-body">
            <div class="event-users-list">
              ${usersProfileAndName}
            </div>
          </div>
        </div></div>`
      $("#LikesModalOpen").append(el);
      $(".thirdDiv").show();
    },
    error: function (xhr, textStatus) {
      let el =`<div id="LikesCountModal"><div class="modal-content">
          <div class="modal-header">
            <div class="popup-icon">
              <i class="fa fa-heart modalLikesIcon"  aria-hidden="true"/>
            </div>
            <h5 class="modal-title">Has Liked</h5>
            <button type="button" data-bs-toggle="modal" aria-hidden="true" class="boostClickCross popupCloseBtn">&times;</span>
          </div>
          <div class="modal-body">
            <div class="event-users-list">
            </div>
          </div>
        </div></div>`
      $("#LikesModalOpen").append(el);
      $(".thirdDiv").show();
    }
  });
}
}

function postBoostEvent(newsfeedEventId,boostsCount){
    boostEventloader(true)
    $.ajax({
        url: pgDetails.baseAppUrl + "/api/achievers/getnewsFeedActionsInfo",
        // timeout: 10000,
        headers: {
            authorization: pgDetails.token
        },
        data: {
            newsFeedEventId:newsfeedEventId,
            actionType:"boosts",
            userToken: pgDetails.token,
            postCall: true
        },
        type: 'POST',
        crossDomain: false,
        success: function (data, textStatus, xhr) {
            boostEventloader(false)
            // If we get below error message -- Demo purpose
            // data.message ="You cannot Boost this event. You either do not have enough budget, or the recipient is unable to receive points."
            if(data && data.message){
                $("#errorMessageBoost").text("You do not have enough points.");
                $("#boost-post-" + newsfeedEventId).addClass('boostDisable');
            }else{
                $("#exampleModalCenter").modal("hide");
                $("#BoostModalCenter").modal("hide");
                $("#LikesModalCenter").modal("hide");
                $("#boost-post-" + newsfeedEventId).addClass('boostDisable');
                // $(".boostClass").prop('disabled', true);
                $("#boostImage-" + newsfeedEventId).attr("src","../../docs/boostPost.png");
                $("#boost-"+newsfeedEventId).text(`${boostsCount+1}`)
            }
        },
        error:function (xhr, textStatus) {
            boostEventloader(false)
        }
    });
}


function postLikesEvent(newsfeedEventId, numberOfLikes, liked_by_user){
  var  fired_button = $(`#newsfeedLikeClick-${newsfeedEventId}`).attr('class');
  if(fired_button == "btn likeClass like") {
    $.ajax({
      url: pgDetails.baseAppUrl + "/api/achievers/getnewsFeedActionsInfo",
      // timeout: 10000,
      headers: {
        authorization: pgDetails.token
      },
      data: {
        newsFeedEventId:newsfeedEventId,
        actionType:"likes",
        userToken: pgDetails.token,
        postLikesCall: true
      },
      type: 'POST',
      crossDomain: false,
      success: function (data, textStatus, xhr) {
        console.log("==data",data);
        if(data.items){
          var newsfeedLikeClick = $("#newsfeedLikeClick-" + newsfeedEventId);
          var newsfeedLikeIcon = $("#newsfeedLikeIcon-" + newsfeedEventId);
          var newsfeedLikeCount = $("#likes-" + newsfeedEventId);
          numberOfLikes = newsfeedLikeCount.text().trim();
          numberOfLikes = parseInt(numberOfLikes);
          $(newsfeedLikeIcon).removeClass('fa-heart-o');
          $(newsfeedLikeIcon).addClass('fa-heart');
          $(newsfeedLikeClick).removeClass('like').addClass('unlike');
          $(newsfeedLikeCount).text(`${numberOfLikes+1}`);
        }
        DeatilsPagedefaultLikesTrue(liked_by_user, newsfeedEventId, numberOfLikes);
      },
      error:function (xhr, textStatus) {
        //  console.log(textStatus);
      }
    });
  }
  else {
    $.ajax({
      url: pgDetails.baseAppUrl + "/api/achievers/getnewsFeedActionsInfo",
      // timeout: 10000,
      headers: {
        authorization: pgDetails.token
      },
      data: {
        newsFeedEventId:newsfeedEventId,
        actionType:"likes",
        userToken: pgDetails.token,
        deleteLikesCall: true
      },
      type: 'DELETE',
      crossDomain: false,
      success: function (data, textStatus, xhr) {
          // console.log("==data",data);
          if(data){
            var newsfeedLikeClick = $("#newsfeedLikeClick-" + newsfeedEventId);
            var newsfeedLikeIcon = $("#newsfeedLikeIcon-" + newsfeedEventId);
            var newsfeedLikeCount = $("#likes-" + newsfeedEventId);
            numberOfLikes = newsfeedLikeCount.text().trim();
            numberOfLikes = parseInt(numberOfLikes);
            $(newsfeedLikeIcon).addClass('fa-heart-o');
            $(newsfeedLikeIcon).removeClass('fa-heart');
            $(newsfeedLikeClick).removeClass('unlike').addClass('like');
            $(newsfeedLikeCount).text(`${numberOfLikes - 1}`);
            var newsfeedLikeClickDetailePage = $("#newsfeedLikeClickDetailePage-" + newsfeedEventId);
            var newsfeedLikeIconDetailPage = $("#newsfeedLikeIconDetailPage-" + newsfeedEventId);
            var newsfeedLikeCountDetailPage = $("#newsfeedLikeCountDetailPage-" + newsfeedEventId);
            $(newsfeedLikeIconDetailPage).addClass('fa-heart-o');
          $(newsfeedLikeIconDetailPage).removeClass('fa-heart');
          $(newsfeedLikeClickDetailePage).removeClass('unlike').addClass('like');
          $(newsfeedLikeCountDetailPage).text(`${numberOfLikes - 1}`);
        }
      },
      error:function (xhr, textStatus) {
        // console.log(textStatus);
      }
    });
  }
}

function fetchCelebrations() {
  celebrationLoader(true);
  $.ajax({
    url: pgDetails.baseAppUrl + "/api/achievers/fetchCelebrationsOrRecognitions",
    headers: {
      authorization: pgDetails.userToken
    },
    data: {
      userToken: pgDetails.userToken,
    },
    type: 'POST',
    crossDomain: true,
    success: function success(data, textStatus, xhr) {
      var _container = $("#nudgesUserProfileOnly");
      data.forEach(function (item) {
        var el = "<div class=\"col-sm-12 userProfileOnly\"><a href=".concat(item.redirectURL, " target=\"_blank\"><div class=\"nudegeslistbody\">")
        el += "<div class=\"nudgesuserProfileImage\"><img class=\"cardImage rounded-circle userProfileImageTag\" src=".concat(item.userProfileImageFromUser, " alt=\"Card image cap\">\n </div>");
        el += "<div class=\"nudgesuserProfileName\"><div class=\"nudgesrecipientName\">".concat(item.user.name),
        el += "</div>  <div class=\"nudgesrecipientDate\">".concat(item.finalDate),
        el += "</div>  <div class=\"nudgesrecipientEvent\"> ".concat(item.cardTitle),
        el += "</div>   <img class=\"nudegesListArrow \" src=\"../../docs/long-arrow-right.png\", >\n </div>"
        el += "</div>"
        el += "</div>"
        _container.append(el);
      });
      celebrationLoader(false);
    },
    error: function error(error, xhr, textStatus) {
      celebrationLoader(false);
      console.log("errorrrr")
    }
  });
}

function pendoTracking( event,programPk, userPk, properties ) {

  var programPk = programPk && programPk!= "null" && programPk != "undefined" ? programPk : "";
  var userPk = userPk && userPk != "null" && userPk != "undefined" ? userPk : "";
  if ( programPk && userPk ) {
    $.ajax({
      url: pgDetails.baseAppUrl + "/api/achievers/pendo",
      headers: {
        authorization: pgDetails.token
      },
      timeout: 10000,
      data: {
        event: event,
        accountId: programPk,
        visitorId: userPk,
        properties: properties
      },
      type: "POST",
      crossDomain: true,
      success: function success( data, textStatus, xhr ) {},
      error: function error( error, textStatus ) {}
    });
  }
}


function fetchRecognitionSuggestions() {
  if (!pgDetails.token) {
    recognitionLoader(false);
  } else {
    recognitionLoader(true);
    $.ajax({
      url: pgDetails.baseAppUrl + "/api/achievers/getRecognitionSuggestions",
      // timeout: 10000,
      headers: {
        authorization: pgDetails.token
      },
      data: {
        userToken: pgDetails.token,
        azureUserId: pgDetails.azureUserId,
        tenantId: pgDetails.tenantId,
        userName: pgDetails.userName
      },
      type: 'POST',
      crossDomain: false,
      success: function (data, textStatus, xhr) {

        let showLink = data.askRecognitionSuggestion || false;
        if (showLink === true) {
          const link = data.link
          pgDetails.showLink = showLink;
          pgDetails.link = link;
          pgDetails.dataReload = data.reload;
          document.getElementsByClassName("recognitionSuggestions")[0].style.display = "none";
          document.getElementsByClassName("Get_recognition_suggestions")[0].style.display = "flex";
          $('.Get_recognition_suggestions').css('cursor', 'pointer');
          $(".Get_recognition_suggestions").click(function () {
            sendCalenderPermission(link, data.reload)
          })
        }
        else {
          document.getElementsByClassName("Get_recognition_suggestions")[0].style.display = "none";
          let finalArray = data.finalArray;
          if (!finalArray.length) {
            $("#noSuggestionsPreview").show();
            recognitionLoader(false);
            return
          }
          finalArray.forEach(function (item, index) {
            if (item.type === "group") {
              var ProfilesLength = item.profileImages;
              if (ProfilesLength.length == 1) {
                var _groupImagesSingle = $("#groupImagesSingle");
                var el1 = `<div class=\"recognitionSuggestionsList\"><div class=\"col-sm-12 userProfileOnly\"  onclick="feedSuggestionRecognition('${item.email}', '${item.type}')"><div class=\"nudegeslistbody rowAClass\">`
                el1 += `<div class=\"nudgesuserProfileImage\"><img src=${item.profileImages[0]} class="cardImage rounded-circle userProfileImageTag rImg${index + 1}" alt="Cinque Terre"></div>`
                el1 += `<div class=\"nudgesuserProfileName\">`
                el1 += `<div class=\"nudgesrecipientName\"> ${item.profileNames[0]}</div>`
                el1 += `<div class=\"suggestionMsg\">You recently had a meeting together</div>`
                el1 += `<img class=\"nudegesListArrow \" src=\"../../docs/long-arrow-right.png\">`
                el1 += `</div></div></div></div>`;
                _groupImagesSingle.append(el1);
              }

              if (ProfilesLength.length == 2) {
                var profilNamesObj = item.profileNames.join(',');
                var profileMemberIdObj = item.memberId.join(',');
                var profileemailObj = item.email.join(',');
                console.log("profilNamesObj", profilNamesObj, profileMemberIdObj, profileemailObj)
                var _groupImagesDouble = $("#groupImagesDouble");
                var el1 = `<div class=\"recognitionSuggestionsList\"><div class=\"col-sm-12 userProfileOnly\" onclick="feedSuggestionRecognition('${item.email}', '${item.type}')"><div class=\"nudegeslistbody rowAClass\">`
                el1 += `<div class=\"nudgesuserProfileImage\">`
                el1 += `<img src=${item.profileImages[0]} class="cardImage rounded-circle userProfileImageTag rImg${index + 1}" alt="Cinque Terre">`
                el1 += `<img src=${item.profileImages[1]} class="cardImage rounded-circle userProfileImageTag rImg${index + 1} secondLeft" alt="Cinque Terre">`
                el1 += `</div> <div class=\"nudgesuserProfileName\">`
                el1 += `<div class=\"nudgesrecipientName\"> ${item.profileNames[0]} and ${item.profileNames[1]}</div>`
                el1 += `<div class=\"suggestionMsg\">You recently had a meeting together</div>`
                el1 += "<img class=\"nudegesListArrow \" src=\"../../docs/long-arrow-right.png\">"
                el1 += "</div></div></div></div>"
                _groupImagesDouble.append(el1);

              }

              if (ProfilesLength.length > 2) {
                const userEmails = item.email.join(',')
                var othersProfileImagesCount = item.profileImages.splice(1);
                var othersProfileNamesCount = item.profileNames.splice(1);
                var otherProfileMemberId = item.memberId.splice(1);
                var otherProfileEmailId = item.email.splice(1);
                var _groupImagesAboveTwo = $("#groupImagesAboveTwo");
                var el1 = `<div class="recognitionSuggestionsList aboveTo">
                      <div class="col-sm-12 userProfileOnly\"  onclick="feedSuggestionRecognition('${userEmails}', '${item.type}')">
                        <div class="nudegeslistbody rowAClass">
                          <div class="nudgesuserProfileImage"><img src=${item.profileImages[0]} class="cardImage rounded-circle userProfileImageTag rImg${index + 1}" alt="Cinque Terre">
                            <div class="recogniseInsightsRounded recognitionOthersDiv">+${othersProfileImagesCount.length}</div>
                          </div>
                          <div class="nudgesuserProfileName">
                            <div class="nudgesrecipientName">
                              <div class="aboveTwoNames">${item.profileNames[0]} and <span class="othersCount otherUsersList-${index + 1} hide" id="numbering">${othersProfileImagesCount.length} others</span></div>
                              <span class="suggestionMsg newMsg">You recently had a meeting together</span>
                              <img class="nudegesListArrow" src="../../docs/long-arrow-right.png">
                    </div>
                  </div>
                <div class="otherUsersList-${index + 1} hide" id="sugesstionspopup">`
                othersProfileImagesCount.forEach(function (othersProfileImage, index) {
                  el1 += `<div class="othersProfileDIV">
                              <div class="othersProfileImg">
                                <img src=${othersProfileImage} class="cardImage rounded-circle userProfileImageTag rImg${index + 1}">
                                <span class="nudgesrecipientName" onclick="feedSuggestionRecognition('${otherProfileEmailId[index]}', '${item.type}')"> ${othersProfileNamesCount[index]} </span>
                              </div>
                            </div>

                    `
                })
                el1 += `</div></div></div>`

                _groupImagesAboveTwo.append(el1);

                $(`.otherUsersList-${index + 1}`).hover(function (e) {
                  // $(`#sugesstionspopup-${index + 1}`).show();
                  $(`.otherUsersList-${index + 1}`).toggleClass('hide');
                  // e.stopPropagation();
                });

                // $(`#sugesstionspopup-${index + 1}`).click(function (e) {
                //   e.stopPropagation();
                // });

                // $(document).click(function () {
                //   $(`#sugesstionspopup-${index + 1}`).hide();
                // });

              }
            }
            if (item.type === "collaboration") {
              var _collaborationImages = $("#collaborationImages");
              var el1 = `<div class="recognitionSuggestionsList"><div class="col-sm-12 userProfileOnly\" onclick="feedSuggestionRecognition('${item.email}', '${item.type}')"><div class="nudegeslistbody rowAClass">`
              el1 += `<div class=\"nudgesuserProfileImage\">`
              el1 += `<img src=${item.profileImages} class="cardImage rounded-circle userProfileImageTag rImg${index + 1}" alt="Cinque Terre">`
              el1 += `</div>  <div class=\"nudgesuserProfileName\">`
              el1 += `<div class=\"nudgesrecipientName\"> ${item.profileNames}</div> `
              el1 += `<div class=\"suggestionMsg\">You met ${item.count} times during the last ${item.daysDiff} days`
              el1 += `</div>   <img class=\"nudegesListArrow \" src=\"../../docs/long-arrow-right.png\", >\n`
              el1 += `</div></div></div></div>`
              _collaborationImages.append(el1);
            }

          });
          recognitionLoader(false)
          // $("#numbering").hover(function (e) {
          //   $("#sugesstionspopup").show();
          //   e.stopPropagation();
          // });

          // $("#sugesstionspopup").click(function (e) {
          //   e.stopPropagation();
          // });

          // $(document).click(function () {
          //   $("#sugesstionspopup").hide();
          // });
        }
      },
      error: function error(error, xhr, textStatus) {
        document.getElementsByClassName("recognitionSuggestions")[0].style.display = "none";
        document.getElementsByClassName("Get_recognition_suggestions")[0].style.display = "none";
        recognitionLoader(false);
        console.log("sugg errorrrr")
      }
    });
  }
}

function getUpcomingCelebrationConfig() {
  let response;
  // Return false to show celebrations
  // Return true to NOT show celebrations
  try {
    $.ajax({
      url: pgDetails.baseAppUrl + "/api/achievers/fetchUpcomingCelebrationConfig",
      async: false,
      // timeout: 10000,
      headers: {
        authorization: pgDetails.userToken
      },
      data: {
          azureUserId: pgDetails.azureUserId,
          tenantId: pgDetails.tenantId,
      },
      crossDomain: false,
      success: function (data, textStatus, xhr) {
        if (data.items && data.items.length > 0) {
          // document.getElementsByClassName("celebrationsnudgesList")[0].style.display = "none";
          response = false;
        } else {
            respone = true;
        }
      },
      error: function error(error, xhr, textStatus) {
        response = true;
      }
    });
    return response;
  } catch (e) {
    return true;
  }
}
function closeSuggestionDialog(){
  $('#RecogSuggestionModalCenter').modal('hide');
  $("#RecogSuggestionModalOpen").hide();
  return false
}
function sendCalenderPermission(link,reload) {

  // call api and get info based on that either refresh the page or show the modal
  console.log("in sendCalenderPermission",link,reload)
  if(reload && reload === true){
    boostEventloader(true)
    window.location.reload();
    RecognitionSuggestionToggle(false)
  }
  else{
      // opening a modal
  document.getElementById("RecogSuggestionModalOpen").innerHTML=""
  $('#RecogSuggestionModalCenter').modal('show');
  $("#RecogSuggestionModalOpen").show();
  let el = `<div id="sugesstionModal">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h3>Recognition Suggestions</h3>
                      <button type="button" data-bs-toggle="modal"  aria-hidden="true" class="boostClickCross popupCloseBtn">&times;</span>
                    </div>
                    <div class="modal-body suggestionBody">
                      <div class="suggestionText">
                      The Achievers app would like to access basic calendar information such as meeting time, attendees, and schedules to provide recognition suggestions for you and your program.
                      </div>
                      <div class="boostClickPointsTxt">
                        <button type="button" onClick="closeSuggestionDialog()" class="btn noThanksButton">
                            No, Thanks
                        </button>
                        <button type="button" onClick="closeSuggestionDialog()" class="btn authorizeButton">
                            <a href="${link}" target="_blank">Authorize</a>
                        </button>
                    </div>
                    </div>
                  </div>
            </div>`;
  $("#RecogSuggestionModalOpen").append(el);
  $(".thirdDiv").show();
  }
}

function RecognitionSuggestionToggle(shouldHide) {
  try {
    $.ajax({
      url: pgDetails.baseAppUrl + "/api/achievers/updateIsRecognitionSuggestionHide",
      async: false,
      type: 'POST',
      headers: {
        authorization: pgDetails.userToken
      },
      data: {
          azureUserId: pgDetails.azureUserId,
          tenantId: pgDetails.tenantId,
          "isRecognitionSuggestionHide": shouldHide
      },
      JSON: true,
      crossDomain: false,
      success: function (data, textStatus, xhr) {
        // $("#hide_Suggestions").hide();
        if(data.message && data.message === "User not found"){
          // handle this
          console.log("user not found")
        }
        else{
          if(shouldHide){
            document.getElementsByClassName("recognitionSuggestions")[0].style.display = "none";
            document.getElementsByClassName("Get_recognition_suggestions")[0].style.display = "flex";
            $('.Get_recognition_suggestions').css('cursor', 'pointer');
            $(".Get_recognition_suggestions").click(function () {
              sendCalenderPermission(data.link,true)
            })
          }
        }
      },
      error: function error(error, xhr, textStatus) {
        console.log("hideSugg Err");
        response = true;
      }
    });
    return response;
  } catch (e) {
    return true;
  }
}

function commentsPost(newsfeedEventId, numberOfComments) {
  commentsData = document.getElementById(`comment-text-${newsfeedEventId}`).value;
  detailsCmmentData = document.getElementById(`comment-text-detailed-${newsfeedEventId}`)?document.getElementById(`comment-text-detailed-${newsfeedEventId}`).value: null
  commentsData = commentsData? commentsData : detailsCmmentData;
  isDetailedView = detailsCmmentData? true:false;
  try {
    $.ajax({
      url: pgDetails.baseAppUrl + "/api/achievers/createComment",
      async: false,
      type: 'POST',
      headers: {
        authorization: pgDetails.userToken
      },
      data: {
        azureUserId: pgDetails.azureUserId,
        tenantId: pgDetails.tenantId,
        newsfeedEventId:newsfeedEventId,
        comment: commentsData
      },
      JSON: true,
      crossDomain: false,
      success: function (data, textStatus, xhr) {

        if(isDetailedView) {
          document.getElementById(`comment-text-detailed-${newsfeedEventId}`).value = "";
          document.getElementById(`detailCommentText-${newsfeedEventId}`).innerHTML = `Comments - ${data.paging.totalItems}`
          data.items.forEach(function (commentslist) {
            console.log("comments Data commentslist");
            let el = `<div class="commentsList">
              <div class="col-lg-12">
              <p class="item-comment">${commentslist.comment}</p>
                <div class="comment-user-img">
                  <img src="${commentslist.user.profileImageUrl}" class="rounded-circle">
                  <span class="comment-username">${commentslist.user.name}</span>

                  <span class='dot'></span><span class="commented-date">${commentslist.dateCommented}</span>
                </div>
              </div>
            </div>`
            $(".all-comments-list").append(el);
          })
        }
        else {
       document.getElementById(`comment-text-${newsfeedEventId}`).value = "";

        var el = `<div class="commentsList">
                        <div class="col-lg-12">
                        <p class="item-comment">${data.items[0].comment}</p>
                          <div class="comment-user-img">
                            <img src="${data.items[0].user.profileImageUrl}" class="rounded-circle">
                            <span class="comment-username">${data.items[0].user.name}</span>
                            <span class='dot'></span><span class="commented-date">${data.items[0].dateCommented}</span>
                          </div>
                        </div>
                      </div>`
        $(".single-latest-comments-list").append(el);
        document.getElementById("commentText").innerHTML = `Comments - ${data.paging.totalItems}`
      }
      },
      error: function error(error, xhr, textStatus) {
        console.log("hideSugg Err", error);
        response = true;
      }
    });
    return response;
  } catch (e) {
    return true;
  }

}

function  getFeedDetailView(newsfeedEventId){
  staticTabloader(true, false);
  try {
    $.ajax({
      url: pgDetails.baseAppUrl + "/api/achievers/feedDetailInfo",
      // timeout: 10000,
      headers: {
          authorization: pgDetails.userToken
      },
      data: {
        authorization: pgDetails.userToken,
        newsfeedEventId: newsfeedEventId
      },

      type: 'GET',
      crossDomain: true,
      success: function (data, textStatus, xhr) {
        $(".feedDetailedView").html("");
        detailBoostpopup = true;
        staticTabloader(false, false);
        getFeedDetailComments(newsfeedEventId)
        $(".recentActivity").hide();
        $(".backtoRecentActivity").show();
        $(".feedDetailedView").show();
        $(".showMoreClass").show();
        // $(".filterDropdown").hide();
        // $(".recogniseFromMobileView").hide();
        let item = data;
        // let participantsText = "Participants are"
        const boostedImage  = item.boostedByCurrentMember ? `<img class="boostImage" id="boostImage-${item.id}" src="../../docs/boostPost.png" />` : `<img class="boostImage" id="boostImage-${item.id}" src="../../docs/boost.png" />`
        $(".activityArea").hide();
        $(".recentActivity").hide();
        $(".showMoreClass").hide();
        var el = `<div class="card cardClass">
        <div class="row participantDisplayPanel">
        <div class="participantsImagesDiv">`
       // item.participants.length && item.participants.forEach(function (imageItem, index) {
          //el += `<img src=${imageItem.profileImageUrl}  class="img-thumbnail recogniseInsightsRounded rImg${index + 1}">`
        // });
        // <button class="btn boostBtn" id="boost-post-${item.id}" type="button" data-bs-toggle="modal" data-bs-target="#exampleModalCenter" onclick="showBoostModal(${newsfeedEventId},'https://over.sandbox.achievers.com/event/117756927/abegmnoqruv13bcfghiklmrsuvwxz123?utm_medium=msteams&utm_term=customTab_comment','unlimited',0,false)">
        // ${boostedImage}
        // </button>
        el += `${item.profileImgComment}`
        if (item.participantsCount) {
          el += `<div class="recogniseInsightsRounded recognitionOthersDiv">+${item.participantsCount}</div>`
        }
        el += `<strong class="participantsRecText2"><text>${item.participantsText}</text></strong>`
        el += `</div>`
        el += item.imageHref ? `
        <div class="card-media-container">
        <div class="d-image-background-wrapper">
            <div class="ff-container ff-responsive">
                <div class="BlurPicture">
                <img class="crop-image" src=${item.imageHref}>
                </div>

            </div>
        </div>
        <img class="card-img-top" id="commentsAll" src=${item.imageHref}>
    </div>

        <div class="card-body">` : `<div class="card-body">`
        el += `<h5 class="card-title">${item.title}
        </h5><p class="card-text">${item.message}</p></div>
        <div class="userDisplayPanelBelow">
        <img src=${item.creator.profileImageUrl} class="rounded-circle userDisplayImage">
        <span class="creatorName"><a href="${item.creator.href}" target="_blank">${item.creator.name + "  "}</a><span class='dot'></span>${"  " + item.date}</span></div>
        <div class="commentsSection">
            <div class="col-sm-7" style="padding:0px !important">
                <div class='modal-dialog hoverDialog' id=${item.id} style="display:none">
                    <div class="modal-dialog boostHoverDialog">
                        <div class="card ${item.id}boost cardClassHover">
                            <div class="boostClassHover">
                                <div class="boostHoverImage">
                                    <img src="/docs/boostPost.png" class="boostedTextImage">
                                </div>
                                <span class="hasBoostedText">Has Boosted</span>
                            </div>
                            <div class="cardHoverBorder"></div>
                        </div>
                    </div>
                </div>
                <div class="boost-btn-group btn-group boostClass">
                  <button class="btn boostBtn" id="boost-post-${item.id}" type="button" data-bs-toggle="modal" data-bs-target="#exampleModalCenter" onclick="showBoostModal(${newsfeedEventId},'https://over.sandbox.achievers.com/event/117756927/abegmnoqruv13bcfghiklmrsuvwxz123?utm_medium=msteams&utm_term=customTab_comment','unlimited',0,false, detailBoostpopup)">
                  ${boostedImage}
                  </button>
                    <div class="dividerline"></div>
                    <button class="btn boostCount" id="boost-${item.id}" type="button" data-bs-target="#BoostModalCenter"  onclick="BoostModelCount(${item.id}, ${item.boosts.counts})">${item.boosts.counts ? "  " + item.boosts.counts : 0}</button>
                </div>
                <div class="like-btn-group btn-group likeClasspopup">
                    <button class="btn likeClass like" value="false" id="newsfeedLikeClickDetailePage-${item.id}" onclick="postLikesEvent(${item.id}, ${item.likes.counts})" type="button">
                        <i class="fa fa-heart-o likeImage" id="newsfeedLikeIconDetailPage-${item.id}" aria-hidden="true"/>
                    </button>
                    <div class="dividerline"></div>
                    <button class="btn likesCount"  type="button" data-bs-target="#LikesModalCenter" onclick="LikestModelCount(${item.id}, ${item.likes.counts})" id="newsfeedLikeCountDetailPage-${item.id}">${item.likes.counts ? "  " + item.likes.counts : 0}</button>
                </div>
            </div>
        </div>
        <div  class="comments-Section DeatilComments" id="comment-post-${item.id}">
            <div class="col-lg-12 comments-Count"><span id="detailCommentText-${newsfeedEventId}"> Comments - ${item.comments.counts ? "  " + item.comments.counts : 0}</span></div>
            <div class="commentSectionMain">
            <div class="col-lg-10 col-10  comments_input">
                <input type="text"  id="comment-text-detailed-${item.id}" class="form-control comments-place" placeholder="Leave a comment...">
                <button type="button" class="btn comment-send-icon commentMainSecBtn"  disabled='disabled'  id="comment-Btn-detailed-${item.id}" onclick="commentsPost(${item.id},${item.comments.counts})">
                <span class="sendBtn-label">Send</span> <i class="fa fa-paper-plane" aria-hidden="true"></i>
                </button>
              </div>
            </div>
            <div class="all-comments-list">

            </div>
        </div>

    </div>`;
        $(".feedDetailedView").append(el);
        if(item.boostedByCurrentMember){
          $(".boostClass").prop('disabled', true);
      }
      if(item.likedByCurrentMember === true) {
        var newsfeedLikeClickDetailePage = $("#newsfeedLikeClickDetailePage-" + newsfeedEventId);
        var newsfeedLikeIconDetailPage = $("#newsfeedLikeIconDetailPage-" + newsfeedEventId);
        var newsfeedLikeCountDetailPage = $("#newsfeedLikeCountDetailPage-" + newsfeedEventId);
        $(newsfeedLikeIconDetailPage).removeClass('fa-heart-o');
          $(newsfeedLikeIconDetailPage).addClass('fa-heart');
          $(newsfeedLikeClickDetailePage).removeClass('like').addClass('unlike');
          $(newsfeedLikeCountDetailPage).text(`${parseInt(numberOfLikes)}`);
       // DeatilsPagedefaultLikesTrue(item.likedByCurrentMember, item.id, item.likes.counts);
      }
        $(function() {
          $(`#comment-text-detailed-${item.id}`).keyup(function() {
            var inputTextLen = document.getElementById(`comment-text-detailed-${item.id}`).value;
            if(inputTextLen.length  >  0) {
              $(inputTextLen).eq(1).focus();
              $(`#comment-Btn-detailed-${item.id}`).prop('checked', false);
              $(`#comment-Btn-detailed-${item.id}`).prop('disabled', false);

            } else {
              //disable again if a field is cleared
              $(`#comment-Btn-detailed-${item.id}`).prop('checked', true);
              $(`#comment-Btn-detailed-${item.id}`).prop('disabled', true);
              $("input[value='']").eq(0).focus();
            }
          });
        });

      }
    })
  }
  catch(error) {
    console.log("errror", error);
    staticTabloader(false, false);
  }
}

function DeatilsPagedefaultLikesTrue(liked_by_user, newsfeedEventId, numberOfLikes) {
  var newsfeedLikeClickDetailePage = $("#newsfeedLikeClickDetailePage-" + newsfeedEventId);
        var newsfeedLikeIconDetailPage = $("#newsfeedLikeIconDetailPage-" + newsfeedEventId);
        var newsfeedLikeCountDetailPage = $("#newsfeedLikeCountDetailPage-" + newsfeedEventId);
        if(liked_by_user) {
          $(newsfeedLikeIconDetailPage).removeClass('fa-heart-o');
          $(newsfeedLikeIconDetailPage).addClass('fa-heart');
          $(newsfeedLikeClickDetailePage).removeClass('like').addClass('unlike');
          $(newsfeedLikeCountDetailPage).text(`${parseInt(numberOfLikes)+1}`);
        }
       else {
          $(newsfeedLikeIconDetailPage).addClass('fa-heart-o');
          $(newsfeedLikeIconDetailPage).removeClass('fa-heart');
          $(newsfeedLikeClickDetailePage).removeClass('unlike').addClass('like');
          $(newsfeedLikeCountDetailPage).text(`${numberOfLikes}`);
       }
              if(liked_by_user === undefined) {
          $(newsfeedLikeIconDetailPage).removeClass('fa-heart-o');
          $(newsfeedLikeIconDetailPage).addClass('fa-heart');
          $(newsfeedLikeClickDetailePage).removeClass('like').addClass('unlike');
          $(newsfeedLikeCountDetailPage).text(`${parseInt(numberOfLikes)+1}`);
        }
}

function  getFeedDetailComments(newsfeedEventId){

  try {
    $.ajax({
      url: pgDetails.baseAppUrl + "/api/achievers/feedDetailComments",
      // timeout: 10000,
      headers: {
          authorization: pgDetails.userToken
      },
      data: {
        authorization: pgDetails.userToken,
        newsfeedEventId: newsfeedEventId
      },
      type: 'GET',
      crossDomain: true,
      success: function (data, textStatus, xhr) {
       data.items.forEach(function (commentslist) {
        let el = `<div class="commentsList">
          <div class="col-lg-12">
          <p class="item-comment">${commentslist.comment}</p>
            <div class="comment-user-img">
              <img src="${commentslist.user.profileImageUrl}" class="rounded-circle">
              <span class="comment-username">${commentslist.user.name}</span>

              <span class='dot'></span><span class="commented-date">${commentslist.dateCommented}</span>
            </div>
          </div>
        </div>`
        $(".all-comments-list").append(el);
      })
      },
      error: function (xhr, textStatus) {
        staticTabloader(false, false)
      }
    });
  }
  catch (e) {
    console.log("error", e)
    load(false);
  }

}


function getgrpcNewsfeedInitial() {
    try {
      $.ajax({
        url: pgDetails.baseAppUrl + '/api/achievers/getNewsfeedConfig',
        // timeout: 10000,
        async: false,
        headers: {
          authorization: pgDetails.userToken
        },
        data: {
          userId: pgDetails.achieverMemberId,
          programId:pgDetails.achieverApplicationID,
          language_code: "en-US"
        },
        type: 'GET',
        crossDomain: true,
        success: function (data, textStatus, xhr) {
          const dataItems = data;
          let newArrayNewsFeedItems = [];
          let uniqueNewsFeedItemsObject = {};
          for (let i in dataItems.newsfeeds) {
              // Extract the title
              newsFeedItemsObjTitle = dataItems.newsfeeds[i]['name'];
              uniqueNewsFeedItemsObject[newsFeedItemsObjTitle] = dataItems.newsfeeds[i];
          }
          // Loop to push unique object into array
          for (i in uniqueNewsFeedItemsObject) {
            newArrayNewsFeedItems.push(uniqueNewsFeedItemsObject[i]);
          }
          newArrayNewsFeedItems.forEach(function (item) {
            el = `<li>
            <label class="radio-btn checkContainer">
              <span class="filterItems">  ${item.name}</span>
              <input type="checkbox" class="largecheckbox" name="acs" value='${item.newsfeed_id}' ${item.user_selected ? "checked" : ""}>
              <span class="checkmark"></span>
            </label>
            </li>`
            $(".filterSection").append(el);
          });
        },
        error: function (xhr, textStatus) {
          console.log("Error in getGrpcNewsfeedInitial : ",xhr, textStatus);
        }
      });
    }
    catch (e) {
      console.log("Error : ", e)
      load(false);
    }

}


function updateNewsfeedConfigurationOfUser() {
    try {
      const items = document.getElementsByName('acs');
      const selectedItems = [];
      for (let i = 0; i < items.length; i++) {
          if (items[i].type == 'checkbox' && items[i].checked) {
              selectedItems.push(items[i].value);
          }
      }
      $.ajax({
        url: pgDetails.baseAppUrl + '/api/achievers/getNewsfeedConfig',
        // timeout: 10000,
        async: false,
        type: 'POST',
        headers: {
          authorization: pgDetails.userToken
        },
        data: {
          userId: pgDetails.achieverMemberId,
          programId: pgDetails.achieverApplicationID,
          language_code: "en-US",
          newsfeedConfigPks: selectedItems.join(',')
        },
        crossDomain: true,
        success: function (data, textStatus, xhr) {
            setTimeout(() => {
                if (data.is_success) {
                  $("#regionBasedFilter").removeClass("show");
                  $("#dropdownMenuButtonMobile").removeClass("show");
                  $("#dropdownMenuButtonWeb").removeClass("show");
                  $("#dropdownMenuButtonWeb").addClass("hide");
                  $("#dropdownMenuButtonMobile").addClass("hide");
                  $("#regionBasedFilter").addClass("hide");
                    pageNumber = 0;
                    showMoreActivities();
                }
            }, 500);
        },
        error: function (xhr, textStatus) {
          console.log("error in updateNewsfeedConfigurationOfUser : ", xhr, textStatus);
        }
      });
    }
    catch (e) {
      console.log("Error in updateNewsfeedConfigurationOfUser : ", e)
      load(false);
    }

}
