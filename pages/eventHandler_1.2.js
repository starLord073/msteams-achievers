var pageNumber = ''
// image upload code

function imgUpload(event) {
    var fileSelect = document.getElementById('inputFile');
    if (fileSelect) {
        var files = event.files || event.dataTransfer.files;
        // Process all File objects
        for (var i = 0, f; f = files[i]; i++) {
            parseFile(f);
        }
    }
    function imgTagAddRemove() {
        $('div#gifimg > img').remove();
        document.getElementById('closebtn').classList.remove("close");
        document.getElementById('start').classList.remove("hidden");
        var image = document.createElement("IMG");
        image.alt = "Preview";
        image.setAttribute('class', 'hidden');
        image.setAttribute('id', 'file-image');
        image.src = "#";
        $('#gifimg').html(image);
    }

    // Output
    function parseFile(file) {
        var imageName = file.name;
        var ImageSize = file.size / 1024 / 1024;
        var imageType = (/\.(?=gif|jpg|png|jpeg)/gi).test(imageName);
        if (imageType) {
            if (ImageSize >= 10) {
                $("#errorMessageImage").text(global.userData.localizedText["Image_size_would_be_less_than_10_MB"]);
                $("#inputFile").val('');
                imgTagAddRemove();
            }
            else {
                document.getElementById('start').classList.add("hidden");
                // Thumbnail Preview
                $('#closebtn').add();
                document.getElementById('file-image').classList.remove("hidden");
                document.getElementById('file-image').src = URL.createObjectURL(file);
                pgDetails.uploadImageSrc = URL.createObjectURL(file);
                document.getElementById('closebtn').classList.add("close");
                $("#errorMessageImage").text("");
            }
        }
        else {
            $("#errorMessageImage").text(global.userData.localizedText["This_file_cannot_be_uploaded_The_file_type_is_not_supported_Supported_file_types_are_JPG_PNG_and_GIF"]);
            imgTagAddRemove();
        }
        document.getElementById("closebtn").onclick = function () {
            imgTagAddRemove();
            $("#inputFile").val('');
        }
    }
}

// post recognition preview

function showPreview() {
    $("#recogTypePlaceholder").text('"' + global.userData.criterion + '"');
    $("#previewRecogNominees").text(getCommaSeparatedString(getFullNames(global.userData.nomineesSelected)));
    $("#previewRecogMessage").text(global.userData.recogText);
    $("#previewRecogFrom").text(" " + pgDetails.userFullName);

    var selectId = $(".active").attr("data_id");
    let imageUrl = pgDetails.recognitionImage;

    if (selectId == '3') {
        pgDetails.imageSelected = 'upload';
        if ($('#gifimg > img').attr('src').length === 1) {
            alertWarningImg(global.userData.localizedText["You_must_upload_an_image"]);
        } else {
            imageUrl = pgDetails.uploadImageSrc;
            clearAllWarnings();
            viewPage("previewRecog");
        }

    } else if (selectId == '2') {
        pgDetails.imageSelected = 'giphy';

        if (!pgDetails.imageUrl) {
            alertWarningImg(global.userData.localizedText["You_must_select_an_image"]);
        } else {
            imageUrl = pgDetails.imageUrl;
            clearAllWarnings();
            viewPage("previewRecog");
        }

    } else {
        pgDetails.imageSelected = 'default';
        clearAllWarnings();
        viewPage("previewRecog");
    }
    document.getElementById("previewImage").innerHTML = '<img src="' + imageUrl + '" style="width:100%">';
}


// Points page

function showPreviewOrPoints(){
    $("#shareRecognition").hide();
    // points page
    if(showPointsScaleTab && pgDetails.pointsOptionsToDisplay.selectedModuleType === POINTBASED){
      if(pgDetails.showPointsUIOnly){
        viewPage("pointsDiv");
      }else{
        showPoints();
      }
    }else{
      showPreview();
    }
}

// poitns page

function showPoints(){
    var displayType = pgDetails.pointsOptionsToDisplay.displayOptionsSty;
    var miRange = pgDetails.pointsOptionsToDisplay.minRange;
    var maRange = pgDetails.pointsOptionsToDisplay.maxRange;
    var nomineesSelectedLength = pgDetails.pointsOptionsToDisplay.nomineesSelectedLength;

    $("#pointsAvailToShow").empty();
    var budgetObj = pgDetails.memberBudget.recognitionBudgets[global.userData.recognitionTypeId];
    if(budgetObj){
        $("#pointsAvailToShow").html(getLocalizedNumber(budgetObj.budgetRemaining.points));
        viewPage("pointsDiv");
    }

    $(POINTSDIVDROPDOWN).hide();
    $(POINTSDIVRADIO).hide();
    $(POINTSDIVTEXTBOX).hide();
    if(displayType === "Radio"){

        $(POINTSDIVRADIO).show();
        var optionsToDisplay = pgDetails.pointsOptionsToDisplay.options;
        var checked = false
        // Radio buttons design
        document.getElementById("selectPointsDivRadio").innerHTML = "";
        for(var i=0; i<optionsToDisplay.length; i++){
            if(!optionsToDisplay[i].defaultValue && i == 0){
                checked = true;
                global.userData.pointval = calPointsValue(optionsToDisplay[i].spentBudget);
            }else if(optionsToDisplay[i].defaultValue == true){
                checked = true;
                global.userData.pointval = calPointsValue(optionsToDisplay[i].spentBudget);
            }else{
                checked = false;
            }
            checkedVal = checked ? "checked" : ""
            $(POINTSDIVRADIO).append("<div><input class='pointsRadionIn' type='radio' "+
            checkedVal +" name='pointsRadioBtn' value="+optionsToDisplay[i].spentBudget+">"+
            "<label class='pointsRadioLabel'>"+optionsToDisplay[i].aboveText+"</label>"+
            "<p class='pointsRadioLabel2'>"+ optionsToDisplay[i].beneathText+"</p></div>");
        }
    }else if(displayType === "Dropdown"){
        document.getElementById("selectPointsType").innerHTML = "";
        var optionsToDisplay = pgDetails.pointsOptionsToDisplay.options;
        // Dropdown
        $(POINTSDIVDROPDOWN).show();
        var selected = false;
        for(var i=0;i<optionsToDisplay.length;i++){
            if(!optionsToDisplay[i].defaultValue && i == 0){
                selected = true;
                global.userData.pointval = calPointsValue(optionsToDisplay[i].spentBudget);
            }else if(optionsToDisplay[i].defaultValue == true){
                selected = true;
                global.userData.pointval = calPointsValue(optionsToDisplay[i].spentBudget);
            }else{
                selected = false
            }
            selectedVal = selected ? "selected" : ""
            $('#selectPointsType').append("<option value="+optionsToDisplay[i].spentBudget+" "+selectedVal+">"+optionsToDisplay[i].aboveTextDropdown+"</option>")
            if(selected){
                document.getElementById("calPointsDropdown").innerHTML=""
                // $("#calPointsDropdown").append(nomineesSelectedLength+" recipients = <b>"+
                // optionsToDisplay[i].spentBudget * nomineesSelectedLength+" points </b>");
                var calPointsDropdownData =  getTranslation("NUMBER_recipients",[nomineesSelectedLength]) +
                " = "+ getTranslation("POINTS_points",
                [getLocalizedNumber(optionsToDisplay[i].spentBudget * nomineesSelectedLength)]);
                $("#calPointsDropdown").append(calPointsDropdownData);
            }
        }

        $("#selectPointsType").selectric('refresh');
        $('#selectPointsType').selectric('init');
    }else if(displayType === "Textbox"){
        document.getElementById("pointsTextbox").value="";
        document.getElementById("pointsTextbox").innerHTML="";
        document.getElementById("allowedPoints").innerHTML="";
        document.getElementById("awardedPoints").innerHTML="";
        document.getElementById("calPoints").innerHTML="";
        // Textbox
        $(POINTSDIVTEXTBOX).show();
        $('#allowedPoints').append(getTranslation("Allowable_number_of_points_per_person") + "<b>" +
        getLocalizedNumber(miRange)+ " - "+ getLocalizedNumber(maRange) + "</b>");
        $('#awardedPoints').append(global.userData.localizedText["Total_points_you_are_awarding"] +
        "<b>" +" - " + "</b>");
        // $('#calPoints').append("("+nomineesSelectedLength+" recipients * " +
        // "<b>" +"0 points" + "</b>"+")");
        var calPointsData = "("+getTranslation("NUMBER_recipients",[nomineesSelectedLength]) +
        " x "+ getTranslation("POINTS_points",[0])+")";
        $('#calPoints').append(calPointsData);
    }
}

// points page
function showPointsBckBtn(){
    // using this flag will distinguish whether we already visited points page or not && load only ui
    pgDetails.showPointsUIOnly = true;
    viewPage('shareRecognition');
}



// points page
function afterPointsSelected(){
    // using this flag will distinguish whether we already visited points page or not && load only ui
    pgDetails.showPointsUIOnly = true;
    if (pagePointsValid()) {
        showPreview();
    }
}


// points page
function calPointsValue(spentBudget){
    // Budget
    return spentBudget ;
}


function newsfeedSignOut() {
    newdfeedSignoutHandler(false);
}
function hideBoostsInfo(newsfeedEventId){
    $(`#${newsfeedEventId}`).hide()
    boostHovered= false
}

// News Feed Activity
function mapNewsFeedActivity(newsFeedActivitiesData,nextCursor){

  const usersCommentsEnabled = pgDetails.configFile.Static_tab_show.userPkEnabled;

  if(newsFeedActivitiesData.length){
    newsFeedActivitiesData.forEach(function (item) {
      $(".backtoRecentActivity").hide();
      const boostedImage  = item.boosted_by_user ? `<img class="boostImage" id="boostImage-${item.newsfeedEventId}" src="../../docs/boostPost.png" />` : `<img class="boostImage" id="boostImage-${item.newsfeedEventId}" src="../../docs/boost.png" />`;

      var el =`<div class="card cardClass">
                    <div class="participantDisplayPanel">
                    <div class="participantsImagesDiv">`;
      item.participantProfileImages.length && item.participantProfileImages.forEach(function (imageItem, index) {
        el+=`<img src=${imageItem}  class="img-thumbnail recogniseInsightsRounded rImg${index + 1}" alt="Cinque Terre">`;
      });
      if(item.participantsCount){
        el+=`<div class="recogniseInsightsRounded recognitionOthersDiv">+${item.participantsCount}</div>`;
      }
      el+= item.participantProfileImages.length>2 ? `<strong class="participantsRecText2"><text>${item.participantsText}</text></strong>` : `<strong class="participantsRecText"><text>${item.participantsText}</text></strong>`;
      el+= `</div>`;
      el+= item.recognitionImg && item.event_type == "MilestoneEvent" ? `<div class="feedItem"><a href="${item.SignCardUrl}" target="_blank"><img class="card-img-top milestoneCardImage" id="commentsAll" src=${item.recognitionImg}></a><div class="card-body">`: `<div class="feedItem"  onclick="CommentmapNewsFeedActivity(${item.newsfeedEventId})">
        <div class="card-media-container">
            <div class="d-image-background-wrapper">
                <div class="ff-container ff-responsive">
                    <div class="BlurPicture">
                    <img class="crop-image" src=${item.recognitionImg}>
                    </div>

                </div>
            </div>
            <img class="card-img-top" id="commentsAll" src=${item.recognitionImg}>
        </div>
        <div class="card-body">`;
      el+=`<h5 class="card-title">${item.title}
                    </h5><p class="card-text">${item.message}</p></div></div>
                    <div class="userDisplayPanelBelow">`;
      el+= item.event_type === "PostcardEvent" ? `<img src=${item.userImage} class="rounded-circle userDisplayImage"><span class="creatorName"><a href="${item.createrProfileUrl}" target="_blank">${item.userFullName+"  "}</a><span class='dot'></span><span class="createrDate">${"  "+item.recognizedDate}</span></span></div>` : `<span class="creatorName celebration_card">Aspire<span class='dot'></span>${"  "+item.recognizedDate}</span></div>`;
      el+= `<div class="commentsSection">
                    <div class="col-sm-12" style="padding:0px !important">
                        <div class='modal-dialog hoverDialog' id=${item.newsfeedEventId} style="display:none">
                            <div class="modal-dialog boostHoverDialog">
                                <div class="card ${item.newsfeedEventId}boost cardClassHover">
                                    <div class="boostClassHover">
                                        <div class="boostHoverImage">
                                            <img src="/docs/boostPost.png" class="boostedTextImage">
                                        </div>
                                        <span class="hasBoostedText">Has Boosted</span>
                                    </div>
                                    <div class="cardHoverBorder"></div>
                                </div>
                            </div>
                        </div>
                        <div class="boost-btn-group btn-group boostClass">
                            <button class="btn boostBtn" id="boost-post-${item.newsfeedEventId}" type="button" ${item['can_boost'] ? "" : "disabled"} data-bs-toggle="modal" data-bs-target="#exampleModalCenter" onclick="showBoostModal(${item.newsfeedEventId},'${item.newsFeedURLComment}','${item.totalBudget}',${item.numberOfBoosts},${item.boosted_by_user}, detailBoostpopup)">
                                ${boostedImage}
                            </button>
                            <div class="dividerline"></div>
                            <button class="btn boostCount" id="boost-${item.newsfeedEventId}" type="button" data-bs-target="#BoostModalCenter"  onclick="BoostModelCount(${item.newsfeedEventId}, ${item.numberOfBoosts})">${item.numberOfBoosts?"  "+item.numberOfBoosts:0}</button>
                        </div>
                        <div class="like-btn-group btn-group likeClasspopup">
                            <button class="btn likeClass like" value="false" id="newsfeedLikeClick-${item.newsfeedEventId}" onclick="likesOnPost(${item.newsfeedEventId}, ${item.numberOfLikes})" type="button">
                                <i class="fa fa-heart-o likeImage" id="newsfeedLikeIcon-${item.newsfeedEventId}" aria-hidden="true"/>
                            </button>
                            <div class="dividerline"></div>
                            <button class="btn likesCount" id="likes-${item.newsfeedEventId}"  type="button" data-bs-target="#LikesModalCenter" onclick="LikestModelCount(${item.newsfeedEventId}, ${item.numberOfLikes})" id="newsfeedLikeCount-${item.newsfeedEventId}">${item.numberOfLikes?"  "+item.numberOfLikes:0}</button>
                        </div>`;
      el+= item.event_type === "MilestoneEvent" ? `<div class="signcard-btn-group btn-group signcardpop">
                                    <a class="btn sign_card" href="${item.SignCardUrl}" arial-label="Sign card" target="_blank">Sign card</a>
                                </div>
                            </div>
                        </div>`: ` </div>
                        </div>`;

      el+= item.event_type === "MilestoneEvent"?``:`
                    <div  class="comments-Section showComments" id="comment-post-${item.newsfeedEventId}">
                        <div class="col-lg-12 comments-Count"><span> Comments - ${item.numberOfComments?"  "+item.numberOfComments:0}</span>`;

      el +=    item.numberOfComments >= 1 ? `<div class="commentsSeeAll" onclick="CommentmapNewsFeedActivity(${item.newsfeedEventId})">
                                <span class="See_All">See all</span>
                                <img class="imgArraow" src="../../docs/long-arrow-right.png" alt="">
                            </div>` : ``;
      el += `</div>`;

      el += item.event_type === "MilestoneEvent"? ``: `<div class="commentSectionMain">
                                    <div class="col-lg-10 comments_input">
                                     <input type="text"  id="comment-text-${item.newsfeedEventId}" class="form-control comments-place commentMainSecInput" placeholder="Leave a comment...">
                                    </div>
                                    <button type="button" class="btn comment-send-icon commentMainSecBtn"  disabled='disabled'  id="comment-textBtn-${item.newsfeedEventId}" onclick="commentBtn(${item.newsfeedEventId}, ${item.numberOfComments})">
                                          <span class="sendBtn-label">Send</span><i class="fa fa-paper-plane"aria-hidden="true"></i>
                                    </button>
                            </div>
                        </div>
                        <div class="single-latest-comments-list">

                        </div>

                    </div>
                </div>`;
      $(".activityArea").append(el);
      // if(usersCommentsEnabled.indexOf(pgDetails.achieverMemberId) > -1){
      $(".comments-Section").removeClass("showComments");
      // }

      if(item.boosted_by_user){
        $(".boostClass").prop('disabled', true);
      }
      if (item.liked_by_user) {
        defaultLikesTrue(item.liked_by_user, item.newsfeedEventId, item.numberOfLikes);
      }



      $(function() {
        $(`#comment-text-${item.newsfeedEventId}`).keyup(function() {
          var inputTextLen = document.getElementById(`comment-text-${item.newsfeedEventId}`).value;
          if(inputTextLen.length  >  0) {
            $(inputTextLen).eq(1).focus();
            $(`#comment-textBtn-${item.newsfeedEventId}`).prop('checked', false);
            $(`#comment-textBtn-${item.newsfeedEventId}`).prop('disabled', false);

          } else {
            //disable again if a field is cleared
            $(`#comment-textBtn-${item.newsfeedEventId}`).prop('checked', true);
            $(`#comment-textBtn-${item.newsfeedEventId}`).prop('disabled', true);
            $("input[value='']").eq(0).focus();
          }
        });
      });

    });
    pageNumber = nextCursor;
  }
}

function commentBtn(newsfeedEventId, numberOfComments){
    commentsPost(newsfeedEventId, numberOfComments);
}

function CommentmapNewsFeedActivity(newsfeedEventId) {
    getFeedDetailView(newsfeedEventId);
}

function defaultLikesTrue(liked_by_user, newsfeedEventId, numberOfLikes) {
  var newsfeedLikeClick = $("#newsfeedLikeClick-" + newsfeedEventId);
  var newsfeedLikeIcon = $("#newsfeedLikeIcon-" + newsfeedEventId);
  var newsfeedLikeCount = $("#newsfeedLikeCount-" + newsfeedEventId);
  if (liked_by_user) {
    $(newsfeedLikeIcon).removeClass('fa-heart-o');
    $(newsfeedLikeIcon).addClass('fa-heart');
    $(newsfeedLikeClick).removeClass('like').addClass('unlike');
    $(newsfeedLikeCount).text(`${numberOfLikes}`);
  }
  else {
    $(newsfeedLikeIcon).addClass('fa-heart-o');
    $(newsfeedLikeIcon).removeClass('fa-heart');
    $(newsfeedLikeClick).removeClass('unlike').addClass('like');
    $(newsfeedLikeCount).text(`${numberOfLikes}`);
  }
}

// number of likes


function likesOnPost(newsfeedEventId, numberOfLikes){
    postLikesEvent(newsfeedEventId, numberOfLikes);
}

// function likesOnPost(){
//   $.ajax({
//     url: "https://example.sandbox.achievers.com/api/v5/newsfeed-events/71440601/likes",
//     // url: "https://" + pgDetails.subDomain + process.env.ACHIEVER_API_BASE_URI + "/newsfeed-events/" + newsFeedEventId + "/"+ actionType,
//     timeout: 10000,
//     headers: {
//       authorization: "Bearer " + pgDetails.achieverAccessToken
//     },
//     data: {
//       userToken: "Bearer " + pgDetails.achieverAccessToken
//     },
//     type: 'POST',
//     crossDomain: true,
//     success: function (data) {
//       console.log("sucessssss");
//       $('.likeImage').removeClass('fa-heart-o');
//       $('.likeImage').addClass('fa-heart');
//       $('.likeImage').on('click', function(e){
//         e.preventDefault();
//         var anchorLink = $(this);
//         anchorLink.toggleClass('fa-heart');
//         var text = anchorLink.hasClass('fa-heart') ? 'fa-heart' : 'fa-heart-o'
//         anchorLink.addClass("fa-heart-o");
//       });
//     },

//     error: function (resp, textStatus, errorMessage) {
//     }
//   });
// }

// News feed loader
function staticTabloader(changeSwitch,showMoreFlag){
    if (showMoreFlag) {
        $("#showMoreloaderId").show();
        $("#showMoreButtonId").hide();
    }
    if(!showMoreFlag){
        $("#showMoreloaderId").hide();
        $("#showMoreButtonId").show();
    }
    changeSwitch ?   $("#staticTabloaderId").show() :$("#staticTabloaderId").hide()
}
function boostEventloader(showLoader){
    showLoader ?   $("#boostEventloaderId").show() :$("#boostEventloaderId").hide()
}

function recognitionLoader(showLoader) {
    showLoader ? $("#recognitionloaderId").show(): $("#recognitionloaderId").hide()
}
function celebrationLoader(showLoader) {
    showLoader ? $("#celebrationloaderId").show(): $("#celebrationloaderId").hide()
}

function hidePopupTest() {
    var hide_tooltip = document.getElementById("hide_recognition_popup");
    hide_tooltip.classList.toggle("show");
}
// dropdown filter code
function unCheckFilterSelection(){
    var items=document.getElementsByName('acs');
    for(var i=0; i<items.length; i++){
        if(items[i].type=='checkbox')
            items[i].checked=false;
    }
}

$(document).on('click', '.allow-focus', function (e) {
    e.stopPropagation();
  });
function recSuggestionMobile() {
    if(pgDetails.showLink) {
        sendCalenderPermission(pgDetails.link, pgDetails.dataReload);
        $(".activityArea").show();
        $(".recentActivity").show();
        $(".filterDropdown").hide();
        $(".celebrationsnudgesList").hide();
        $(".showMoreClass").hide();
        $(".backtoRecentActivity").hide();

        $(".recogniseFromMobileView").hide();
        $(".feedDetailedView").hide();
    }
    else {
        $(".recognitionSuggestions").show();
        $(".activityArea").hide();
        $(".recentActivity").hide();
        $(".filterDropdown").hide();
        $(".celebrationsnudgesList").hide();
        $(".showMoreClass").hide();
        $(".backtoRecentActivity").show();
        $(".recogniseFromMobileView").hide();
        $(".feedDetailedView").hide();
    }

//   $(".activityArea").show();
//   $(".recentActivity").hide();
//   $(".filterDropdown").hide();
//   $(".celebrationsnudgesList").hide();
//   // $(".ProfilSection").hide();
//   $(".showMoreClass").hide();
//   $(".backtoRecentActivity").hide();

//   $(".recogniseFromMobileView").hide();
//   $(".feedDetailedView").hide();
}


function upcomingCelMobile() {
  $(".activityArea").hide();
  $(".recentActivity").hide();
  $(".filterDropdown").hide();
  $(".recognitionSuggestions").hide();

  // $(".ProfilSection").hide();
  $(".showMoreClass").hide();
  $(".backtoRecentActivity").show();
  $(".celebrationsnudgesList").show();
  $(".recogniseFromMobileView").hide();
  $(".feedDetailedView").hide();
}
