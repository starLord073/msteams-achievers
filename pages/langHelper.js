function getTranslations(locale) {

    locale = locale.toLowerCase();

    const supportedLocales = ["en-us", "en-au", "en-gb", "es-mx", "fr-ca", "fr-fr", "zh-cn"]
    if (!supportedLocales.includes(locale)) {
        locale = "en-us";
    }

    const localeToFileLookUp = {
        "en-us": "msteams_connect_en-US.json",
        "en-au": "translated/msteams_connect_en-AU.json",
        "en-gb": "translated/msteams_connect_en-GB.json",
        "es-mx": "translated/msteams_connect_es-MX.json",
        "fr-ca": "translated/msteams_connect_fr-CA.json",
        "fr-fr": "translated/msteams_connect_fr-FR.json",
        "zh-cn": "translated/msteams_connect_zh-CN.json"

    }
    var jsonData = null;
    $.ajax({
        'async': false,
        'global': false,
        'url': pgDetails.baseAppUrl + `/languages/${localeToFileLookUp[locale]}`,
        'dataType': "json",
        'type': 'GET',
        'success': function (data) {
            jsonData = data;
        }
    });
    return jsonData;
}


function changeHTML(source) {
    // console.log("In changeHTML function")
//     var locale = pgDetails.locale || "en-US";
//     locale = "en-us"
//     console.log("LANGUAGE : ", locale)
    const divIds = [
        "Create_a_recognition", //2
        "To",
        "Enter_a_name",
        "Add_channel_members",
        "Some_members_in_this_group_may_not_be_eligible_to_receive_recognitions_They_will_not_be_added_to_the_recipient_list",
        "Remove_all",
        "Recipients",
        "Recognition_type",
        "Recognition_value",
        
        "Sign_out",
        "Next",

        "Recognition_message",
        "Give_a_reason_for_this_recognition",
        "Back",
        "Continue",
        "Choose_an_image",
        "Default",
        "GIF",
        "Upload",
        "Search_GIFs",
        "Search_term",
        "Upload_an_image",
        "File_size_limit_is_10MB",
        "Share_Recognition",
        "Share_with_recipients_managers",
        "Who_else_should_know",
        "Share_with",
        "Preview",
        "Preview_recognition",
        "Recognized_for",
        "From",
        "Post",
        "You_have_been_successfully_signed_out_Please_close_this_window",
        "Your_session_has_expired_To_continue_please_close_this_window_and_reopen_the_Achievers_app",
        "Add_points", //points page
        "Select_points_to_add_per_person",
        "Enter_number_of_points_each",
        "Points_available_to_award" 

    ]
    const placeHolders = [
        "Enter_a_name",
        "Give_a_reason_for_this_recognition",
        "Search_term"
    ];
    const staticTabDivs = [
        "Recent_Activity",
        "Show_More",
        "fall_back_card_above_text",
        "fall_back_card_below_text",
        "Learn_More",
        "See_Profile",
        "Create_Recognition",
        "Get_recognition_suggestions",
        "Recognition_Suggestions",
        "Hide_Suggestions",
        "Celebrations",
        "See_All",
        "Sign_out"
    ]
    var langData = global.userData.localizedText;
    var fallbackLang = global.userData.fallbackStrings;

    pgDetails.langData = langData;
    pgDetails.fallBackLang_EN_US = fallbackLang;
    if(source === "popuptab"){
        for (const divId of divIds) {
            if (placeHolders.includes(divId)) {
                // console.log("Changing placeHolder");
                if (langData[divId]) {
                    for (var element of document.getElementsByClassName(divId)) {
                        element.placeholder = langData[divId];
                    }
                }
                else {
                    // console.log("CHECK LOCALIZATION FOR : ", divId);
                    for (var element of document.getElementsByClassName(divId)) {
                        element.placeholder = fallbackLang[divId];
                    }
                }
            }
            else {
                // console.log(document.getElementsByClassName(divId)[0].innerHTML)
                if (langData[divId]) {
                    for (var element of document.getElementsByClassName(divId)) {
                        element.innerHTML = element.innerHTML.replace(fallbackLang[divId], langData[divId]);
                    }
                }
                else {
                    // console.log("CHECK LOCALIZATION FOR : ", divId);
                    for (var element of document.getElementsByClassName(divId)) {
                        element.innerHTML = element.innerHTML.replace(fallbackLang[divId], fallbackLang[divId]);
                    }
                }
                // console.log(document.getElementsByClassName(divId)[0].innerHTML)
            }
        }
    } else if(source === "hometab"){
        for(const staticTabDiv of staticTabDivs){
            if (staticTabDivs.includes(staticTabDiv)) {
                if (langData[staticTabDiv]) {
                    for (var element of document.getElementsByClassName(staticTabDiv)) {
                        element.innerHTML = element.innerHTML.replace(fallbackLang[staticTabDiv], langData[staticTabDiv]);
                    }
                }
            }
        }
    }
    
    // console.log("Translation successful!");
}



function getTranslation(key, args=[]) {
    // console.log("In get translation language. Language is : ", this.userLang, "and key and value  are : ", key, this.langMapper[key]);
    if (args.length > 0) {
      let keyValue =  pgDetails.langData[key] || pgDetails.fallBackLang_EN_US[key];
      const pattern = new RegExp(/{[^{}]*}/g);
      const keysToChange = keyValue.match(pattern);
    //   console.log("Keys to change : ",keysToChange);
      for (let i = 0; i < keysToChange.length; i++){
          keyValue = keyValue.replace(keysToChange[i], args[i]);
      }
      return keyValue;
    } else {
      return pgDetails.langData[key] || pgDetails.fallBackLang_EN_US[key];
    }
        
}

function getLocalizedNumber(num) {
    if (typeof num != "number") {
        try {
        num = parseInt(num);
        return num.toLocaleString(pgDetails.locale || "en-US");
        }
        catch (err) {
        return num
        }
    }
    return num.toLocaleString(pgDetails.locale || "en-US");
}