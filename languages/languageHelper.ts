import * as enUS from "./msteams_connect_en-US.json"
import * as enAU from "./translated/msteams_connect_en-AU.json"
import * as enGB from "./translated/msteams_connect_en-GB.json"
import * as esMX from "./translated/msteams_connect_es-MX.json"
import * as frCA from "./translated/msteams_connect_fr-CA.json"
import * as frFR from "./translated/msteams_connect_fr-FR.json"
import * as zhCN from "./translated/msteams_connect_zh-CN.json"
export class LanguageHelper {
    langMapper: any;
    fallbackLang: any;
    userLang: any;
    constructor(userLanguage) {
        const supportedLangs = ["en-US", "en-AU", "en-GB", "es-MX", "fr-CA", "fr-FR", "zh-CN"];
        // const supportedLangs = ["en-US"]
        if (supportedLangs.includes(userLanguage)) {
            userLanguage = userLanguage
        }
        else {
            userLanguage = "en-US"
        }
        this.userLang = userLanguage || "en-US";
        // console.log("Setting language to ", this.userLang);
        this.setLanguage(this.userLang);
    }
    public setLanguage(locale) {
        this.fallbackLang = enUS;
        switch (locale) {
            case "en-US":
                this.langMapper = enUS;
                break;
            case "en-AU":
                this.langMapper = enAU;
                break;
            case "en-GB":
                this.langMapper = enGB;
                break;
            case "es-MX":
                this.langMapper = esMX;
                break;
            case "fr-CA":
                this.langMapper = frCA;
                break;
            case "fr-FR":
                this.langMapper = frFR;
                break;
            case "zh-CN":
                this.langMapper = zhCN;
                break;
            default:
                this.langMapper = enUS;
                break;
        }

    }
    public getTranslation(key, args = []) {
        // console.log("In get translation language. Language is : ", this.userLang, "and key and value  are : ", key, this.langMapper[key]);
        if (args.length > 0) {
            let keyValue;
            if (this.langMapper[key]) {
                keyValue = this.langMapper[key];
            }
            else {
                // console.log(`CHECK TRANSLATION FOR ${key} in language ${this.userLang}`)
                keyValue = this.fallbackLang[key];
            }
            const pattern = new RegExp(/{[^{}]*}/g);
            const keysToChange = keyValue.match(pattern);
            // console.log("Keys to change : ", keysToChange);
            for (let i = 0; i < keysToChange.length; i++) {
                keyValue = keyValue.replace(keysToChange[i], args[i]);
            }
            return keyValue;
        } else {
            if (this.langMapper[key]) {
                return this.langMapper[key];
            }
            else {
                // console.log(`CHECK TRANSLATION FOR ${key} in language ${this.userLang}`)
                return this.fallbackLang[key];
            }
        }

    }
}