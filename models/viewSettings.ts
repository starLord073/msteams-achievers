export class viewSettings {
    userFullName: string;
    userName: string;
    userId: string;
    tenantId: string;
    userEmail: string;
    userToken: string;
    baseAppUrl: string;
    getNames: string = "/api/achievers/userfetch";
    getRecogTypes: string = "/api/achievers/getrecogtypes";
    getNewsfeedActionsInfo: string = "/api/achievers/getnewsFeedActionsInfo";
    postrecog: string = "/api/achievers/postrecog";
    logout: string = "/api/achievers/logout";
    appId: string;
    curMember: string = "/api/achievers/curMember";
    imageSearch: string = "/api/achievers/imageSearch";
    newsFeedActivities: string = "/api/achievers/newsFeedActivities";
    proactiveLogout: string = "/api/achievers/sendLogoutSuccess";
    fetchCelebrationsOrRecognitions:string = "/api/achievers/fetchCelebrationsOrRecognitions";
    feedDetailView: string = "/api/achievers/feedDetailInfo";
    feedDetailComments: string = "/api/achievers/feedDetailComments";
    sentryDsnFe: string;
    constructor(_baseUrl,_appId){
        this.baseAppUrl = _baseUrl;
        this.appId = _appId;
    }
}