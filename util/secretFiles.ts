const fs = require('fs');
const directory = '/secrets/';
const filePath = '.env';

fs.closeSync(fs.openSync(filePath, 'a'))

function readFiles(dirname, onFileContent, onError) {
  fs.readdir(dirname, function (err, filenames) {
    if (err) {
      onError(err);
      return;
    }
    filenames.forEach(function (filename) {
      fs.readFile(dirname + filename, 'utf-8', function (err, content) {
        if (err) {
          onError(err);
          return;
        }
        onFileContent(filename, content);
      });
    });
  });
}
if (fs.existsSync(directory)) {

  readFiles(directory, (filename, content) => {

    // console.log("************ secretfile filename **************", filename);
    // console.log("************ secretfile content **************", content);

    fs.appendFileSync(filePath, filename.toUpperCase() + "=" + content +"\n");
    process.env[filename.toUpperCase()] = content;

  }, function (err) {
    // console.log("************ secretfile err **************", err);
  });
}

