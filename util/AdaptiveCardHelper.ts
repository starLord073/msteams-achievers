import { LanguageHelper } from "../languages/languageHelper"
export class CardHelper {
    constructor() { }
    getDeniedCard(userLocale, user1Name, user1Email, user2Name, user2Email) {
        const langHelper = new LanguageHelper(userLocale);
        const deniedCard = {
            contentType: "application/vnd.microsoft.card.adaptive",
            content: {
                $schema:
                    "http://adaptivecards.io/schemas/adaptive-card.json",
                type: "AdaptiveCard",
                version: "1.0",
                body: [
                    {
                        type: "ColumnSet",
                        columns: [
                            {
                                type: "Column",
                                items: [
                                    {
                                        type: "TextBlock",
                                        text: langHelper.getTranslation("Find_available_time_with_help_of_the_scheduling_assistant"),
                                        spacing: "Medium",
                                        wrap: true
                                    }
                                ],
                                width: "60"
                            }
                        ],
                        bleed: true
                    }
                ],
                actions: [
                    {
                        type: "Action.OpenUrl",
                        title: langHelper.getTranslation("Go_to_Scheduling_Assistant"),
                        url: encodeURI("https://teams.microsoft.com/l/meeting/new?subject=Coffee Chat | " + user1Name + " and " + user2Name + "&attendees=" + user2Email + "&content=" + langHelper.getTranslation("Lets_meet_for_a_Coffee_Chat"))
                    }
                ]
            }
        };
        return deniedCard;
    }
    getGlobalDeniedCard(userLocale) {
        const langHelper = new LanguageHelper(userLocale);
        const deniedCard = {
            contentType: "application/vnd.microsoft.card.adaptive",
            content: {
                $schema:
                    "http://adaptivecards.io/schemas/adaptive-card.json",
                type: "AdaptiveCard",
                version: "1.0",
                body: [
                    {
                        type: "ColumnSet",
                        columns: [
                            {
                                type: "Column",
                                items: [
                                    {
                                        type: "TextBlock",
                                        text: "Denied Calender Permission",
                                        spacing: "Medium",
                                        wrap: true
                                    }
                                ],
                                width: "60"
                            }
                        ],
                        bleed: true
                    }
                ]
            }
        };
        return deniedCard;
    }
    getGlobalAuthorizeCard(userLocale) {
        // const langHelper = new LanguageHelper(userLocale);
        const deniedCard = {
            contentType: "application/vnd.microsoft.card.adaptive",
            content: {
                $schema:
                    "http://adaptivecards.io/schemas/adaptive-card.json",
                type: "AdaptiveCard",
                version: "1.0",
                body: [
                    {
                        type: "ColumnSet",
                        columns: [
                            {
                                type: "Column",
                                items: [
                                    {
                                        type: "TextBlock",
                                        text: "Authorized Calender Permission",
                                        spacing: "Medium",
                                        wrap: true
                                    }
                                ],
                                width: "60"
                            }
                        ],
                        bleed: true
                    }
                ]
            }
        };
        return deniedCard;
    }
    getFallBackCard(userLocale, user1Name, user1Email, user2Name, user2Email) {
        const langHelper = new LanguageHelper(userLocale);
        const fallBackCard = {
            contentType: "application/vnd.microsoft.card.adaptive",
            content: {
                $schema:
                    "http://adaptivecards.io/schemas/adaptive-card.json",
                type: "AdaptiveCard",
                version: "1.0",
                body: [
                    {
                        type: "ColumnSet",
                        columns: [
                            {
                                type: "Column",
                                items: [
                                    {
                                        type: "TextBlock",
                                        text: langHelper.getTranslation("Find_available_time_with_help_of_the_scheduling_assistant"),
                                        spacing: "Medium",
                                        wrap: true
                                    }
                                ],
                                width: "60"
                            }
                        ],
                        bleed: true
                    }
                ],
                actions: [
                    {
                        type: "Action.OpenUrl",
                        title: langHelper.getTranslation("Go_to_Scheduling_Assistant"),
                        url: encodeURI("https://teams.microsoft.com/l/meeting/new?subject=Coffee Chat | " + user1Name + " and " + user2Name + "&attendees=" + user2Email + "&content=" + langHelper.getTranslation("Lets_meet_for_a_Coffee_Chat"))
                    }
                ]
            }
        };
        return fallBackCard;
    }
    getPermissionsCard(userLocale, btnUrl, user1Name, user1Email, user2Name, user2Email, useEmbeddedBrowser) {
        const langHelper = new LanguageHelper(userLocale);
        let permissionsCard;
        if (useEmbeddedBrowser) {
            permissionsCard = {
                "contentType": "application/vnd.microsoft.card.hero",
                "content": {
                    "title": "<b>" + langHelper.getTranslation("Scheduling_Assistant") + "</b>",
                    "text": langHelper.getTranslation("By_authorizing_the_Achiever_bot_to_access_basic_calendar_information_we_can_make_scheduling_easy_by_finding_common_time_with_your_Coffee_Chat_partner"),
                    "buttons": [
                        {
                            "type": "signin",
                            "title": langHelper.getTranslation("Authorize"),
                            "value": btnUrl
                        },
                        {
                            "type": "messageBack",
                            "title": langHelper.getTranslation("No_Thanks"),
                            "value": {
                                "source": "PERMISSION_CARD",
                                "calendarPermission": "DENIED",
                                "user1Name": user1Name,
                                "user1Email": user1Email,
                                "user2Name": user2Name,
                                "user2Email": user2Email,
                                "ccLocale": userLocale
                            }
                        }
                    ]
                }
            }
        }
        else {
            permissionsCard = {
                contentType: "application/vnd.microsoft.card.adaptive",
                content: {
                    $schema:
                        "http://adaptivecards.io/schemas/adaptive-card.json",
                    type: "AdaptiveCard",
                    version: "1.0",
                    body: [
                        {
                            type: "ColumnSet",
                            columns: [
                                {
                                    type: "Column",
                                    items: [
                                        {
                                            type: "TextBlock",
                                            text: "**" + langHelper.getTranslation("Scheduling_Assistant") + "**",
                                            spacing: "Large",
                                            size: "Large",
                                            wrap: true
                                        },
                                        {
                                            type: "TextBlock",
                                            text: langHelper.getTranslation("By_authorizing_the_Achiever_bot_to_access_basic_calendar_information_we_can_make_scheduling_easy_by_finding_common_time_with_your_Coffee_Chat_partner"),
                                            spacing: "Medium",
                                            wrap: true
                                        }
                                    ],
                                    width: "60"
                                }
                            ],
                            bleed: true
                        }
                    ],
                    actions: [
                        {
                            type: "Action.OpenUrl",
                            title: langHelper.getTranslation("Authorize"),
                            url: btnUrl
                        },
                        {
                            type: "Action.Submit",
                            title: langHelper.getTranslation("No_Thanks"),
                            data: {
                                "source": "PERMISSION_CARD",
                                "calendarPermission": "DENIED",
                                "user1Name": user1Name,
                                "user1Email": user1Email,
                                "user2Name": user2Name,
                                "user2Email": user2Email,
                                "ccLocale": userLocale
                            }
                        }
                    ]
                }
            };
        }
        return permissionsCard;
    }
    getCard(userLocale, cardMessage, actionsArrData) {
        const langHelper = new LanguageHelper(userLocale);
        const card = {
            contentType: "application/vnd.microsoft.card.adaptive",
            content: {
                $schema:
                    "http://adaptivecards.io/schemas/adaptive-card.json",
                type: "AdaptiveCard",
                version: "1.0",
                body: [
                    {
                        type: "ColumnSet",
                        columns: [
                            {
                                type: "Column",
                                items: [
                                    {
                                        type: "TextBlock",
                                        text: cardMessage,
                                        spacing: "Medium",
                                        wrap: true
                                    }
                                ],
                                width: "60"
                            }
                        ],
                        bleed: true
                    }
                ],
                actions: actionsArrData
            }
        };
        return card;
    }

    getNotificationsRecognitionCard(userLocale, mentionedText, appUserId, headline, cardMessage, btnName, btnUrl) {

        let card, mentionData;
        if (mentionedText.length > 0) {
            mentionData = {
                entities: [
                    {
                        type: "mention",
                        text: "<at>" + mentionedText + "</at>",
                        mentioned: {
                            id: appUserId,
                            name: mentionedText
                        }
                    }
                ]
            }
        } else {
            mentionData = {}
        }
        card = {
            contentType: "application/vnd.microsoft.card.adaptive",
            content: {
                $schema:
                    "http://adaptivecards.io/schemas/adaptive-card.json",
                type: "AdaptiveCard",
                version: "1.0",
                msteams: mentionData,
                body: [
                    {
                        type: "ColumnSet",
                        columns: [
                            {
                                type: "Column",
                                items: [
                                    {
                                        type: "TextBlock",
                                        text: "**" + headline + "**",
                                        size: "Large",
                                        wrap: true
                                    },
                                    {
                                        type: "TextBlock",
                                        text: cardMessage,
                                        spacing: "Medium",
                                        wrap: true
                                    }
                                ],
                                width: "60"
                            }
                        ],
                        bleed: true
                    }
                ],
                actions: [
                    {
                        type: "Action.OpenUrl",
                        title: btnName,
                        url: btnUrl
                    }
                ]
            }
        };
        return card;
    }

    getAnnouncementCard(mentionedText, appUserId, headline, cardMessageHead, imgUrl, cardMessage, btnName, btnUrl) {
        let card, mentionData;
        if (mentionedText.length > 0) {
            mentionData = {
                entities: [
                    {
                        type: "mention",
                        text: "<at>" + mentionedText + "</at>",
                        mentioned: {
                            id: appUserId,
                            name: mentionedText
                        }
                    }
                ]
            }
        } else {
            mentionData = {}
        }
        card = {
            contentType: "application/vnd.microsoft.card.adaptive",
            content: {
                $schema:
                    "http://adaptivecards.io/schemas/adaptive-card.json",
                type: "AdaptiveCard",
                version: "1.0",
                msteams: mentionData,
                body: [
                    {
                        type: "ColumnSet",
                        columns: [
                            {
                                type: "Column",
                                items: [
                                    {
                                        type: "TextBlock",
                                        text: "**" + headline + "**",
                                        size: "Large",
                                        wrap: true
                                    },
                                    {
                                        type: "TextBlock",
                                        text: cardMessageHead,
                                        spacing: "Medium",
                                        wrap: true
                                    },
                                    {
                                        type: "Image",
                                        url: imgUrl,
                                        size: "auto"

                                    },
                                    {
                                        type: "TextBlock",
                                        text: cardMessage,
                                        spacing: "Medium",
                                        wrap: true
                                    }
                                ],
                                width: "60"
                            }
                        ],
                        bleed: true
                    }
                ],
                actions: [
                    {
                        type: "Action.OpenUrl",
                        title: btnName,
                        url: btnUrl
                    }
                ]
            }
        };
        return card;
    }

    getNotificationsCoffeeChatCard(userLocale, mentionedText, appUserId, headline, cardMessage, cardActionDetails) {
        let card, mentionData;
        if (mentionedText.length > 0) {
            mentionData = {
                entities: [
                    {
                        type: "mention",
                        text: "<at>" + mentionedText + "</at>",
                        mentioned: {
                            id: appUserId,
                            name: mentionedText
                        }
                    }
                ]
            }
        } else {
            mentionData = {}
        }
        card = {
            contentType: "application/vnd.microsoft.card.adaptive",
            content: {
                $schema:
                    "http://adaptivecards.io/schemas/adaptive-card.json",
                type: "AdaptiveCard",
                version: "1.0",
                msteams: mentionData,
                body: [
                    {
                        type: "ColumnSet",
                        columns: [
                            {
                                type: "Column",
                                items: [
                                    {
                                        type: "TextBlock",
                                        text: "**" + headline + "**",
                                        size: "Large",
                                        wrap: true
                                    },
                                    {
                                        type: "TextBlock",
                                        text: cardMessage,
                                        spacing: "Medium",
                                        wrap: true
                                    }
                                ],
                                width: "60"
                            }
                        ],
                        bleed: true
                    }
                ],
                actions: cardActionDetails
            }
        };
        return card;

    }

    getNotificationspeerToPeerCard(userLocale, mentionedText, appUserId, headline, cardMessageSummary, cardMessage, btnName, btnUrl) {

        let card, mentionData;
        if (mentionedText.length > 0) {
            mentionData = {
                entities: [
                    {
                        type: "mention",
                        text: "<at>" + mentionedText + "</at>",
                        mentioned: {
                            id: appUserId,
                            name: mentionedText
                        }
                    }
                ]
            }
        } else {
            mentionData = {}
        }

        card = {
            contentType: "application/vnd.microsoft.card.adaptive",
            content: {
                $schema:
                    "http://adaptivecards.io/schemas/adaptive-card.json",
                type: "AdaptiveCard",
                version: "1.0",
                msteams: mentionData,
                body: [
                    {
                        type: "ColumnSet",
                        columns: [
                            {
                                type: "Column",
                                items: [
                                    {
                                        type: "TextBlock",
                                        text: "**" + headline + "**",
                                        size: "Large",
                                        wrap: true
                                    },
                                    {
                                        type: "TextBlock",
                                        text: cardMessageSummary,
                                        spacing: "Medium",
                                        wrap: true,
                                        weight: "bolder"
                                    },
                                    {
                                        type: "TextBlock",
                                        text: cardMessage,
                                        spacing: "Medium",
                                        wrap: true
                                    }
                                ],
                                width: "60"
                            }
                        ],
                        bleed: true
                    }
                ],
                actions: [
                    {
                        type: "Action.OpenUrl",
                        title: btnName,
                        url: btnUrl
                    }
                ]
            }
        };
        return card;
    }

    getNewPulseSurveyCard(appUserId, headline, cardMessage, btnName, btnUrl, endText, mentionedText, isManager = false, managerData = null) {
        let card, mentionData;
        if (mentionedText.length > 0) {
            mentionData = {
                entities: [
                    {
                        type: "mention",
                        text: "<at>" + mentionedText + "</at>",
                        mentioned: {
                            id: appUserId,
                            name: mentionedText
                        }
                    }
                ]
            }
        } else {
            mentionData = {}
        }
        if (isManager) {
            card = {
                contentType: "application/vnd.microsoft.card.adaptive",
                content: {
                    $schema:
                        "http://adaptivecards.io/schemas/adaptive-card.json",
                    type: "AdaptiveCard",
                    version: "1.0",
                    msteams: mentionData,
                    body: [
                        {
                            type: "TextBlock",
                            text: "**" + headline + "**",
                            size: "Large",
                            wrap: true
                        },
                        {
                            type: "TextBlock",
                            text: cardMessage,
                            spacing: "Medium",
                            wrap: true
                        },
                        {
                            type: "ActionSet",
                            actions: [
                                {
                                    type: "Action.OpenUrl",
                                    title: btnName,
                                    url: btnUrl
                                }
                            ]
                        },
                        {
                            "type": "TextBlock",
                            "wrap": true,
                            "text": endText,
                            "weight": "lighter",
                            "size": "small"
                        },
                        {
                            type: "TextBlock",
                            text: managerData['responseRateText'],
                            spacing: "Medium",
                            wrap: true,
                            weight: "bolder"
                        },
                        {
                            type: "TextBlock",
                            text: managerData['encourageEmployeesText'],
                            spacing: "Medium",
                            wrap: true
                        }
                    ],
                }
            };

        }
        else {
            card = {
                contentType: "application/vnd.microsoft.card.adaptive",
                content: {
                    $schema:
                        "http://adaptivecards.io/schemas/adaptive-card.json",
                    type: "AdaptiveCard",
                    version: "1.0",
                    msteams: mentionData,
                    body: [
                        {
                            type: "TextBlock",
                            text: "**" + headline + "**",
                            size: "Large",
                            wrap: true
                        },
                        {
                            type: "TextBlock",
                            text: cardMessage,
                            spacing: "Medium",
                            wrap: true
                        },
                        {
                            type: "ActionSet",
                            actions: [
                                {
                                    type: "Action.OpenUrl",
                                    title: btnName,
                                    url: btnUrl
                                }
                            ]
                        },
                        {
                            "type": "TextBlock",
                            "wrap": true,
                            "text": endText,
                            "weight": "lighter",
                            "size": "small"
                        }
                    ],
                }
            };
        }
        return card;
    }

    getPulseCheckInCard(appUserId, headline, cardMessage, btnNames, btnUrls, endText, mentionedText) {
        let card, mentionData;
        if (mentionedText.length > 0) {
            mentionData = {
                entities: [
                    {
                        type: "mention",
                        text: "<at>" + mentionedText + "</at>",
                        mentioned: {
                            id: appUserId,
                            name: mentionedText
                        }
                    }
                ]
            }
        } else {
            mentionData = {}
        }
        card = {
            contentType: "application/vnd.microsoft.card.adaptive",
            content: {
                $schema:
                    "http://adaptivecards.io/schemas/adaptive-card.json",
                type: "AdaptiveCard",
                version: "1.0",
                msteams: mentionData,
                "body": [
                    {
                        type: "TextBlock",
                        text: "**" + headline + "**",
                        size: "Large",
                        wrap: true
                    },
                    {
                        type: "TextBlock",
                        text: cardMessage,
                        spacing: "Medium",
                        wrap: true
                    },
                    {
                        type: "ColumnSet",
                        columns: [
                            {
                                type: "Column",
                                items: [
                                    {
                                        type: "Image",
                                        url: btnNames['angry'],
                                        selectAction: {
                                            type: "Action.OpenUrl",
                                            url: btnUrls['angry']
                                        }
                                    }
                                ]
                            },
                            {
                                type: "Column",
                                items: [
                                    {
                                        type: "Image",
                                        url: btnNames['sad'],
                                        selectAction: {
                                            type: "Action.OpenUrl",
                                            url: btnUrls['sad']
                                        }
                                    }
                                ]
                            },
                            {
                                type: "Column",
                                items: [
                                    {
                                        type: "Image",
                                        url: btnNames['happy'],
                                        selectAction: {
                                            type: "Action.OpenUrl",
                                            url: btnUrls['happy']
                                        }
                                    }
                                ]
                            },
                            {
                                type: "Column",
                                items: [
                                    {
                                        type: "Image",
                                        url: btnNames['excited'],
                                        selectAction: {
                                            type: "Action.OpenUrl",
                                            url: btnUrls['excited']
                                        }
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        type: "TextBlock",
                        wrap: true,
                        text: endText,
                        isSubtle: true,
                        weight: "lighter",
                        size: "small"
                    }
                ]
            }
        };
        return card;
    }

    getScheduleSessionCard(appUserId, headline, cardMessage, btnName, btnUrl, mentionedText) {
        let card, mentionData;
        if (mentionedText.length > 0) {
            mentionData = {
                entities: [
                    {
                        type: "mention",
                        text: "<at>" + mentionedText + "</at>",
                        mentioned: {
                            id: appUserId,
                            name: mentionedText
                        }
                    }
                ]
            }
        } else {
            mentionData = {}
        }
        card = {
            contentType: "application/vnd.microsoft.card.adaptive",
            content: {
                $schema:
                    "http://adaptivecards.io/schemas/adaptive-card.json",
                type: "AdaptiveCard",
                version: "1.0",
                msteams: mentionData,
                body: [
                    {
                        type: "TextBlock",
                        text: "**" + headline + "**",
                        size: "Large",
                        wrap: true
                    },
                    {
                        type: "TextBlock",
                        text: cardMessage,
                        spacing: "Medium",
                        wrap: true
                    },
                    {
                        type: "ActionSet",
                        actions: [
                            {
                                type: "Action.OpenUrl",
                                title: btnName,
                                url: btnUrl
                            }
                        ]
                    }
                ]
            }
        };
        return card;
    }

    getPulseSurveyClosureCard(appUserId, headline, cardMessage, btnName, btnUrl, mentionedText, isManager = false, managerData = null) {
        let card, mentionData;
        if (mentionedText.length > 0) {
            mentionData = {
                entities: [
                    {
                        type: "mention",
                        text: "<at>" + mentionedText + "</at>",
                        mentioned: {
                            id: appUserId,
                            name: mentionedText
                        }
                    }
                ]
            }
        } else {
            mentionData = {}
        }
        card = {
            contentType: "application/vnd.microsoft.card.adaptive",
            content: {
                $schema:
                    "http://adaptivecards.io/schemas/adaptive-card.json",
                type: "AdaptiveCard",
                version: "1.0",
                msteams : mentionData,
                body: [
                    {
                        type: "TextBlock",
                        text: "**" + headline + "**",
                        size: "Large",
                        wrap: true
                    },
                    {
                        type: "TextBlock",
                        text: cardMessage,
                        spacing: "Medium",
                        wrap: true
                    },
                    {
                        type: "TextBlock",
                        text: managerData['engagementScoreText'],
                        spacing: "Medium",
                        wrap: true,
                        weight: "bolder"
                    },
                    {
                        type: "TextBlock",
                        text: managerData['responseRateText'],
                        spacing: "Medium",
                        wrap: true,
                        weight: "bolder"
                    },

                    {
                        type: "TextBlock",
                        text: managerData['encourageEmployeesText'],
                        spacing: "Medium",
                        wrap: true
                    },
                    {
                        type: "ActionSet",
                        actions: [
                            {
                                type: "Action.OpenUrl",
                                title: btnName,
                                url: btnUrl
                            }
                        ]
                    },
                ],
            }
        };

        return card;
    }

    getNewPulseSurveyfollowupCard(appUserId, headline, cardMessage,  FollowupActionText, FollowupActionEmailText, btnName, btnUrl, followBtnUrl, mentionedText, managerData = null) {
        let card, mentionData;
        if (mentionedText.length > 0) {
            mentionData = {
                entities: [
                    {
                        type: "mention",
                        text: "<at>" + mentionedText + "</at>",
                        mentioned: {
                            id: appUserId,
                            name: mentionedText
                        }
                    }
                ]
            }
        } else {
            mentionData = {}
        }
            card = {
                contentType: "application/vnd.microsoft.card.adaptive",
                content: {
                    $schema:
                        "http://adaptivecards.io/schemas/adaptive-card.json",
                    type: "AdaptiveCard",
                    version: "1.0",
                    msteams: mentionData,
                    body: [
                        {
                            type: "TextBlock",
                            text: "**" + headline + "**",
                            size: "Large",
                            wrap: true
                        },
                        {
                            type: "TextBlock",
                            text: cardMessage,
                            spacing: "Medium",
                            wrap: true
                        },
                        {
                            type: "TextBlock",
                            text: "1. " + "" + FollowupActionText,
                            spacing: "small",
                            wrap: true
                        },
                        {
                            type: "TextBlock",
                            text: "2. " + "" + FollowupActionEmailText + '' + ` [Follow-up section of Action Builder](${followBtnUrl})`,
                            spacing: "small",
                            wrap: true,
                            actions: [
                                {
                                    type: "Action.OpenUrl",
                                    title: btnName,
                                    url: followBtnUrl
                                }
                            ]
                        },
                        {
                            type: "ActionSet",
                            actions: [
                                {
                                    type: "Action.OpenUrl",
                                    title: btnName,
                                    url: btnUrl
                                }
                            ]
                        },
                        {
                            "type": "TextBlock",
                            "wrap": true,
                            "weight": "lighter",
                            "size": "small"
                        }
                    ],
                }
            };

        return card;
    }
    getGlobalCalenderPermissionsCard(userLocale, btnUrl, useEmbeddedBrowser,mentionedText,appUserId){
        const langHelper = new LanguageHelper(userLocale);
        let card, mentionData;
        if (mentionedText.length > 0) {
            mentionData = {
                entities: [
                    {
                        type: "mention",
                        text: "<at>" + mentionedText + "</at>",
                        mentioned: {
                            id: appUserId,
                            name: mentionedText
                        }
                    }
                ]
            }
        } else {
            mentionData = {}
        }
        if(!useEmbeddedBrowser) {
            return {
                contentType: "application/vnd.microsoft.card.adaptive",
                content: {
                    $schema:
                        "http://adaptivecards.io/schemas/adaptive-card.json",
                    type: "AdaptiveCard",
                    version: "1.0",
                    msteams: mentionData,
                    body:[
                        {
                            type: "TextBlock",
                            text: "**" + langHelper.getTranslation("Recognition_Suggestions") + "**",
                            size: "Large",
                            wrap: true
                        },
                        {
                            type: "TextBlock",
                            text: "The Achievers app would like to access basic calendar information such as meeting time, attendees, and schedules to provide recognition suggestions for you and your program.",
                            spacing: "Medium",
                            wrap: true
                        }
                    ],
                    actions: [
                        {
                            type: "Action.OpenUrl",
                            title: langHelper.getTranslation("Authorize"),
                            url:btnUrl
                        },
                        
                        {
                            type: "Action.Submit",
                            title: langHelper.getTranslation("No_Thanks"),
                            data: {
                                "source": "GLOBAL_PERMISSION_CARD",
                                "calendarPermission": "DENIED"
                            }
                        }
                    ]
                }
            }
        }
    }
}