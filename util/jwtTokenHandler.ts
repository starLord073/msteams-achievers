import * as jwt from "jsonwebtoken";
const logger = require("./winlogger").logger;
import { newRelicErrLog, loggerLogData, consoleLogData } from "./logsHandler";

export const jwtVerify = (token:any) => {
    try {
        let publicKey: any;
        if(process.env.RSA_PUBLIC_KEY){
          publicKey = process.env.RSA_PUBLIC_KEY.replace(/\\n/g, '\n')
        } else {
          publicKey = process.env.CONNECTOR_SECRET_TOKEN;
      }
        let decodedValue
        jwt.verify(token, publicKey, (err: any, decoded: any) => {
          if (err) {  
            consoleLogData(false,"err with public key",err,true)  
            if (err.message === "invalid algorithm") {
              try {
                decoded = jwt.verify(
                  token,
                  process.env.CONNECTOR_SECRET_TOKEN
                );
                decodedValue = decoded;
              } catch (err) {
                consoleLogData(false,"err with secret token",err,true)  
                throw new Error(err.message)
              }
            }
            else{
              throw new Error(err.message);
            }
          } else {
            decodedValue = decoded;
          }
        });
        return decodedValue
      } catch (err) {
        consoleLogData(true,"****JwtVerify Err********",err,true)
        const sourceFunction = "utils/jwtTokenHandler.ts/jwtVerify"
        const subDomain = 'jwtVerifyErr'
        newRelicErrLog(err,sourceFunction,'')
        return Promise.reject({ message: "Unauthorized Error" })
      }
  };

  export const jwtSign = (payload: any) => {
    try {
      let token: any;
      let privateKey: any;
      if(process.env.RSA_SECRET_KEY){
        privateKey = process.env.RSA_SECRET_KEY.replace(/\\n/g, '\n');
      } else {
        privateKey = process.env.CONNECTOR_SECRET_TOKEN;
      }
      token = jwt.sign(payload, privateKey, {
        algorithm: (process.env.RSA_SECRET_KEY)? 'RS256': 'HS256',
        issuer: "achievers.com",
        expiresIn: (60 * 60 * 24) - 1,
        subject: "msteams-connect"
      });
      return token;
    } catch (err) {
        consoleLogData(true,"****JwrSign Err********",err,true)
        const sourceFunction = "utils/jwtTokenHandler.ts/jwtSign"
        const subDomain = 'jwtSignErr'
        newRelicErrLog(err,sourceFunction,'')
        return Promise.reject({ message: "Unauthorized Error" })
    }
};