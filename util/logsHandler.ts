const newrelic = require("newrelic");
const logger = require('./winlogger').logger;

let botAuth:any;

export const loggerLogData = ( shouldGoToPrd: boolean, descriptionToLog: string, data: any, isErr: boolean ) => {
  try {
    if ( !["production", "staging"].includes( process.env.APP_ENV ) ) {
      if ( isErr ) {
        if ( data ) {
          let errArr = ["try-catch-err", "msteams-connect", descriptionToLog, data.message];
          logger.error( errArr.join( " : " ) );
        }
        else {
          logger.error( descriptionToLog, data );
        }
      }
      else {
        logger.debug( descriptionToLog, data );
      }
    }
    if ( ["production", "staging"].includes( process.env.APP_ENV ) && shouldGoToPrd ) {
      const sourceFunction = "loggerLogData";
      const subDomain = "notFound";
      const logLevel = isErr ? "error" : "info";
      const description: any = {
        descriptionToLog,
        data

      };
      botAuth.saveLogData( JSON.stringify( description ), sourceFunction, subDomain, logLevel );
    }
  }
  catch ( err ) {
    const sourceFunction = "loggerLogData";
    const subDomain = "notFound";
    const description: any = {
      descriptionToLog,
      data
    };
    botAuth.saveLogData( JSON.stringify( description ), sourceFunction, subDomain, "error" );
  }
};
export const consoleLogData = ( shouldGoToPrd: boolean, descriptionToLog: string, data: any, isErr: boolean ) => {

  try {
    if ( !["production", "staging"].includes( process.env.APP_ENV ) ) {
      if ( isErr ) {
        console.error( descriptionToLog, data );
      }
      else {
        console.log( descriptionToLog, data );
      }
    }
    if ( ["production", "staging"].includes( process.env.APP_ENV ) && shouldGoToPrd ) {
      const sourceFunction = "consoleLogData";
      const subDomain = "notFound";
      const description: any = {
        description: descriptionToLog,
        data
      };
      const logLevel = isErr ? "error" : "info";
      botAuth.saveLogData( description, sourceFunction, subDomain, logLevel );
    }
  }
  catch ( err ) {
    const sourceFunction = "consoleLogData";
    const subDomain = "notFound";
    const description: any = {
      description: descriptionToLog,
      data
    };
    botAuth.saveLogData( JSON.stringify( description ), sourceFunction, subDomain, "error" );
  }
};
export const newRelicErrLog = ( error: any, filePath: string, attributes: any ) => {
  try {
    // ignore 401,403
    if ( error.statusCode && [401, 403].indexOf( parseInt( error.statusCode ) ) === -1 ) {
      if ( attributes )
        newrelic.noticeError( error, attributes );
      else
        newrelic.noticeError( error );
    }
  }
  catch ( err ) {
    const sourceFunction = "newRelicErrLog";
    const subDomain = "notFound";
    const description: any = {
      filePath,
      attributes
    };
    botAuth.saveLogData( JSON.stringify( description ), sourceFunction, subDomain, "error" );
  }
};
