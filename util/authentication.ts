import * as jwt from "jsonwebtoken";
import { jwtVerify } from './jwtTokenHandler';
const logger = require('./winlogger').logger;
const newrelic = require('newrelic')


export const checkIfAuthorized = async (req: any, res: any, next: any) => {
    try{
        const decoded = await jwtVerify(req.headers.authorization)
        next();
      }
      catch(err){
        return res.status(401).json({
            message: "Unauthorized Error"
        });
      }
    
};
