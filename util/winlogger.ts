const winston = require('winston');
const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, prettyPrint, json, colorize } = format;
require('winston-daily-rotate-file');
const dotenv = require('dotenv');
dotenv.config();

const transport = new (winston.transports.DailyRotateFile)({
    filename: 'Achievers_Teams-%DATE%.log',
    dirname: 'logs',
    datePattern: 'YYYY-MM-DD',
    zippedArchive: true,
    maxSize: process.env.APP_LOG_MAX_SIZE || '10m',
    maxFiles: process.env.APP_LOG_MAX_ARCHIVE_LIMIT || '60d',
    level: process.env.APP_LOG_LEVEL || 'info'
});

transport.on('rotate', function (oldFilename: string, newFilename: string) {
    //event trigger for file name change
});

exports.logger = winston.createLogger({
    format: combine(
        timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
          }),
        json()
      ),
    transports: [
        transport,
        new winston.transports.Console({ level: 'debug' }),
    ]
});