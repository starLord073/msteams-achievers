import { optional } from "@hapi/joi";
import { EROFS } from "constants";
import { loggers } from "winston";
import { LanguageHelper } from "../languages/languageHelper";
import * as countriesAndTimezones from "countries-and-timezones";
const logger = require('../util/winlogger.js').logger;
const newrelic = require('newrelic');

const jwtSimple = require('jwt-simple');
const request = require("request-promise");
let botAuth: any;
import { CardHelper } from "../util/AdaptiveCardHelper";

import { consoleLogData, loggerLogData, newRelicErrLog } from '../util/logsHandler';


function getUserTimeOffsets(user1Timezone, user2Timezone, timeOffSet: string): Array<number> {
  try {
    console.log("In getUserTimeOffsets");
    loggerLogData(true, "User timezones received are : ", { user1Timezone, user2Timezone }, false);
    let user1TimeOffset;
    let user2TimeOffset;
    if (user1Timezone && user2Timezone) {
      user1TimeOffset = countriesAndTimezones.getTimezone(user1Timezone);
      user2TimeOffset = countriesAndTimezones.getTimezone(user2Timezone);
      return [user1TimeOffset.utcOffset, user2TimeOffset.utcOffset];
    }
    else if (user1Timezone || user2Timezone) {
      if (user1Timezone) {
        user1TimeOffset = countriesAndTimezones.getTimezone(user1Timezone);
        return [user1TimeOffset.utcOffset, user1TimeOffset.utcOffset];
      } else if (user2Timezone) {
        user2TimeOffset = countriesAndTimezones.getTimezone(user2Timezone);
        return [user2TimeOffset.utcOffset, user2TimeOffset.utcOffset];
      }
    }
    else {
      let usersTimeOffSet: number;
      if (timeOffSet.indexOf('-') > -1) {
        timeOffSet = "-:" + timeOffSet.split("-")[1];
      } else if (timeOffSet.indexOf('+') > -1) {
        timeOffSet = "+:" + timeOffSet.split("+")[1];
      }
      if (timeOffSet.split(":")[0] == "-") {
        usersTimeOffSet = -1 * (parseInt(timeOffSet.split(":")[1]) * 60) + parseInt(timeOffSet.split(":")[2]);
      } else if (timeOffSet.split(":")[0] == "+") {
        usersTimeOffSet = 1 * (parseInt(timeOffSet.split(":")[1]) * 60) + parseInt(timeOffSet.split(":")[2]);
      }
      return [usersTimeOffSet, usersTimeOffSet];
    }

  } catch (error) {
    loggerLogData(true, "***COULD NOT GET ACTUAL TIMINGS. USING TEAMS TIMEZONE FOR BOTH USERS***", "", false);
    loggerLogData(true, "User timezones received are : ", { user1Timezone, user2Timezone }, false);
    let usersTimeOffSet: number;
    if (timeOffSet.indexOf('-') > -1) {
      timeOffSet = "-:" + timeOffSet.split("-")[1];
    } else if (timeOffSet.indexOf('+') > -1) {
      timeOffSet = "+:" + timeOffSet.split("+")[1];
    }
    if (timeOffSet.split(":")[0] == "-") {
      usersTimeOffSet = -1 * (parseInt(timeOffSet.split(":")[1]) * 60) + parseInt(timeOffSet.split(":")[2]);
    } else if (timeOffSet.split(":")[0] == "+") {
      usersTimeOffSet = 1 * (parseInt(timeOffSet.split(":")[1]) * 60) + parseInt(timeOffSet.split(":")[2]);
    }
    return [usersTimeOffSet, usersTimeOffSet];

  }


}

function findStartAndEndUserTimes(startDateUTC, endDateUTC, user1TimeOffset, user2TimeOffset) {
  console.log("In findStartAndEndUserTimes");
  // Find MS passed when it is 00:00 next day for user1, user2
  const user1NextDaySeconds = startDateUTC - (user1TimeOffset * 60000);
  const user2NextDaySeconds = startDateUTC - (user2TimeOffset * 60000);
  // Find MS passed when it is 00:00 next day+7 for user1, user2
  const user1LastDaySeconds = endDateUTC - (user1TimeOffset * 60000);
  const user2LastDaySeconds = endDateUTC - (user2TimeOffset * 60000);

  // Helper min, max functions
  const findMax = function (a, b) {
    return a < b ? b : a;
  };
  const findMin = function (a, b) {
    return a < b ? a : b;
  };

  // Start time is time in UTC of the user for whom it is midnight the next day at the earliest w.r.t UTC
  const reqStartTime = findMin(user1NextDaySeconds, user2NextDaySeconds);
  const reqEndTime = findMax(user1LastDaySeconds, user2LastDaySeconds);
  return [reqStartTime, reqEndTime];

}

function getUserFreeTimes(result, user1Email, user2Email, reqStartTime, serverTimeOffset, user1TimeOffset, user2TimeOffset) {
  console.log("In getUserFreeTimes");
  let availabilityViews = {};
  // Store availability views with email as key
  result.value.forEach(element => {
    availabilityViews[element.scheduleId] = (element.availabilityView);
  });
  // console.log("Availability views ", availabilityViews);
  // Get individual user views
  const user1View = availabilityViews[user1Email].split('');
  const user2View = availabilityViews[user2Email].split('');
  // console.log("User1, user2 views : ", user1View, user2View);
  let possibleSuggestions = {};
  for (let i = 0; i < user1View.length; i++) {

    // Finding the MS passed in user1,2 timezones at index 0 of the availability view respectively
    let timeUser1 = reqStartTime + (i * 30 * 60000) + serverTimeOffset + (user1TimeOffset * 60000);
    let timeUser2 = reqStartTime + (i * 30 * 60000) + serverTimeOffset + (user2TimeOffset * 60000);

    let user1FullDate = new Date(timeUser1);
    let user2FullDate = new Date(timeUser2);
    if (user1FullDate.getDay() != 0 && user1FullDate.getDay() != 6 && user1FullDate.getHours() >= 9 && user1FullDate.getHours() <= 16) {
      if (user2FullDate.getDay() != 0 && user2FullDate.getDay() != 6 && user2FullDate.getHours() >= 9 && user2FullDate.getHours() <= 16) {
        if (user1View[i] == '0' && user2View[i] == "0") {
          let d = new Date(timeUser1);
          // DO NOT convert to ISOString directly as we already added offsets while calculating timeUser1 and 2
          let sugg = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate() + "T" + d.getHours() + ":" + d.getMinutes() + ":00.000Z";
          d = new Date(timeUser1 + 30 * 60000);
          possibleSuggestions[sugg] = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate() + "T" + d.getHours() + ":" + d.getMinutes() + ":00.000Z";
        }
      }
    }
  }
  // console.log("Possible suggestions : ", possibleSuggestions);
  const allpossibleTimes = [];
  for (const key in possibleSuggestions) {
    allpossibleTimes.push([key, possibleSuggestions[key]]);
  }
  // console.log("All possible begin time slots : ", allpossibleTimes);
  const fourPossibleTimeSlots = [];
  while (fourPossibleTimeSlots.length < 4 && allpossibleTimes.length > 0) {
    let r = Math.floor(Math.random() * allpossibleTimes.length);
    fourPossibleTimeSlots.push(allpossibleTimes.splice(r, 1)[0]);
  }
  let sortFunction = function (a, b) {
    let d1 = Date.UTC(a[0].split('T')[0].split('-')[0], a[0].split('T')[0].split('-')[1], a[0].split('T')[0].split('-')[2], a[0].split('T')[1].split(':')[0], a[0].split('T')[0].split('-')[1]);
    let d2 = Date.UTC(b[0].split('T')[0].split('-')[0], b[0].split('T')[0].split('-')[1], b[0].split('T')[0].split('-')[2], b[0].split('T')[1].split(':')[0], b[0].split('T')[0].split('-')[1]);
    return d1 - d2;
  };
  fourPossibleTimeSlots.sort(sortFunction);
  // console.log("Four possible slots : ", fourPossibleTimeSlots);
  let scheduleMeetingDetails = {};
  fourPossibleTimeSlots.forEach(ele => {
    // console.log(ele);
    scheduleMeetingDetails[ele[0]] = ele[1];
  });
  // console.log("Schedule meeting details : ", scheduleMeetingDetails);
  return scheduleMeetingDetails;
}

function getCardData(userLocale, possibleTimes, user1Email, user1Name, user2Email, user2Name): Array<string> {
  console.log("In getCardData");
  const langHelper = new LanguageHelper(userLocale);
  let actionsArrData = [];
  for (let h in possibleTimes) {
    let startDate = h.split("T")[0];
    let startTime = h.split("T")[1];
    let endDate = possibleTimes[h].split("T")[0];
    let endTime = possibleTimes[h].split("T")[1];
    let startDateExpanded = new Date(parseInt(startDate.split("-")[0]), (parseInt(startDate.split("-")[1]) - 1), parseInt(startDate.split("-")[2]), parseInt(startTime.split(".")[0].split(":")[0]), parseInt(startTime.split(".")[0].split(":")[1]));
    let endDateExpanded = new Date(parseInt(endDate.split("-")[0]), (parseInt(endDate.split("-")[1]) - 1), parseInt(endDate.split("-")[2]), parseInt(endTime.split(".")[0].split(":")[0]), parseInt(endTime.split(".")[0].split(":")[1]));


    // console.log(startDateExpanded, endDateExpanded);

    actionsArrData.push({
      type: "Action.OpenUrl",
      title: startDateExpanded.toString().split(" ")[1] + " " + startDateExpanded.toString().split(" ")[2] + ", " + startDateExpanded.toString().split(" ")[3] + ", " + (parseInt(startDateExpanded.toString().split(" ")[4].split(":")[0]) % 12 || 12) + ":" + startDateExpanded.toString().split(" ")[4].split(":")[1] + (parseInt(startDateExpanded.toString().split(" ")[4].split(":")[0]) >= 12 ? "PM" : "AM") + " - " + (parseInt(endDateExpanded.toString().split(" ")[4].split(":")[0]) % 12 || 12) + ":" + endDateExpanded.toString().split(" ")[4].split(":")[1] + (parseInt(endDateExpanded.toString().split(" ")[4].split(":")[0]) >= 12 ? "PM" : "AM"),
      url: encodeURI("https://teams.microsoft.com/l/meeting/new?subject=Coffee Chat | " + user1Name + " and " + user2Name + "&attendees=" + user2Email + "&startTime=" + startDate.split('-')[1] + "/" + startDate.split('-')[2] + "/" + startDate.split('-')[0] + " " + startTime.split(":")[0] + ":" + startTime.split(":")[1] + ":" + startTime.split(":")[2].split(".")[0] + "&endTime=" + endDate.split('-')[1] + "/" + endDate.split('-')[2] + "/" + endDate.split('-')[0] + " " + endTime.split(":")[0] + ":" + endTime.split(":")[1] + ":" + endTime.split(":")[2].split(".")[0] + "&content=Let's meet for a Coffee Chat")
    });
  }
  actionsArrData.push({
    type: "Action.OpenUrl",
    title: langHelper.getTranslation("Go_to_Scheduling_Assistant"),
    url: encodeURI("https://teams.microsoft.com/l/meeting/new?subject=Coffee Chat | " + user1Name + " and " + user2Name + "&attendees=" + user2Email + "&content=Let's meet for a Coffee Chat")
  });
  return actionsArrData;
}
async function getUserTimeViews(accessToken, user1Email, user2Email, reqStartTime, reqEndTime) {
  console.log("In getUserTimeViews");
  const bodyDetails = {
    "schedules": [user1Email, user2Email],
    "startTime": { "dateTime": new Date(reqStartTime).toISOString(), "timeZone": "UTC" },
    "endTime": { "dateTime": new Date(reqEndTime).toISOString(), "timeZone": "UTC" },
    "availabilityViewInterval": 30
  };
  // console.log("REQ body details : ", bodyDetails);

  let options = {
    'method': 'POST',
    'url': 'https://graph.microsoft.com/v1.0/me/calendar/getSchedule',
    'headers': {
      'Authorization': 'Bearer ' + accessToken,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(bodyDetails)
  };
  let result;
  try {
    result = await request(options);
  } catch (error) {
    // let errArr = ['try-catch-err', 'msteams-connect', 'calendarController.ts', 'getUserTimeViews', error.message];
    // newrelic.noticeError(error);
    // logger.debug(errArr.join(":"));
    newRelicErrLog(error, "calendarController.ts/getUserTimeViews", "");
    const description = {
      "RequestDetails": { user1Email, user2Email },
      "ErrMessage": error.message
    };
    logger.debug("***** getUserTimeViews *********");
    logger.debug(error);

    // botAuth.saveLogData(JSON.stringify(description), "calendarController.ts/getUserTimeViews", "notFound", "error");


    loggerLogData(true, "*******************error in getUserTimeViews function*******************", "", true);
    result = JSON.stringify({ err: 'Error' });
    // logger.debug("Error trying to get views : ", error);
  }
  return JSON.parse(result);

}
export class CalendarController {
  constructor(botAuth) {
    botAuth = botAuth;
  }

  async getUserSchedule(userLocale, user1Name, user1Email, user2Name, user2Email, accessToken, user1Time, user1Timezone, user2Timezone) {
    const cardHelper = new CardHelper();
    const langHelper = new LanguageHelper(userLocale);
    loggerLogData(true, "Trying to get user schedule", "", false);
    return new Promise(async function (resolve, reject) {
      try {
        // serverTimeOffset is time Off set in Milliseconds of the server
        console.log("In getUserSchedule");
        const serverTimeOffset = new Date().getTimezoneOffset() * 60000;
        if(!user1Time){
          user1Time = new Date().toISOString();
        }
        const currentFullDate = user1Time.split('T')[0];
        const currentYear = parseInt(currentFullDate.split("-")[0]);
        const currentMonth = parseInt(currentFullDate.split("-")[1]);
        const currentDate = parseInt(currentFullDate.split("-")[2]);

        // Getting time off set **REPLACE AFTER WE GET TIMEZONES IN NOTIFICATION OBJECT**
        let user1TimeOffset;
        let user2TimeOffset;
        [user1TimeOffset, user2TimeOffset] = getUserTimeOffsets(user1Timezone, user2Timezone, user1Time.split("T")[1]);
        console.log("user1TimeOffset, user2TimeOffset : ", user1TimeOffset, user2TimeOffset);
        // console.log("Time off sets of both users is : ", user1TimeOffset, user2TimeOffset);
        // CHANGE AFTER NOTIFICATIONOBJ GETS TIMEZONES
        // ================================================================

        // startDateUTC is number of MS passed at midnight, next day of receiving CoffeeChat notification in UTC
        const startDateUTC = Date.UTC(currentYear, currentMonth - 1, currentDate + 1);
        // console.log("START_TIME ", startDateUTC);
        // endDateUTC is 14 days after startDateUTC in UTC
        const endDateUTC = Date.UTC(currentYear, currentMonth - 1, currentDate + 15);
        // console.log("END_TIME ", endDateUTC);

        const [reqStartTime, reqEndTime] = findStartAndEndUserTimes(startDateUTC, endDateUTC, user1TimeOffset, user2TimeOffset);

        console.log("Before getUserTimeViews");
        const result = await getUserTimeViews(accessToken, user1Email, user2Email, reqStartTime, reqEndTime);
        console.log("After getUserTimeViews");
        if (result.err) {
          console.log("In error of getUserTimeViews call...");
          const fallbackCard = cardHelper.getFallBackCard(userLocale, user1Name, user1Email, user2Name, user2Email);
          // console.log("Error in getUserTimeViews. Sending fallback card.")
          resolve(JSON.stringify(fallbackCard));
        } else {

          console.log("In success of getUserTimeViews");
          const possibleTimes = getUserFreeTimes(result, user1Email, user2Email, reqStartTime, serverTimeOffset, user1TimeOffset, user2TimeOffset);
          console.log("After getUserFreeTimes");


          let actionsArrData = getCardData(userLocale, possibleTimes, user1Email, user1Name, user2Email, user2Name);


          let cardMessage;
          if (actionsArrData.length >= 2) {
            langHelper.getTranslation("");
            cardMessage = langHelper.getTranslation("Both_you_and_USERNAME_are_available_at_these_times", [user2Name]);
            // console.log(cardMessage);
            // console.log(actionsArrData);
          } else {
            cardMessage = langHelper.getTranslation("You_do_not_share_available_time_with_USERNAME_in_the_next_7_days_Please_schedule_your_chat_using_the_scheduling_assistant", [user2Name]);
            // console.log(cardMessage);
            // console.log(actionsArrData);
          }
          let adaptiveCard = cardHelper.getCard(userLocale, cardMessage, actionsArrData);
          consoleLogData(true, "Trying to stringify card...", "", false);
          let card = JSON.stringify(adaptiveCard);
          consoleLogData(true, "Sending stringified card.", "", false);
          resolve(card);

        }
      } catch (error) {
        // let errArr = ['try-catch-err', 'msteams-connect', 'calendarController.ts', 'getUserSchedule', error.message];
        // newrelic.noticeError(error);
        // logger.debug(errArr.join(":"));
        newRelicErrLog(error, "calendarController.ts/getUserSchedule", "");
        const description = {
          "RequestDetails": { user1Email, user2Email },
          "ErrMessage": error.message
        };
        logger.debug("***** getUserSchedule *********");
        logger.debug(error);
    

        // botAuth.saveLogData(JSON.stringify(description), "calendarController.ts/getUserSchedule", "notFound", "error");

        loggerLogData(true, "*******************error in getUserSchedule function*******************", "", true);
        const fallbackCard = cardHelper.getFallBackCard(userLocale, user1Name, user1Email, user2Name, user2Email);
        // console.log("Error in calendarController. Sending fallback card.")
        resolve(JSON.stringify(fallbackCard));
      }
    });
  }
}
