const builder = require("botbuilder");
const teams = require("botbuilder-teams");
import { toKeyAlias } from "@babel/types";
import { Session } from "inspector";
const util = require("util");
const fs = require("fs");
const logger = require("../util/winlogger.js").logger;
const jwt = require("jsonwebtoken");
import * as rp from 'request-promise';
import { LanguageHelper } from "../languages/languageHelper";
import { CardHelper } from "../util/AdaptiveCardHelper";
import * as configFile from "../config.json";
import { trackEventInPendo } from './../src/pendoTrackEvent';
const newrelic = require('newrelic');
const jwtSimple = require('jwt-simple');

import { consoleLogData, loggerLogData, newRelicErrLog } from '../util/logsHandler';

let baseUrl: string;
let MicrosoftAppId: string;
let bot: any;
let botAuth: any;

function extractComment(encodedComment: string): string {
  let encodedCommentLength: number = encodedComment.length;
  let commentBody = "";
  let i = -1;
  if (encodedComment && typeof encodedComment == "string" && encodedComment.indexOf("u003c") > -1 && encodedComment.indexOf("u003e") > -1) {
    while (i < encodedCommentLength + 1) {
      i += 1;
      if (i >= encodedCommentLength - 5) {
        break;
      }
      if (encodedComment.substring(i, i + 5) == "u003e") {
        i += 5;
        while (i < encodedCommentLength + 1) {
          if (encodedComment.substring(i, i + 5) == "u003c") {
            i += 5;
            break;
          }
          commentBody += encodedComment[i];
          i += 1;
        }
      }
    }
    return commentBody;
  } else if (encodedComment && typeof encodedComment == "string" && encodedComment.indexOf("<") > -1 && encodedComment.indexOf(">") > -1) {
    while (i < encodedCommentLength + 1) {
      i += 1;
      if (i >= encodedCommentLength - 1) {
        break;
      }
      if (encodedComment.substring(i, i + 1) == ">") {
        i += 1;
        while (i < encodedCommentLength + 1) {
          if (encodedComment.substring(i, i + 1) == "<") {
            i += 1;
            break;
          }
          commentBody += encodedComment[i];
          i += 1;
        }
      }
    }
    return commentBody;
  } else {
    return encodedComment;
  }
}

function findMentionedText(possibleMentionedTexts) {
  let mentionedText;
  for (let i of possibleMentionedTexts) {
    if (i && typeof i == "string" && i.indexOf("<at>") > -1 && i.indexOf("</at>") > -1) {
      mentionedText = i.substring(i.indexOf("<at>") + 4, i.indexOf("</at>"));
      return mentionedText;
    }
  }
  return "";
}

function stripMentions(mentionString) {
  var mentionedText;
  if (mentionString && typeof mentionString == "string" && mentionString.indexOf("<at>") > -1) {
    mentionedText = mentionString.substring(mentionString.indexOf("<at>") + 4, mentionString.indexOf("</at>"));
    return mentionString.replace(mentionString.substring(mentionString.indexOf("<at>"), mentionString.indexOf("</at>") + 5), mentionedText);
  } else {
    return mentionString;
  }
}

export class MyBot {
  constructor(_baseUrl: string, _bot: any, _MicrosoftAppId: any, _botAuth: any) {
    bot = _bot;
    botAuth = _botAuth;
  }

  async sendNotification(req: any, res: any) {
    try {
      res.json({ message: "success" });
      let mentionedText: string;
      if (req.body && req.body.notificationObj && req.body.notificationObj.campaignId && req.body.notificationObj.dataFields && req.body.notificationObj.dataFields.coffee_chat_subject) {
        axpSendCoffeeChatPairing(req, res);
      } else {



        // Move the request data into user and notification objects
        let notificationObj = req.body.notificationObj;
        let curLang = notificationObj.locale;
        const langHelper = new LanguageHelper(curLang);
        const cardHelper = new CardHelper();

        // console.log("CURRENT LANGUAGE IN NOTIFICATION CONTROLLER IS : ", curLang);

        let userDetails = req.body.user;

        if (!notificationObj.url) {
          return;
        }
        // Set prefix to notification url
        if (notificationObj.url && typeof notificationObj.url == "string" && notificationObj.url.indexOf("https://") == -1) {
          notificationObj.url = "https://" + notificationObj.url;
        }

        // Create address to send notifications to using userDetails object
        let address = {
          channelId: "msteams",
          user: { id: userDetails.appUserId },
          channelData: {
            tenant: {
              id: userDetails.appId
            }
          },
          bot: {
            id: userDetails.msTeamBotId,
            name: userDetails.msTeamBotName
          },
          serviceUrl: userDetails.msTeamBotServiceUrl,
          useAuth: true
        };

        let cardMessage;
        let headline;
        let btnName;
        let activityUrl;
        let btnUrl;
        let adaptiveCard;
        let utmTerm;
        let FollowupActionText;
        let FollowupActionEmailText;
        let followBtnUrl;
        const supportedNotifications = ["recognition", "announcement", "achievement", "boost", "mention"];
        if (notificationObj.pushNotification && supportedNotifications.includes(notificationObj.pushNotification.pushNotificationType)) {
          // Logic for boost, announcement and announcements
          consoleLogData(true, "In boost, announceent block", "", false);
          loggerLogData(true, "In boost, announceent block", "", false);
          cardMessage = langHelper.getTranslation("USERNAME_just_recognized_you_for_CRITERIA", [notificationObj.nominatorPreferredName, notificationObj.criteriaName]) + '.';

          headline = langHelper.getTranslation("Youve_been_recognized");
          btnName = langHelper.getTranslation("View_recognition");
          utmTerm = "&utm_term=recognition";
          activityUrl = notificationObj.url + "/event/" + notificationObj.pushNotification.newsfeedEventPK + "/" + notificationObj.pushNotification.accessKey;
          activityUrl += "?utm_notificationType=" + notificationObj.pushNotification.pushNotificationType + "&utm_medium=msteams&utm_source=platform";

          if (notificationObj.pushNotification.pushNotificationType == "announcement") {
            headline = langHelper.getTranslation("A_new_announcement_has_been_shared_with_you");
            cardMessage = notificationObj.pushNotification.pushNotificationDetails[0];
            btnName = langHelper.getTranslation("Read_more");
            utmTerm = "&utm_term=announcement";
          } else if (notificationObj.pushNotification.pushNotificationType == "achievement") {
            cardMessage =
              notificationObj.pushNotification
                .pushNotificationDetails[0];
            headline = langHelper.getTranslation("Congratulations_on_your_Achievement");
            btnName = langHelper.getTranslation("View_achievement");
            utmTerm = "&utm_term=achievement";
          } else if (notificationObj.pushNotification.pushNotificationType == "boost") {
            cardMessage = langHelper.getTranslation("USERNAME_gave_your_recognition_a_Boost", [notificationObj.nominatorName]);
            headline = langHelper.getTranslation("Youve_received_a_Boost");
          } else if (notificationObj.pushNotification.pushNotificationType == "mention") {

            loggerLogData(true, "*** In mention : notificationObj : ***", "", false);
            consoleLogData(true, "*** notificationObj.comment ***", "", false);

            headline = langHelper.getTranslation("USERNAME_has_mentioned_you_in_a_comment", [notificationObj.commenterFullName]);
            let commentText = extractComment(notificationObj.comment);
            cardMessage = commentText;
            btnName = langHelper.getTranslation("View_comment");
            utmTerm = "&utm_term=comment";
          }
          // let btnUrl = process.env.MIDDLWARE_API_URI + '/api/shared/redirectNotificationUrl?state='+state;
          activityUrl = activityUrl + utmTerm;
          mentionedText = findMentionedText([headline, cardMessage]);
          btnUrl = activityUrl;
          if (notificationObj.pushNotification.pushNotificationType != "announcement") {
            adaptiveCard = cardHelper.getNotificationsRecognitionCard(curLang, mentionedText || "", userDetails.appUserId, headline, cardMessage, btnName, btnUrl);
          } else if (notificationObj.pushNotification.pushNotificationType == "announcement") {
            const cardMessageHead = "**" + notificationObj.title.trim() + "**";
            const imgUrl = notificationObj.url + notificationObj.imageUrl;
            if (cardMessage.length > 252) {
              cardMessage = cardMessage.substr(0, 252) + "...";
            }
            adaptiveCard = cardHelper.getAnnouncementCard(mentionedText || "", userDetails.appUserId, headline, cardMessageHead, imgUrl, cardMessage, btnName, btnUrl);

          }
        }
        // Logic for coffee chat
        else if (notificationObj.template_name == "coffee_chat_pairing" || notificationObj.template_name == "coffee_chat_reminder") {
          let cardActionDetails: any;
          if (notificationObj.template_name == "coffee_chat_pairing") {

            consoleLogData(true, "In coffee chat block", "", false);
            loggerLogData(true, "In coffee chat block", "", false);


            headline = langHelper.getTranslation("Its_time_for_a_Coffee_Chat");
            cardMessage =
              langHelper.getTranslation("Youve_been_paired_up_with_USERNAME_for_a_Coffee_Chat_Take_this_opportunity_to_meet_your_coworker_Please_reach_out_to_USERNAME_to_schedule_your_15_to_30_minute_chat_soon", [notificationObj.user_2_name, notificationObj.user_2_first]);
            btnName = langHelper.getTranslation("Message_USERNAME", [notificationObj.user_2_first]);
            btnUrl = "https://teams.microsoft.com/l/chat/0/0?users=" + notificationObj.user_2_email;
          }

          else if (notificationObj.template_name == "coffee_chat_reminder") {
            headline = langHelper.getTranslation("Have_you_had_your_Coffee_Chat_yet");
            cardMessage = langHelper.getTranslation("Your_Coffee_Chat_with_USERNAME_will_expire_soon_Take_a_moment_to_find_1530_minutes_in_your_schedule_to_chat_with_each_other", [notificationObj.user_2_first]);
            btnName = langHelper.getTranslation("Message_USERNAME", [notificationObj.user_2_first]);
            btnUrl = "https://teams.microsoft.com/l/chat/0/0?users=" + notificationObj.user_2_email;
          }
          loggerLogData(true, "User time zones in notification object : ", { user1: notificationObj.user_1_timezone, user2: notificationObj.user_2_timezone }, false);
          notificationObj.user_1_timezone = notificationObj.user_1_timezone || "";
          notificationObj.user_2_timezone = notificationObj.user_2_timezone || "";
          const scheduleMeetingWithNoTimeSlots = "https://teams.microsoft.com/l/meeting/new?subject=Coffee%20Chat%20with%20" + notificationObj.user_2_name + "&attendees=" + notificationObj.email + "," + notificationObj.user_2_email + "&content=Let's%20meet%20for%20a%20Coffee%20Chat";
          const userTokenDetails = await botAuth.getUserInfo(userDetails.appUserId, userDetails.appId);
          if (userTokenDetails.user) {
            if (userTokenDetails.user.msGraphRefreshToken) {
              if (userTokenDetails.user.msGraphRefreshToken == "DENIED") {
                consoleLogData(true, "Refresh token is denied", "", false);
                cardActionDetails = [
                  {
                    type: "Action.OpenUrl",
                    title: btnName,
                    url: btnUrl
                  },
                  {
                    type: "Action.OpenUrl",
                    title: langHelper.getTranslation("Schedule_Coffee_Chat"),
                    url: scheduleMeetingWithNoTimeSlots
                  }
                ];
              } else {
                consoleLogData(true, "Access token exists", "", false);
                cardActionDetails = [
                  {
                    type: "Action.OpenUrl",
                    title: btnName,
                    url: btnUrl
                  },
                  {
                    type: "Action.Submit",
                    title: langHelper.getTranslation("Schedule_Coffee_Chat"),
                    data: {
                      "source": "CC_NOTIFICATION",
                      "refreshToken": userTokenDetails.user.msGraphRefreshToken,
                      "userId": userDetails.appUserId,
                      "tenantId": userDetails.appId,
                      "user1Name": notificationObj.user_1_name,
                      "user1Email": notificationObj.email,
                      "user2Name": notificationObj.user_2_name,
                      "user2Email": notificationObj.user_2_email,
                      "ccLocale": notificationObj.locale,
                      "user1Timezone": notificationObj.user_1_timezone,
                      "user2Timezone": notificationObj.user_2_timezone
                    }
                  }
                ];
              }
            } else {
              // Graph token value is null
              consoleLogData(true, "Graph token value is null", "", false);
              cardActionDetails = [
                {
                  type: "Action.OpenUrl",
                  title: btnName,
                  url: btnUrl
                },
                {
                  type: "Action.Submit",
                  title: langHelper.getTranslation("Schedule_Coffee_Chat"),
                  data: {
                    "source": "CC_NOTIFICATION",
                    "refreshToken": "NO_VALUE",
                    "userId": userDetails.appUserId,
                    "tenantId": userDetails.appId,
                    "user1Name": notificationObj.user_1_name,
                    "user1Email": notificationObj.email,
                    "user2Name": notificationObj.user_2_name,
                    "user2Email": notificationObj.user_2_email,
                    "ccLocale": notificationObj.locale
                  }
                }
              ];

            }
          } else {
            // User does not exist in the database
            consoleLogData(true, "User does not exist in the database", "", false);
            cardActionDetails = [
              {
                type: "Action.OpenUrl",
                title: btnName,
                url: btnUrl
              },
              {
                type: "Action.OpenUrl",
                title: langHelper.getTranslation("Schedule_Coffee_Chat"),
                url: encodeURI("https://teams.microsoft.com/l/meeting/new?subject=Coffee Chat | " + notificationObj.user_1_name + ' and ' + notificationObj.user_2_name + "&attendees=" + notificationObj.user_2_email + "&content=Let's meet for a Coffee Chat")
              }
            ];
          }

          mentionedText = findMentionedText([headline, cardMessage]);
          adaptiveCard = cardHelper.getNotificationsCoffeeChatCard(curLang, mentionedText || "", userDetails.appUserId, headline, cardMessage, cardActionDetails);

        }
        // Logic for peer notification and send to manager
        else if (notificationObj.template_name == "peer_to_peer_reports_to_soa" || notificationObj.template_name == "peer_to_peer_cc_soa") {

          consoleLogData(true, "In peer and manager notification block", "", false);
          loggerLogData(true, "In peer and manager notification block", "", false);

          let cardMessageSummary;
          if (notificationObj.template_name == "peer_to_peer_reports_to_soa") {
            if (notificationObj.nomineeCount > 1) {
              headline = langHelper.getTranslation("USERNAME_has_recognized_your_team_members", [notificationObj.nominatorName]);
              cardMessageSummary = langHelper.getTranslation("USERNAME_have_been_recognized_for_CRITERIA", [notificationObj.nomineeNameString, notificationObj.criteriaName]);
            } else {
              headline = langHelper.getTranslation("USERNAME_has_recognized_one_of_your_team_members", [notificationObj.nominatorName]);
              cardMessageSummary = langHelper.getTranslation("USERNAME_has_been_recognized_for_CRITERIA", [notificationObj.nomineeNameString, notificationObj.criteriaName]);
            }
          } else if (notificationObj.template_name == "peer_to_peer_cc_soa") {
            if (notificationObj.nomineeCount > 1) {
              cardMessageSummary = langHelper.getTranslation("USERNAME_were_recognized_for_CRITERIA", [notificationObj.nomineeNameString, notificationObj.criteriaName]);
            } else {
              cardMessageSummary = langHelper.getTranslation("USERNAME_was_recognized_for_CRITERIA", [notificationObj.nomineeNameString, notificationObj.criteriaName]);
            }
            headline = langHelper.getTranslation("USERNAME_wrote_a_recognition_you_should_see", [notificationObj.nominatorFullName]);
            cardMessage = notificationObj.nominationExplanation;
          }
          cardMessage = notificationObj.nominationExplanation;
          btnName = langHelper.getTranslation("View_recognition");
          utmTerm = "&utm_term=recognition";
          if (notificationObj.newsfeedEventAccessKey) {
            activityUrl =
              notificationObj.url +
              "/event/" +
              notificationObj.newsfeedEventPK +
              "/" +
              notificationObj.newsfeedEventAccessKey;
            activityUrl +=
              "?utm_notificationType=" +
              notificationObj.template_name +
              "&utm_medium=msteams&utm_source=platform";
          }
          else {
            activityUrl =
              notificationObj.url + "/user/mark_message_from_email/" + notificationObj.P2PLandingPage + "/" + notificationObj.user;
          }
          activityUrl = activityUrl + utmTerm;
          btnUrl = activityUrl;

          mentionedText = findMentionedText([headline, cardMessage, cardMessageSummary]);
          adaptiveCard = cardHelper.getNotificationspeerToPeerCard(curLang, mentionedText || "", userDetails.appUserId, headline, cardMessageSummary, cardMessage, btnName, btnUrl);
        }
        // Listen notifications
        else if (notificationObj.template_name == "listen_new_pulse_survey" || notificationObj.template_name == "listen_pulse_one_week_reminder" || notificationObj.template_name == "listen_pulse_two_day_reminder") {
          utmTerm = "&utmTerm=" + notificationObj.template_name;
          if (notificationObj.template_name == "listen_new_pulse_survey") {
            headline = langHelper.getTranslation("You_have_a_new_Pulse_survey_available");
            cardMessage = langHelper.getTranslation("Please_take_a_few_moments_to_complete_this_short_survey_and_share_your_feedback_Your_confidential_responses_will_help_to_make_us_the_best_we_can_be");
            btnName = langHelper.getTranslation("Take_the_survey");

            btnUrl = notificationObj.url + "/" + notificationObj.pulse_url + "&utm_medium=msteams" + utmTerm;
            const endText = langHelper.getTranslation("The_survey_link_is_customized_just_for_you_so_please_dont_forward_it");
            mentionedText = findMentionedText([headline, cardMessage]);
            adaptiveCard = cardHelper.getNewPulseSurveyCard(userDetails.appUserId, headline, cardMessage, btnName, btnUrl, endText, mentionedText);
          }
          else if (notificationObj.template_name == "listen_pulse_one_week_reminder") {
            headline = langHelper.getTranslation("There_is_only_one_week_left_until_the_Pulse_survey_closes");
            cardMessage = langHelper.getTranslation("Your_feedback_is_essential_Please_take_a_few_minutes_to_complete_this_short_survey_Your_confidential_responses_will_help_make_us_the_best_we_can_be");
            btnName = langHelper.getTranslation("Take_the_survey");
            btnUrl = notificationObj.url + "/" + notificationObj.pulse_url + "&utm_medium=msteams" + utmTerm;
            const endText = langHelper.getTranslation("The_survey_link_is_customized_just_for_you_so_please_dont_forward_it");
            const isManager = notificationObj.is_recipient_manager;
            mentionedText = findMentionedText([headline, cardMessage]);
            if (notificationObj.is_recipient_manager) {
              const managerData = {
                "responseRateText": langHelper.getTranslation("Team_Response_Rate_NUMBER", [notificationObj.response_rate]),
                "encourageEmployeesText": langHelper.getTranslation("We_want_to_hear_what_everyone_has_to_say_Please_encourage_your_team_members_to_complete_the_Pulse_survey_If_everyone_on_your_team_has_already_completed_the_survey_congratulations")
              };
              adaptiveCard = cardHelper.getNewPulseSurveyCard(userDetails.appUserId, headline, cardMessage, btnName, btnUrl, endText, mentionedText, isManager, managerData);
            } else {
              adaptiveCard = cardHelper.getNewPulseSurveyCard(userDetails.appUserId, headline, cardMessage, btnName, btnUrl, endText, mentionedText);
            }
          }
          else if (notificationObj.template_name == "listen_pulse_two_day_reminder") {
            headline = langHelper.getTranslation("There_are_only_two_days_left_until_the_Pulse_survey_closes");
            cardMessage = langHelper.getTranslation("This_is_your_last_chance_to_have_your_voice_be_heard_Your_feedback_is_essential_so_please_take_a_few_minutes_to_complete_this_short_survey");
            btnName = langHelper.getTranslation("Take_the_survey");
            btnUrl = notificationObj.url + "/" + notificationObj.pulse_url + "&utm_medium=msteams" + utmTerm;
            const endText = langHelper.getTranslation("The_survey_link_is_customized_just_for_you_so_please_dont_forward_it");
            mentionedText = findMentionedText([headline, cardMessage]);
            if (notificationObj.is_recipient_manager) {
              const isManager = notificationObj.is_recipient_manager;
              cardMessage = langHelper.getTranslation("This_is_your_last_chance_to_have_your_voice_be_heard_Your_feedback_is_essential_so_please_take_a_few_minutes_to_complete_this_short_survey_and_encourage_your_team_to_complete_it_as_well");
              const managerData = {
                "responseRateText": langHelper.getTranslation("Team_Response_Rate_NUMBER", [notificationObj.response_rate]),
                "encourageEmployeesText": langHelper.getTranslation("We_want_to_hear_what_everyone_has_to_say_PLease_encourage_your_team_members_to_complete_the_pulse_survey_If_everyone_on_your_team_has_already_completed_the_survey_congratulations")
              };
              adaptiveCard = cardHelper.getNewPulseSurveyCard(userDetails.appUserId, headline, cardMessage, btnName, btnUrl, endText, mentionedText, isManager, managerData);
            } else {
              adaptiveCard = cardHelper.getNewPulseSurveyCard(userDetails.appUserId, headline, cardMessage, btnName, btnUrl, endText, mentionedText);
            }
          }
        }
        else if (notificationObj.template_name == "listen_check_in_reminder") {
          utmTerm = "&utmTerm=" + notificationObj.template_name;
          headline = langHelper.getTranslation("Allie_Checkingin");
          cardMessage = langHelper.getTranslation("Hi_USERNAME_how_do_you_feel_about_work_today", [notificationObj.preferred_first_name]);
          const btnNames = {
            'angry': 'https://over.achievers.com/resources/listen/images/Emoji_1_Angry_@1x.png',
            'sad': 'https://over.achievers.com/resources/listen/images/Emoji_2_Sad_@1x.png',
            'happy': 'https://over.achievers.com/resources/listen/images/Emoji_3_Happy_@1x.png',
            'excited': 'https://over.achievers.com/resources/listen/images/Emoji_4_Very_Happy_@1x.png'
          };
          const btnUrls = {
            'angry': notificationObj.url + notificationObj.angry_url + "&utm_medium=msteams" + utmTerm,
            'sad': notificationObj.url + notificationObj.not_happy_url + "&utm_medium=msteams" + utmTerm,
            'happy': notificationObj.url + notificationObj.positive_url + "&utm_medium=msteams" + utmTerm,
            'excited': notificationObj.url + notificationObj.happy_url + "&utm_medium=msteams" + utmTerm
          };
          const endText = langHelper.getTranslation("Responses_are_confidential");

          mentionedText = findMentionedText([headline, cardMessage]);
          adaptiveCard = cardHelper.getPulseCheckInCard(userDetails.appUserId, headline, cardMessage, btnNames, btnUrls, endText, mentionedText);
        }
        else if (notificationObj.template_name == "listen_schedule_session") {
          utmTerm = "&utmTerm=" + notificationObj.template_name;

          const startDateArr = notificationObj.listen_schedule_session_date.split('-');
          const endDateArr = notificationObj.listen_schedule_session_end_date.split('-');
          const startDate = new Date(startDateArr[0], startDateArr[1] - 1, startDateArr[2]);
          const endDate = new Date(endDateArr[0], endDateArr[1] - 1, endDateArr[2]);


          headline = langHelper.getTranslation("A_new_Pulse_survey_is_coming_soon");

          cardMessage = langHelper.getTranslation("Your_team_will_receive_the_Pulse_survey_on_DATE_The_Pulse_survey_is_open_until_DATE_Schedule_a_Listening_Session_with_your_team_to_discuss_the_survey_results", [startDate.toLocaleString('default', { 'month': 'long' }) + " " + startDate.getDate() + ", " + startDate.getFullYear(), endDate.toLocaleString('default', { 'month': 'long' }) + " " + endDate.getDate() + ", " + endDate.getFullYear()]);
          btnName = langHelper.getTranslation("Schedule");

          btnUrl = encodeURI(notificationObj.url + "/" + notificationObj.listen_action_builder_schedule_url + + "&utm_medium=msteams" + utmTerm);
          mentionedText = findMentionedText([headline, cardMessage]);
          adaptiveCard = cardHelper.getScheduleSessionCard(userDetails.appUserId, headline, cardMessage, btnName, btnUrl, mentionedText);
        }

        else if (notificationObj.template_name == "listen_pulse_extend_survey") {
          utmTerm = "&utmTerm=" + notificationObj.template_name;
          headline = langHelper.getTranslation("The_pulse_survey_has_been_extended");
          cardMessage = langHelper.getTranslation("Your_feedback_is_essential_We_have_extended_the_survey_tp_give_you_more_time_to_complete_it_Please_take_a_few_minutes_to_complete_this_short_survey_Your_confidential_responses_will_help_make_us_the_best_we_can_be");
          btnName = langHelper.getTranslation("Take_the_survey");
          btnUrl = notificationObj.url + "/" + notificationObj.listen_action_builder_followup_url + "&utm_medium=msteams" + utmTerm;
          const endText = langHelper.getTranslation("The_survey_link_is_customized_just_for_you_so_please_dont_forward_it");
          mentionedText = findMentionedText([headline, cardMessage]);
          if (notificationObj.is_recipient_manager) {
            const isManager = notificationObj.is_recipient_manager;
            const managerData = {
              "responseRateText": langHelper.getTranslation("Team_Response_Rate_NUMBER", [notificationObj.response_rate]),
              "encourageEmployeesText": langHelper.getTranslation("We_want_to_hear_what_everyone_has_to_say_PLease_encourage_your_team_members_to_complete_the_pulse_survey_If_everyone_on_your_team_has_already_completed_the_survey_congratulations")
            };
            adaptiveCard = cardHelper.getNewPulseSurveyCard(userDetails.appUserId, headline, cardMessage, btnName, btnUrl, endText, mentionedText, isManager, managerData);
          } else {
            adaptiveCard = cardHelper.getNewPulseSurveyCard(userDetails.appUserId, headline, cardMessage, btnName, btnUrl, endText, mentionedText);
          }
        }

        else if (notificationObj.template_name == "listen_followup_reminder") {
          utmTerm = "&utmTerm=" + notificationObj.template_name;
          headline = langHelper.getTranslation("How_did_your_Listening_Session_go");
          cardMessage = langHelper.getTranslation("We_hope_you_had_a_productive_Listen_Session_with_your_team_To_complete_the_session_make_sure_you_ve_done_the_following");
          FollowupActionText = langHelper.getTranslation("Add_your_actions_in_Action_Builder");
          FollowupActionEmailText = langHelper.getTranslation("Send_a_follow_up_email_with_any_actions_or_hotspots_that_your_team_discussed_If_you_need_help_creating_this_email_you_can_use_the_email_template_provided_in_the_Follow_up_section_of_Action_Builder");
          btnName = langHelper.getTranslation("Add_actions");
          btnUrl = notificationObj.url + "/" + notificationObj.listen_action_tracker_url + "&utm_medium=msteams" + utmTerm;
          followBtnUrl = notificationObj.url + "/" + notificationObj.listen_action_builder_followup_url + "&utm_medium=msteams" + utmTerm;
          mentionedText = findMentionedText([headline, cardMessage]);
          adaptiveCard = cardHelper.getNewPulseSurveyfollowupCard(userDetails.appUserId, headline, cardMessage, FollowupActionText, FollowupActionEmailText, btnName, btnUrl, followBtnUrl, mentionedText, managerData);
        }

        else if (notificationObj.template_name == "listen_pulse_survey_closure_notification") {
          utmTerm = "&utmTerm=" + notificationObj.template_name;
          headline = langHelper.getTranslation("Your_teams_pulse_survey_results_are_in");
          cardMessage = langHelper.getTranslation("Here_are_your_teams_results_for_the_Pulse_survey");
          btnName = langHelper.getTranslation("View_Results");
          btnUrl = notificationObj.url + "/" + notificationObj.pulse_report_url + "&utm_medium=msteams" + utmTerm;
          mentionedText = findMentionedText([headline, cardMessage]);
          if (notificationObj.is_recipient_manager) {
            var managerData = {
              "engagementScoreText": langHelper.getTranslation("Engagement_Score", [notificationObj.engagement_score]),
              "responseRateText": langHelper.getTranslation("Response_Rate_NUMBER", [notificationObj.response_rate]),
              "encourageEmployeesText": langHelper.getTranslation("Note_that_comments_from_your_team_are_not_edited_to_encourage_constructive_feedback")
            };
          }
          adaptiveCard = cardHelper.getPulseSurveyClosureCard(userDetails.appUserId, headline, cardMessage, btnName, btnUrl, mentionedText, notificationObj.is_recipient_manager, managerData);

        }

        let msg = new builder.Message()
          .address(address)
          .addAttachment(adaptiveCard)
          .summary(stripMentions(headline));

        bot.send(msg);
      }
    } catch (e) {
      // newrelic.noticeError(e);
      // let errArr = ['try-catch-err', 'msteams-connect', 'activityController.ts', 'sendNotification', e.message];

      newRelicErrLog(e, "activityController.ts/sendNotification", "");
      const description = {
        "RequestDetails": { appId: req.body.user.appId, appUserId: req.body.user.appUserId, reqUrl: req.url, reqMethod: req.method },
        "ErrMessage": e.message
      };
      botAuth.saveLogData(JSON.stringify(description), "activityController.ts/sendNotification", req.body.user.appId, "error");
      loggerLogData(true, "******************* sendNotification err *******************", "", true);
      console.log(e);
      // logger.debug(errArr.join(":"));
      // logger.error("error in sendNotification: Error: \n" + e);
    }
  }

  async sendProactiveLogout(req: any, res: any) {
    try {
      let userDetails = req.query;
      let address = {
        channelId: "msteams",
        user: { id: userDetails.appUserId },
        channelData: {
          tenant: {
            id: userDetails.appId
          }
        },
        bot: {
          id: userDetails.msTeamBotId,
          name: userDetails.msTeamBotName
        },
        serviceUrl: userDetails.msTeamBotServiceUrl,
        useAuth: true
      };
      // Tracking an event in pendo when user clicks signout from custom tab
      // External | Integration: Custom tab – Sign out
      if (userDetails.achieverApplicationID && userDetails.achieverMemberId) {
        trackEventInPendo("External | Integration: Custom tab – Sign out", "", "", {
          medium: "msTeams"
        });
      }
      let msg = new builder.Message()
        .address(address)
        .text("You have been successfully signed out. Please note that you have to sign in again to be able to utilize Achiever's app functionality.");
      bot.send(msg);
      res.status(200).send();
    } catch (error) {
      newRelicErrLog(error, "activityController.ts/sendProactiveLogout", "");
      const description = {
        "RequestQuery": req.query,
        "ErrMessage": error.message
      };
      botAuth.saveLogData(JSON.stringify(description), "activityController.ts/sendProactiveLogout", "TEAMS_MESSAGE_ERROR", "error");
      res.status(200).send("Error in sending signout  message to user");
    }
  }
  async sendCalenderPermissionCard(req: any, res: any) {
    try {
      const cardHelper = new CardHelper();
      const userDetails = await botAuth.getUserInfo(req.body.appUserId, req.body.appId, true);
      if (userDetails.user && userDetails.user.msGraphRefreshToken && userDetails.user.msGraphRefreshToken != 'DENIED' && [true].includes(userDetails.user.isRecognitionSuggestionHide)) {
        // donot send any permission card but update the flag in Database
        const updatedValue = await botAuth.updateRecognitionSuggestionStatus(false, userDetails.user.appId, userDetails.user.appUserId, userDetails.user.appType);
      }
      else {
        const locale = "en-US";
        const langHelper = new LanguageHelper(locale);
        // Create address to send notifications to using userDetails object
        let address = {
          channelId: "msteams",
          user: { id: userDetails.user.appUserId },
          channelData: {
            tenant: {
              id: userDetails.user.appId
            }
          },
          bot: {
            id: userDetails.user.msTeamBotId,
            name: userDetails.user.msTeamBotName
          },
          serviceUrl: userDetails.user.msTeamBotServiceUrl,
          useAuth: true
        };
        const stateData = jwtSimple.encode({
          tenantId: userDetails.user.appId,
          userId: userDetails.user.appUserId,
          type: "GlobalCalenderPermission",
          msTeamBotId: userDetails.user.msTeamBotId,
          msTeamBotName: userDetails.user.msTeamBotName,
          msTeamBotServiceUrl: userDetails.user.msTeamBotServiceUrl,
          ccLocale: userDetails.user.locale || "en-US",
        }, "12345");
        const url = "https://login.microsoftonline.com/" + userDetails.user.appId + "/oauth2/v2.0/authorize?client_id=" + process.env.APP_CLIENT_ID + "&response_type=code&redirect_uri=" + process.env.APP_BASE_URL + "/auth/redirect&response_mode=query&scope=Calendars.Read%20offline_access&state=" + stateData;
        const headLine = langHelper.getTranslation("Recognition_Suggestions");
        const mentionedText = findMentionedText([headLine]);
        const adaptiveCard = cardHelper.getGlobalCalenderPermissionsCard(locale, url, false, mentionedText, userDetails.user.appUserId);
        let msg = new builder.Message()
          .address(address)
          .addAttachment(adaptiveCard)
          .summary(stripMentions(headLine));
        bot.send(msg);
      }
      return res.send("success");
    }
    catch (err) {
      newRelicErrLog(err, "activityController.ts/sendCalenderPermissionCard", "");
      const description = {
        "RequestDetails": { appUserId: req.body.appUserId, appId: req.body.appId, reqUrl: req.url, reqMethod: req.method },
        "ErrMessage": err.message
      };
      botAuth.saveLogData(JSON.stringify(description), "activityController.ts/sendCalenderPermissionCard", req.body.appId, "error");
      loggerLogData(true, "******************* Error in sending permission card to user err *******************", "", true);
      // console.log("*****************err in sendCalenderPermissionCard *****************")
      return res.status(500).send("Error in sending permission card to user");

    }
  }

}
async function axpSendCoffeeChatPairing(req: any, res: any) {
  try {
    console.log("In new coffee chat function");
    let notificationObj = req.body.notificationObj;
    let userDetails = req.body.user;

    // Create address to send notifications to using userDetails object
    let address = {
      channelId: "msteams",
      user: { id: userDetails.appUserId },
      channelData: {
        tenant: {
          id: userDetails.appId
        }
      },
      bot: {
        id: userDetails.msTeamBotId,
        name: userDetails.msTeamBotName
      },
      serviceUrl: userDetails.msTeamBotServiceUrl,
      useAuth: true
    };


    let cardActionDetails, headline, cardMessage, btnName, btnUrl, mentionedText, adaptiveCard;
    const cardHelper = new CardHelper();
    let programPk = notificationObj.recipientUserId.split('-')[0];
    let userPk = notificationObj.recipientUserId.split('-')[1];
    userDetails.additionalInfo.forEach(user => {
      if (user.user_pk == userPk) {
        notificationObj.user_1_email = user.email;
        notificationObj.user_1_name = user.first_name + ' ' + user.last_name;
        notificationObj.user_1_first = user.first_name;
        notificationObj.locale = user.locale;
        notificationObj.user_1_timezone = user.timezone;
      } else {
        notificationObj.user_2_email = user.email;
        notificationObj.user_2_name = user.first_name + ' ' + user.last_name;
        notificationObj.user_2_first = user.first_name;
        notificationObj.user_2_timezone = user.timezone;
      }
    });




    headline = notificationObj.dataFields.coffee_chat_subject;
    cardMessage = notificationObj.dataFields.coffee_chat_body;
    btnName = "Message " + notificationObj.user_2_first;
    btnUrl = "https://teams.microsoft.com/l/chat/0/0?users=" + notificationObj.user_2_email;


    loggerLogData(true, "User time zones in notification object : ", { user1: notificationObj.user_1_timezone, user2: notificationObj.user_2_timezone }, false);
    notificationObj.user_1_timezone = notificationObj.user_1_timezone || "";
    notificationObj.user_2_timezone = notificationObj.user_2_timezone || "";
    const scheduleMeetingWithNoTimeSlots = "https://teams.microsoft.com/l/meeting/new?subject=Coffee%20Chat%20with%20" + notificationObj.user_2_name + "&attendees=" + notificationObj.user_1_email + "," + notificationObj.user_2_email + "&content=Let's%20meet%20for%20a%20Coffee%20Chat";
    const userTokenDetails = await botAuth.getUserInfo(userDetails.appUserId, userDetails.appId);
    if (userTokenDetails.user) {
      if (userTokenDetails.user.msGraphRefreshToken) {
        if (userTokenDetails.user.msGraphRefreshToken == "DENIED") {
          consoleLogData(true, "Refresh token is denied", "", false);
          cardActionDetails = [
            {
              type: "Action.OpenUrl",
              title: btnName,
              url: btnUrl
            },
            {
              type: "Action.OpenUrl",
              title: "Schedule Coffee Chat",
              url: scheduleMeetingWithNoTimeSlots
            }
          ];
        } else {
          consoleLogData(true, "Access token exists", "", false);
          cardActionDetails = [
            {
              type: "Action.OpenUrl",
              title: btnName,
              url: btnUrl
            },
            {
              type: "Action.Submit",
              title: "Schedule Coffee Chat",
              data: {
                "source": "CC_NOTIFICATION",
                "refreshToken": userTokenDetails.user.msGraphRefreshToken,
                "userId": userDetails.appUserId,
                "tenantId": userDetails.appId,
                "user1Name": notificationObj.user_1_name,
                "user1Email": notificationObj.user_1_email,
                "user2Name": notificationObj.user_2_name,
                "user2Email": notificationObj.user_2_email,
                "ccLocale": notificationObj.locale,
                "user1Timezone": notificationObj.user_1_timezone,
                "user2Timezone": notificationObj.user_2_timezone
              }
            }
          ];
        }
      } else {
        // Graph token value is null
        consoleLogData(true, "Graph token value is null", "", false);
        cardActionDetails = [
          {
            type: "Action.OpenUrl",
            title: btnName,
            url: btnUrl
          },
          {
            type: "Action.Submit",
            title: "Schedule Coffee Chat",
            data: {
              "source": "CC_NOTIFICATION",
              "refreshToken": "NO_VALUE",
              "userId": userDetails.appUserId,
              "tenantId": userDetails.appId,
              "user1Name": notificationObj.user_1_name,
              "user1Email": notificationObj.user_1_email,
              "user2Name": notificationObj.user_2_name,
              "user2Email": notificationObj.user_2_email,
              "ccLocale": notificationObj.locale
            }
          }
        ];

      }
    } else {
      // User does not exist in the database
      consoleLogData(true, "User does not exist in the database", "", false);
      cardActionDetails = [
        {
          type: "Action.OpenUrl",
          title: btnName,
          url: btnUrl
        },
        {
          type: "Action.OpenUrl",
          title: "Schedule Coffee Chat",
          url: encodeURI("https://teams.microsoft.com/l/meeting/new?subject=Coffee Chat | " + notificationObj.user_1_name + ' and ' + notificationObj.user_2_name + "&attendees=" + notificationObj.user_2_email + "&content=Let's meet for a Coffee Chat")
        }
      ];
    }

    mentionedText = findMentionedText([headline, cardMessage]);
    adaptiveCard = cardHelper.getNotificationsCoffeeChatCard("en-US", mentionedText || "", userDetails.appUserId, headline, cardMessage, cardActionDetails);

    let msg = new builder.Message()
      .address(address)
      .addAttachment(adaptiveCard)
      .summary(stripMentions(headline));
    console.log(JSON.stringify(adaptiveCard))
    bot.send(msg);






  } catch (err) {
    console.log(err);
  }
}

