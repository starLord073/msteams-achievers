const logger = require('../util/winlogger.js').logger;
const newrelic = require("newrelic");

import { consoleLogData, loggerLogData, newRelicErrLog } from '../util/logsHandler';

export class HealthController {
    public async getHealthInfo(req: any, res: any, next: any){
        try {
            let response: any = {
                "status": "pass",
                "notes": [""],
                app_environment: process.env.APP_ENV,
                region: process.env.PFA_REGION,
                axp_region: process.env.AXP_REGION,
                "description": "health of msteams service",
            };
            
            res.json({health_data: response})
            
        } catch (err) {
            let errArr = ['try-catch-err', 'msteams-connect', 'healthController.ts', 'getHealthInfo', err.message];
            newrelic.noticeError(err);
            logger.debug(errArr.join(":"));
        }
    }
}