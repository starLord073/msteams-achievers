
import { trackEventInPendo } from "../src/pendoTrackEvent";
const logger = require('../util/winlogger.js').logger;
const newrelic = require("newrelic");

import { consoleLogData, loggerLogData, newRelicErrLog } from '../util/logsHandler';

export class PendoController {
  pendoTracking(req: any, res: any) {
    try {
      consoleLogData(true,"************In pendo Controller*************","",false);
      trackEventInPendo(req.body.event, req.body.accountId, req.body.visitorId, JSON.parse(req.body.properties));
      res.send("successfull");
    }
    catch (err) {
      // newrelic.noticeError(err);
      // const errArr = ['try-catch-err', 'msteams-connect', 'pendoController.ts', 'pendoTracking', err.message];
      // logger.debug(errArr.join(":"));
      newRelicErrLog(err,"pendoController.ts/pendoTracking","");
      loggerLogData(true,"*******************error in pendoTracking function *******************","",true);
      res.send(500, err);
    }
  }
}