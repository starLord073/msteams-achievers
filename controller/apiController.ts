const express = require("express");
const jwt = require("jsonwebtoken");
const builder = require("botbuilder");
const teams = require("botbuilder-teams");
const request = require("request-promise");
const dotenv = require("dotenv");
const multer = require("multer");
const fs = require('fs');
const _ = require("lodash");
const moment = require("moment-timezone");
const jwtSimple = require('jwt-simple');
import AchieversAuth from "../src/middlewareAuth";
import uuid = require("uuid");
import * as rp from "request-promise";
import { CalendarController } from './calendarController';
import { CardHelper } from "../util/AdaptiveCardHelper";
const util = require('util');
const logger = require('../util/winlogger.js').logger;
import { LanguageHelper } from "../languages/languageHelper";
import { trackEventInPendo } from "../src/pendoTrackEvent";
const writeFilePromise = util.promisify(fs.writeFile);
const deleteFilePromise = util.promisify(fs.unlink);
const newrelic = require('newrelic');
const configFile = require("../config.json");


import { consoleLogData, loggerLogData, newRelicErrLog } from '../util/logsHandler';
import { jwtSign } from "../util/jwtTokenHandler";

let botAuth: any;
let achieverAPI: any;
let bot: any;
let calendarController;
let connector: any;
let viewSettings: any;
let config: any;
let settings: any;

const GRPCHost = "userquery-grpc.stg-user-usermanagement";
const CONTENTTYPEJSON = "application/json";
const ERRORRESPONSEFROMACHIEVERS = "Error in getting response from achievers API";
const LOGININVALIDACCESS = "Please login agaian, invalid access";

// const botAuth = achieversAuth.SimpleFBAuth.create(server, connector, botSigninSettings);
connector = new teams.TeamsChatConnector({
  appId: process.env.APP_CLIENT_ID,
  appPassword: process.env.APP_CLIENT_SECRET
});


async function middlewareGrpcCalls({ endpoint, userList, programId, authorization, offset }) {
  let newsFeed: any;
  if (endpoint === "grpcNewsFeedData") {
    const recogParams = {
      uri: `${process.env.MIDDLEWARE_INTERNAL_API_URI}/api/shared/grpcNewsFeedData?programId=${programId}&userPk=${userList}&event_type=["PostcardEvent","MilestoneEvent"]&limit=10&offset=${offset}&language_code=en-US`,
      method: 'GET',
      headers: {
        'Authorization': authorization
      },
      json: true
    };
    newsFeed = await request(recogParams);
    
  }
  if (endpoint === "grpcUsersDetailInfo") {
    const recogUserParams = {
      uri: `${process.env.MIDDLEWARE_INTERNAL_API_URI}/api/shared/grpcUsersDetailInfo?programId=${programId}&userList=[${userList}]`,
      method: 'GET',
      headers: {
        'Authorization': authorization
      },
      json: true
    };
    newsFeed = await request(recogUserParams);
  }

  return newsFeed;
}

async function uploadFile(subdomain, baseUrl, achieversAccessToken, imagePostUrl, file) {
  let saveImageParams: any;
  return new Promise(async function (resolve, reject) {
    try {
      const achieverAccessToken = achieversAccessToken;
      loggerLogData(true, "************************inside saveImage*******************************", "", false);
      consoleLogData(true, "************************inside saveImage*******************************", "", false);
      const filePath = 'uploads/' + 'giphy' + uuid.v1() + ".gif";

      await writeFilePromise(filePath, file.buffer);
      loggerLogData(true, "**************** fileSaved *********************", "", false);
      consoleLogData(true, "**************** fileSaved *********************", "", false);

      saveImageParams = {
        uri: "https://" + subdomain + baseUrl + imagePostUrl,
        method: 'POST',
        formData: {
          file: fs.createReadStream(filePath)
        },
        headers: {
          'Authorization': 'Bearer ' + achieversAccessToken,
          'Content-Type': 'multipart/form-data; charset=UTF-8'
        },
        json: true
      };

      const siresp = await request(saveImageParams);

      await deleteFilePromise(filePath);

      resolve(siresp);
    }
    catch (e) {
      // newrelic.noticeError(e);
      // let errArr = ['try-catch-err', 'msteams-connect', 'apiController.ts', 'uploadFile', e.message];
      // logger.debug(errArr.join(":"));
      // logger.error("error in saveImage: " + e);
      // console.log("error in saveImage: " + e);
      newRelicErrLog(e, "apiController.ts/uploadFile", "");
      const description = {
        "RequestDetails": { subdomain, baseUrl, saveImageParams },
        "ErrMessage": e.message
      };
      botAuth.saveLogData(JSON.stringify(description), "apiController.ts/uploadFile", subdomain, "error");
      loggerLogData(true, "******************* error in saveImage *******************", "", true);
      reject(e);
    }
  });
}

function sendRecognitionToAchievers(recogParams, res, subdomain, _imageUrl, curLang,retryCount = 0) {
  // console.log("CURLANG in sendRecogToAchievers is : ", curLang);
  let respRet: any;
  let langHelper = new LanguageHelper(curLang);
  request(recogParams)
    .then(function (resp) {
      // console.log(resp);
      respRet = {
        message: "",
        newsFeedUrl: "",
        imageUrl: _imageUrl
      };
      loggerLogData(true, "inside submit post response handler", "", false);
      // logger.debug(resp);
      consoleLogData(true, "inside submit post response handler", "", false);
      // console.log(resp);
      let statusCode = 200;
      if (resp.code && resp.code == -1) {
        if (resp.message.indexOf("Invalid point amount provided") > -1) {
          respRet.message = langHelper.getTranslation("You_do_not_have_enough_points_for_this_recognition_Consider_sending_one_without_points");
        }
        else {
          respRet.message = resp.message;
        }
        statusCode = 400;
      }
      else {
        respRet.message = "Success";
        respRet.newsFeedUrl = "https://" + subdomain + ".achievers.com/recent_activity";
        statusCode = 200;
      }
      const finResp = JSON.stringify(respRet);
      res.writeHead(statusCode, {
        'Content-Length': Buffer.byteLength(finResp),
        'Content-Type': 'application/json'
      });
      res.write(finResp);
      res.end();
    })
    .catch(function (err) {
      // let errArr = ['try-catch-err', 'msteams-connect', 'apiController.ts', 'sendRecognitionToAchievers', err.message];
      // logger.debug(errArr.join(":"));
      // logger.error(err);
      // console.log(err);
      if (err && err.message && err.message.indexOf("Invalid upload ID provided, no file found") > -1 && retryCount < 5) {
        setTimeout(() => {
          sendRecognitionToAchievers(recogParams, res, subdomain, _imageUrl, curLang, retryCount + 1);
        }, 300 * retryCount);
      } else {
      newRelicErrLog(err, "apiController.ts/sendRecognitionToAchievers", "");
      const description = {
        "RequestDetails": { subdomain, respRet },
          "ErrMessage": err.message,
          "recogParams": recogParams
      };
      botAuth.saveLogData(JSON.stringify(description), "apiController.ts/sendRecognitionToAchievers", subdomain, "error");
      loggerLogData(true, "******************* Error in send recognition to Achievers*******************", "", true);
      if (err.statusCode && parseInt(err.statusCode) == 401) {
        res.send(401, "User not logged in.");
      }
      else {
        let errMessage = '';

        errMessage = err.error.message;
        // newrelic.noticeError(err);

        if ((errMessage.indexOf("Invalid point amount provided") > -1) || (errMessage.indexOf("insufficient budget") > -1)) {
          errMessage = langHelper.getTranslation("You_do_not_have_enough_points_for_this_recognition_Consider_sending_one_without_points");
      }
        res.send(400, errMessage);
      }
      }
    });
}


async function fetchRecognitions(subDomain, baseUrl, userId, achieverAccessToken) {
  let requestData: any;
  try {
    requestData = {
      uri: `https://${subDomain}${baseUrl}/recognitions`,
      // timeout: shouldTimeout? 3000 : 30000,
      headers: {
        Authorization: `Bearer ${achieverAccessToken}`
      },
      qs: {
        userId,
        startDate: moment().subtract(7, "days").format(), // Change this to 7 days, kept 9 only for demo
        endDate: moment().format(),
        favouritesOnly: false,
        useUserIds: true,
        page: 1,
        pageSize: 50
      },
      json: true // Automatically parses the JSON string in the response
    };
    const result = await rp(requestData);

    return result && result.items ? result.items : null;

  } catch (err) {
    //  console.log("==err",err)
    // newrelic.noticeError(err);
    newRelicErrLog(err, "apiController.ts/uploadFile", "");
    const description = {
      "RequestDetails": { userId, requestData },
      "ErrMessage": err.message
    };
    botAuth.saveLogData(JSON.stringify(description), "apiController.ts/fetchRecognitions", subDomain, "error");
    loggerLogData(true, "******************* error in fetchRecognitions *******************", "", true);
  }
}


function calculateDate(endDate) {
  const lastMeetingDate = moment(endDate, 'YYYY-MM-DD HH:mm:ss');
  const currentDate = moment(new Date(), 'YYYY-MM-DD HH:mm:ss');
  const duration = moment.duration(currentDate.diff(lastMeetingDate));
  const daysDiff = Math.floor(duration.asDays());
  const timeDiff = Math.floor(duration.asMinutes());
  return { daysDiff, timeDiff };
}

function getCelebrationObj(obj: any, currentDate: any, type: string) {
  const endDate = moment(obj.celebrationDate, 'YYYY-MM-DD');
  const duration = moment.duration(endDate.diff(currentDate));
  const daysDiff = _.floor(duration.asDays());
  const year = moment(obj.celebrationDate).format("YYYY");
  const finalDate = `${moment(obj.celebrationDate).format("MMMM DD")}, ${year}`;
  const nudgesFirstName = (obj.user.name).split(' ').slice(0, -1).join(' ');

  const cardTitle = type.includes("birthday") ? `Wish ${nudgesFirstName} a happy birthday!` : `Congratulate ${nudgesFirstName} on ${obj.numberOfYears} years!`;

  obj.upcomingDateDiff = daysDiff;
  obj.userProfileImageFromUser = obj.user.profileImageUrl;
  obj.userFullName = obj.user.name;
  obj.cardTitle = cardTitle;
  obj.cardText = "Sign their celebration card";
  obj.cardType = "celebration";
  obj.finalDate = finalDate;
  obj.typeOfCelebration = type.includes("birthday") ? "birthday" : (type.includes("anniversary") ? "anniversary" : "");
  obj.redirectURL = obj.cardLandingPageURL;

  return obj;
}

async function saveFileFromUrlAndUploadFile(accessToken, fileUrl, subDomain, basAppUrl) {
  let recognitionRequest: any;
  return new Promise(async function (resolve, reject) {
    try {
      // logger.debug("inside saveFileFromUrlAndUploadFile", fileUrl);
      // console.log("inside saveFileFromUrlAndUploadFile", fileUrl);

      const achieverAccessToken = accessToken;
      const filePath = 'uploads/' + 'giphy' + uuid.v1() + ".gif";

      const options = {
        url: fileUrl,
        encoding: null
      };
      const resultimagebuffer = await request(options);
      await writeFilePromise(filePath, resultimagebuffer);
      recognitionRequest = {
        method: "POST",
        uri: "https://" + subDomain + basAppUrl + "/uploads",
        headers: {
          Authorization: 'Bearer ' + achieverAccessToken
        },
        formData: {
          file: fs.createReadStream(filePath)
        },
        json: true
      };

      const result = await request(recognitionRequest);
      deleteFilePromise(filePath);
      resolve(result);
    } catch (err) {
      // let errArr = ['try-catch-err', 'msteams-connect', 'apiController.ts', 'saveFileFromUrlAndUploadFile', err.message];
      // newrelic.noticeError(err);
      // logger.debug(errArr.join(":"));
      // logger.error("******************* uploadFile err *******************", err.message);
      // console.log("******************* uploadFile err *******************", err.message);
      newRelicErrLog(err, "apiController.ts/saveFileFromUrlAndUploadFile", "");
      const description = {
        "RequestDetails": { fileUrl, subDomain, recognitionRequest },
        "ErrMessage": err.message
      };
      botAuth.saveLogData(JSON.stringify(description), "apiController.ts/saveFileFromUrlAndUploadFile", subDomain, "error");
      loggerLogData(true, "******************* uploadFile err *******************", "", true);
      const errObj = {
        status: 400,
        message: err.error.message
      };
      reject(errObj);
    }
  });
}
export class apiController {

  constructor(_botAuth: any, _achieversAPI: any, _bot: any, _viewSettings: any) {
    botAuth = _botAuth;
    achieverAPI = _achieversAPI;
    bot = _bot;
    viewSettings = _viewSettings;
    calendarController = new CalendarController(botAuth);
  }

  async getNames(req: any, res: any, next: any) {
    let appUserData: any;
    let options: any;
    try {
      // logger.debug("***************inside userFetch *********************");
      // console.log("***************inside userFetch *********************");
      const uName = req.body.userName;
      const userToken = req.body.userToken;
      let cb;
      const usersToAdd = uName.split(",");

      appUserData = await botAuth.jwtDecodePayload(userToken);

      const email = appUserData.appEmail;
      const response = await botAuth.getUserInfo(appUserData.appUserId, appUserData.appTenantId);


      if (response.user && response.user.achieverAccessToken) {

        if (response.user.achieverAccessToken.length > 0) {

          let calls = [];
          let resp = { members: [] }
          let searchString = "";
          while (usersToAdd.length > 0) {
            options = {
              uri: "https://" + response.user.subDomain + achieverAPI.baseUrl + achieverAPI.getUsers,
              qs: {
                'q': usersToAdd.splice(0,3).join(',')
              },
              headers: {
                'Authorization': 'Bearer ' + response.user.achieverAccessToken
              },
              json: true
            };
            calls.push(request(options));
          }
          await Promise.all(calls).then((responses) => {
            responses.forEach((response) => {
              if (response && response.members) {
                resp.members = resp.members.concat(response.members);
              }
            })
          });

          if (resp.members.length > 0) {
            let i: number;
            for (i = 0; i < resp.members.length; i++) {
              if (resp.members[i].emailAddress == email) {
                resp.members.splice(i, 1);
              }
            }
          }

          const userData = JSON.stringify(resp);
          res.writeHead(200, {
            'Content-Length': Buffer.byteLength(userData),
            'Content-Type': 'application/json'
          });
          res.write(userData);
          res.end();
        }
      }
      else {
        res.send(200, "invalid_token");
      }
    }
    catch (e) {
      // let errArr = ['try-catch-err', 'msteams-connect', 'apiController.ts', 'getNames', e.message];
      // logger.debug(errArr.join(":"));
      newRelicErrLog(e, "apiController.ts/getNames", "");
      const description = {
        "RequestDetails": { appUserId: appUserData.appUserId, appTenantId: appUserData.appTenantId, options },
        "ErrMessage": e.message
      };
      botAuth.saveLogData(JSON.stringify(description), "apiController.ts/getNames", appUserData.appTenantId, "error");
      loggerLogData(true, "******************* Exception in get user data for dropdown err *******************", "", true);
      if (parseInt(e.statusCode) == 401) {
        // logger.error("Achievers Token expiry. Error: \n" + e);
        // console.log("Achievers Token expiry. Error: \n" + e);
        res.send(200, "invalid_token");
      }
      else {
        // logger.error("Exception in get user data for dropdown. Error: \n" + e);
        // console.log("Exception in get user data for dropdown. Error: \n" + e);
        res.send(400, "Error in getting response from achievers API");
      }
    }
  }

  async getRecogTypes(req: any, res: any, next: any) {
    let appUserData: any;
    let optionsRecog: any;
    try {
      appUserData = await botAuth.jwtDecodePayload(req.body.userToken);

      // logger.debug("user Ids: " + req.body.userIds);
      // console.log("user Ids: " + req.body.userIds);

      const userInfoResp = await botAuth.getUserInfo(appUserData.appUserId, appUserData.appTenantId);

      const description = {
        event: "Fetching recognition types",
        userInfo: userInfoResp,
        reqData: req.body
      };
      botAuth.saveLogData(description, "apiController.ts/getRecogTypes", userInfoResp.user.subDomain || "AUDIT_LOGS");

      loggerLogData(true, "inside getRecogTypes", "", false);
      consoleLogData(true, "inside getRecogTypes", "", false);

      if (userInfoResp.user && userInfoResp.user.achieverAccessToken) {
        if (userInfoResp.user.achieverAccessToken.length > 0) {
          optionsRecog = {
            uri: "https://" + userInfoResp.user.subDomain + achieverAPI.baseUrl + achieverAPI.getRecogTypes,
            method: 'OPTIONS',
            qs: {
              page: 1,
              pageSize: 100,
              useUserIds: true,
              nominees: req.body.userIds
            },
            headers: {
              'Authorization': 'Bearer ' + userInfoResp.user.achieverAccessToken
            },
            json: true
          };

          const grtResp = await request(optionsRecog);

          // console.log("Response : ", grtResp);
          const recogTypes = JSON.stringify(grtResp);
          res.writeHead(200, {
            'Content-Length': Buffer.byteLength(recogTypes),
            'Content-Type': 'application/json'
          });
          res.write(recogTypes);
          res.end();

        }
      }
      else {
        res.send(200, "invalid_token");
      }
    }
    catch (e) {
      // let errArr = ['try-catch-err', 'msteams-connect', 'apiController.ts', 'getRecogTypes', e.message];
      // logger.debug(errArr.join(":"));
      newRelicErrLog(e, "apiController.ts/getRecogTypes", "");
      const description = {
        "RequestDetails": { appUserId: appUserData.appUserId, appTenantId: appUserData.appTenantId, optionsRecog },
        "ErrMessage": e.message
      };
      botAuth.saveLogData(JSON.stringify(description), "apiController.ts/getRecogTypes", appUserData.appTenantId, "error");
      loggerLogData(true, "*******************Error in getRecogTypes err *******************", "", true);
      if (parseInt(e.statusCode) == 401) {
        // logger.error("Achievers Token expiry. Error: \n" + e);
        // console.log("Achievers Token expiry. Error: \n" + e);
        res.send(200, "invalid_token");
      }
      else {
        // logger.error("Error in getRecogTypes: " + e);
        // console.log("Error in getRecogTypes: " + e);
        res.send(400, "Error in getting response from achievers API");
      }
    }

  }


  // Celebrations
  async fetchCelebrationsOrRecognitions(req: any, res: any) {
    let appUserData: any;
    try {
      loggerLogData(true, '***********In fetchCelebrationsOrRecognitionsAndFilter********************', "", false);

      let endPoint: string;
      const currentDate = moment(new Date(), 'YYYY-MM-DD');
      appUserData = await botAuth.jwtDecodePayload(req.body.userToken);

      const userInfo = await botAuth.getUserInfo(appUserData.appUserId, appUserData.appTenantId);

      if (!userInfo.user) {
        return res.status(401).send({ message: LOGININVALIDACCESS });
      }
      // console.log("===TIME START====");

      let startDate = moment().format();
      let endDate = moment().add(60, "days").format();
      startDate = moment.tz(startDate, "America/Toronto").format();
      endDate = moment.tz(endDate, "America/Toronto").format();
      let celebrationsArray = [];

      let birthdayResult: any = await botAuth.fetchCelebrationsOrRecognitionsapi(
        userInfo,
        endPoint = "upcoming-celebrations",
        startDate,
        endDate,
        "Birthday",
        "Program",
        1,
        50);
      let serviceAwardResult: any = await botAuth.fetchCelebrationsOrRecognitionsapi(
        userInfo,
        endPoint = "upcoming-celebrations",
        startDate,
        endDate,
        "Service Awards",
        "Program",
        1,
        50);
      if (birthdayResult && birthdayResult.length) {
        loggerLogData(true, `***********Birthday looping started********************${new Date()}`, "", false);
        birthdayResult.map((obj: any) => {
          //If user has Birthday filter and add [image,name] from users search list
          if (obj && obj.user) {
            const type = "a birthday";
            const celebrationObj = getCelebrationObj(obj, currentDate, type);
            celebrationsArray.push(celebrationObj);
          }
        });
        loggerLogData(true, `***********Birthday looping ended********************${new Date()}`, "", false);
      }

      if (serviceAwardResult && serviceAwardResult.length) {
        loggerLogData(true, `***********Service looping started********************${new Date()}`, "", false);
        serviceAwardResult.map((obj: any) => {
          //If user has any service award filter and add [image,name] from users search list
          if (obj && obj.user) {
            const type = "an anniversary";
            const celebrationObj = getCelebrationObj(obj, currentDate, type);
            celebrationsArray.push(celebrationObj);
          }
        });
        loggerLogData(true, `***********Service looping ended********************${new Date()}`, "", false);
      }
      // console.log("====celebrationsArray", celebrationsArray);

      if (celebrationsArray && celebrationsArray.length) {
        celebrationsArray = _.slice(_.orderBy(celebrationsArray, "upcomingDateDiff", "asc"), 0, 3);
      }

      const userData = JSON.stringify(celebrationsArray);
      res.writeHead(200, {
        'Content-Length': Buffer.byteLength(userData),
        'Content-Type': 'application/json'
      });
      res.write(userData);
      res.end();


    } catch (e) {
      // newrelic.noticeError(e);
      // const errArr = ['try-catch-err', 'outlook-connect', 'command.ts', 'fetchCelebrationsOrRecognitionsAndFilter', e.message];
      // logger.debug(errArr.join(":"));
      // logger.error("fetchCelebrationsOrRecognitions error", e);
      newRelicErrLog(e, "apiController.ts/fetchCelebrationsOrRecognitionsAndFilter", "");
      const description = {
        "RequestDetails": { appUserId: appUserData.appUserId, appTenantId: appUserData.appTenantId, reqUrl: req.url, reqMethod: req.method },
        "ErrMessage": e.message
      };
      botAuth.saveLogData(JSON.stringify(description), "apiController.ts/fetchCelebrationsOrRecognitionsAndFilter", appUserData.appTenantId, "error");
      loggerLogData(true, "*******************Error in fetchCelebrationsOrRecognitions err *******************", "", true);
      res.send(400, ERRORRESPONSEFROMACHIEVERS);
    }
  }






  async logout(req: any, res: any, next: any) {
    let appUserData: any;
    try {
      // logger.debug("inside logout method");
      // console.log("inside logout method");
      const userToken = req.body.userToken;

      appUserData = await botAuth.jwtDecodePayload(userToken);

      const callback = function (response, err) {
        if (err) {
          // logger.error(err);
          res.send(400, "Error processing the request.");
        }
        else {
          res.send(200, "User successfully logged out.");
        }
      };

      botAuth.extensionSignOut(appUserData.appUserId, appUserData.appTenantId, callback);
    }
    catch (e) {
      // let errArr = ['try-catch-err', 'msteams-connect', 'apiController.ts', 'logout', e.message];
      // newrelic.noticeError(e);
      // logger.debug(errArr.join(":"));
      // logger.error("error in user log out. Error: \n" + e);
      newRelicErrLog(e, "apiController.ts/logout", "");
      const description = {
        "RequestDetails": { appUserId: appUserData.appUserId, appTenantId: appUserData.appTenantId, reqUrl: req.url, reqMethod: req.method },
        "ErrMessage": e.message
      };
      botAuth.saveLogData(JSON.stringify(description), "apiController.ts/logout", appUserData.appTenantId, "error");
      loggerLogData(true, "*******************Error in logout err *******************", "", true);
    }
  }

  async getCurrentMember(req: any, res: any, next: any) {
    let appUserData: any;
    let options: any;
    try {

      loggerLogData(true, "************************inside get recog types****************************", "", false);
      consoleLogData(true, "************************inside get recog types****************************", "", false);
      appUserData = await botAuth.jwtDecodePayload(req.body.userToken);

      const userInfo = await botAuth.getUserInfo(appUserData.appUserId, appUserData.appTenantId);

      loggerLogData(true, "inside getRecogTypes", "", false);
      consoleLogData(true, "inside getRecogTypes", "", false);

      if (userInfo.user.achieverAccessToken) {

        if (userInfo.user.achieverAccessToken.length > 0) {

          options = {
            uri: "https://" + userInfo.user.subDomain + achieverAPI.baseUrl + achieverAPI.currentMember,
            headers: {
              'Authorization': 'Bearer ' + userInfo.user.achieverAccessToken
            },
            json: true
          };

          const curInfoData = await request(options);

          curInfoData.subDomain = userInfo.user.subDomain;
          curInfoData.achieverApplicationID = userInfo.user.achieverApplicationID;

          const recogTypes = JSON.stringify(curInfoData);
          res.writeHead(200, {
            'Content-Length': Buffer.byteLength(recogTypes),
            'Content-Type': 'application/json'
          });
          res.write(recogTypes);
          res.end();
        }
      }
      else {
        res.send(200, "invalid_token");
      }
    }
    catch (e) {
      // let errArr = ['try-catch-err', 'msteams-connect', 'apiController.ts', 'getCurrentMember', e.message];
      // logger.debug(errArr.join(":"));
      newRelicErrLog(e, "apiController.ts/getCurrentMember", "");
      const description = {
        "RequestDetails": { appUserId: appUserData.appUserId, appTenantId: appUserData.appTenantId, options },
        "ErrMessage": e.message
      };
      botAuth.saveLogData(JSON.stringify(description), "apiController.ts/getCurrentMember", appUserData.appTenantId, "error");
      loggerLogData(true, "*******************Error in getCurrentMember err *******************", "", true);
      if (parseInt(e.statusCode) == 401) {
        // logger.debug("Achievers Token expiry. Error: \n" + e);
        // console.log("Achievers Token expiry. Error: \n" + e);
        res.send(200, "invalid_token");
      }
      else {
        // newrelic.noticeError(e);
        // logger.debug("Error in getCurrent-member: " + e);
        // console.log("Error in getCurrent-member: " + e);
        res.send(400, "Error in getting response from achievers API");
      }
    }
  }

  async getgifgrid(req: any, res: any, next: any) {
    let appUserData: any;
    let options: any;
    try {
      loggerLogData(true, "************************inside user fetch****************************", "", false);
      consoleLogData(true, "************************inside user fetch****************************", "", false);

      const squery = req.body.squery;
      // logger.debug(squery);
      // console.log(squery);
      appUserData = await botAuth.jwtDecodePayload(req.body.userToken);

      const userInfo = await botAuth.getUserInfo(appUserData.appUserId, appUserData.appTenantId);

      if (userInfo.user) {
        if (userInfo.user.achieverAccessToken.length > 0) {
          options = {
            uri: "https://" + userInfo.user.subDomain + achieverAPI.baseUrl + achieverAPI.imageSearch,
            qs: {
              pageSize: 24,
              q: squery
            },
            headers: {
              'Authorization': 'Bearer ' + userInfo.user.achieverAccessToken
            },
            json: true
          };

          const gifGrid = await request(options);

          loggerLogData(true, 'inside request handler in userFetch', "", false);
          consoleLogData(true, 'inside request handler in userFetch', "", false);
          const imgData = JSON.stringify(gifGrid);
          // logger.debug(imgData);
          // console.log(imgData);
          res.writeHead(200, {
            'Content-Length': Buffer.byteLength(imgData),
            'Content-Type': 'application/json'
          });
          res.write(imgData);
          res.end();
        }
      }
      else {
        res.send(401, "User not logged in.");
      }
    }
    catch (e) {
      // let errArr = ['try-catch-err', 'msteams-connect', 'apiController.ts', 'getgifgrid', e.message];
      // logger.debug(errArr.join(":"));
      newRelicErrLog(e, "apiController.ts/getgifgrid", "");
      const description = {
        "RequestDetails": { appUserId: appUserData.appUserId, appTenantId: appUserData.appTenantId, options },
        "ErrMessage": e.message
      };
      botAuth.saveLogData(JSON.stringify(description), "apiController.ts/getgifgrid", appUserData.appTenantId, "error");
      loggerLogData(true, "*******************Error fetching images *******************", "", true);
      if (parseInt(e.statusCode) == 401) {
        // logger.error("Achievers Token expiry. Error: \n" + e);
        res.send(401, "Unauthorized request");
      }
      else {
        // newrelic.noticeError(e);
        // logger.error(e);
        res.send(400, "Error fetching images");
      }
    }
  }

  async postRecognition(req: any, res: any, next: any) {
    let appUserData: any;
    let recogParams: any;
    try {
      // logger.debug("************************inside post recog function****************************", req.body);
      // console.log("************************inside post recog function****************************", req.body);

      consoleLogData(true, "****************** postRecognition-1 ************************", "", false);
      // logger.debug(req.body);

      appUserData = await botAuth.jwtDecodePayload(req.body.userToken);

      consoleLogData(true, "****************** postRecognition-2 ************************", "", false);

      const userInfo = await botAuth.getUserInfo(appUserData.appUserId, appUserData.appTenantId);
      const info = {
        achieverMemberId: userInfo.user.achieverMemberId,
        achieverAccessToken: userInfo.user.achieverAccessToken,
        subDomain: userInfo.user.subDomain
      };
      let userInfoFromAPI;
      userInfoFromAPI = await botAuth.getUserInfoFromAPI(info);
      if (userInfoFromAPI) {
        userInfoFromAPI = JSON.parse(userInfoFromAPI);
      }
      else {
        consoleLogData(true, "Making up userInfoFromAPI", "", false);
        userInfoFromAPI = {
          preferredLanguage: "en-US"
        };
      }
      const curLang = userInfoFromAPI.preferredLanguage;

      consoleLogData(true, "****************** postRecognition-3 ************************", "", false);

      if (userInfo.user) {
        consoleLogData(true, "****************** postRecognition-4 ************************", "", false);

        if (userInfo.user.achieverAccessToken.length > 0) {
          loggerLogData(true, "inside if condition", "", true);
          loggerLogData(true, "constructed params", "", false);
          consoleLogData(true, "inside if condition", "", false);
          consoleLogData(true, "constructed params", "", false);

          consoleLogData(true, "****************** postRecognition-5 ************************", "", false);

          // tracking when recognition is complete on Teams in Pendo
          // Track event : External | Integration: Recognition – Complete
          // console.log("nominees",req.body.nominees)
          // console.log(req.body.scope);
          const properties = {
            medium: "msTeams",
            scope: req.body.scope,
            recipientCount: req.body.nominees.split(",").length
          };
          trackEventInPendo("External | Integration: Recognition – Complete", userInfo.user.achieverApplicationID, userInfo.user.achieverMemberId, properties);
          if (req.body.imageSelected == "upload") {
            const imageData: any = await uploadFile(userInfo.user.subDomain, achieverAPI.baseUrl, userInfo.user.achieverAccessToken, achieverAPI.iamgePostUrl, req.file);
            recogParams = {
              uri: "https://" + userInfo.user.subDomain + achieverAPI.baseUrl + achieverAPI.postRecog,
              method: 'POST',
              formData: {
                nominees: req.body.nominees,
                recognitionText: req.body.recognitionText,
                criterionId: req.body.criterionId,
                points: req.body.points,
                fileUploadId: imageData && imageData.fileUploadId,
                shareWith: req.body.shareWith,
                notifyManagers: req.body.notifyManagers,
                useUserIds: req.body.useUserIds,
                source: 'teams'
              },
              headers: {
                'Authorization': 'Bearer ' + userInfo.user.achieverAccessToken,
                'Content-Type': 'multipart/form-data; charset=UTF-8'
              },
              json: true
              };
              setTimeout(() => {
                  sendRecognitionToAchievers(recogParams, res, userInfo.user.subDomain, imageData.url, curLang);
              }, 100);

          } else if (req.body.imageSelected == "giphy") {
            const imageData: any = await saveFileFromUrlAndUploadFile(userInfo.user.achieverAccessToken, req.body.imageUrl, userInfo.user.subDomain, achieverAPI.baseUrl);
            recogParams = {
              uri: "https://" + userInfo.user.subDomain + achieverAPI.baseUrl + achieverAPI.postRecog,
              method: 'POST',
              formData: {
                nominees: req.body.nominees,
                recognitionText: req.body.recognitionText,
                criterionId: req.body.criterionId,
                points: req.body.points,
                fileUploadId: imageData && imageData.fileUploadId,
                shareWith: req.body.shareWith,
                notifyManagers: req.body.notifyManagers,
                useUserIds: req.body.useUserIds,
                source: 'teams'
              },
              headers: {
                'Authorization': 'Bearer ' + userInfo.user.achieverAccessToken,
                'Content-Type': 'multipart/form-data; charset=UTF-8'
              },
              json: true
            };

            setTimeout(() => {
                sendRecognitionToAchievers(recogParams, res, userInfo.user.subDomain, imageData.url, curLang);
            }, 100);
          }
          else {
            // logger.debug("******************inside post for default image ************************");
            // logger.debug("https://" + userInfo.user.subDomain + achieverAPI.baseUrl + achieverAPI.postRecog);
            // console.log("******************inside post for default image ************************");
            // console.log("https://" + userInfo.user.subDomain + achieverAPI.baseUrl + achieverAPI.postRecog);

            consoleLogData(true, "****************** postRecognition-53-1 ************************", "", false);

            recogParams = {
              uri: "https://" + userInfo.user.subDomain + achieverAPI.baseUrl + achieverAPI.postRecog,
              method: 'POST',
              formData: {
                nominees: req.body.nominees,
                recognitionText: req.body.recognitionText,
                criterionId: req.body.criterionId,
                points: req.body.points,
                shareWith: req.body.shareWith,
                notifyManagers: req.body.notifyManagers,
                useUserIds: req.body.useUserIds,
                source: 'teams'
              },
              headers: {
                'Authorization': 'Bearer ' + userInfo.user.achieverAccessToken,
                'Content-Type': 'multipart/form-data; charset=UTF-8'
              },
              json: true
            };
            // console.log("****************** postRecognition-53-2 ************************", recogParams.formData);


            setTimeout(() => {
              sendRecognitionToAchievers(recogParams, res, userInfo.user.subDomain, req.body.imageUrl, curLang);
            }, 100);

            // console.log("****************** postRecognition-53-3 ************************", recogParams.formData);

          }
        }
      }
      else {
        res.send(401, "User not logged in.");
      }
    }

    catch (e) {
      // let errArr = ['try-catch-err', 'msteams-connect', 'apiController.ts', 'postRecognition', e.message];
      // newrelic.noticeError(e);
      // logger.debug(errArr.join(":"));

      // console.log(e);
      // logger.error(e);
      newRelicErrLog(e, "apiController.ts/postRecognition", "");
      const description = {
        "RequestDetails": { appUserId: appUserData.appUserId, appTenantId: appUserData.appTenantId, recogParams },
        "ErrMessage": e.message
      };
      botAuth.saveLogData(JSON.stringify(description), "apiController.ts/postRecognition", appUserData.appTenantId, "error");
      loggerLogData(true, "*******************Error in postRecognition *******************", "", true);

      let errMessage: string;
      if (e.message.indexOf("maximum height or width") > -1) {
        errMessage = "This image could not be uploaded. It exceeds the maximum height or width.";
        res.send(400, errMessage);
      }
      else {
        // logger.error("There was an error creating your recognition. Please try again." + e);
        // logger.error(e);
        // console.log("There was an error creating your recognition. Please try again." + e);
        res.send(400, "There was an error creating your recognition. Please try again.");
      }
    }
  }

  async getAccessTokens(req: any, res: any) {
    let options: any;
    const stateData = jwtSimple.decode(req.query.state, "12345");
    const cardHelper = new CardHelper();
    const address = {
      channelId: "msteams",
      user: { id: stateData.userId },
      channelData: {
        tenant: {
          id: stateData.tenantId
        }
      },
      bot: {
        id: stateData.msTeamBotId,
        name: stateData.msTeamBotName
      },
      serviceUrl: stateData.msTeamBotServiceUrl,
      useAuth: true
    };
    let isGlobalPermissionCard = false;
    try {
      // logger.debug("Encoded state : ", req.query.state);
      // logger.debug("Decoded state : ", stateData);
      // const userInfo = botAuth.getUserInfo(stateData.userId, stateData.tenantId);
      if (stateData && stateData.type && stateData.type === "GlobalCalenderPermission") {
        isGlobalPermissionCard = true;
      }
      const curLang = stateData.ccLocale;
      const langHelper = new LanguageHelper(curLang);
      if (!req.query.code) {
        consoleLogData(true, "User denied it in the grant page", "", false);
        loggerLogData(true, "User denied it in the grant page", "", false);
        await botAuth.saveTokens(stateData.userId, stateData.tenantId, stateData.name, stateData.msTeamBotId, stateData.msTeamBotName, stateData.msTeamBotServiceUrl, "DENIED", "DENIED");
        let deniedCard;
        // handling global permission card denial
        if(isGlobalPermissionCard){
          // res.status(200).send("We cannot show you recognition suggestions as you have not given us permission to access your calender");
          res.redirect(`https://teams.microsoft.com/l/entity/${process.env.APP_CLIENT_ID}/home`)
        }
        else {
          res.status(200).send(langHelper.getTranslation("We_cannot_show_you_suggested_times_because_you_have_not_given_us_permission_to_access_your_calendar_However_you_can_still_schedule_your_Coffee_Chats_manually") + "<script>window.close()</script>");
          deniedCard = cardHelper.getDeniedCard(curLang, stateData.user1Name, stateData.user1Email, stateData.user2Name, stateData.user2Email);
          let msg = new builder.Message().address(address).addAttachment(deniedCard);
          bot.send(msg);
        }
      }
      else {
        options = {
          'method': 'POST',
          'url': 'https://login.microsoftonline.com/' + stateData.tenantId + '/oauth2/v2.0/token/',
          formData: {
            'client_id': process.env.APP_CLIENT_ID,
            'code': req.query.code,
            'scope': 'Calendars.Read offline_access',
            'grant_type': 'authorization_code',
            'client_secret': process.env.APP_CLIENT_SECRET,
            'redirect_uri': process.env.APP_BASE_URL + '/auth/redirect'
          }
        };
        // console.log("OPTIONS FOR POST", options);
        request.post(options, async function (error, response) {
          if (error) {
            throw new Error(error);
          }
          else {
            // console.log(response.body);
            let graphTokenData = JSON.parse(response.body);
            await botAuth.saveTokens(stateData.userId, stateData.tenantId, stateData.name, stateData.msTeamBotId, stateData.msTeamBotName, stateData.msTeamBotServiceUrl, graphTokenData.access_token, graphTokenData.refresh_token);
            if (isGlobalPermissionCard) {
              const updatedValue = await botAuth.updateRecognitionSuggestionStatus(false, stateData.tenantId, stateData.userId,'msteams');
              consoleLogData(true,"user authroized the permission","",false)
              res.redirect(`https://teams.microsoft.com/l/entity/${process.env.APP_CLIENT_ID}/home`)
              // const userFacingCard = cardHelper.getGlobalAuthorizeCard(curLang);
              // let msg = new builder.Message().address(address).addAttachment(userFacingCard);
              // bot.send(msg);
            }
            else {
              const timingsCard = '' + (await calendarController.getUserSchedule(curLang, stateData.user1Name, stateData.user1Email, stateData.user2Name, stateData.user2Email, graphTokenData.access_token, stateData.user1Time, stateData.user1Timezone, stateData.user2Timezone));
              let msg = new builder.Message().address(address).addAttachment(JSON.parse(timingsCard));
              bot.send(msg);
              res.status(200).send("Success<script>window.close();</script >");
            }
          }
        });
      }
      // tracking Authorize click in pendo
      // get details from database using tenantId and userId
      // Track event : External | Integration: Notification – Clicked
      try {
        const userDetailsFromDB = await botAuth.getUserInfo(stateData.userId, stateData.tenantId);
        if (!isGlobalPermissionCard && userDetailsFromDB.user && userDetailsFromDB.user.achieverApplicationID && userDetailsFromDB.user.achieverMemberId) {
          // tracking in pendo only if we find the user details in db
          const programPk = userDetailsFromDB.user.achieverApplicationID;
          const userPk = userDetailsFromDB.user.achieverMemberId;
          const properties = {
            medium: "msTeams",
            event: "coffeeChat",
            buttonClicked: "Authorize",
          };
          trackEventInPendo("External | Integration: Notification – Clicked", programPk, userPk, properties);
        }
      }
      catch (error) {
        // let errArr = ['try-catch-err', 'msteams-connect', 'apiController.ts', 'getAccessTokens', error.message];
        // newrelic.noticeError(error);
        // logger.debug(errArr.join(":"));
        // console.log(error.message);
        newRelicErrLog(error, "apiController.ts/getAccessTokens", "");
        const description = {
          "RequestDetails": { userId: stateData.userId, tenantId: stateData.tenantId, options },
          "ErrMessage": error.message
        };
        botAuth.saveLogData(JSON.stringify(description), "apiController.ts/getAccessTokens", stateData.tenantId, "error");
        loggerLogData(true, "*******************Error in getAccessTokens *******************", "", true);
      }

    } catch (error) {
      // let errArr = ['try-catch-err', 'msteams-connect', 'apiController.ts', 'getAccessTokens', error.message];
      // newrelic.noticeError(error);
      // logger.debug(errArr.join(":"));
      const stateData = jwtSimple.decode(req.query.state, "12345");

      newRelicErrLog(error, "apiController.ts/getAccessTokens", "");
      const description = {
        "RequestDetails": { userId: stateData.userId, tenantId: stateData.tenantId, options },
        "ErrMessage": error.message
      };
      botAuth.saveLogData(JSON.stringify(description), "apiController.ts/getAccessTokens", stateData.tenantId, "error");
      loggerLogData(true, "*******************Error in getAccessTokens222 *******************", "", true);

      const curLang = stateData.ccLocale;
      const fallBackCard = cardHelper.getFallBackCard(curLang, stateData.user1Name, stateData.user1Email, stateData.user2Name, stateData.user2Email);
      let msg = new builder.Message().address(address).addAttachment(fallBackCard);
      bot.send(msg);
    }
  }

  getAllUserDetails(req: any, res: any) {
    // console.log("Request body in getAllUserDetails : ", req.body);
    connector.fetchMembers(req.body.serviceUrl, req.body.conversationId, async function (err: any, result: any) {
      consoleLogData(true, "Using connector.fetch members", "", false);
      if (err) {
        // logger.error('Fetch members error. Error: \n' + err);
        // console.log('Fetch members error. Error: \n' + err);
      }
      else {
        // console.log("********* fetchMembers ***************", result);
        let nameString = "";
        let nameStringArr = [];
        if (result.length > 0) {
          for (let i = 0; i < result.length; i++) {
            nameStringArr.push(result[i].email);
          }
        }
        nameString = nameStringArr.join("**&&**");
        // console.log(nameString);
        res.status(200).send(nameString);
      }
      // console.log("number of users in conversation : ",result.length)
      // tracking the event when user clicked on add all members
      // Track event: External | Integration: Recognition - Add all members
      const properties = {
        "medium": "msTeams",
        "scope": req.body.scope,
        "recipientCount": result.length
      };
      trackEventInPendo("External | Integration: Recognition - Add all members", req.body.programPk, req.body.userPk, properties);
    });
  }

  async newsFeedActivities(req: any, res: any) {
    try {
      const userAzureId = true;
      const azureUserId = req.query.azureUserId;
      const tenantId = req.query.tenantId;


      let finalObject: any = [];
      let finalNudgesObject: any = [];
      let pgDetails;

      const userData = await botAuth.getUserInfo(azureUserId, tenantId, userAzureId);
      let languageHelper;

      // tracking when Custom tab is – Opened on Teams in Pendo
      // Track event : External | Integration: Custom tab – Opened
      const properties = {
        medium: "msTeams"
      };
      if (userData.user && userData.user.achieverApplicationID && userData.user.achieverMemberId) {
        trackEventInPendo("External | Integration: Custom tab – Opened", userData.user.achieverApplicationID, userData.user.achieverMemberId, properties);
      }
      pgDetails = viewSettings;
      pgDetails['configFile'] = configFile;
      pgDetails['azureUserId'] = azureUserId;
      pgDetails['tenantId'] = tenantId;
      pgDetails['source'] = "homeTab";
      if (userData && userData.user) {
        languageHelper = new LanguageHelper(userData.user.locale || "en-US");
        const tokenData = {
          appUserId: userData.user.appUserId,
          appTenantId: tenantId
        };
        const token = botAuth.jwtEncodePayload(tokenData);
        pgDetails["userToken"] = token;
        pgDetails["userPk"] = userData.user.achieverMemberId;
      }
      pgDetails = JSON.stringify(pgDetails);
      const Static_tab_show = configFile.Static_tab_show;
      const programsEnabled = Static_tab_show["programPkEnabled"];
      const programsNotEnabled = Static_tab_show["programPkNotEnabled"];
      const usersEnabled = Static_tab_show["userPkEnabled"];

      let showActivityFeed = false;
      if (userData && userData.user && userData.user.achieverAccessToken && (userData.user.expireAt >= (Date.now() / 1000))) {
            pgDetails = JSON.parse(pgDetails);
            pgDetails.accessTokenValid = true;
            pgDetails = JSON.stringify(pgDetails);
        if (programsNotEnabled.indexOf(userData.user.achieverApplicationID) > -1) {
          showActivityFeed = false;
        } else if (programsEnabled.indexOf(userData.user.achieverApplicationID) > -1 || programsEnabled.indexOf("all") > -1) {
          showActivityFeed = true;
        }
        if(usersEnabled.indexOf(userData.user.achieverMemberId) > -1){
          showActivityFeed = true;
        }
      }

      if (userData && userData.user && userData.user.achieverAccessToken && showActivityFeed) {

        // User is signed in. Show news feed
        const appId = req.query.tenantId;
        const appUserId = userData.user.appUserId;
        const userToken = await jwtSign({
          createdDate: new Date(),
          appUserId: appUserId,
          appTenantId: appId,
          connectorType: "msteams"
        });

        pgDetails = JSON.parse(pgDetails);

        pgDetails['token'] = userToken;
        pgDetails['appId'] = appId;
        pgDetails['appUserId'] = appUserId;
        pgDetails['msTeamBotId'] = userData.user.msTeamBotId;
        pgDetails['msTeamBotName'] = userData.user.msTeamBotName;
        pgDetails['msTeamBotServiceUrl'] = userData.user.msTeamBotServiceUrl;
        pgDetails['achieverAccessToken'] = userData.user.achieverAccessToken;
        pgDetails['subDomain'] = userData.user.subDomain;

        // for pendo tracking
        const notValidForTracking = ["null", "undefined"];
        const achieverApplicationID = userData.user.achieverApplicationID && !notValidForTracking.includes(userData.user.achieverApplicationID) ? userData.user.achieverApplicationID : "";
        const achieverMemberId = userData.user.achieverMemberId && !notValidForTracking.includes(userData.user.achieverMemberId) ? userData.user.achieverMemberId : "";
        pgDetails['achieverApplicationID'] = achieverApplicationID;
        pgDetails['achieverMemberId'] = achieverMemberId;

        const viewPage = req.query.viewPage || true;
        const offset = req.query.offset || '';
        const domain = "https://" + userData.user.subDomain + ".achievers.com";

        let newsFeed: any = await middlewareGrpcCalls({ endpoint: "grpcNewsFeedData", userList: userData.user.achieverMemberId, programId: userData.user.achieverApplicationID, authorization: userToken, offset: offset });
        const nextCursor = newsFeed.pageInfo.end_cursor
        const totalParticipants = [];
        totalParticipants.push(userData.user.achieverMemberId);
        // Gather all the participants ids   event_type: 'MilestoneEvent',
        for (let i = 0; i < newsFeed.newsfeedEvents.length; i++) {

            let obj = JSON.parse(JSON.stringify(newsFeed.newsfeedEvents[i]));
            let participantsCount = obj.participants.length;
            let participantsFromNewFeed = [];
            if (participantsCount > 3) {
              participantsFromNewFeed = obj.participants.splice(0, 3);
            } else {
              participantsFromNewFeed.push(...obj.participants);
            }

            totalParticipants.push(...participantsFromNewFeed);
            // console.log("participantsFromNewFeed", participantsFromNewFeed);
            // Push creator id as well
            totalParticipants.push(obj.creator);

        }

        //Single api call for Creators, user, n Participants
        let totalParticipantsResponse: any = await middlewareGrpcCalls({ endpoint: "grpcUsersDetailInfo", userList: totalParticipants, programId: userData.user.achieverApplicationID, authorization: userToken, offset: null });


        totalParticipantsResponse = totalParticipantsResponse.users;
        let userObj = totalParticipantsResponse.find(user => user.user_pk == userData.user.achieverMemberId);

        pgDetails['userName'] = userObj.first_name + " " + userObj.last_name;
        pgDetails['nextCursor'] = nextCursor
        pgDetails = JSON.stringify(pgDetails);

        userObj = JSON.stringify({
          first_name: userObj.first_name,
          last_name: userObj.last_name,
          profile_image_url: domain + userObj.profile_image_url,
          subDomain: domain,
          user_pk: userObj.user_pk
        });

        // Current member total budget
        const options = {
          uri: "https://" + userData.user.subDomain + achieverAPI.baseUrl + achieverAPI.currentMember,
          headers: {
            'Authorization': 'Bearer ' + userData.user.achieverAccessToken
          },
          json: true
        };

        const curInfoData = await request(options);
        const totalBudget = curInfoData && curInfoData.memberBudget && curInfoData.memberBudget.totalPointsAvailable ? curInfoData.memberBudget.totalPointsAvailable : false;

        // Newsfeed data

        for (let i = 0; i < newsFeed.newsfeedEvents.length; i++) {
          if(newsFeed.newsfeedEvents[i].event_type === "PostcardEvent" || newsFeed.newsfeedEvents[i].event_type === "MilestoneEvent") {

            let obj = newsFeed.newsfeedEvents[i];
            obj['can_boost'] = obj['can_boost'] || true;
            let newObj = {};
            let participantsResponse: any = [];
            let participantsCount = obj.participants.length;
            if (participantsCount > 3) {
              obj.participants = obj.participants.splice(0, 3);
            }

            let userResponse = _.find(totalParticipantsResponse, { user_pk: obj.creator.toString() });
            for (let i = 0; i < obj.participants.length; i++) {
              let participantObj = _.find(totalParticipantsResponse, { user_pk: obj.participants[i].toString() });
              if (participantObj) {
                participantsResponse.push(participantObj);
              }
            }

            let participantsUserProfilePK = [];

            let createrProfileUrl = [];

            participantsResponse.forEach(obj => {
              var lastArray = participantsResponse[participantsResponse.length - 1];
              createrProfileUrl.push(lastArray.user_pk);
            });

            participantsResponse.forEach(obj => {
              participantsResponse;
              participantsUserProfilePK.push(obj.user_pk);
            });


            let participantProfileImages = _.map(participantsResponse, "profile_image_url");
            const participantProfileNames = _.map(participantsResponse, o => o["first_name"] + " " + o["last_name"]);
            newObj["participantProfileImages"] = _.map(participantProfileImages, ele => ele ? `${domain}${ele}` : process.env.APP_BASE_URL + "/docs/user_profile.jpg");
            newObj["participantsCount"] = participantsCount > 3 ? participantsCount - 3 : "";
            newObj["title"] = obj.title.replace('\r\nRecognized for \"', "").replace('\"', "");
            newObj["event_type"] = obj.event_type;
            newObj["message"] = obj.message;
            newObj["numberOfLikes"] = obj["number_of_likes"];
            newObj["numberOfComments"] = obj["number_of_comments"];
            newObj["numberOfBoosts"] = obj["number_of_boosts"];
            newObj["numberOfAction"] = obj["number_of_action"];
            newObj["newsfeedEventId"] = obj["newsfeed_event_id"];
            newObj["boosted_by_user"] = obj["boosted_by_user"];
            newObj["totalBudget"] = totalBudget;
            newObj['can_boost'] = obj['can_boost'];
            newObj["recognizedDate"] = moment(obj['date']).format('LL') + " at " + moment(obj.date).format('HH:mm');
            newObj["userImage"] = userResponse && userResponse.profile_image_url ? domain + userResponse.profile_image_url : process.env.APP_BASE_URL + "/docs/user_profile.jpg";
            newObj["newsFeedURL"] = domain + obj["public_url"] + "?utm_medium=msteams&utm_term=customTab";
            newObj["newsFeedURLComment"] = domain + obj["public_url"] + "?utm_medium=msteams&utm_term=customTab_comment";
            newObj["liked_by_user"] = obj["liked_by_user"];
            if(obj.event_type === "PostcardEvent"){
              newObj["userFullName"] = userResponse.first_name + " " + userResponse.last_name;
              newObj["recognitionImg"] = (obj.image_href).indexOf("https://") == 0 || (obj.image_href).indexOf("http://") == 0 ? `${obj.image_href}` : `${domain}${obj["image_href"]}`;
            } else if(obj.event_type === "MilestoneEvent") {
              newObj["userFullName"] = "Aspire";
              newObj["SignCardUrl"] = `${domain}${obj.card_url}`;
              newObj["recognitionImg"] = process.env.APP_BASE_URL + "/docs/celebration.svg";
            }
            if (obj.event_type === "PostcardEvent") {
              if (participantsCount === 1) {
                newObj["createrProfileUrl"] = `${domain}/profile/${obj.creator}`;
                newObj["participantsText"] = `<b class="recgTextBTag USER_was_recognized_by_FROMUSER"><a href="${domain}/profile/${participantsUserProfilePK[0]}"  target="_blank">${participantProfileNames[0]}</a></b>${languageHelper.getTranslation("USER_was_recognized_by_FROMUSER", ["", newObj["userFullName"]])}<a href="${domain}/profile/${obj.creator}"  target="_blank"></a>`;
              } else if (participantsCount === 2) {
                newObj["createrProfileUrl"] = `${domain}/profile/${obj.creator}`;
                newObj["participantsText"] = `<b class="recgTextBTag USERS_were_recognized_by_FROMUSER"><a href="${domain}/profile/${participantsUserProfilePK[0]}" target="_blank">${participantProfileNames[0]}</a> and <a href="${domain}/profile/${participantsUserProfilePK[1]}" target="_blank">${participantProfileNames[1]}</a></b>${languageHelper.getTranslation("USERS_were_recognized_by_FROMUSER", ["", newObj["userFullName"]])}<a href="${domain}/profile/${obj.creator}"  target="_blank"></a>`;
              } else if (participantsCount === 3) {
                newObj["createrProfileUrl"] = `${domain}/profile/${obj.creator}`;
                newObj["participantsText"] = `<b class="recgTextBTag USERS_were_recognized_by_FROMUSER"><a href="${domain}/profile/${participantsUserProfilePK[0]}" target="_blank">${participantProfileNames[0]}</a>, <a href="${domain}/profile/${participantsUserProfilePK[1]}" target="_blank">${participantProfileNames[1]}</a>, and <a href="${domain}/profile/${participantsUserProfilePK[2]}" target="_blank">${participantProfileNames[2]}</a></b>${languageHelper.getTranslation("USERS_were_recognized_by_FROMUSER", ["", newObj["userFullName"]])}<a href="${domain}/profile/${obj.creator}"  target="_blank"></a>`;
              } else if (participantsCount > 3) {
                newObj["createrProfileUrl"] = `${domain}/profile/${obj.creator}`;
                newObj["participantsText"] = `<b class="recgTextBTag USERS_were_recognized_by_FROMUSER"><a href="${domain}/profile/${participantsUserProfilePK[0]}" target="_blank">${participantProfileNames[0]}</a>, <a href="${domain}/profile/${participantsUserProfilePK[1]}" target="_blank">${participantProfileNames[1]}</a>, <a href="${domain}/profile/${participantsUserProfilePK[2]}" target="_blank">${participantProfileNames[2]}</a>, and ${participantsCount-3} others</b>${languageHelper.getTranslation("USERS_were_recognized_by_FROMUSER", ["", newObj["userFullName"]])}<a href="${domain}/profile/${obj.creator}"  target="_blank"></a>`;
              }
            } else if(obj.event_type === "MilestoneEvent") {
              newObj["participantsText"] = `<b class="recgTextBTag"><a href="${domain}/profile/${participantsUserProfilePK[0]}"  target="_blank">${participantProfileNames[0]}</a></b> had a celebration`;
            }
            finalObject.push(newObj);

          }

        }

        if (finalObject.length) {
          finalObject = JSON.stringify(finalObject);
          // If show static tabs is disabled
          if (viewPage !== 'false') {
            res.render("staticTabPage", { nextCursor, finalObject, pgDetails, userObj, showHelpPage: false });
          }
          // For pagination send only resultset
          if (viewPage === 'false') {
            res.json({ nextCursor, finalObject, userObj });
          }
        } else {
          finalObject = JSON.stringify(finalObject);
          res.render("staticTabPage", { finalObject, pgDetails, userObj, showHelpPage: true });
        }
      } else {
        finalObject = JSON.stringify(finalObject);
        // User is not signed in. Show help page
        res.render("staticTabPage", { finalObject, pgDetails, userObj: null, showHelpPage: true });
      }

    } catch (error) {
      // let errArr = ['try-catch-err', 'msteams-connect', 'apiController.ts', 'newsFeedActivities', error.message];
      // newrelic.noticeError(error);
      newRelicErrLog(error, "apiController.ts/newsFeedActivities", "");
      const description = {
        "RequestDetails": { azureUserId: req.query.azureUserId, tenantId: req.query.tenantId, reqUrl: req.url, reqMethod: req.method },
        "ErrMessage": error.message
      };
      botAuth.saveLogData(JSON.stringify(description), "apiController.ts/newsFeedActivities", req.query.tenantId, "error");
      loggerLogData(true, "*******************Error in newsFeedActivities *******************", "", true);
      let finalObject: any = [];
      let pgDetails = viewSettings;
      pgDetails['nextCursor'] = ""
      finalObject = JSON.stringify(finalObject);
      pgDetails = JSON.stringify(pgDetails);
      res.render("staticTabPage", { finalObject, pgDetails, userObj: null, showHelpPage: true });
      // logger.debug(errArr.join(":"));
    }
  }

  async newsFeedActions(req: any, res: any) {
    let appUserData: any;
    try {
      const { actionType, newsFeedEventId, userToken, optionsCall = false, postCall = false, postLikesCall = false, deleteLikesCall = false, getBoostsCall = false, getLikesCall = false } = req.body;
      appUserData = await botAuth.jwtDecodePayload(userToken);
      const userInfo = await botAuth.getUserInfo(appUserData.appUserId, appUserData.appTenantId);
      const userDetails = {
        achieverAccessToken: userInfo.user.achieverAccessToken,
        subDomain: userInfo.user.subDomain,
        achieverApplicationID: userInfo.user.achieverApplicationID,
        achieverMemberId: userInfo.user.achieverMemberId
      };
      const actionResponse = await botAuth.getNewsFeedActionDetails(actionType, newsFeedEventId, userDetails, optionsCall, postCall, postLikesCall, deleteLikesCall, getBoostsCall, getLikesCall);
      res.send(actionResponse);
    } catch (error) {
      // let errArr = ['try-catch-err', 'msteams-connect', 'apiController.ts', 'newsFeedActions', error.message];
      // newrelic.noticeError(error);
      // logger.debug(errArr.join(":"));
      newRelicErrLog(error, "apiController.ts/newsFeedActions", "");
      const description = {
        "RequestDetails": { appUserId: appUserData.appUserId, appTenantId: appUserData.appTenantId, reqUrl: req.url, reqMethod: req.method },
        "ErrMessage": error.message
      };
      botAuth.saveLogData(JSON.stringify(description), "apiController.ts/newsFeedActions", appUserData.appTenantId, "error");
      loggerLogData(true, "*******************Error in newsFeedActions *******************", "", true);
      res.status(500).send(error);
    }
  }
  async getRecognitionSuggestions(req: any, res: any) {
    try {
      const userAzureId = true;
      const azureUserId = req.body.azureUserId;
      const tenantId = req.body.tenantId;
      const userName = req.body.userName;
      const userData = await botAuth.getUserInfo(azureUserId, tenantId, userAzureId);
      const stateData = jwtSimple.encode({
        tenantId: userData.user.appId,
        userId: userData.user.appUserId,
        type: "GlobalCalenderPermission",
        msTeamBotId: userData.user.msTeamBotId,
        msTeamBotName: userData.user.msTeamBotName,
        msTeamBotServiceUrl: userData.user.msTeamBotServiceUrl,
        ccLocale: userData.user.locale || "en-US",
      }, "12345");

      const url = "https://login.microsoftonline.com/" + userData.user.appId + "/oauth2/v2.0/authorize?client_id=" + process.env.APP_CLIENT_ID + "&response_type=code&redirect_uri=" + process.env.APP_BASE_URL + "/auth/redirect&response_mode=query&scope=Calendars.Read%20offline_access&state=" + stateData;
      if (userData.user && userData.user.msGraphRefreshToken && userData.user.msGraphRefreshToken != 'DENIED') {
        const newTokenDetails = JSON.stringify(await botAuth.refreshTokens(userData.user.msGraphRefreshToken));
        const nt = JSON.parse(newTokenDetails);
        if (nt.refreshToken == "Invalid") {
          return res.json({ finalArray: [],askRecognitionSuggestion:true,link:url,reload:false});
        } else {
          // Save the new tokens to the database
          userData.user.msGraphRefreshToken = nt.refresh_token
          userData.user.msGraphAccessToken = nt.access_token
          await botAuth.saveTokens(userData.user.appUserId, tenantId, userName, userData.user.msTeamBotId, userData.user.msTeamBotName, userData.user.msTeamBotServiceUrl, nt.access_token, nt.refresh_token);
        }
      }
      else{
        return res.json({ finalArray: [],askRecognitionSuggestion:true,link:url,reload:false});
      }
      if (userData.user && userData.user.msGraphRefreshToken && userData.user.msGraphRefreshToken != 'DENIED'  && [false,null].includes(userData.user.isRecognitionSuggestionHide)) {
        let currentUserAddress = userData.user.achieverEmailId.toLowerCase();
        let currentUserMemberId = userData.user.achieverMemberId;
        let response = await botAuth.getOutlookCalendarView(userData.user.msGraphAccessToken);
        let collaborationArray = [];
        let groupRecArray = [];
        if (response && response.value) {
          response.value = response.value.sort(function(a,b){
            const date1: any = new Date(a.start.dateTime.split('T')[0])
            const date2: any = new Date(b.start.dateTime.split('T')[0])
            return date2 - date1
          })
          if(response.value.length > 20){
            response.value = response.value.slice(response.value.length-20,response.value.length)
          }
          response.value.forEach((mainObj) => {
            if(mainObj.attendees){
            const meetingAttendees = JSON.parse(JSON.stringify(mainObj.attendees));
            // Consider meeting attendees more than 2 and less than 10
            if (meetingAttendees.length > 2 && meetingAttendees.length <= 10) {
              let isOrganizerAParticipant = meetingAttendees.findIndex(o2 => o2.emailAddress.address.toLowerCase() === mainObj.organizer.emailAddress.address.toLowerCase());
              if(isOrganizerAParticipant === -1){
                mainObj.organizer['type'] = "required"
                mainObj.organizer['status'] ={...meetingAttendees[0].status}
                mainObj.organizer['status']['response'] = "accepted"
                meetingAttendees.push(mainObj.organizer)
              }
              let currentUserIndex = meetingAttendees.findIndex(o2 => o2.emailAddress.address.toLowerCase() === currentUserAddress);
              if (currentUserIndex > -1) {
                const startTime = mainObj.start.dateTime;
                const endTime = mainObj.end.dateTime;
                const organizerEmailAdd = mainObj.organizer.emailAddress.address.toLowerCase();
                const groupMeetingAttendees = [];
                const emailListOfAttendees = []
                for (let i = 0; i < meetingAttendees.length; i++) {
                  let attendeeObj: any = {};
                  const attendeeEmailAdd = meetingAttendees[i].emailAddress.address.toLowerCase();
                  attendeeObj.email = attendeeEmailAdd;
                  attendeeObj.startTime = startTime;
                  attendeeObj.endTime = endTime;
                  attendeeObj.organizerEmailAdd = organizerEmailAdd;
                  if ((meetingAttendees[i].status.response === "accepted" || attendeeEmailAdd == organizerEmailAdd) && attendeeEmailAdd != currentUserAddress) {
                    emailListOfAttendees.push(attendeeEmailAdd)
                    groupMeetingAttendees.push({ ...attendeeObj, type: "group"});
                    // set the count value for occurance of same user in every meeting
                    let obj = { ...attendeeObj, type: "collaboration" };
                    if (collaborationArray && collaborationArray.length) {
                      let obIndex = collaborationArray.findIndex(o2 => o2.email.toLowerCase() === obj.email);
                      if (obIndex > -1) {
                        collaborationArray[obIndex].count += 1;
                      } else {
                        obj.count = 1;
                        const { daysDiff, timeDiff } = calculateDate(obj.startTime);
                        obj.daysDiff = daysDiff;
                        obj.timeDiff = timeDiff;
                        collaborationArray.push(obj);
                      }
                    } else {
                      obj.count = 1;
                      const { daysDiff, timeDiff } = calculateDate(obj.startTime);
                      obj.daysDiff = daysDiff;
                      obj.timeDiff = timeDiff;
                      collaborationArray.push(obj);
                    }
                  }
                }
                if (groupRecArray.length <= 3 && groupMeetingAttendees.length) {
                  const email = groupMeetingAttendees.map(o => o.email);
                  let result = groupRecArray.map(function (element) {
                    let counter = 0
                    emailListOfAttendees.map((eachEmail)=>{
                      if(element.email.includes(eachEmail))
                        counter += 1
                    })
                    if(counter === emailListOfAttendees.length && element.email.length === emailListOfAttendees.length) return true
                    else return false
                  });
                  if(!result.includes(true)){
                    const { daysDiff, timeDiff } = calculateDate(groupMeetingAttendees[0].startTime);
                    groupRecArray.push({ ...groupMeetingAttendees[0], email, daysDiff, timeDiff });

                  }
                }
              }
            }
          }
          });
          if (collaborationArray.length) {
            collaborationArray = _.orderBy(_.filter(collaborationArray, o => o.count > 5), "count", "desc"); // count should be greater than 5, added 1 for demo
            const emailList2 = []
            collaborationArray.forEach((eachRecord)=>{
              emailList2.push(eachRecord.email.toLowerCase())
            })
            const emailList = []
            groupRecArray.forEach((eachRecord,index)=>{
              if(eachRecord.email.length === 1){
                if(emailList2.includes(eachRecord.email.toString().toLowerCase()))
                {
                  groupRecArray.splice(index,1)
                }
              }
              emailList.push(eachRecord.email.toString())
            })
          }

          let consolidatedArray = [...groupRecArray, ...collaborationArray];

          let allEmails = _.uniq(_.flattenDeep(_.map(consolidatedArray, "email")));
          allEmails = allEmails.join(",");
          // Check if user exists in Achievers for recognition type collaboration
          const requestData = {
            uri: "https://" + userData.user.subDomain + achieverAPI.baseUrl + achieverAPI.getUsers,
            qs: {
              'q': allEmails
            },
            headers: {
              'Authorization': 'Bearer ' + userData.user.achieverAccessToken
            },
            json: true
          };
          let memberResult = await rp(requestData);
          memberResult = memberResult.members;

          // Filter the users if they exist on Achievers platform
          for (let i = 0; i < consolidatedArray.length; i++) {
            let obj = JSON.parse(JSON.stringify(consolidatedArray[i]));
            if (obj.type == "group") {
              const profileImages = [];
              const profileNames = [];
              const achieversMemberIds = [];
              if (obj.email) {
                obj.email.map(emailAddress => {
                  let ifExist = _.find(memberResult, o => o.emailAddress.toLowerCase() == emailAddress);
                  if (ifExist) {
                    profileImages.push(ifExist.profileImageUrl);
                    profileNames.push(ifExist.fullName);
                    achieversMemberIds.push(ifExist.id);
                  } else {
                    const emailIndex = _.indexOf(consolidatedArray[i].email, emailAddress);
                    consolidatedArray[i].email.splice(emailIndex, 1);
                  }
                });
              }
              if (profileImages.length) {
                consolidatedArray[i].profileImages = profileImages;
                consolidatedArray[i].profileNames = profileNames;
                consolidatedArray[i].memberId = achieversMemberIds;
              } else {
                consolidatedArray[i] = null;
              }
            } else {
              let ifExist = _.find(memberResult, o => o.emailAddress.toLowerCase() == consolidatedArray[i].email);
              if (ifExist) {
                consolidatedArray[i].profileImages = ifExist.profileImageUrl;
                consolidatedArray[i].profileNames = ifExist.fullName;
                consolidatedArray[i].memberId = ifExist.id;
              } else {
                consolidatedArray[i] = null;
              }
            }
          }

          // Check if the user has been recognized in the past week or not for collaboration type, if yes don't consider the user
          consolidatedArray = _.filter(consolidatedArray)
          let groupRecFinalArray = _.filter(consolidatedArray, o => o.type == "group")
          let collabFilterIfRecInWeek = _.filter(consolidatedArray, o => o.type == "collaboration")
          // if (groupRecFinalArray.length) {
          //   mainLoop : for (let index = 0; index < groupRecFinalArray.length; index++) {
          //     for (let j = 0; j < groupRecFinalArray[index].memberId.length; j++) {
          //       let recogResult = await fetchRecognitions(userData.user.subDomain, achieverAPI.baseUrl, groupRecFinalArray[index].memberId[j], userData.user.achieverAccessToken)
          //       if (recogResult) {
          //         for (const groupRecogResult of recogResult) {
          //           if (groupRecogResult.nominator.id == _.toInteger(currentUserMemberId)) {
          //             groupRecFinalArray.splice(index, 1);
          //             if(!groupRecFinalArray.length){
          //               break mainLoop
          //             }
          //           }
          //         }
          //       }
          //     }
          //   }
          // }
          let collabRecFinalArray = []
          let finalArray = []

          if (collabFilterIfRecInWeek.length) {
            for (let i = 0; i < collabFilterIfRecInWeek.length; i++) {
              let recognitionsArray = [];
              if (collabRecFinalArray.length <= 2) {
                let recognitionResult = await fetchRecognitions(userData.user.subDomain, achieverAPI.baseUrl, collabFilterIfRecInWeek[i].memberId, userData.user.achieverAccessToken);
                if (recognitionResult) {
                  // We would get the list in ascending order, take only the recent nominee record

                  for (const recognitionResultObj of recognitionResult) {
                    let nomineeslist = _.find(recognitionResultObj.nominees, { id: _.toInteger(collabFilterIfRecInWeek[i].memberId) });
                    if (nomineeslist) {
                      if (recognitionResultObj.nominator.id != _.toInteger(currentUserMemberId)) {
                        collabRecFinalArray.push(collabFilterIfRecInWeek[i])
                        recognitionsArray.push(collabFilterIfRecInWeek[i])
                        break
                      }
                    }
                  }
                }
              }
            }
            if (collabRecFinalArray.length || groupRecFinalArray.length) {
              finalArray.push(...groupRecFinalArray, ...collabRecFinalArray);
              finalArray = _.orderBy(finalArray, "timeDiff", "asc");
            }
            res.json({ finalArray });
          } else {
            if (groupRecFinalArray.length) {
              finalArray.push(...groupRecFinalArray);
              finalArray = _.orderBy(finalArray, "timeDiff", "asc");
            }
            res.json({ finalArray });
          }
        } else {
          const description = {
            "RequestDetails": { body: req.body },
            "ErrMessage": response
          };
          botAuth.saveLogData(JSON.stringify(description), "apiController.ts/getRecognitionSuggestions", "recogSuggestionsLoadingError", "error");
          res.json({ finalArray: [] });
        }
      } else {
        const homeTab = `https://teams.microsoft.com/l/entity/${process.env.APP_CLIENT_ID}/home`
        res.json({ finalArray: [],askRecognitionSuggestion:true,link:homeTab,reload:true});
      }

    } catch (error) {
      let errArr = ['try-catch-err', 'msteams-connect', 'apiController.ts', 'getRecognitionSuggestions', error.message];
      newrelic.noticeError(error);
      logger.debug(errArr.join(":"));
      const description = {
        "RequestDetails": { body: req.body },
        "ErrMessage": error.message
      };
      botAuth.saveLogData(JSON.stringify(description), "apiController.ts/getRecognitionSuggestions", req.body.tenantId, "error");
        res.status(500).send({ "message": error.message });
    }
  }
  async fetchUpcomingCelebrationConfig(req: any, res: any) {
    let options: any;
    try {
      const userInfo = await botAuth.getUserInfo(req.query.azureUserId, req.query.tenantId, true);
      if (userInfo && userInfo.user && userInfo.user.achieverAccessToken) {
        if (userInfo.user.achieverAccessToken.length > 0) {
          options = {
            uri: "https://" + userInfo.user.subDomain + achieverAPI.baseUrl + "/upcoming-celebrations",
            method: "GET",
            headers: {
              'Authorization': 'Bearer ' + userInfo.user.achieverAccessToken
            },
            qs: {
              startDate: moment().format(), // Change this to 7 days, kept 9 only for demo
              endDate: moment().add(365, "days").format()
            },
            json: true
          };
          const celebrationDetails = await request(options);
          res.send(celebrationDetails);
        }
      }
      else {
        res.send(200, "invalid_token");
      }
    }
    catch (e) {
      newRelicErrLog(e, "apiController.ts/fetchUpcomingCelebrationConfig", "");
      const description = {
        "RequestDetails": { appUserId: req.query.azureUserId, appTenantId: req.query.tenantId },
        "ErrMessage": e.message
      };
      botAuth.saveLogData(JSON.stringify(description), "apiController.ts/fetchUpcomingCelebrationConfig", req.query.tenantId, "error");
      loggerLogData(true, "*******************Error in fetchUpcomingCelebrationConfig err *******************", "", true);
      if (parseInt(e.statusCode) == 401) {
        res.send(200, "invalid_token");
      }
      else {
        res.send(400, "Error in getting response from achievers API");
      }
    }
  }

  async createComment(req: any, res: any) {
    try {

      const userAzureId = true;

      const { comment, newsfeedEventId, azureUserId, tenantId } = req.body;
      const userInfo = await botAuth.getUserInfo(azureUserId, tenantId, userAzureId);

      // get current language
      const userLocale = userInfo && userInfo.user && userInfo.user.locale ? userInfo.user.locale : "en-US";
      let langHelper = new LanguageHelper(userLocale);
      // Adds a comment to the newsfeed item.
      const requestData = {
        method: "POST",
        uri: "https://" + userInfo.user.subDomain + achieverAPI.baseUrl + `/newsfeed-events/${newsfeedEventId}/comments`,
        form: {
          newsfeedEventId: req.body.newsfeedEventId,
          comment: comment
        },
        headers: {
          Authorization: 'Bearer ' + userInfo.user.achieverAccessToken
        },
        json: true
      };

      let memberResult = await rp(requestData);
      for (let i = 0; i < memberResult.items.length; i++) {
        let obj = memberResult.items[i];
        memberResult.items[i]["dateCommented"]= langHelper.getTranslation("DATE_at_TIME",[moment(obj['date']).format('LL'),moment(obj.dateCommented).format('HH:mm')]);
      }
      res.json(memberResult);

    } catch (error) {
      let errArr = ['try-catch-err', 'msteams-connect', 'apiController.ts', 'createComment', error.message];
      newrelic.noticeError(error);
      logger.debug(errArr.join(":"));
    }
  }

  async updateUserRecognitionSuggestionHide(req: any, res: any) {
    try {
      const userAzureId = true;
      const { isRecognitionSuggestionHide, azureUserId, tenantId } = req.body;

      let userInfo = await botAuth.getUserInfo(azureUserId, tenantId, userAzureId);
      // update isRecognitionSuggestionHide in user tables
      if(userInfo.user){
        const updatedValue = await botAuth.updateRecognitionSuggestionStatus(isRecognitionSuggestionHide, userInfo.user.appId, userInfo.user.appUserId, userInfo.user.appType);
        res.json({link:`https://teams.microsoft.com/l/entity/${process.env.APP_CLIENT_ID}/home`});
      } else {
        res.json({ message: "User not found" });
      }
    } catch (error) {
      let errArr = ['try-catch-err', 'msteams-connect', 'apiController.ts', 'updateUserRecognitionSuggestionHide', error.message];
      newrelic.noticeError(error);
      logger.debug(errArr.join(":"));
    }
  }


  async feedDetailView(req: any, res: any) {
    let appUserData;
    let deatiledData;
    let newsfeedEventId = req.query.newsfeedEventId;
    try {
      appUserData = await botAuth.jwtDecodePayload(req.query.authorization);


      const userInfoResp = await botAuth.getUserInfo(appUserData.appUserId, appUserData.appTenantId);
      const languageHelper = new LanguageHelper(userInfoResp.user.locale);

      // get current language
      const userLocale = userInfoResp && userInfoResp.user && userInfoResp.user.locale ? userInfoResp.user.locale : "en-US";
      let langHelper = new LanguageHelper(userLocale);

      if (userInfoResp.user && userInfoResp.user.achieverAccessToken) {
        if (userInfoResp.user.achieverAccessToken.length > 0) {
          deatiledData = {
            uri: "https://" + userInfoResp.user.subDomain + achieverAPI.baseUrl + `/newsfeed-events/${newsfeedEventId}`,
            // uri: "https://example.sandbox.achievers.com/api/v5/newsfeed-events/71441161",
            method: 'GET',
            headers: {
              'Authorization': 'Bearer ' + userInfoResp.user.achieverAccessToken
            },
            json: true
          };

          const grtResp = await request(deatiledData);
          const participantsCount = grtResp.participants.length;

          if (participantsCount === 1) {
            grtResp["profileImgComment"] = `<img src=${grtResp.participants[0].profileImageUrl}  class="img-thumbnail recogniseInsightsRounded rImg1">`;
            grtResp["createrProfileUrl"] = `${userInfoResp.user.subDomain}/profile/${grtResp.creator.id}`;
            grtResp["participantsText"] = `<b class="recgTextBTag USER_was_recognized_by_FROMUSER"><a href="${userInfoResp.user.subDomain}/profile/${grtResp.participants[0].id}"  target="_blank">${grtResp.participants[0].name}</a></b>${languageHelper.getTranslation("USER_was_recognized_by_FROMUSER", ["", grtResp.creator["name"]])}<a href="${userInfoResp.user.subDomain}/profile/${grtResp.creator.id}"  target="_blank"></a>`;
          } else if (participantsCount === 2) {
            grtResp["profileImgComment"] = `<img src=${grtResp.participants[0].profileImageUrl}  class="img-thumbnail recogniseInsightsRounded rImg1"> <img src=${grtResp.participants[1].profileImageUrl}  class="img-thumbnail recogniseInsightsRounded rImg2">`;
            grtResp["createrProfileUrl"] = `${userInfoResp.user.subDomain}/profile/${grtResp.creator.id}`;
            grtResp["participantsText"] = `<b class="recgTextBTag USERS_were_recognized_by_FROMUSER"><a href="${userInfoResp.user.subDomain}/profile/${grtResp.participants[0].id}" target="_blank">${grtResp.participants[0].name}</a> and <a href="${userInfoResp.user.subDomain}/profile/${grtResp.participants[1].id}" target="_blank">${grtResp.participants[1].name}</a></b>${languageHelper.getTranslation("USERS_were_recognized_by_FROMUSER", ["", grtResp.creator["name"]])}<a href="${userInfoResp.user.subDomain}/profile/${grtResp.creator.id}"  target="_blank"></a>`;
          } else if (participantsCount === 3) {
            grtResp["profileImgComment"] = `<img src=${grtResp.participants[0].profileImageUrl}  class="img-thumbnail recogniseInsightsRounded rImg1"> <img src=${grtResp.participants[1].profileImageUrl}  class="img-thumbnail recogniseInsightsRounded rImg2"> <img src=${grtResp.participants[2].profileImageUrl}  class="img-thumbnail recogniseInsightsRounded rImg3">`;
            grtResp["createrProfileUrl"] = `${userInfoResp.user.subDomain}/profile/${grtResp.creator.id}`;
            grtResp["participantsText"] = `<b class="recgTextBTag USERS_were_recognized_by_FROMUSER"><a href="${userInfoResp.user.subDomain}/profile/${grtResp.participants[0].id}" target="_blank">${grtResp.participants[0].name}</a>, <a href="${userInfoResp.user.subDomain}/profile/${grtResp.participants[1].id}" target="_blank">${grtResp.participants[1].name}</a>, and <a href="${userInfoResp.user.subDomain}/profile/${grtResp.participants[2].id}" target="_blank">${grtResp.participants[2].name}</a></b>${languageHelper.getTranslation("USERS_were_recognized_by_FROMUSER", ["", grtResp.creator["name"]])}<a href="${userInfoResp.user.subDomain}/profile/${grtResp.creator.id}"  target="_blank"></a>`;
          } else if (participantsCount > 3) {
            grtResp["participantsCount"] = participantsCount > 3 ? participantsCount - 3 : "";
            grtResp["profileImgComment"] = `<img src=${grtResp.participants[0].profileImageUrl}  class="img-thumbnail recogniseInsightsRounded rImg1"> <img src=${grtResp.participants[1].profileImageUrl}  class="img-thumbnail recogniseInsightsRounded rImg2"> <img src=${grtResp.participants[2].profileImageUrl}  class="img-thumbnail recogniseInsightsRounded rImg3">`;
            grtResp["createrProfileUrl"] = `${userInfoResp.user.subDomain}/profile/${grtResp.creator.id}`;
            grtResp["participantsText"] = `<b class="recgTextBTag USERS_were_recognized_by_FROMUSER"><a href="${userInfoResp.user.subDomain}/profile/${grtResp.participants[0].id}" target="_blank">${grtResp.participants[0].name}</a>, <a href="${userInfoResp.user.subDomain}/profile/${grtResp.participants[1].id}" target="_blank">${grtResp.participants[1].name}</a>, <a href="${userInfoResp.user.subDomain}/profile/${grtResp.participants[2].id}" target="_blank">${grtResp.participants[2].name}</a>, and ${participantsCount-3} others</b>${languageHelper.getTranslation("USERS_were_recognized_by_FROMUSER", ["", grtResp.creator["name"]])}<a href="${userInfoResp.user.subDomain}/profile/${grtResp.creator.id}"  target="_blank"></a>`;
          }

          grtResp["date"]= langHelper.getTranslation("DATE_at_TIME",[moment(grtResp['date']).format('LL'),moment(grtResp.date).format('HH:mm')]);
          // }
          res.json(grtResp);
        }
      }
      else {
        res.send(200, "invalid_token");
      }
    }
    catch (error) {
      let errArr = ['try-catch-err', 'msteams-connect', 'apiController.ts', 'feedDetailView', error.message];
      newrelic.noticeError(error);
      logger.debug(errArr.join(":"));
      // console.log("errrrrrrrr", error);
    }

  }


  async feedDetailComments(req:any, res: any) {
    let appUserData;
    let deatiledData;
    let newsfeedEventId = req.query.newsfeedEventId;
    try {
      appUserData = await botAuth.jwtDecodePayload(req.query.authorization);

      const userInfoResp = await botAuth.getUserInfo(appUserData.appUserId, appUserData.appTenantId);
      // get current language
      const userLocale = userInfoResp && userInfoResp.user && userInfoResp.user.locale ? userInfoResp.user.locale : "en-US";
      let langHelper = new LanguageHelper(userLocale);

      if (userInfoResp.user && userInfoResp.user.achieverAccessToken) {
        if (userInfoResp.user.achieverAccessToken.length > 0) {
          deatiledData = {
            uri: "https://" + userInfoResp.user.subDomain + achieverAPI.baseUrl + `/newsfeed-events/${newsfeedEventId}/comments`,
            // uri: "https://example.sandbox.achievers.com/api/v5/newsfeed-events/71441161/comments",
            method: 'GET',
            headers: {
              'Authorization': 'Bearer ' + userInfoResp.user.achieverAccessToken
            },
            json: true
          };

          const grtResp = await request(deatiledData);

          for (let i = 0; i < grtResp.items.length; i++) {
            let obj = grtResp.items[i];
            obj["dateCommented"]= langHelper.getTranslation("DATE_at_TIME",[moment(obj['date']).format('LL'),moment(obj.date).format('HH:mm')]);
          }

          res.json(grtResp);

        }
      }
      else {
        res.send(200, "invalid_token");
      }
    }
    catch (error) {
      let errArr = ['try-catch-err', 'msteams-connect', 'apiController.ts', 'feedDetailComments', error.message];
      newrelic.noticeError(error);
      logger.debug(errArr.join(":"));
    }

  }

  async getUserNewsfeedConfig(req: any, res: any) {
    try {
      const { userId, programId, subDomain } = req.query;
      const response = await botAuth.getUserNewsfeedConfiguration(userId, programId, subDomain);
      res.send(response);
    } catch(error) {
      let errArr = ['try-catch-err', 'msteams-connect', 'apiController.ts', 'getUserNewsfeedSuggestion', error.message];
      newrelic.noticeError(error);
      logger.debug(errArr.join(":"));
    }
  }

  async updateUserNewsfeedConfig(req: any, res: any) {
    try {
      const { userId, programId, subDomain, newsfeedConfigPks } = req.body;
      const response = await botAuth.updateUserNewsfeedConfiguration(userId, programId, subDomain, newsfeedConfigPks);
      res.send(response);
    } catch(error) {
      let errArr = ['try-catch-err', 'msteams-connect', 'apiController.ts', 'updateUserNewsfeedSuggestion', error.message];
      newrelic.noticeError(error);
      logger.debug(errArr.join(":"));
    }
  }

}
export default apiController;
