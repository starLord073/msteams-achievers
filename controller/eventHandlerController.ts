import { AnyARecord } from "dns";

const builder = require("botbuilder");
const teams = require("botbuilder-teams");
const fs = require('fs');
const util = require('util');
const writeFilePromise = util.promisify(fs.writeFile);
const deleteFilePromise = util.promisify(fs.unlink);
const logger = require('../util/winlogger.js').logger;
const newrelic = require('newrelic');
import { LanguageHelper } from "../languages/languageHelper";
import { trackEventInPendo } from './../src/pendoTrackEvent';


import { consoleLogData, loggerLogData, newRelicErrLog } from '../util/logsHandler';


let bot: any;
let botAuth: any;
let baseUrl: string;
let connector: any;

const invokeHandlerFunc = async function (event, callback, botAuth, baseUrl, connector) {
  // console.log(req);
  // console.log("**************** invokeHandlerFunc event*************",event);
  loggerLogData(true, "inside invoke handler", "", false);
  consoleLogData(true, "inside invoke handler", "", false);
  const curLang = event.entities[0].locale;
  let langHelper = new LanguageHelper(curLang);
  try {
    let userId;
    let conversationId;
    let serviceUrl;
    let email;
    let tenantId;
    let name;
    let cb;
    let objectId;

    userId = event.address.user.id;
    conversationId = event.address.conversation.id;
    serviceUrl = event.address.serviceUrl;
    tenantId = event.sourceEvent.tenant.id;
    name = event.address.user.name;

    let botId = event.address.bot.id;
    let botName = event.address.bot.name;

    // logger.debug("userId: " + userId);
    // logger.debug("tenantId: " + tenantId);
    // logger.debug("serviceUrl: " + serviceUrl);
    // console.log("userId: " + userId);
    // console.log("tenantId: " + tenantId);
    // console.log("serviceUrl: " + serviceUrl);
    // console.log("********* event.address-2 ***************", event.address);
    // console.log("********* event.sourceEvent ***************", event.sourceEvent);
    // console.log("--------------------------Will try to fetch channel list-----------------------------------------");
    // console.log("Event.address.conversation.conversationType : ", event.address.conversation.conversationType);
    // var teamId = session.message.sourceEvent.team.id;
    if (event.sourceEvent && event.sourceEvent.team && event.sourceEvent.team.id) {
      consoleLogData(true, "Going to call the connector.fetchChannelList", "", false);
      connector.fetchChannelList(
        serviceUrl,
        event.sourceEvent.team.id,
        (err: any, result: any) => {
          // console.log("Inside connector.fetch channel list :err,result : ", err, result);
          if (err) {

            // console.log("********* fetchChannelList-err ***************", err);

            // session.endDialog('There is an error');
          }
          else {
            // console.log("********* fetchChannelList-result ***************", result);

            // session.endDialog('%s', JSON.stringify(result));
          }
        }
      );
    }

    consoleLogData(true, "Trying to get ConversationInfo", "", false);
    let conversationResult;
    try {
      conversationResult = await botAuth.getConversationInfo(tenantId, conversationId);
      consoleLogData(true, "Successful in getting info from CONVERSATIONS table", "", false);
    } catch (error) {
      // let errArr = ['try-catch-err', 'msteams-connect', 'eventHandlerController.ts', 'invokeHandlerFunc', error.message];
      // newrelic.noticeError(error);
      // logger.debug(errArr.join(":"));
      // console.log("Error getting result from database CONVERSATIONS table ", error);
      newRelicErrLog(error, "eventHandlerController.ts/invokeHandlerFunc", "");
      const description = {
        "RequestDetails": { tenantId, conversationId, userId, serviceUrl },
        "ErrMessage": error.message
      };
      botAuth.saveLogData(JSON.stringify(description), "eventHandlerController.ts/invokeHandlerFunc", tenantId, "error");
      loggerLogData(true, "*******************error in invokeHandlerFunc function*******************", "", true);
    }
    // console.log("Conversation result : ", conversationResult);


    let channelSize;
    connector.fetchMembers(serviceUrl, conversationId, async function (err: any, result: any) {
      consoleLogData(true, "Using connector.fetch members", "", false);
      if (err) {
        // logger.error('Fetch members error. Error: \n' + err);
        // console.log('Fetch members error. Error: \n' + err);
        let adaptiveCard;
        adaptiveCard = {
          "type": "AdaptiveCard",
          "body": [
            {
              "type": "TextBlock",
              "text": langHelper.getTranslation("To_send_a_recognition_you_must_add_the_Achievers_for_Teams_app_to_this_team_or_chat_To_do_so_visit_the_Achievers_App_in_the_Microsoft_Teams_App_store_and_hit_the_drop_down_to_add_to_a_team_chat_or_meeting"),
              "size": "Medium",
              "wrap": true
            },
            {
              "type": "ColumnSet",
              "columns": [
                {
                  "type": "Column",
                  "items": [
                    {
                      "type": "Image",
                      "url": baseUrl + "/docs/howToAddAchievers_1.png"
                    }
                  ],
                  "width": 50
                },
                {
                  "type": "Column",
                  "items": [
                    {
                      "type": "Image",
                      "url": baseUrl + "/docs/howToAddAchievers_2.png"
                    }
                  ],
                  "width": 50
                },
                {
                  "type": "Column",
                  "items": [
                    {
                      "type": "Image",
                      "url": baseUrl + "/docs/howToAddAchievers_3.png"
                    }
                  ],
                  "width": 50
                }
              ]
            }
          ],
          "version": "1.0"
        };
        const platform = event.entities[0].platform.toLowerCase();

        if (platform == "android" || platform == "ios") {
          adaptiveCard.body = [
            {
              "type": "TextBlock",
              "text": langHelper.getTranslation("To_send_a_recognition_you_must_add_the_Achievers_for_Teams_app_to_this_team_or_chat_To_do_so_visit_the_Achievers_App_in_the_Microsoft_Teams_App_store_and_hit_the_drop_down_to_add_to_a_team_chat_or_meeting"),
              "size": "small",
              "wrap": true
            },
            {
              "type": "Image",
              "url": baseUrl + "/docs/howToAddAchievers_1.png"
            },
            {
              "type": "Image",
              "url": baseUrl + "/docs/howToAddAchievers_2.png"
            },
            {
              "type": "Image",
              "url": baseUrl + "/docs/howToAddAchievers_3.png",
            }
          ];
        }

        const respFetchError = new teams.TaskModuleContinueResponse()
          .card(adaptiveCard)
          .height("Medium")
          .width("Medium")
          .title(langHelper.getTranslation("Achievers_Recognition_portal"))
          .toResponseOfFetch();

        callback(null, respFetchError, 200);
      }
      else {

        // console.log("********* fetchMembers ***************", result);


        loggerLogData(true, "inside fetch members else", "", false);
        consoleLogData(true, "inside fetch members else", "", false);
        let i: number;
        // console.log("++++++++++ extension triggered from ++++++++++ ", event.sourceEvent.source.name);
        // console.log("EVENT : ", event);
        channelSize = result.length;
        // console.log("Channel size : ", channelSize)
        const resp = await botAuth.getUserInfo(userId, tenantId);
        for (i = 0; i < result.length; i++) {

          if (event.sourceEvent.source.name != "powerbar") {
            if (result[i].id == userId && result[i].email && resp.client && resp.client.programFK) {
              await botAuth.updateUserInfo(result[i].id, result[i].tenantId, result[i].email, result[i].name, botId, botName, serviceUrl, null, result[i].objectId);
            }

            if (!conversationResult) {
              if (i == 0) {
                loggerLogData(true, "Tenant/Convo pair not found in conversation table. Updating user list", "", false);
                await botAuth.updateConversationInfo(tenantId, conversationId);
              }
              if (result[i].id != userId && result[i].email && resp.client && resp.client.programFK) {

                await botAuth.updateUserInfo(result[i].id, result[i].tenantId, result[i].email, result[i].name, botId, botName, serviceUrl, null, result[i].objectId);
              }
            } else {
              loggerLogData(true, "Tenant/Convo pair exists. Not updating user infos", "", true);
            }

          }
          if (userId == result[i].id) {
            email = result[i].email;
            objectId = result[i].objectId;
          }
        }
        // console.log(email);
        // console.log(resp)
        consoleLogData(true, "***************User info from Achievers***************", "", false);
        // console.log("event.address.conversation.conversationType:  ", event.address.conversation.conversationType)
        // console.log("event.sourceEvent.source.name:   ", event.sourceEvent.source.name)


        let teamsScope;
        if (event.address.conversation.conversationType === undefined && event.sourceEvent.source.name === "powerbar")
          teamsScope = "searchBar";
        else if (event.address.conversation.conversationType === "groupChat")
          teamsScope = "chat";
        else
          teamsScope = event.address.conversation.conversationType;

        // console.log("Sub domain : ", resp.user.subDomain);
        const info = {
          achieverMemberId: resp.user.achieverMemberId,
          achieverAccessToken: resp.user.achieverAccessToken,
          subDomain: resp.user.subDomain
        };
        let userInfoFromAPI;
        userInfoFromAPI = await botAuth.getUserInfoFromAPI(info);
        if (userInfoFromAPI) {
          userInfoFromAPI = JSON.parse(userInfoFromAPI);
        }
        else {
          consoleLogData(true, "Making up userInfoFromAPI", "", false);
          userInfoFromAPI = {
            preferredLanguage: "en-US",
            fullName: name
          };
        }
        const locale = userInfoFromAPI.preferredLanguage;
        // console.log("LOCALE EVENT : ", locale);
        langHelper = new LanguageHelper(locale);
        let languageUpdatedNotificationSent = resp.user.languageUpdatedNotificationSent;
        const makelanguageUpdationMessageFeatureLive = false;
        if (!languageUpdatedNotificationSent && (info.achieverAccessToken && info.achieverMemberId && info.subDomain) && makelanguageUpdationMessageFeatureLive) {
          // console.log("Inside update preferred language")
          let msg = new builder.Message().address(event.address);
          msg.text(langHelper.getTranslation("We_have_updated_your_preferred_language_based_on_the_settings_in_your_Achievers_program"));
          bot.send(msg);
          await botAuth.updateUserInfo(userId, tenantId, email, name, botId, botName, serviceUrl, true, objectId);
        }


        loggerLogData(true, "at extensionFetchHandler after receiving response", "", false);
        consoleLogData(true, "at extensionFetchHandler after receiving response", "", false);
        // console.log(response);
        const description = {
          event: "User opened Achievers app",
          achieversAccessToken: resp.user.achieverAccessToken || "No access token in database",
          userId,
          conversationId,
          serviceUrl,
          tenantId,
          name,
          botId,
          botName
        };
        botAuth.saveLogData(description, "eventHanlderController.ts/invokeHandlerFunc", resp.user.subDomain || "AUDIT_LOGS");
        if (resp.user && resp.user.achieverAccessToken) {
          if (resp.user.achieverAccessToken.length > 0) {
            if (!resp.client.programFK) {
              botAuth.saveClientDetails(tenantId, botId, botName, serviceUrl, resp.user.achieverApplicationID || null);
            }
            loggerLogData(true, "Inside OPenFOrm", "", false);
            consoleLogData(true, "Inside OPenFOrm", "", false);
            const tokenData = {
              appUserId: userId,
              appTenantId: tenantId,
              appEmail: email
            };
            const token = botAuth.jwtEncodePayload(tokenData);
            const response = new teams.TaskModuleContinueResponse()
              .url(encodeURI(baseUrl + "/views/viewRecognizePage/?state=" + token + "&userName=" + userInfoFromAPI.fullName + "&lang=" + locale + "&serviceUrl=" + encodeURIComponent(serviceUrl) + "&conversationId=" + conversationId + "&sourceName=" + event.sourceEvent.source.name + "&conversationType=" + event.address.conversation.conversationType + "&subDomain=" + resp.user.subDomain + "&channelSize=" + channelSize + "&userPk=" + parseInt(resp.user.achieverMemberId.trim()) + "&programPk=" + parseInt(resp.user.achieverApplicationID.trim()) + "&platform=" + event.entities[0].platform))
              .height("large")
              .width("large")
              .title(langHelper.getTranslation("Achievers_Recognition_portal"));
            callback(null, response.toResponseOfFetch(), 200);
            // tracking an event in pendo when recognition has been started
            // Track event : External | Integration: Recognition – Started
            const properties = {
              medium: "msTeams",
              scope: teamsScope,
            };
            trackEventInPendo("External | Integration: Recognition – Started", resp.user.achieverApplicationID, resp.user.achieverMemberId, properties);

          }
        }
        else {
          let subDomain = "";
          if (resp && resp.user && resp.user.subDomain) {
            subDomain = resp.user.subDomain;
          } else if (resp && resp.client && resp.client.subDomain) {
            subDomain = resp.client.subDomain;
          }
          const resp1 = botAuth.ExtensionSignIn(event, subDomain);
          callback(null, resp1, 200);
        }
      }
    });
  }
  catch (e) {
    // let errArr = ['try-catch-err', 'msteams-connect', 'eventHandlerController.ts', 'invokeHandlerFunc', e.message];
    // newrelic.noticeError(e);
    // logger.debug(errArr.join(":"));
    // logger.error("Error inside extension fetch task handler. Error: \n" + e);
    // console.log("Error inside extension fetch task handler. Error: \n" + e);

    newRelicErrLog(e, "eventHandlerController.ts/invokeHandlerFunc", "");
    const description = {
      "RequestDetails": { userId: event.address.user.id, conversationId: event.address.conversation.id },
      "ErrMessage": e.message
    };
    botAuth.saveLogData(JSON.stringify(description), "eventHandlerController.ts/invokeHandlerFunc", event.address.user.id, "error");
    loggerLogData(true, "*******************error in invokeHandlerFunc function*******************", "", true);

    callback(e, null, 400);
  }
};

export class botEventHandler {

  constructor(_bot: any, _botAuth: any, _baseUrl: any, _connector: any) {
    // logger.debug("inside botEventHandler constructor.");
    // console.log("inside botEventHandler constructor.");
    // logger.debug(_connector);
    // console.log(_connector);
    bot = _bot;
    botAuth = _botAuth;
    connector = _connector;
    baseUrl = _baseUrl;
  }

  public invokeHandler(event: any, callback: any) {
    consoleLogData(true, "inside involkeHandler: Connector below:", "", false);
    // console.log(connector);
    if (event.name == "task/submit") {
      this.composeExtensionSubmitActionHandler(event, event.value, callback);
    } else {
      invokeHandlerFunc(event, callback, botAuth, baseUrl, connector);
    }
  }
  public composeExtensionSubmitActionHandler(event: any, req: any, callback: any) {
    //Submit event handling comes here.
    // const curLang = event.entities[0].locale;
    const langHelper = new LanguageHelper(req.data.userLocale);
    // console.log("REWDATA : ", req.data);
    try {
      loggerLogData(true, "inside submit action handler....", "", false);
      consoleLogData(true, "inside submit action handler....", "", false);
      if (req.data && req.data.nominees) {
        loggerLogData(true, "inside submit action handler", "", false);
        consoleLogData(true, "inside submit action handler", "", false);
        consoleLogData(true, "*********URL for view recognition*************", "", false);
        // console.log(req.data.newsFeedUrl)
        req.data.newsFeedUrl += "?utm_medium=msteams&utm_term=recognition";
        // console.log(req.data.newsFeedUrl)
        const adaptiveCard = {
          "contentType": "application/vnd.microsoft.card.adaptive",
          "content": {
            "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
            "type": "AdaptiveCard",
            "version": "1.0",
            "body": [
              {
                "type": "ColumnSet",
                "columns": [
                  {
                    "type": "Column",
                    "items": [
                      {
                        "type": "Image",
                        "url": req.data.imageUrl
                      }
                    ],
                    "width": "40"
                  },
                  {
                    "type": "Column",
                    "items": [
                      {
                        "type": "TextBlock",
                        "text": langHelper.getTranslation("Recognized_for") + " \"" + [req.data.criterionVal] + "\"",
                        "size": "Large",
                        "wrap": true
                      },
                      {
                        "type": "TextBlock",
                        "text": req.data.nominees,
                        "weight": "Bolder",
                        "spacing": "None",
                        "wrap": true
                      },
                      {
                        "type": "TextBlock",
                        "text": req.data.recognitionText,
                        "spacing": "Medium",
                        "wrap": true
                      },
                      {
                        "type": "TextBlock",
                        "text": langHelper.getTranslation("From") + " " + [req.data.userFullName],
                        "spacing": "Medium",
                        "wrap": true
                      }
                    ],
                    "width": "60"
                  }
                ],
                "bleed": true
              }
            ],
            "actions": [
              {
                "type": "Action.OpenUrl",
                "title": req.data.newsFeedUrl.toLowerCase().indexOf("apidemo") > 1 ? "View in Achievers" : langHelper.getTranslation("View_recognition"),
                "url": req.data.newsFeedUrl
              }
            ]
          }
        };
        const description = {
          event: "User hit post recognition button",
          reqData: req.data
        };
        botAuth.saveLogData(description, "eventHandlerController.ts/composeExtensionSubmitActionHandler", "AUDIT_LOGS");
        // console.log("EVENT SUBMIT : ", event);
        if (!event.address.conversation.conversationType) {
          consoleLogData(true, "Sending  after building address!", "", false);
          let addressToSendRecognition = {
            channelId: "msteams",
            user: { id: event.address.user.id },
            channelData: {
              tenant: {
                id: event.address.conversation.tenantId
              }
            },
            bot: {
              id: event.address.bot.id,
              name: event.address.bot.name
            },
            serviceUrl: event.address.serviceUrl,
            useAuth: true
          };
          var botmessage = new builder.Message()
            .address(addressToSendRecognition)
            .addAttachment(adaptiveCard);
        }
        else {
          consoleLogData(true, "Sending without building!", "", false);
          var botmessage = new builder.Message()
            .address(event.address)
            .addAttachment(adaptiveCard);
        }
        bot.send(botmessage, function (err) { });
        callback(null, null, 200);
      }
      else {
        loggerLogData(true, "inside submit logout handler", "", false);
        consoleLogData(true, "inside submit logout handler", "", false);
        // var response = new teams.TaskModuleMessageResponse().text("You have been successfully logged out");
        // callback(null, response.toResponseOfSubmit(), 200);
        var botmessage = new builder.Message()
          .address(event.address)
          .text(langHelper.getTranslation("You_have_been_successfully_signed_out_Please_note_that_you_have_to_sign_in_again_to_be_able_to_utilize_Achievers_app_functionality"));

        bot.send(botmessage, function (err) { });

        callback(null, null, 200);
      }

    }
    catch (e) {
      // let errArr = ['try-catch-err', 'msteams-connect', 'eventHandlerController.ts', 'composeExtensionSubmitActionHandler', e.message];
      // newrelic.noticeError(e);
      // logger.debug(errArr.join(":"));
      // logger.error("exception in form submit. Error: \n" + e);
      // console.log("exception in form submit. Error: \n" + e);

      newRelicErrLog(e, "eventHandlerController.ts/composeExtensionSubmitActionHandler", "");
      const description = {
        "RequestDetails": { userId: event.address.user.id, conversationId: event.address.conversation.id },
        "ErrMessage": e.message
      };
      botAuth.saveLogData(JSON.stringify(description), "eventHandlerController.ts/composeExtensionSubmitActionHandler", event.address.user.id, "error");
      loggerLogData(true, "*******************error in composeExtensionSubmitActionHandler function*******************", "", true);

      callback(e, null, 400);
    }
  }

  public async composeExtensionFetchHandler(event: any, req: any, callback: any) {
    const curLang = event.entities[0].locale;
    const langHelper = new LanguageHelper(curLang);
    loggerLogData(true, "+++++++++++++++++Compose Extension fetch request ++++++++++++++++++++", "", false);
    consoleLogData(true, "+++++++++++++++++Compose Extension fetch request ++++++++++++++++++++", "", false);
    // logger.debug(req);
    // console.log(req);
    loggerLogData(true, "+++++++++++++++++Compose Extension fetch platform ++++++++++++++++++++", "", false);
    consoleLogData(true, "+++++++++++++++++Compose Extension fetch platform ++++++++++++++++++++", "", false);
    // console.log(event);
    // logger.debug(event.entities[0].platform);
    // console.log(event.entities[0].platform);
    const userId = event.address.user.id;
    const tenantId = event.sourceEvent.tenant.id;
    const userInfo = await botAuth.getUserInfo(userId, tenantId);
    let userProgram = "";
    if (userInfo && userInfo.user && userInfo.user.subDomain) {
      userProgram = userInfo.user.subDomain;
    }


    loggerLogData(true, "inside composeExtensionEvent: Connector below:", "", false);
    consoleLogData(true, "inside composeExtensionEvent: Connector below:", "", false);
    // logger.debug(connector);
    invokeHandlerFunc(event, callback, botAuth, baseUrl, connector);
  }

  public async onSigninStateVerificationHandler(event: any, query: any, callback: any) {
    const curLang = event.entities[0].locale;
    const langHelper = new LanguageHelper(curLang);
    loggerLogData(true, "inside onSigninStateVerification", "", false);
    consoleLogData(true, "inside onSigninStateVerification", "", false);
    try {
      const userId = event.address.user.id;
      const tenantId = event.sourceEvent.tenant.id;
      let cb;
      // logger.debug(userId);
      // console.log(userId);
      // logger.debug(tenantId);
      // console.log(tenantId);

      cb = function (resp, err) {
        if (resp.user && resp.user.achieverAccessToken) {
          if (resp.user.achieverAccessToken.length > 0) {
            const description = {
              event: "User signed in successfully",
              achieverAccessToken: resp.user.achieverAccessToken,
              userId,
              tenantId
            };
            botAuth.saveLogData(description, "eventHandlerController.ts/onSigninStateVerificationHandler", resp.user.subDomain);
            const card = {
              contentType: "application/vnd.microsoft.card.adaptive",
              content: {
                $schema:
                  "http://adaptivecards.io/schemas/adaptive-card.json",
                type: "AdaptiveCard",
                version: "1.0",
                body: [
                  {
                    type: "ColumnSet",
                    columns: [
                      {
                        type: "Column",
                        items: [
                          {
                            type: "TextBlock",
                            text: langHelper.getTranslation("You_have_successfully_signed_in"),
                            spacing: "Medium",
                            wrap: true
                          }
                        ],
                        width: "60"
                      }
                    ],
                    bleed: true
                  }
                ],
                actions: [
                  {
                    "type": "Action.Submit",
                    "title": "Send a recognition",
                    "data": {
                      "msteams": {
                        "type": "task/fetch",
                      }
                    }
                  }
                ]
              }
            };
            var msg = new builder.Message().address(event.address)
              .addAttachment(card);

            connector.send([msg.toMessage()], function (err, address) {
              // logger.debug(err);
              // console.log(err);
            });
          }
          else {
            var msg = new builder.Message().address(event.address)
              .text(langHelper.getTranslation("There_was_a_problem_signing_in_Please_try_again"));

            connector.send([msg.toMessage()], function (err, address) {
              // logger.debug(err);
              // console.log(err);
            });
          }
        }
        else {
          var msg = new builder.Message().address(event.address)
            .text(langHelper.getTranslation("There_was_a_problem_signing_in_Please_try_again"));

          connector.send([msg.toMessage()], function (err, address) {
            consoleLogData(true, "************onSigninStateVerificationHandler*****", err, false);
            newRelicErrLog(err, "eventHandlerController.ts/onSigninStateVerificationHandler", "");
            const description = {
              "RequestDetails": { userId: event.address.user.id, tenantId: event.sourceEvent.tenant.id },
              "ErrMessage": err.message
            };
            botAuth.saveLogData(JSON.stringify(description), "eventHandlerController.ts/onSigninStateVerificationHandler", event.sourceEvent.tenant.id, "error");
            loggerLogData(true, "*******************error in onSigninStateVerificationHandler function*******************", "", true);

            // logger.error(err);
            // console.log(err);
          });
        }

        callback(null, null, 200);
        return;
      };
      const resp = await botAuth.getUserInfo(userId, tenantId);
      cb(resp, null);
    }
    catch (e) {
      // let errArr = ['try-catch-err', 'msteams-connect', 'eventHandlerController.ts', 'onSigninStateVerificationHandler', e.message];
      // newrelic.noticeError(e);
      // logger.debug(errArr.join(":"));
      newRelicErrLog(e, "eventHandlerController.ts/onSigninStateVerificationHandler", "");
      const description = {
        "RequestDetails": { userId: event.address.user.id, tenantId: event.sourceEvent.tenant.id },
        "ErrMessage": e.message
      };
      botAuth.saveLogData(JSON.stringify(description), "eventHandlerController.ts/onSigninStateVerificationHandler", event.sourceEvent.tenant.id, "error");
      loggerLogData(true, "*******************error in onSigninStateVerificationHandler function*******************", "", true);

      const msg = new builder.Message().address(event.address)
        .text(langHelper.getTranslation("There_was_a_problem_signing_in_Please_try_again"));

      connector.send([msg.toMessage()], function (err, address) {
        // logger.error(err);
        // console.log(err);
      });
    }
  }
}