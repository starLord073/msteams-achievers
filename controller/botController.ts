const builder = require("botbuilder");
const teams = require("botbuilder-teams");
import { Session } from "inspector";
import { CalendarController } from "./calendarController";
const util = require('util');
const fs = require('fs');
const writeFilePromise = util.promisify(fs.writeFile);
const deleteFilePromise = util.promisify(fs.unlink);
const logger = require('../util/winlogger.js').logger;
const jwtSimple = require('jwt-simple');
import { CardHelper } from "../util/AdaptiveCardHelper";
import { trackEventInPendo } from "../src/pendoTrackEvent";
const MicrosoftAppClientId = process.env.APP_CLIENT_ID;
const MicrosoftAppPassword = process.env.APP_CLIENT_SECRET;
const newrelic = require('newrelic');
const connector = new teams.TeamsChatConnector({
  appId: MicrosoftAppClientId,
  appPassword: MicrosoftAppPassword
});
import { LanguageHelper } from "../languages/languageHelper";

import { consoleLogData, loggerLogData, newRelicErrLog } from '../util/logsHandler';

let baseUrl: string;
let MicrosoftAppId: string;
let bot: any;
let botAuth: any;

export class botDialog {

  constructor(_baseUrl: string, _bot: any, _MicrosoftAppId: any, _botAuth: any) {
    loggerLogData(true, "inside botController constructor", "", false);
    consoleLogData(true, "inside botController constructor", "", false);
    baseUrl = _baseUrl;
    MicrosoftAppId = _bot;
    bot = _bot;
    botAuth = _botAuth;
  }

  async choiceDialog(session: any) {
    const helpInDifferentLanguages = ["help", "ayuda", "aide", "帮助"];
    const signInInDifferentLanguages = ["sign in", "iniciar sesión", "connexion", "登录"];
    const signOutInDifferentLanguages = ["sign out", "cerrar sesión", "déconnexion", "退出"];
    const userId = session.message.address.user.id;
    const tenantId = session.message.sourceEvent.tenant.id;
    loggerLogData(true, "inside bot.dialog", "", false);
    consoleLogData(true, "inside bot.dialog", "", false);
    const curLang = session._locale;
    const langHelper = new LanguageHelper(curLang);
    // const description = {
    //   event: "User interaction with bot via message or Coffee Chats",
    //   sessionInfo: session.message
    // }
    // botAuth.saveLogData(description, "botController.ts/choiceDialog", "AUDIT_LOGS");

    logger.debug("***** choiceDialog *********");

    try {
      let ccLocale;
      const cardHelper = new CardHelper();
      // Handling Action.Submit of the adaptive card actions
      if (session.message.value) {

        if (session.message.value.source == "PERMISSION_CARD") {
          // Handling the permission card "No Thanks" click
          if (session.message.value.calendarPermission == "DENIED") {
            let ccLocale = session.message.value.ccLocale;
            loggerLogData(true, "User denied permission to Calendar", "", false);
            await botAuth.saveTokens(session.message.user.id, session.message.sourceEvent.tenant.id, session.message.user.name, session.message.address.bot.id, session.message.address.bot.name, session.message.address.serviceUrl, "DENIED", "DENIED");
            consoleLogData(true, "Dealing with denied permissions...", "", false);
            const deniedCard = cardHelper.getDeniedCard(ccLocale, session.message.value.user1Name, session.message.value.user1Email, session.message.value.user2Name, session.message.value.user2Email);
            let msg = new builder.Message().address(session.message.address).addAttachment(deniedCard);
            bot.send(msg);
            session.endDialog();
            // tracking No Thanks click in pendo
            // get details from database using tenantId and userId
            // Track event : External | Integration: Notification – Clicked
            try {
              const userDetailsFromDB = await botAuth.getUserInfo(session.message.user.id, session.message.address.conversation.tenantId);
              if (userDetailsFromDB.user && userDetailsFromDB.user.achieverApplicationID && userDetailsFromDB.user.achieverMemberId) {
                // tracking in pendo only if we find the user details in db
                const programPk = userDetailsFromDB.user.achieverApplicationID;
                const userPk = userDetailsFromDB.user.achieverMemberId;
                const properties = {
                  medium: "msTeams",
                  event: "coffeeChat",
                  buttonClicked: "NoThanks",
                };
                trackEventInPendo("External | Integration: Notification – Clicked", programPk, userPk, properties);
              }
            }
            catch (error) {
              // let errArr = ['try-catch-err', 'msteams-connect', 'botController.ts', 'choiceDialog', error.message];
              // newrelic.noticeError(error);
              // logger.debug(errArr.join(":"));
              // console.log(error.message)
              newRelicErrLog(error, "botController.ts/choiceDialog", "");
              const description = {
                "RequestDetails": { appUserId: session.message.address.user.id, appTenantId: session.message.sourceEvent.tenant.id },
                "ErrMessage": error.message
              };
              botAuth.saveLogData(JSON.stringify(description), "botController.ts/choiceDialog", session.message.sourceEvent.tenant.id, "error");
              loggerLogData(true, "*******************Error in choiceDialog *******************", "", true);
            }
          }
        }
        if (session.message.value.source == "GLOBAL_PERMISSION_CARD") {
          // Handling the global permission card "No Thanks" click
          if (session.message.value.calendarPermission == "DENIED") {
            let ccLocale = session.message.value.ccLocale;
            loggerLogData(true, "User denied permission to Calendar", "", false);
            await botAuth.saveTokens(session.message.user.id, session.message.sourceEvent.tenant.id, session.message.user.name, session.message.address.bot.id, session.message.address.bot.name, session.message.address.serviceUrl, "DENIED", "DENIED");
            consoleLogData(true, "Dealing with denied permissions...", "", false);
            const deniedCard = cardHelper.getGlobalDeniedCard(ccLocale);
            let msg = new builder.Message().address(session.message.address).addAttachment(deniedCard);
            bot.send(msg);
            session.endDialog();
          }
        }
        // Handling the user click on "Schedule Coffee Chat" in the notification sent
        else if (session.message.value.source == "CC_NOTIFICATION") {
          if (session.message.value.refreshToken) {
            const refreshTokenValue = session.message.value.refreshToken;
            ccLocale = session.message.value.ccLocale;
            if (refreshTokenValue == "NO_VALUE") {
              // Refresh token is null. Send permission card
              loggerLogData(true, "Refresh token is null. Sending permission card", "", false);
              consoleLogData(true, "Graph access tokens do not exist. Permission card is to be sent", "", false);

            

              const stateObj = {
                tenantId: session.message.address.conversation.tenantId,
                userId: session.message.user.id,
                name: session.message.user.name,
                msTeamBotId: session.message.address.bot.id,
                msTeamBotName: session.message.address.bot.name,
                msTeamBotServiceUrl: session.message.address.serviceUrl,
                user1Name: session.message.value.user1Name,
                user1Email: session.message.value.user1Email,
                user2Name: session.message.value.user2Name,
                user2Email: session.message.value.user2Email,
                user1Time: session.message.localTimestamp,
                ccLocale: ccLocale,
                user1Timezone: session.message.value.user1Timezone,
                user2Timezone: session.message.value.user2Timezone
              };
              // console.log("User local time stamp : ", session.message.localTimestamp);
              // logger.debug("User local time stamp : ", session.message.localTimestamp);
              const stateData = jwtSimple.encode(stateObj, "12345");
              const btnUrl = "https://login.microsoftonline.com/" + session.message.address.conversation.tenantId + "/oauth2/v2.0/authorize?client_id=" + process.env.APP_CLIENT_ID + "&response_type=code&redirect_uri=" + process.env.APP_BASE_URL + "/auth/redirect&response_mode=query&scope=Calendars.Read%20offline_access&state=" + stateData;
              const useEmbeddedBrowser = false;
              const permissionsCard = cardHelper.getPermissionsCard(ccLocale, btnUrl, session.message.value.user1Name, session.message.value.user1Email, session.message.value.user2Name, session.message.value.user2Email, useEmbeddedBrowser);
              let msg = new builder.Message().address(session.message.address).addAttachment(permissionsCard);
              // console.log("Sending message... ", msg);
              bot.send(msg);
              session.endDialog();
            } else {
              const calendarController = new CalendarController(botAuth);
              const newTokenDetails = JSON.stringify(await botAuth.refreshTokens(session.message.value.refreshToken));
              const nt = JSON.parse(newTokenDetails);
              if (nt.refreshToken == "Invalid") {
                throw new Error(nt.error);
              } else {
                // Save the new tokens to the database
                await botAuth.saveTokens(session.message.value.userId, session.message.value.tenantId, session.message.value.user1Name, session.message.address.bot.id, session.message.address.bot.name, session.message.address.serviceUrl, nt.access_token, nt.refresh_token);
                const cardToSend = '' + await calendarController.getUserSchedule(ccLocale, session.message.value.user1Name, session.message.value.user1Email, session.message.value.user2Name, session.message.value.user2Email, nt.access_token, session.message.localTimestamp, session.message.value.user1Timezone, session.message.value.user2Timezone);
                let msg = new builder.Message().address(session.message.address).addAttachment(JSON.parse(cardToSend));
                bot.send(msg);
                session.endDialog();
              }
            }
            // tracking schedule coffee chat click in pendo
            // get details from database using tenantId and userId
            // Track Event : External | Integration: Notification – Clicked
            try {
              const userDetailsFromDB = await botAuth.getUserInfo(session.message.user.id, session.message.address.conversation.tenantId);

              if (userDetailsFromDB.user && userDetailsFromDB.user.achieverApplicationID && userDetailsFromDB.user.achieverMemberId) {
                // tracking in pendo only if we find the user details in db
                const programPk = userDetailsFromDB.user.achieverApplicationID;
                const userPk = userDetailsFromDB.user.achieverMemberId;
                const properties = {
                  medium: "msTeams",
                  event: "coffeeChat",
                  buttonClicked: "Schedule",
                };
                trackEventInPendo("External | Integration: Notification – Clicked", programPk, userPk, properties);
              }
            }
            catch (error) {
              newRelicErrLog(error, "botController.ts/choiceDialog", "");
              const description = {
                "RequestDetails": { appUserId: session.message.address.user.id, appTenantId: session.message.sourceEvent.tenant.id },
                "ErrMessage": error.message
              };
              botAuth.saveLogData(JSON.stringify(description), "botController.ts/choiceDialog", session.message.sourceEvent.tenant.id, "error");
              loggerLogData(true, "*******************Error in choiceDialog 333*******************", "", true);
              // let errArr = ['try-catch-err', 'msteams-connect', 'botController.ts', 'choiceDialog', error.message];
              // newrelic.noticeError(error);
              // logger.debug(errArr.join(":"));
              // console.log(error.message)
            }

          }
        }
      }
    }
    catch (err) {
      let ccLocale = session.message.value.ccLocale;
      // let errArr = ['try-catch-err', 'msteams-connect', 'botController.ts', 'choiceDialog', err.message];
      // newrelic.noticeError(err);
      // logger.debug(errArr.join(":"));
      // logger.error("Error with the session.value management... Error: \n" + err);
      // console.log("Error with the session.value management... Error: \n" + err);
      // console.log("Saving access tokens to database.")
      newRelicErrLog(err, "botController.ts/choiceDialog", "");
      const description = {
        "RequestDetails": { appUserId: session.message.address.user.id, appTenantId: session.message.sourceEvent.tenant.id },
        "ErrMessage": err.message
      };
      botAuth.saveLogData(JSON.stringify(description), "botController.ts/choiceDialog", session.message.sourceEvent.tenant.id, "error");
      loggerLogData(true, "*******************Error in choiceDialog 444*******************", "", true);

      const cardHelper = new CardHelper();
      await botAuth.saveTokens(session.message.value.userId, session.message.value.tenantId, session.message.value.user1Name, session.message.address.bot.id, session.message.address.bot.name, session.message.address.serviceUrl, "INVALID", "INVALID");
      consoleLogData(true, "Tokens saved... Invalid", "", false);
      const fallbackCard = cardHelper.getFallBackCard(ccLocale, session.message.value.user1Name, session.message.value.user1Email, session.message.value.user2Name, session.message.value.user2Email);
      let msg = new builder.Message().address(session.message.address).addAttachment(fallbackCard);
      bot.send(msg);
      session.endDialog();
    }

    try {
      // if(session.message.sourceEvent.teamsTeamId){
      if (session.message.address.conversation.isGroup == true) {
        var text = session.message.text;
        const botMri = session.message.address.bot.id.toLowerCase();

        if (session.message.entities) {
          session.message.entities
            .filter(entity => ((entity.type === "mention") && (entity.mentioned.id.toLowerCase() === botMri)))
            .forEach(entity => {
              text = text.replace(entity.text, "");
            });
          text = text.toLowerCase().trim();
        }
        if (helpInDifferentLanguages.indexOf(text) > -1) {
          if (session.message.address.conversation.conversationType.toLowerCase() == "groupchat") {
            session.beginDialog('Group Help');
          }
          else {
            session.beginDialog('Help');
          }
        }
        else if (signInInDifferentLanguages.indexOf(text) > -1) {
          const card = {
            "contentType": "application/vnd.microsoft.card.adaptive",
            "content": {
              "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
              "type": "AdaptiveCard",
              "version": "1.0",
              "body": [
                {
                  "type": "TextBlock",
                  "text": langHelper.getTranslation("Sign_in_to_Achievers_chatbot"),
                  "spacing": "Medium",
                  "wrap": true
                }
              ],
              "actions": [
                {
                  "type": "Action.OpenUrl",
                  "title": langHelper.getTranslation("Sign_in"),
                  "url": "https://teams.microsoft.com/l/chat/0/0?users=" + session.message.address.bot.id
                }
              ]
            }
          };


          const msg = new builder.Message(session)
            .addAttachment(card);
          session.send(msg);
          session.message.address = {
            channelId: "msteams",
            user: { id: session.message.address.user.id },
            channelData: {
              tenant: {
                id: session.message.address.conversation.tenantId
              }
            },
            bot: {
              id: session.message.address.bot.id,
              name: session.message.address.bot.name
            },
            serviceUrl: session.message.address.serviceUrl
          };
          session.beginDialog("Sign In");
        }
        else if (signOutInDifferentLanguages.indexOf(text) > -1) {
          const card = {
            "contentType": "application/vnd.microsoft.card.adaptive",
            "content": {
              "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
              "type": "AdaptiveCard",
              "version": "1.0",
              "body": [
                {
                  "type": "TextBlock",
                  "text": langHelper.getTranslation("Sign_out_from_Achievers_chatbot"),
                  "spacing": "Medium",
                  "wrap": true
                }
              ],
              "actions": [
                {
                  "type": "Action.OpenUrl",
                  "title": langHelper.getTranslation("Sign_out"),
                  "url": "https://teams.microsoft.com/l/chat/0/0?users=" + session.message.address.bot.id
                }
              ]
            }
          };
          const msg = new builder.Message(session)
            .addAttachment(card);
          session.send(msg);
          session.message.address = {
            channelId: "msteams",
            user: { id: session.message.address.user.id },
            channelData: {
              tenant: {
                id: session.message.address.conversation.tenantId
              }
            },
            bot: {
              id: session.message.address.bot.id,
              name: session.message.address.bot.name
            },
            serviceUrl: session.message.address.serviceUrl
          };
          session.beginDialog("Sign Out");
        }
        else {
          session.endDialog(langHelper.getTranslation("I_dont_understand_this_command_please_type_help_for_more_information"));
        }
      }
      else {
        var text = session.message.text.trim().toLowerCase();

        if (helpInDifferentLanguages.indexOf(text) > -1) {
          session.beginDialog('Bot Help');
        }
        else if (signInInDifferentLanguages.indexOf(text) > -1) {
          session.beginDialog('Sign In');
        }
        else if (signOutInDifferentLanguages.indexOf(text) > -1) {
          session.beginDialog('Sign Out');
        }
        else {
          session.endDialog(langHelper.getTranslation("I_dont_understand_this_command_please_type_help_for_more_information"));
        }
      }
    }
    catch (e) {
      // let errArr = ['try-catch-err', 'msteams-connect', 'botController.ts', 'choiceDialog', e.message];
      // newrelic.noticeError(e);
      // logger.debug(errArr.join(":"));
      // logger.error("error in bot.dialog. Error: \n" + e);
      // console.log("error in bot.dialog. Error: \n" + e);
      newRelicErrLog(e, "botController.ts/choiceDialog", "");
      const description = {
        "RequestDetails": { appUserId: session.message.address.user.id, appTenantId: session.message.sourceEvent.tenant.id },
        "ErrMessage": e.message
      };
      botAuth.saveLogData(JSON.stringify(description), "botController.ts/choiceDialog", session.message.sourceEvent.tenant.id, "error");
      loggerLogData(true, "*******************Error in choiceDialog 555*******************", "", true);

      session.endDialog(langHelper.getTranslation("Error_communicating_with_our_servers_at_the_moment_Please_try_again_after_sometime"));
    }
  }

  Help(session: any) {
    const curLang = session._locale;
    const langHelper = new LanguageHelper(curLang);
    try {
      session.sendTyping();
      const card = {
        "contentType": "application/vnd.microsoft.card.adaptive",
        "content": {
          "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
          "type": "AdaptiveCard",
          "version": "1.0",
          "body": [
            {
              "type": "TextBlock",
              "text": langHelper.getTranslation("Hi_there_Im_Achievers"),
              "color": "Accent",
              "size": "Large"
            },
            {
              "type": "TextBlock",
              "text": langHelper.getTranslation("A_teammate_of_yours_recently_added_me_to_help_your_team_recognize_colleagues_right_from_Teams"),
              "spacing": "Medium",
              "wrap": true
            },
            {
              "type": "Image",
              "url": baseUrl + "/docs/appStoreImage.png",
              "id": "img_help",
              "size": "Auto"
            },
            {
              "type": "TextBlock",
              "text": langHelper.getTranslation("To_recognize_a_colleague_click_the__menu_below_the_Type_a_new_message_field_and_select_the_Achievers_app_If_in_mobile_select_the_button_To_use_this_app_you_must_have_an_active_Achievers_account_If_you_have_any_questions_about_this_app_click_Learn_More_We_recommend_using_the_desktop_Microsoft_Teams_or_the_uptodate_browser_to_ensure_you_have_the_best_Achievers_for_Teams_experience_This_app_supports_Chrome_Microsoft_Edge_and_Firefox_on_desktop_and_both_iOS_and_Android_devices"),
              "wrap": true
            }
          ],
          "actions": [
            {
              "type": "Action.OpenUrl",
              "title": langHelper.getTranslation("Learn_More"),
              "url": "https://www.achievers.com/connect/teams/",
              "style": "positive"
            }
          ]
        }
      };


      const msg = new builder.Message(session)
        .addAttachment(card);
      // session.send(msg);
      session.endDialog(msg);
    }
    catch (e) {
      // let errArr = ['try-catch-err', 'msteams-connect', 'botController.ts', 'Help', e.message];
      // newrelic.noticeError(e);
      // logger.debug(errArr.join(":"));
      // logger.error("error in help dialog handler: Error: \n" + e);
      // console.log("error in help dialog handler: Error: \n" + e);
      newRelicErrLog(e, "botController.ts/Help", "");
      const description = {
        "RequestDetails": "session",
        "ErrMessage": e.message
      };
      botAuth.saveLogData(JSON.stringify(description), "botController.ts/Help", "notFound", "error");
      loggerLogData(true, "*******************error in help function*******************", "", true);

      session.endDialog(langHelper.getTranslation("Error_while_processing_your_request_Please_try_again_after_sometime"));
    }
  }

  GroupHelp(session: any) {
    const curLang = session._locale;
    const langHelper = new LanguageHelper(curLang);
    try {
      session.sendTyping();
      const card = {
        "contentType": "application/vnd.microsoft.card.adaptive",
        "content": {
          "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
          "type": "AdaptiveCard",
          "version": "1.0",
          "body": [
            {
              "type": "TextBlock",
              "text": langHelper.getTranslation("Hi_there_Im_Achievers"),
              "color": "Accent",
              "size": "Large"
            },
            {
              "type": "TextBlock",
              "text": langHelper.getTranslation("A_teammate_of_yours_recently_added_me_to_help_your_team_recognize_colleagues_right_from_Teams"),
              "spacing": "Medium",
              "wrap": true
            },
            {
              "type": "Image",
              "url": baseUrl + "/docs/appStoreImage.png",
              "id": "img_help",
              "size": "Auto"
            },
            {
              "type": "TextBlock",
              "text": langHelper.getTranslation("To_recognize_a_colleague_click_the__menu_below_the_Type_a_new_message_field_and_select_the_Achievers_app_If_in_mobile_select_the_button_To_use_this_app_you_must_have_an_active_Achievers_account_If_you_have_any_questions_about_this_app_click_Learn_More_We_recommend_using_the_desktop_Microsoft_Teams_or_the_uptodate_browser_to_ensure_you_have_the_best_Achievers_for_Teams_experience_This_app_supports_Chrome_Microsoft_Edge_and_Firefox_on_desktop_and_both_iOS_and_Android_devices"),
              "wrap": true
            }
          ],
          "actions": [
            {
              "type": "Action.OpenUrl",
              "title": langHelper.getTranslation("Learn_More"),
              "url": "https://www.achievers.com/connect/teams/",
              "style": "positive"
            }
          ]
        }
      };
      const msg = new builder.Message(session)
        .addAttachment(card);
      session.endDialog(msg);
    }
    catch (e) {
      // let errArr = ['try-catch-err', 'msteams-connect', 'botController.ts', 'GroupHelp', e.message];
      // newrelic.noticeError(e);
      // logger.debug(errArr.join(":"));
      // logger.error("error in help dialog handler. Error: \n" + e);
      newRelicErrLog(e, "botController.ts/GroupHelp", "");
      const description = {
        "RequestDetails": "session",
        "ErrMessage": e.message
      };
      botAuth.saveLogData(JSON.stringify(description), "botController.ts/GroupHelp", "notFound", "error");
      loggerLogData(true, "*******************error in GroupHelp function*******************", "", true);

      session.endDialog(langHelper.getTranslation("Error_while_processing_your_request_Please_try_again_after_sometime"));
    }
  }

  BotHelp(session: any) {
    const curLang = session._locale;
    const langHelper = new LanguageHelper(curLang);
    try {
      session.sendTyping();
      loggerLogData(true, "inside bot help", "", false);
      consoleLogData(true, "inside bot help", "", false);
      const card = {
        "contentType": "application/vnd.microsoft.card.adaptive",
        "content": {
          "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
          "type": "AdaptiveCard",
          "version": "1.0",
          "body": [
            {
              "type": "TextBlock",
              "text": langHelper.getTranslation("Hi_there_Im_Achievers"),
              "color": "Accent",
              "size": "Large"
            },
            {
              "type": "TextBlock",
              "text": langHelper.getTranslation("A_teammate_of_yours_recently_added_me_to_help_your_team_recognize_colleagues_right_from_Teams"),
              "spacing": "Medium",
              "wrap": true
            },
            {
              "type": "Image",
              "url": baseUrl + "/docs/appStoreImage.png",
              "id": "img_help",
              "size": "Auto"
            },
            {
              "type": "TextBlock",
              "text": langHelper.getTranslation("To_recognize_a_colleague_click_the__menu_below_the_Type_a_new_message_field_and_select_the_Achievers_app_If_in_mobile_select_the_button_To_use_this_app_you_must_have_an_active_Achievers_account_If_you_have_any_questions_about_this_app_click_Learn_More_We_recommend_using_the_desktop_Microsoft_Teams_or_the_uptodate_browser_to_ensure_you_have_the_best_Achievers_for_Teams_experience_This_app_supports_Chrome_Microsoft_Edge_and_Firefox_on_desktop_and_both_iOS_and_Android_devices"),
              "wrap": true
            }
          ],
          "actions": [
            {
              "type": "Action.OpenUrl",
              "title": langHelper.getTranslation("Learn_More"),
              "url": "https://www.achievers.com/connect/teams/",
              "style": "positive"
            }
          ]
        }
      };


      const msg = new builder.Message(session)
        .addAttachment(card);
      session.endDialog(msg);
    }
    catch (e) {
      // let errArr = ['try-catch-err', 'msteams-connect', 'botController.ts', 'BotHelp', e.message];
      // newrelic.noticeError(e);
      // logger.debug(errArr.join(":"));
      // logger.error("error in help dialog handler. Error: \n" + e);
      // console.log("error in help dialog handler. Error: \n" + e);
      newRelicErrLog(e, "botController.ts/BotHelp", "");
      const description = {
        "RequestDetails": "session",
        "ErrMessage": e.message
      };
      botAuth.saveLogData(JSON.stringify(description), "botController.ts/BotHelp", "notFound", "error");
      loggerLogData(true, "*******************error in BotHelp function*******************", "", true);

      session.endDialog(langHelper.getTranslation("Error_while_processing_your_request_Please_try_again_after_sometime"));
    }
  }

  async conversationUpdate(message: any) {
    // Make changes from here on down
    // console.log("Message in conversationUpdate context : ", message)
    const tenantId = message.address.conversation.tenantId;
    const conversationId = message.address.conversation.id;
    try {
      const langHelper = new LanguageHelper("en-US");
      if (message.membersAdded) {
        const members = message.membersAdded;
        let membersAddedArr = [];
        for (let i = 0; i < members.length; i++) {
          // membersAddedArr contains userIds of all users added and botId if bot was added to a channel/chat.
          membersAddedArr.push(members[i].id);
        }

        const card = {
          "contentType": "application/vnd.microsoft.card.adaptive",
          "content": {
            "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
            "type": "AdaptiveCard",
            "version": "1.0",
            "body": [
              {
                "type": "TextBlock",
                "text": langHelper.getTranslation("Hi_there_Im_Achievers"),
                "color": "Accent",
                "size": "Large"
              },
              {
                "type": "TextBlock",
                "text": langHelper.getTranslation("A_teammate_of_yours_recently_added_me_to_help_your_team_recognize_colleagues_right_from_Teams"),
                "spacing": "Medium",
                "wrap": true
              },
              {
                "type": "Image",
                "url": baseUrl + "/docs/appStoreImage.png",
                "id": "img_help",
                "size": "Auto"
              },
              {
                "type": "TextBlock",
                "text": langHelper.getTranslation("To_recognize_a_colleague_click_the__menu_below_the_Type_a_new_message_field_and_select_the_Achievers_app_If_in_mobile_select_the_button_To_use_this_app_you_must_have_an_active_Achievers_account_If_you_have_any_questions_about_this_app_click_Learn_More_We_recommend_using_the_desktop_Microsoft_Teams_or_the_uptodate_browser_to_ensure_you_have_the_best_Achievers_for_Teams_experience_This_app_supports_Chrome_Microsoft_Edge_and_Firefox_on_desktop_and_both_iOS_and_Android_devices"),
                "wrap": true
              }
            ],
            "actions": [
              {
                "type": "Action.OpenUrl",
                "title": "Learn More",
                "url": "https://www.achievers.com/connect/teams/",
                "style": "positive"
              }
            ]
          }
        };
        consoleLogData(true, "In conversation update ... ", "", false);
        const botId = message.address.bot.id;
        const botName = message.address.bot.name;
        const serviceUrl = message.address.serviceUrl;
        // console.log("TID CID : ", tenantId, conversationId);
        await connector.fetchMembers(message.address.serviceUrl, message.address.conversation.id, async function (err: any, result: any) {
          if (err) {
            consoleLogData(true, "Error fetching members", "", false);
          } else {
            // console.log("Result of connector.fetchMembers is : ", result);
            let userInfoFromTenant;
            if (result.length > 0 && result[0].id) {
              userInfoFromTenant = await botAuth.getUserInfo(result[0].id, tenantId);
            }
            for (let i = 0; i < result.length; i++) {
              if (membersAddedArr.indexOf(result[i].id) > -1 || membersAddedArr.indexOf(botId) > -1 && userInfoFromTenant && userInfoFromTenant.client && userInfoFromTenant.client.programFK) {
                // Update user info in db if new user is added to conversation
                // Update user info of everyone in channel/chat id bot is added to the channel
                botAuth.updateUserInfo(result[i].id, result[i].tenantId, result[i].email, result[i].name, botId, botName, serviceUrl, null, result[i].objectId);
              }
            }
            // call getConversationbased on appid here
            try {
              const convDataByAppId = await botAuth.getConversationInfoByAppId(tenantId);
              // console.log(convDataByAppId)
              if (result.length && !convDataByAppId) {
                consoleLogData(true, "first record in the conversation table with appId   :   ", tenantId, false);
                // tracking the event when we dont have instance of appid in DB and have members in the result
                const companyName = result[0].email.split('@').pop().split('.')[0];
                // console.log(result[0].email, "=>", companyName)
                // console.log("accountID : connect")
                // console.log("visitorID : ", companyName)
                const properties = {
                  medium: "msTeams"
                };
                // call pendo track event here to check the installations in teams
                trackEventInPendo("External | Integration: Installations", "connect", companyName, properties);
              }
            }
            catch (err) {
              // let errArr = ['try-catch-err', 'msteams-connect', 'botController.ts', 'conversationUpdate', err.message];
              // newrelic.noticeError(err);
              // logger.debug(errArr.join(":"));
              // logger.error(`error while getting conversation data based on AppId: ${err}`);
              // console.log(`error while getting conversation data based on AppId: ${err}`);
              newRelicErrLog(err, "botController.ts/conversationUpdate", "");
              const description = {
                "RequestDetails": { tenantId, conversationId },
                "ErrMessage": err.message
              };
              botAuth.saveLogData(JSON.stringify(description), "botController.ts/conversationUpdate", tenantId, "error");
              loggerLogData(true, "*******************error in conversationUpdate function*******************", "", true);
            }
            const convRes = await botAuth.updateConversationInfo(tenantId, conversationId);
            consoleLogData(true, "Conversations table updated. Response : ", convRes, false);
          }
        });
        // console.log("Going to check if bot was added to conversations")
        // Check if botId is present in mmbersAddedArr and send welcome message if it is added
        if (membersAddedArr.indexOf(botId) > -1) {
          const botmessage = new builder.Message()
            .address(message.address)
            .addAttachment(card);
          bot.send(botmessage, function (err) {
            if (err) {
              consoleLogData(true, `Error sending "Welcome" message to user`, "", false);
            }
          });
        }
      }
    }
    catch (e) {
      // let errArr = ['try-catch-err', 'msteams-connect', 'botController.ts', 'conversationUpdate', e.message];
      // newrelic.noticeError(e);
      // logger.debug(errArr.join(":"));
      // logger.error(`error in conversationUpdate: ${e}`);
      // console.log(`error in conversationUpdate: ${e}`);
      newRelicErrLog(e, "botController.ts/conversationUpdate", "");
      const description = {
        "RequestDetails": { tenantId, conversationId },
        "ErrMessage": e.message
      };
      botAuth.saveLogData(JSON.stringify(description), "botController.ts/conversationUpdate", tenantId, "error");
      loggerLogData(true, "*******************error in conversationUpdate function*******************", "", true);
    }
  }

  async signOut(session: any) {
    const curLang = session._locale;
    const languageHelper = new LanguageHelper(curLang);
    const userId = session.message.address.user.id;
    const tenantId = session.message.sourceEvent.tenant.id;
    let resp: any;
    try {


      const signOutCb = function (response, err) {
        if (err) {
          session.endDialog(languageHelper.getTranslation("There_was_trouble_signing_you_out_Please_try_again_after_some_time"));
        }
        else {
          // logger.debug("inside signoutcb else");
          // console.log("inside signoutcb else");
          session.endDialog(languageHelper.getTranslation("You_have_been_successfully_signed_out_Please_note_that_you_have_to_sign_in_again_to_be_able_to_utilize_Achievers_app_functionality"));
        }
      };

      resp = await botAuth.getUserInfo(userId, tenantId);

      if (resp.user && resp.user.achieverAccessToken) {
        if (resp.user.achieverAccessToken.length > 0) {
          botAuth.BotSignOut(session, signOutCb);
        }
      }
      else {
        session.endDialog(languageHelper.getTranslation("You_are_already_signed_out"));
      }
    }
    catch (e) {
      // let errArr = ['try-catch-err', 'msteams-connect', 'botController.ts', 'signOut', e.message];
      // newrelic.noticeError(e);
      // logger.debug(errArr.join(":"));
      // logger.error(`error in sign out: ${e}`);
      // console.log(`error in sign out: ${e}`);
      newRelicErrLog(e, "botController.ts/signOut", "");
      const description = {
        "RequestDetails": { tenantId, userId, resp },
        "ErrMessage": e.message
      };
      botAuth.saveLogData(JSON.stringify(description), "botController.ts/signOut", tenantId, "error");
      loggerLogData(true, "*******************error in signOut function*******************", "", true);
      session.endDialog(languageHelper.getTranslation("There_was_trouble_signing_you_out_Please_try_again_after_some_time"));
    }
  }

  async signIn(session: any) {
    const curLang = session._locale;
    const languageHelper = new LanguageHelper(curLang);
    const userId = session.message.address.user.id;
    const tenantId = session.message.sourceEvent.tenant.id;
    let resp: any;
    try {

      // console.log('*************-signIn- session.message ******************', session.message);
      resp = await botAuth.getUserInfo(userId, tenantId);
      // resp.user.expireAt format is in sec , converting current date into sec
      const isAccessTokenExpired = (resp.user && resp.user.expireAt - ((new Date().getTime()) / 1000) > 0) ? false : true
      if (resp.user && resp.user.achieverAccessToken && !isAccessTokenExpired) {
        session.endDialog(languageHelper.getTranslation("You_are_already_signed_in"));
      }
      else {
        let subDomain = "";
        if (resp && resp.user && resp.user.subDomain) {
          subDomain = resp.user.subDomain;
        } else if (resp && resp.client && resp.client.subDomain) {
          subDomain = resp.client.subDomain;
        }
        botAuth.BotSignIn(session, subDomain);
      }
    }
    catch (e) {
      // let errArr = ['try-catch-err', 'msteams-connect', 'botController.ts', 'signIn', e.message];
      // newrelic.noticeError(e);
      // logger.debug(errArr.join(":"));
      // logger.error(`error in sign in: ${e}`);
      // console.log(`error in sign in: ${e}`);
      newRelicErrLog(e, "botController.ts/signIn", "");
      const description = {
        "RequestDetails": { tenantId, userId, resp },
        "ErrMessage": e.message
      };
      botAuth.saveLogData(JSON.stringify(description), "botController.ts/signIn", tenantId, "error");
      loggerLogData(true, "*******************error in signIn function*******************", "", true);
      session.endDialog(languageHelper.getTranslation("There_was_trouble_signing_you_in_Please_try_again_after_some_time"));
    }
  }

}