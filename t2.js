const moment = require("moment");
var currentUserAddress = "Dreama.Jain@achievers.com"
var response = {
    "@odata.context":"https://graph.microsoft.com/v1.0/$metadata#users('3f00f2e0-172b-4fa8-8e20-ba5723bf0021')/calendarView",
    "value":[
       {
          "@odata.etag":"W/\"Ouwt/NUfgEaQY+TJmJNmaQAA9hRlQw==\"",
          "id":"AAMkADUyZTZmY2M1LTkwNDgtNDBlMi1hN2NkLWNlYTM4N2Q3ODcxMQBGAAAAAAA2tXs4hZfaRbqsv8EUvO9lBwA67C381R_ARpBj5MmYk2ZpAAAAAAENAAA67C381R_ARpBj5MmYk2ZpAAD2b13IAAA=",
          "createdDateTime":"2021-08-05T07:43:41.9802511Z",
          "lastModifiedDateTime":"2021-08-05T08:05:21.2731236Z",
          "changeKey":"Ouwt/NUfgEaQY+TJmJNmaQAA9hRlQw==",
          "categories":[
             
          ],
          "transactionId":null,
          "originalStartTimeZone":"Eastern Standard Time",
          "originalEndTimeZone":"Eastern Standard Time",
          "iCalUId":"040000008200E00074C5B7101A82E00800000000D0A761D1AB89D7010000000000000000100000009662FC00CD464D4ABD245F27AFF20E41",
          "reminderMinutesBeforeStart":15,
          "isReminderOn":true,
          "hasAttachments":false,
          "subject":"Microsoft Teams Issue",
          "bodyPreview":"Shitij Sharma is inviting you to a scheduled Zoom meeting.\r\n\r\nJoin Zoom Meeting\r\nhttps://achievers.zoom.us/j/94305225014?pwd=ZHlyUzdvVUQ0Zit5NnhhbUp1elZSUT09\r\n\r\nMeeting ID: 943 0522 5014\r\nPasscode: 749722\r\nOne tap mobile\r\n+16465588656,,94305225014#,,,,*74",
          "importance":"normal",
          "sensitivity":"normal",
          "isAllDay":false,
          "isCancelled":false,
          "isOrganizer":false,
          "responseRequested":true,
          "seriesMasterId":null,
          "showAs":"tentative",
          "type":"singleInstance",
          "webLink":"https://outlook.office365.com/owa/?itemid=AAMkADUyZTZmY2M1LTkwNDgtNDBlMi1hN2NkLWNlYTM4N2Q3ODcxMQBGAAAAAAA2tXs4hZfaRbqsv8EUvO9lBwA67C381R%2BARpBj5MmYk2ZpAAAAAAENAAA67C381R%2BARpBj5MmYk2ZpAAD2b13IAAA%3D&exvsurl=1&path=/calendar/item",
          "onlineMeetingUrl":null,
          "isOnlineMeeting":false,
          "onlineMeetingProvider":"unknown",
          "allowNewTimeProposals":true,
          "occurrenceId":null,
          "isDraft":false,
          "hideAttendees":false,
          "responseStatus":{
             "response":"notResponded",
             "time":"0001-01-01T00:00:00Z"
          },
          "body":{
             "contentType":"html",
             "content":"<html>\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\r\n<meta content=\"text/html; charset=us-ascii\">\r\n<meta name=\"Generator\" content=\"Microsoft Word 15 (filtered medium)\">\r\n<style>\r\n<!--\r\n@font-face\r\n\t{font-family:\"Cambria Math\"}\r\n@font-face\r\n\t{font-family:Calibri}\r\np.MsoNormal, li.MsoNormal, div.MsoNormal\r\n\t{margin:0cm;\r\n\tfont-size:11.0pt;\r\n\tfont-family:\"Calibri\",sans-serif}\r\na:link, span.MsoHyperlink\r\n\t{color:blue;\r\n\ttext-decoration:underline}\r\nspan.EmailStyle18\r\n\t{}\r\n.MsoChpDefault\r\n\t{font-size:10.0pt}\r\n@page WordSection1\r\n\t{margin:72.0pt 72.0pt 72.0pt 72.0pt}\r\ndiv.WordSection1\r\n\t{}\r\n-->\r\n</style>\r\n</head>\r\n<body lang=\"EN-CA\" link=\"blue\" vlink=\"purple\" style=\"word-wrap:break-word\">\r\n<div class=\"WordSection1\">\r\n<p>Shitij Sharma is inviting you to a scheduled Zoom meeting. </p>\r\n<p>Join Zoom Meeting <br>\r\n<a href=\"https://achievers.zoom.us/j/94305225014?pwd=ZHlyUzdvVUQ0Zit5NnhhbUp1elZSUT09\">https://achievers.zoom.us/j/94305225014?pwd=ZHlyUzdvVUQ0Zit5NnhhbUp1elZSUT09</a>\r\n</p>\r\n<p>Meeting ID: 943 0522 5014 <br>\r\nPasscode: 749722 <br>\r\nOne tap mobile <br>\r\n+16465588656,,94305225014#,,,,*749722# US (New York) <br>\r\n+16699009128,,94305225014#,,,,*749722# US (San Jose) </p>\r\n<p>Dial by your location <br>\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +1 646 558 8656 US (New York) <br>\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +1 669 900 9128 US (San Jose) <br>\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +1 253 215 8782 US (Tacoma) <br>\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +1 301 715 8592 US (Washington DC) <br>\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +1 312 626 6799 US (Chicago) <br>\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; +1 346 248 7799 US (Houston) <br>\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 877 853 5257 US Toll-free <br>\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 888 475 4499 US Toll-free <br>\r\nMeeting ID: 943 0522 5014 <br>\r\nPasscode: 749722 <br>\r\nFind your local number: <a href=\"https://achievers.zoom.us/u/adOo6qP6Ql\">https://achievers.zoom.us/u/adOo6qP6Ql</a>\r\n</p>\r\n<p>Join by SIP <br>\r\n<a href=\"mailto:94305225014@zoomcrc.com\">94305225014@zoomcrc.com</a> </p>\r\n<p>Join by H.323 <br>\r\n162.255.37.11 (US West) <br>\r\n162.255.36.11 (US East) <br>\r\n115.114.131.7 (India Mumbai) <br>\r\n115.114.115.7 (India Hyderabad) <br>\r\n213.19.144.110 (Amsterdam Netherlands) <br>\r\n213.244.140.110 (Germany) <br>\r\n103.122.166.55 (Australia Sydney) <br>\r\n103.122.167.55 (Australia Melbourne) <br>\r\n149.137.40.110 (Singapore) <br>\r\n64.211.144.160 (Brazil) <br>\r\n149.137.68.253 (Mexico) <br>\r\n69.174.57.160 (Canada Toronto) <br>\r\n65.39.152.160 (Canada Vancouver) <br>\r\n207.226.132.110 (Japan Tokyo) <br>\r\n149.137.24.110 (Japan Osaka) <br>\r\nMeeting ID: 943 0522 5014 <br>\r\nPasscode: 749722 </p>\r\n<p class=\"MsoNormal\">&nbsp;</p>\r\n</div>\r\n</body>\r\n</html>\r\n"
          },
          "start":{
             "dateTime":"2021-08-05T07:45:00.0000000",
             "timeZone":"UTC"
          },
          "end":{
             "dateTime":"2021-08-05T08:15:00.0000000",
             "timeZone":"UTC"
          },
          "location":{
             "displayName":"https://achievers.zoom.us/j/94305225014?pwd=ZHlyUzdvVUQ0Zit5NnhhbUp1elZSUT09",
             "locationType":"default",
             "uniqueId":"https://achievers.zoom.us/j/94305225014?pwd=ZHlyUzdvVUQ0Zit5NnhhbUp1elZSUT09",
             "uniqueIdType":"private"
          },
          "locations":[
             {
                "displayName":"https://achievers.zoom.us/j/94305225014?pwd=ZHlyUzdvVUQ0Zit5NnhhbUp1elZSUT09",
                "locationType":"default",
                "uniqueId":"https://achievers.zoom.us/j/94305225014?pwd=ZHlyUzdvVUQ0Zit5NnhhbUp1elZSUT09",
                "uniqueIdType":"private"
             }
          ],
          "recurrence":null,
          "attendees":[
             {
                "type":"required",
                "status":{
                   "response":"none",
                   "time":"0001-01-01T00:00:00Z"
                },
                "emailAddress":{
                   "name":"Shitij Sharma",
                   "address":"Shitij.Sharma@achievers.com"
                }
             },
             {
                "type":"required",
                "status":{
                   "response":"accepted",
                   "time":"0001-01-01T00:00:00Z"
                },
                "emailAddress":{
                   "name":"Pavan Nadupuru",
                   "address":"Pavan.Nadupuru@achievers.com"
                }
             },
             {
                "type":"required",
                "status":{
                   "response":"accepted",
                   "time":"0001-01-01T00:00:00Z"
                },
                "emailAddress":{
                   "name":"James Cotsen",
                   "address":"James.Cotsen@achievers.com"
                }
             }
          ],
          "organizer":{
             "emailAddress":{
                "name":"Shitij Sharma",
                "address":"Shitij.Sharma@achievers.com"
             }
          },
          "onlineMeeting":null
       },
       {
          "@odata.etag":"W/\"Ouwt/NUfgEaQY+TJmJNmaQAA9hQWJQ==\"",
          "id":"AAMkADUyZTZmY2M1LTkwNDgtNDBlMi1hN2NkLWNlYTM4N2Q3ODcxMQFRAAgI2VbazXzAAEYAAAAANrV7OIWX2kW6rL-BFLzvZQcAOuwt-NUfgEaQY_TJmJNmaQAAAAABDQAAOuwt-NUfgEaQY_TJmJNmaQAA0_drXAAAEA==",
          "createdDateTime":"2021-06-14T13:39:45.812478Z",
          "lastModifiedDateTime":"2021-08-04T14:55:07.7202676Z",
          "changeKey":"Ouwt/NUfgEaQY+TJmJNmaQAA9hQWJQ==",
          "categories":[
             
          ],
          "transactionId":null,
          "originalStartTimeZone":"Eastern Standard Time",
          "originalEndTimeZone":"Eastern Standard Time",
          "iCalUId":"040000008200E00074C5B7101A82E00807E508042D3F76D141FAD6010000000000000000100000006B8AFF6ED8C08D4A97F8160851813F20",
          "reminderMinutesBeforeStart":15,
          "isReminderOn":true,
          "hasAttachments":false,
          "subject":"Connect Weekly checkpoints with SRE and Arch.",
          "bodyPreview":"Hi there,\r\nDreama Jain is inviting you to a scheduled Zoom meeting.\r\nJoin Zoom Meeting\r\nPhone one-tap:\r\nCanada: +15873281099,,93895873660#,,,,*142993# or +16473744685,,93895873660#,,,,*142993#\r\nMeeting URL:\r\nhttps://achievers.zoom.us/j/938958736",
          "importance":"normal",
          "sensitivity":"normal",
          "isAllDay":false,
          "isCancelled":false,
          "isOrganizer":false,
          "responseRequested":false,
          "seriesMasterId":"AAMkADUyZTZmY2M1LTkwNDgtNDBlMi1hN2NkLWNlYTM4N2Q3ODcxMQBGAAAAAAA2tXs4hZfaRbqsv8EUvO9lBwA67C381R_ARpBj5MmYk2ZpAAAAAAENAAA67C381R_ARpBj5MmYk2ZpAADT52tcAAA=",
          "showAs":"tentative",
          "type":"occurrence",
          "webLink":"https://outlook.office365.com/owa/?itemid=AAMkADUyZTZmY2M1LTkwNDgtNDBlMi1hN2NkLWNlYTM4N2Q3ODcxMQFRAAgI2VbazXzAAEYAAAAANrV7OIWX2kW6rL%2FBFLzvZQcAOuwt%2FNUfgEaQY%2BTJmJNmaQAAAAABDQAAOuwt%2FNUfgEaQY%2BTJmJNmaQAA0%2BdrXAAAEA%3D%3D&exvsurl=1&path=/calendar/item",
          "onlineMeetingUrl":"",
          "isOnlineMeeting":false,
          "onlineMeetingProvider":"unknown",
          "allowNewTimeProposals":false,
          "occurrenceId":"OID.AAMkADUyZTZmY2M1LTkwNDgtNDBlMi1hN2NkLWNlYTM4N2Q3ODcxMQBGAAAAAAA2tXs4hZfaRbqsv8EUvO9lBwA67C381R_ARpBj5MmYk2ZpAAAAAAENAAA67C381R_ARpBj5MmYk2ZpAADT52tcAAA=.2021-08-04",
          "isDraft":false,
          "hideAttendees":false,
          "responseStatus":{
             "response":"notResponded",
             "time":"0001-01-01T00:00:00Z"
          },
          "start":{
             "dateTime":"2021-08-04T14:00:00.0000000",
             "timeZone":"UTC"
          },
          "end":{
             "dateTime":"2021-08-04T15:00:00.0000000",
             "timeZone":"UTC"
          },
          "location":{
             "displayName":"https://achievers.zoom.us/j/93895873660?pwd=ayt3WTlkMUI0a1p3RmhyTXFjMzJnQT09&from=addon",
             "locationType":"default",
             "uniqueId":"67fa17f9-f74e-483d-bfd0-d702a513064c",
             "uniqueIdType":"locationStore"
          },
          "locations":[
             {
                "displayName":"https://achievers.zoom.us/j/93895873660?pwd=ayt3WTlkMUI0a1p3RmhyTXFjMzJnQT09&from=addon",
                "locationType":"default",
                "uniqueId":"67fa17f9-f74e-483d-bfd0-d702a513064c",
                "uniqueIdType":"locationStore"
             }
          ],
          "recurrence":null,
          "attendees":[
             {
                "type":"required",
                "status":{
                   "response":"none",
                   "time":"0001-01-01T00:00:00Z"
                },
                "emailAddress":{
                   "name":"Dreama Jain",
                   "address":"Dreama.Jain@achievers.com"
                }
             },
             {
                "type":"required",
                "status":{
                   "response":"notResponded",
                   "time":"0001-01-01T00:00:00Z"
                },
                "emailAddress":{
                   "name":"Stefan Kolesnikowicz",
                   "address":"Stefan.Kolesnikowicz@achievers.com"
                }
             },
             {
                "type":"required",
                "status":{
                   "response":"notResponded",
                   "time":"0001-01-01T00:00:00Z"
                },
                "emailAddress":{
                   "name":"James McGoodwin",
                   "address":"James.McGoodwin@achievers.com"
                }
             },
             {
                "type":"required",
                "status":{
                   "response":"notResponded",
                   "time":"0001-01-01T00:00:00Z"
                },
                "emailAddress":{
                   "name":"Rajesh Pippiri",
                   "address":"Rajesh.Pippiri@achievers.com"
                }
             },
             {
                "type":"required",
                "status":{
                   "response":"notResponded",
                   "time":"0001-01-01T00:00:00Z"
                },
                "emailAddress":{
                   "name":"Pankaj Jindal",
                   "address":"Pankaj.Jindal@achievers.com"
                }
             },
             {
                "type":"required",
                "status":{
                   "response":"notResponded",
                   "time":"0001-01-01T00:00:00Z"
                },
                "emailAddress":{
                   "name":"Melanie Zhang",
                   "address":"Melanie.Zhang@achievers.com"
                }
             },
             {
                "type":"required",
                "status":{
                   "response":"notResponded",
                   "time":"0001-01-01T00:00:00Z"
                },
                "emailAddress":{
                   "name":"Cody Taylor",
                   "address":"Cody.Taylor@achievers.com"
                }
             },
             {
                "type":"optional",
                "status":{
                   "response":"notResponded",
                   "time":"0001-01-01T00:00:00Z"
                },
                "emailAddress":{
                   "name":"Sravani Alwala",
                   "address":"Sravani.Alwala@achievers.com"
                }
             },
             {
                "type":"optional",
                "status":{
                   "response":"accepted",
                   "time":"0001-01-01T00:00:00Z"
                },
                "emailAddress":{
                   "name":"Laxmi Ankem",
                   "address":"Laxmi.Ankem@achievers.com"
                }
             },
             {
                "type":"optional",
                "status":{
                   "response":"accepted",
                   "time":"0001-01-01T00:00:00Z"
                },
                "emailAddress":{
                   "name":"Pavan Nadupuru",
                   "address":"Pavan.Nadupuru@achievers.com"
                }
             }
          ],
          "organizer":{
             "emailAddress":{
                "name":"Dreama Jain",
                "address":"Dreama.Jain@achievers.com"
             }
          },
          "onlineMeeting":null
       }, 
       {
        "@odata.etag":"W/\"Ouwt/NUfgEaQY+TJmJNmaQAA9hQWJQ==\"",
        "id":"AAMkADUyZTZmY2M1LTkwNDgtNDBlMi1hN2NkLWNlYTM4N2Q3ODcxMQFRAAgI2VbazXzAAEYAAAAANrV7OIWX2kW6rL-BFLzvZQcAOuwt-NUfgEaQY_TJmJNmaQAAAAABDQAAOuwt-NUfgEaQY_TJmJNmaQAA0_drXAAAEA==",
        "createdDateTime":"2021-06-14T13:39:45.812478Z",
        "lastModifiedDateTime":"2021-08-04T14:55:07.7202676Z",
        "changeKey":"Ouwt/NUfgEaQY+TJmJNmaQAA9hQWJQ==",
        "categories":[
           
        ],
        "transactionId":null,
        "originalStartTimeZone":"Eastern Standard Time",
        "originalEndTimeZone":"Eastern Standard Time",
        "iCalUId":"040000008200E00074C5B7101A82E00807E508042D3F76D141FAD6010000000000000000100000006B8AFF6ED8C08D4A97F8160851813F20",
        "reminderMinutesBeforeStart":15,
        "isReminderOn":true,
        "hasAttachments":false,
        "subject":"Connect Weekly checkpoints with SRE and Arch.",
        "bodyPreview":"Hi there,\r\nDreama Jain is inviting you to a scheduled Zoom meeting.\r\nJoin Zoom Meeting\r\nPhone one-tap:\r\nCanada: +15873281099,,93895873660#,,,,*142993# or +16473744685,,93895873660#,,,,*142993#\r\nMeeting URL:\r\nhttps://achievers.zoom.us/j/938958736",
        "importance":"normal",
        "sensitivity":"normal",
        "isAllDay":false,
        "isCancelled":false,
        "isOrganizer":false,
        "responseRequested":false,
        "seriesMasterId":"AAMkADUyZTZmY2M1LTkwNDgtNDBlMi1hN2NkLWNlYTM4N2Q3ODcxMQBGAAAAAAA2tXs4hZfaRbqsv8EUvO9lBwA67C381R_ARpBj5MmYk2ZpAAAAAAENAAA67C381R_ARpBj5MmYk2ZpAADT52tcAAA=",
        "showAs":"tentative",
        "type":"occurrence",
        "webLink":"https://outlook.office365.com/owa/?itemid=AAMkADUyZTZmY2M1LTkwNDgtNDBlMi1hN2NkLWNlYTM4N2Q3ODcxMQFRAAgI2VbazXzAAEYAAAAANrV7OIWX2kW6rL%2FBFLzvZQcAOuwt%2FNUfgEaQY%2BTJmJNmaQAAAAABDQAAOuwt%2FNUfgEaQY%2BTJmJNmaQAA0%2BdrXAAAEA%3D%3D&exvsurl=1&path=/calendar/item",
        "onlineMeetingUrl":"",
        "isOnlineMeeting":false,
        "onlineMeetingProvider":"unknown",
        "allowNewTimeProposals":false,
        "occurrenceId":"OID.AAMkADUyZTZmY2M1LTkwNDgtNDBlMi1hN2NkLWNlYTM4N2Q3ODcxMQBGAAAAAAA2tXs4hZfaRbqsv8EUvO9lBwA67C381R_ARpBj5MmYk2ZpAAAAAAENAAA67C381R_ARpBj5MmYk2ZpAADT52tcAAA=.2021-08-04",
        "isDraft":false,
        "hideAttendees":false,
        "responseStatus":{
           "response":"notResponded",
           "time":"0001-01-01T00:00:00Z"
        },
        "start":{
           "dateTime":"2021-08-04T14:00:00.0000000",
           "timeZone":"UTC"
        },
        "end":{
           "dateTime":"2021-08-04T15:00:00.0000000",
           "timeZone":"UTC"
        },
        "location":{
           "displayName":"https://achievers.zoom.us/j/93895873660?pwd=ayt3WTlkMUI0a1p3RmhyTXFjMzJnQT09&from=addon",
           "locationType":"default",
           "uniqueId":"67fa17f9-f74e-483d-bfd0-d702a513064c",
           "uniqueIdType":"locationStore"
        },
        "locations":[
           {
              "displayName":"https://achievers.zoom.us/j/93895873660?pwd=ayt3WTlkMUI0a1p3RmhyTXFjMzJnQT09&from=addon",
              "locationType":"default",
              "uniqueId":"67fa17f9-f74e-483d-bfd0-d702a513064c",
              "uniqueIdType":"locationStore"
           }
        ],
        "recurrence":null,
        "attendees":[
           {
              "type":"required",
              "status":{
                 "response":"none",
                 "time":"0001-01-01T00:00:00Z"
              },
              "emailAddress":{
                 "name":"Dreama Jain",
                 "address":"Dreama.Jain@achievers.com"
              }
           },
           {
              "type":"required",
              "status":{
                 "response":"notResponded",
                 "time":"0001-01-01T00:00:00Z"
              },
              "emailAddress":{
                 "name":"Stefan Kolesnikowicz",
                 "address":"Stefan.Kolesnikowicz@achievers.com"
              }
           },
           {
              "type":"required",
              "status":{
                 "response":"notResponded",
                 "time":"0001-01-01T00:00:00Z"
              },
              "emailAddress":{
                 "name":"Rajesh Pippiri",
                 "address":"Rajesh.Pippiri@achievers.com"
              }
           },
           {
              "type":"optional",
              "status":{
                 "response":"notResponded",
                 "time":"0001-01-01T00:00:00Z"
              },
              "emailAddress":{
                 "name":"Pedro Otero",
                 "address":"Pedro.Otero@achievers.com"
              }
           },
           {
              "type":"optional",
              "status":{
                 "response":"accepted",
                 "time":"0001-01-01T00:00:00Z"
              },
              "emailAddress":{
                 "name":"Hemanth Chennavaram",
                 "address":"Hemanth.Chennavaram@achievers.com"
              }
           },
           {
              "type":"optional",
              "status":{
                 "response":"accepted",
                 "time":"0001-01-01T00:00:00Z"
              },
              "emailAddress":{
                 "name":"Sravani Alwala",
                 "address":"Sravani.Alwala@achievers.com"
              }
           },
           {
              "type":"optional",
              "status":{
                 "response":"accepted",
                 "time":"0001-01-01T00:00:00Z"
              },
              "emailAddress":{
                 "name":"Pavan Nadupuru",
                 "address":"Pavan.Nadupuru@achievers.com"
              }
           }
        ],
        "organizer":{
           "emailAddress":{
              "name":"Dreama Jain",
              "address":"Dreama.Jain@achievers.com"
           }
        },
        "onlineMeeting":null
     }
    ],
    "@odata.nextLink":"https://graph.microsoft.com/v1.0/me/calendarView?startDateTime=2021-08-01T19%3a00%3a00-08%3a00&endDateTime=2021-08-05T19%3a00%3a00-08%3a00&%24top=10&%24skip=10"
 }
 
 let attendeesArr = [];
 let collaborationArray=[];
 let groupRecArray=[];
 response.value.forEach((mainObj,i)=>{
   const meetingAttendees = mainObj.attendees
   if(meetingAttendees.length > 2 && meetingAttendees.length <= 10){
      let currentUserIndex = meetingAttendees.findIndex(o2 => o2.emailAddress.address === currentUserAddress)
      if(currentUserIndex > -1){
         const startTime = mainObj.start.dateTime
         const endTime = mainObj.end.dateTime
         const organizerEmailAdd = mainObj.organizer.emailAddress.address
         const groupMeetingAttendees = [];
         for(let i=0;i<meetingAttendees.length ; i++){
            let attendeeObj = {};
            const attendeeEmailAdd  = meetingAttendees[i].emailAddress.address
            attendeeObj.email =  attendeeEmailAdd
            attendeeObj.startTime = startTime
            attendeeObj.endTime = endTime
            attendeeObj.organizerEmailAdd = organizerEmailAdd
            if((meetingAttendees[i].status.response === "accepted" || attendeeEmailAdd === organizerEmailAdd) && attendeeEmailAdd != currentUserAddress){
               groupMeetingAttendees.push({...attendeeObj,type:"group"})
               setCount({...attendeeObj,type:"collaboration"})
            }
         }
         if(groupRecArray.length <= 3 && groupMeetingAttendees.length){
            const email = groupMeetingAttendees.map(o=> o.email)
            const daysDiff = calculateDate(groupMeetingAttendees[0].startTime);
            groupRecArray.push({...groupMeetingAttendees[0],email,daysDiff})
         }
      }
   }
 })

function calculateDate(endDate){
   const lastMeetingDate = moment(endDate, 'YYYY-MM-DD');
   const currentDate = moment(new Date(), 'YYYY-MM-DD'); 
   const duration = moment.duration(currentDate.diff(lastMeetingDate));
   const daysDiff =  Math.floor(duration.asDays());
   return daysDiff
}

 function setCount(obj){
   if(collaborationArray && collaborationArray.length){
      let obIndex = collaborationArray.findIndex(o2 => o2.email === obj.email)
      if(obIndex >-1){
         collaborationArray[obIndex].count += 1
      }else{
         obj.count = 1
         obj.daysDiff = calculateDate(obj.startTime);
         collaborationArray.push(obj)
      }
   }else{
      obj.count = 1
      obj.daysDiff = calculateDate(obj.startTime);
      collaborationArray.push(obj)
   }
}

console.log("==groupRecArray",groupRecArray,collaborationArray)