FROM node:10

# Create app directory

WORKDIR /usr/src/app

COPY package*.json ./
RUN mkdir 'dist'

# COPY .npmignore ./

#COPY . .
RUN npm install typescript -g

RUN npm install

COPY . .

RUN chown -R node:node "/usr/src/app"

RUN chown -R node:node "/usr/src/app/dist"
USER node
RUN tsc

EXPOSE 4001
# CMD tail -f /dev/null
ENTRYPOINT [ "npm","run", "start2"]
